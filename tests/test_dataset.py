# from ava.core.dataset import AUXILIARY_DATASETS, get_all_datasets
from ava.datasets import *
from ava.core.dataset import Interleaved


def test_interleave():

    for n_samples1, n_samples2 in [
        (10, 50),
        (50, 10),
        (974, 243),
        (100, 351),
        (100, 100),
        (101, 100),
        (5000, 5000),
    ]:
        a = MNIST('train')
        a.resize(n_samples1)

        b = MNIST('train')
        b.resize(n_samples2)
        c = Interleaved(a, b)

        # sampler = BatchSampler(c, 1)

        # samples = [sampler.get() for i in range(sampler.n_batches())]
        count = 0
        for i in range(len(c)):
            count += 1

        assert count == n_samples1 + n_samples2

test_interleave()
