from ava.core.training.trainer import train_high_level


train_high_level('RN18Classifier', 'Cifar10', testrun=True)
train_high_level('RN18Classifier', 'CUB200_2011', testrun=True, subset_val='train_val')
train_high_level('MNISTNet', 'MNIST', testrun=True)
train_high_level('RN18Dense', 'ADE150', testrun=True)
