import pytest
import numpy as np
from ava.transformations import tensor_resize, sample_random_patch
from ava import PATHS


def test_resizing():
    a = np.random.uniform(0, 255, (100, 150, 3))

    assert tensor_resize(a, 0.5).shape == (50, 75, 3)

    b = a.astype('int32')

    with pytest.raises(TypeError):
        tensor_resize(b, 0.5)

    a = np.random.uniform(0, 255, (100, 100))
    assert tensor_resize(a, (200, 200), channel_dim=None).shape == (200, 200)

    b_rs = tensor_resize(b, 0.5, autoconvert=True)
    assert b_rs.shape == (50, 75, 3)
    assert b_rs.dtype.name == 'int16'

    c = np.random.uniform(0, 255, (100, 150))
    c_rs = tensor_resize(c, 2.0)
    assert c_rs.shape == (200, 300)

    c = np.random.uniform(0, 255, (100, 150))
    c_rs = tensor_resize(c, (50, 200), interpret_as_max_bound=True)
    assert c_rs.shape == (50, 75)

    c = np.random.uniform(0, 255, (100, 150))
    c_rs = tensor_resize(c, (50, 50), interpret_as_max_bound=True)
    assert c_rs.shape == (33, 50)

    c = np.random.uniform(0, 255, (50, 20))
    c_rs = tensor_resize(c, (100, 100), interpret_as_max_bound=True)
    assert c_rs.shape == (100, 40)

    c = np.random.uniform(0, 255, (100, 150))
    c_rs = tensor_resize(c, (50, None))
    assert c_rs.shape == (50, 75)

    c = np.random.uniform(0, 255, (333, 150))
    c_rs = tensor_resize(c, (351, None))
    assert c_rs.shape[0] == 351

    for i in range(5, 60, 3):
        c = np.random.uniform(0, 255, (i, 10))
        c_rs = tensor_resize(c, (i, None))
        assert c_rs.shape[0] == i


def test_random_sampling():
    a = np.zeros((150, 150))
    a[30:40, 20:30] = 1

    b = np.zeros((50, 50))
    b[0:20, 20:30] = 1

    c = np.zeros((112, 78))
    c[12:25, 0:20] = 1
    c[50:62, 30:35] = 1

    for arr in [a, b, c]:

        for _ in range(500):

            shape = np.random.randint(10, arr.shape[0]), np.random.randint(10, arr.shape[1])
            # We define an array with a white box and use it as a probability map and image.
            # So after cropping there should still be white pixels.
            indices = sample_random_patch(arr.shape, shape, arr)

            # print(indices, arr.shape, shape)
            assert arr.sum() > 0
            assert arr[indices].sum() > 0
            assert arr[indices].shape == shape

            indices = sample_random_patch(arr.shape, shape, arr, rescale=(50, 50))
            # print(indices, arr.shape, shape)
            assert arr.sum() > 0
            assert arr[indices].sum() > 0
            assert arr[indices].shape == shape
