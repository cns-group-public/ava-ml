## What is Ava ML?

Ava ML is a toolkit that simplifies supervised training of neural networks for research purposes. 
It is designed facilitate many tasks common in applying deep neural networks.

Features of Ava ML include:

- **Standard models, datasets and metrics:** Avoids re-implementations and allows for a quick start. 
Datasets can be downloaded with a single command and shared with other users on the same system.

- **Feature Extractors:** A large number implemented feature extractors pre-trained on ImageNet
(via the `pretrainedmodels` python package)

- **Command Line Interface:** Train and evaluate models by running simple instructions in the command line.

- **Experiment definitions:** Facilitates reproducibility and describes hyperparameters 
in a transparent way. Direct output of latex tables is possible.

- **Visual inspection of datasets:** Enables verification that datasets (including their parameters) 
are properly implemented.

- **Standard Structure:** The division into models, datasets and metrics facilitates 
collaboration in projects when multiple persons are involved.

Note, this is an early preview version not meant to be used in production.
The documentation is available online at: https://alexandria.physik3.uni-goettingen.de/avaml/

## Installation

To install ava, first clone the git repository using

```bash
git clone https://gitlab.gwdg.de/cns-group-public/ava-ml
```

Now the package can be installed by:

```bash
python3 setup.py develop --user
```

The setup will create a folder `~/.ava` containing the file `paths.yaml`.

- `TRAINED_MODELS_PATH` location where trained models are stored. 
To prevent overwriting important models consider changing the access rights `chmod a-w <model-file>`.
- `CACHE_PATH` location where data is cached (sometimes required for fast training)
- `DATASETS` location of datasets
 
Also the path to every individual dataset can be set. E.g. by adding a line:

```yaml
MNIST: /path/to/mnist
```


### Next Steps
Here are a few ideas of what to do next:

- To generate and view the documentation run `./build-doc.py` and open `doc/index.htm` in a browser.
- Verify that the installation was successful by running test is `tests`.
- Start training by `ava train <model> <dataset>`.

