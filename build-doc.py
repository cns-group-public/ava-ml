#!/usr/bin/python3
import shutil
import os
import sys
from functools import partial
from itertools import groupby
from os.path import join, dirname

import yaml
import inspect
import markdown
from pygments.formatters.html import HtmlFormatter

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '.'))
from ava import models, datasets, transformations, losses, metrics

from docutils.parsers.rst import Parser as RSTParser
# markdown = markdown2.Markdown()

rst_parser = RSTParser()

# parse_markdown = mistune.markdown  # markdown.convert
parse_markdown = partial(markdown.markdown, extensions=['fenced_code', 'codehilite'])


def get_args_string(f):

    args = []
    sig = inspect.signature(f).parameters
    for name, param in sig.items():
        if param.default != inspect._empty:
            arg = f'<span class="arg_optional">{name}={str(param.default)}</span>'
        else:
            arg = f'<span class="arg">{name}</span>'

        # if param.default != inspect._empty:
        #     arg = f'{name}={str(param.default)}'
        # else:
        #     arg = f'{name}'

        args += [arg]

    return ', '.join(args)


def parse_docstring(doc_str):

    if doc_str is None:
        return 'No description yet'

    doc_str = str(doc_str)
    doc_str = doc_str.strip()

    doc_str = doc_str.replace('\n', '<br>')
    doc_str = doc_str.replace(' ', '&nbsp;')

    return str(doc_str)


def object_to_html(obj):

    args_str = get_args_string(obj.__init__) if type(obj) == type else get_args_string(obj)

    # TODO: mark default arguments
    arguments = '<span class="args">(' + args_str + ')</span>'
    docstring = f'<h5>{obj.__name__} {arguments}</h5>'

    # docstring = '<h5>' + highlight(f'def {obj.__name__}({args_str})', PythonLexer(), HtmlFormatter()) + '</h5>'

    docstring += '<span class="description"><div class="docstr">' + parse_docstring(obj.__doc__) + '</div></span>'
    return docstring


def item_to_html(item, models, all_datasets, depth=1):
    out = ""
    for el in item:
        if type(el) == dict:
            assert len(el.keys()) == 1
            headline = next(iter(el.keys()))
            out += f'<h{depth} id="{headline}">{headline}</h{depth}>'
            out += item_to_html(next(iter(el.values())), models, all_datasets, depth=depth + 1)
        elif el.endswith('.md'):
            out += parse_markdown(open(el).read())
        elif hasattr(models, el):
            out += object_to_html(getattr(models, el))
        elif hasattr(losses, el):
            out += object_to_html(getattr(losses, el))
        elif hasattr(metrics, el):
            out += object_to_html(getattr(metrics, el))
        elif hasattr(datasets, el):
            out += object_to_html(getattr(datasets, el))
        elif hasattr(transformations, el):
            out += object_to_html(getattr(transformations, el))
        elif el == '<ListFeatureExtractors>':
            from ava.models.feature_extraction import FEATURE_EXTRACTORS
            fes = groupby(sorted(list(FEATURE_EXTRACTORS.items()), key=lambda x:x[1][0]), key=lambda x:x[1][0])

            out += '<pre>'
            for mod, items in fes:
                items = list(items)
                out += f'<b>{mod}</b>: '
                out += ', '.join(i[0] for i in items) + '<br>'
            out += '</pre>'
        elif el == '<ListLosses>':
            out += ''.join(object_to_html(getattr(losses, l)) for l in dir(losses) if callable(getattr(losses, l)))
        else:
            print('Unknown element:', el)

    if depth == 2:
        out += '<hr>'

    return out


def get_toc(structure, depth=1):

    out = ''
    for s in structure:

        if type(s) == dict:
            headline = next(iter(s.keys()))
            out += f'<a class="depth{depth}" href="#{headline}">{headline}</a>'
            out += get_toc(next(iter(s.values())), depth=depth+1)
        else:
            pass

    return out


def main():

    # load templates
    doc_target = 'doc'
    version = str(0.1) + ' (alpha)'

    os.makedirs(doc_target, exist_ok=True)

    structure = yaml.load(open('doc.yaml').read())

    all_datasets = []
    pygments_css = HtmlFormatter().get_style_defs('.codehilite')

    html = """
    <html>
        <head>
            <title>Ava Documentation</title>
            <style>
                body {font-family: Lato; padding: 0px; }
                .all {display: flex; margin:auto}
                .toc {background-color:#fff; padding: 20px; width: 14em}
                .logo {margin:20px;}
                .logo img{width: 150px; margin:auto}
                .container {width: 700px; background-color:#fff; padding: 30px; }
                hr {border: 0px; border-bottom: 1px dotted #ccc; margin-bottom: 50px;}
                .arg_optional {color: #555; font-weight:normal}
                .arg {color: #555; font-weight:bold}
                h5 {margin-bottom: 2px; padding-bottom: 2px; margin-top:5px;}
                span.description, span.description > pre {margin-top:0px; padding-top:0px; margin-bottom: 20px;}
                .toc_items {position: fixed;}
                .toc_items > a { display: block; color: #000; text-decoration:none; } 
                .toc_items > a.depth1 { font-weight: bold; color: #000}
                .toc_items > a.depth2 { margin-left: 1em; color: #000}
                .toc_items > a.depth3 { margin-left: 2em; color: #555}
                .codehilite {padding: 3px; margin:3px;}
                .codehilite > pre {margin: 3px}
                .docstr { font-family: monospace; font-size: 12px; margin-bottom: 30px; overflow-y: hidden}
            """ + pygments_css + """
            </style>
            <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/styles/default.min.css">
            <script src="highlight.js"></script>
            <script>hljs.initHighlightingOnLoad();</script>
        </head>
        <body>
        <div class="all">
            <div class="toc">
                <div class="toc_items">
                    <div class="logo">
                        <img src="logo.svg" /><br />
                        version """ + version + """  
                    </div>
                    """ + get_toc(structure) + """
                </div>
            </div>
            <div class="container">
    """
    html += item_to_html(structure, models, all_datasets)
    html += '</div></div></body></html>'

    open(join(doc_target, 'index.html'), 'w').write(html)

    for dirpath, dirnames, filenames in os.walk('ava/'):
        for f in filenames:
            if f.endswith('svg'):
                print(dirpath, f)
                target = join(doc_target, '/'.join(dirpath.split('/')[1:]), f)
                os.makedirs(dirname(target), exist_ok=True)
                shutil.copyfile(join(dirpath, f), target)


if __name__ == '__main__':
    main()
