from ava.core.model import load_pretrained_model, initialize_model
from ava.core.score import predict_single_sample
from ava.transformations.io import imread
import numpy as np

# replace this
# model = load_pretrained_model('some_checkpoint')
model = initialize_model('RN18Classifier')
model.eval()

# sample = imread('some_sample.jpg')
sample = np.zeros((1, 3, 224, 224))

output = predict_single_sample(model, sample)

print(output[0].shape)
