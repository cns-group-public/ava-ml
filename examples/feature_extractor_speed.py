import time
import torch

from ava.models.feature_extraction.pnas_wrapper import _PNASFeatures
from ava.models.feature_extraction.inception import _IncV4Features, _XceptionFeatures, _BNInceptionFeatures
from ava.models.feature_extraction.resnet import _ResNetFeatures
from ava.models.feature_extraction.se_resnet import _SEFeatures
from ava.models.feature_extraction.lightweight import _LightConvNet, _MNISTConvNet, _LightUNet
from ava.models.feature_extraction.squeezenet import _SqueezeNetFeatures

from ava.models.segmentation.dense_resnet import RN18Dense, RN50Dense
from ava.models.segmentation.pspnet import PSPNet
from contrib.models.segmentation.pspnet_wrapper import PSPNetWrapper
from ava.models.segmentation.deeplab_v3 import DeepLabV3

x = torch.zeros((5, 3, 352, 352))

segmenters = [
    DeepLabV3(out_channels=10),
    PSPNetWrapper(out_channels=10),
    _LightUNet(pretrained=False, feature_output_channels=10),
    RN18Dense(pretrained=False, out_channels=10),
    RN50Dense(pretrained=False, out_channels=10),
    PSPNet(pretrained=False, out_channels=10)
]

with torch.no_grad():
    for segmenter in segmenters:
        t_start = time.time()
        segmenter(x)
        base = segmenter.base if hasattr(segmenter, 'base') else ''
        print(f'Segmenter {segmenter.__class__.__name__} with base {base} took : {(time.time() - t_start):.3f}s')

extractors = [

    _LightConvNet(pretrained=False),
    _SqueezeNetFeatures(pretrained=False),
    _MNISTConvNet(pretrained=False),
    _PNASFeatures(pretrained=False),
    _IncV4Features(pretrained=False),
    _XceptionFeatures(pretrained=False),
    _BNInceptionFeatures(pretrained=False),
    # _IncV3Features(pretrained=False), # > 10s
    _ResNetFeatures(base='resnet18', pretrained=False),
    _ResNetFeatures(base='resnet50', pretrained=False),
    _ResNetFeatures(base='resnet101', pretrained=False),
    _ResNetFeatures(base='resnet152', pretrained=False),
    _SEFeatures(base='se_resnet50', pretrained=False),
    _SEFeatures(base='se_resnet101', pretrained=False),
    _SEFeatures(base='se_resnet152', pretrained=False),
    _SEFeatures(base='se_resnext50_32x4d', pretrained=False),
    _SEFeatures(base='se_resnext101_32x4d', pretrained=False),
]

with torch.no_grad():
    for extractor in extractors:
        t_start = time.time()
        extractor(x)
        base = extractor.base if hasattr(extractor, 'base') else ''
        print(f'Extractor {extractor.__class__.__name__} with base {base} took : {(time.time() - t_start):.3f}s')



