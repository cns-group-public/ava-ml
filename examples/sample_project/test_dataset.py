# this file's name must end with _dataset.py

from ava.datasets import DatasetBase
import numpy as np

class TestDataset(DatasetBase):

    def __init__(self, subset, **kwargs):
        super().__init__('image->C')
        self.sample_ids = tuple(range(100))

    def __getitem__(self, index):
        img = np.random.normal(0,1,(3,128,128)).astype('float32')
        return (img,), (None,)
