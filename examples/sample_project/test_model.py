# this file's name must end with _model.py

from torch import nn
from ava.core.model import ModelBase

class TestModel(ModelBase):

    def __init__(self):
        super().__init__()

        self.net = nn.Sequential(
            nn.Conv2d(3, 16, 3),
            nn.ReLU(),
            nn.BatchNorm2d(16),
            nn.MaxPool2d(2),
            nn.Conv2d(16, 32, 3),
            nn.ReLU(),
            nn.BatchNorm2d(32),
            nn.MaxPool2d(2),
        )


    def forward(self, *x):
        out = self.net(x[0])
        return out,

    def loss(self, y_pred, y_gt):
        return y_pred[0].sum()