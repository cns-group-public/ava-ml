import numpy as np

from .base import BaseMetric


def binary_cross_entropy(y_true, y_pred):
    return -1 * y_true * np.log(y_pred) - (1 - y_true) * np.log(1 - y_pred)


def cross_entropy(y_true, y_pred):
    return -1*(y_true * np.log(y_pred)).sum(2)


class CEMetric(BaseMetric):
    """ Cross entropy metric """

    def __init__(self, pred_range=None, gt_index=0, pred_index=0, name='CE'):

        self.pred_range = pred_range
        self.pred_index = pred_index
        self.gt_index = gt_index

        super().__init__([name, name + '-cw'])

        self.predictions = []
        self.ground_truths = []

    def add(self, predictions, ground_truth):

        pred = predictions[self.pred_index]
        gt = ground_truth[self.gt_index]

        if self.pred_range is not None:
            pred = pred[:, self.pred_range[0]: self.pred_range[1]]

        self.predictions += [pred.detach().cpu().numpy()]
        self.ground_truths += [gt.detach().cpu().numpy()]

    def value(self):

        preds = 1/(1+np.exp(-1*np.concatenate(self.predictions)))
        gts = np.concatenate(self.ground_truths)

        ce = cross_entropy(gts, preds)
        ce_cw = ce.mean(0)

        return float(ce_cw.mean()), tuple(ce_cw.tolist())


class BCEMetric(BaseMetric):
    """ Binary cross entropy metric """

    def __init__(self, pred_range=None, gt_index=0, pred_index=0, name='BCE'):

        self.pred_range = pred_range
        self.pred_index = pred_index
        self.gt_index = gt_index

        super().__init__([name, name + '-cw'])

        self.predictions = []
        self.ground_truths = []

    def add(self, predictions, ground_truth):

        pred = predictions[self.pred_index]
        gt = ground_truth[self.gt_index]

        if self.pred_range is not None:
            pred = pred[:, self.pred_range[0]: self.pred_range[1]]

        self.predictions += [pred.detach().cpu().numpy()]
        self.ground_truths += [gt.detach().cpu().numpy()]

    def value(self):

        preds = 1/(1+np.exp(-1*np.concatenate(self.predictions)))
        gts = np.concatenate(self.ground_truths)
        bce = binary_cross_entropy(gts, preds)

        bce_cw = bce.mean(0)

        return float(bce_cw.mean()), tuple(bce_cw.tolist())
