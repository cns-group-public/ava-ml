from .accuracy import *
from .base import *
from .intersection_over_union import *
from .correlation import *
from .cross_entropy import *

from functools import partial


def init_metrics(metrics, dataset=None):

    if metrics is None:
        return []

    metrics = [] if metrics is None else metrics

    # transform metrics string to list of metrics
    metrics = [m for m in metrics.split(',') if len(m) > 0] if type(metrics) == str else metrics

    if dataset is not None and hasattr(dataset, 'default_metrics'):
        metrics += dataset.default_metrics

    for i in range(len(metrics)):
        if type(metrics[i]) == dict:
            assert len(metrics[i]) == 1
            m = next(iter(metrics[i].items()))
            metrics[i] = partial(globals()[m[0] + 'Metric'], **m[1])
        elif type(metrics[i]) == str:
            metrics[i] = globals()[metrics[i] + 'Metric']

    assert all(issubclass(m if type(m) != partial else m.func, BaseMetric) for m in metrics)

    return metrics
