import numpy as np

from .base import BaseMetric


class AccuracyMetric(BaseMetric):
    """
    Computes accuracy for classification tasks
    """

    def __init__(self, top=None, pred_range=None, gt_index=0, pred_index=0, name='Acc', argmax_on_gt=False):
        self.pred_range = pred_range
        self.pred_index = pred_index
        self.gt_index = gt_index
        self.argmax_on_gt = argmax_on_gt

        top = [1] + top if top is not None else [1]
        assert all(type(t) == int for t in top)
        names = tuple('{}@{}'.format(name, t) for t in top)
        super().__init__(names)

        self.top = top
        self.predictions = []
        self.ground_truths = []

    def add(self, predictions, ground_truth):

        pred = predictions[self.pred_index]
        gt = ground_truth[self.gt_index]

        if self.pred_range is not None:
            pred = pred[:, self.pred_range[0]: self.pred_range[1]]

        max_top = max(self.top)
        self.predictions += [pred.topk(max_top)[1].detach().cpu().numpy()]

        if not self.argmax_on_gt:
            self.ground_truths += gt.detach().cpu().numpy().tolist()
        else:
            self.ground_truths += [gt.topk(max_top)[1].detach().cpu().numpy()]

    def value(self):

        preds = np.concatenate(self.predictions)

        gts = np.array(self.ground_truths) if not self.argmax_on_gt else np.concatenate(self.ground_truths)
        hits = np.equal(preds, gts[:, None])

        return [np.mean(hits[:, :t].sum(1) > 0) for t in self.top]


class AccuracyMulticlassMetric(BaseMetric):

    """
    Accuracy when output is a probability distribution over cases
    no_reshape: This is intended to become the standard case.
    """

    def __init__(self, multiclass, pred_range=None, gt_index=0, pred_index=0, name='Acc', mistakes=(0, 1),
                 argmax_on_gt=False, no_reshape=False):

        # a top parameter could be added, but this would yield a 2d output in value(): top x classes

        self.mistakes = mistakes
        self.multiclass = multiclass

        self.argmax_on_gt = argmax_on_gt
        self.no_reshape = no_reshape

        # top = [1] + top if top is not None else [1]
        # assert all(type(t) == int for t in top)
        metric_names = tuple([name, name + '_cw'] + ['{}m-acc'.format(m) for m in self.mistakes])
        super().__init__(metric_names, pred_range, gt_index, pred_index)

        # self.top = top
        self.predictions = []
        self.ground_truths = []

    def add(self, predictions, ground_truth):

        # pred = predictions[self.pred_index]
        # gt = ground_truth[self.gt_index]

        pred, gt = self._get_pred_gt(predictions, ground_truth)

        # print(pred)
        # print(gt)

        if self.no_reshape:
            pred2 = pred
        else:
            pred2 = pred.view(pred.size(0), -1, self.multiclass)
        # pred3 = pred.view(pred.size(0), self.multiclass, -1)

        # if self.pred_range is not None:
        #     pred2 = pred2[:, self.pred_range[0]: self.pred_range[1]]

        self.predictions += [pred2.topk(1)[1].detach().cpu().numpy()]

        if self.argmax_on_gt:
            self.ground_truths += [gt.topk(1)[1].detach().cpu().numpy().tolist()]
        else:
            self.ground_truths += [gt.detach().cpu().numpy().tolist()]

    def value(self):

        preds = np.concatenate(self.predictions)
        gts = np.concatenate(self.ground_truths)

        if self.argmax_on_gt:
            hits = np.equal(preds, gts)
        else:
            # add dimension for gt to be compatible
            hits = np.equal(preds, np.expand_dims(gts, 2))

        missed = hits.shape[1] - hits[:, :, 0].sum(1)
        misstakes = [(missed <= m).mean() for m in self.mistakes]
        cw_acc = hits.squeeze(2).mean(0)

        return tuple([cw_acc.mean(), cw_acc] + misstakes)
        # return [np.mean(hits[:, c, :1].sum(1) > 0) for c in range(self.multiclass)]




