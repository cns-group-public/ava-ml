import numpy as np
from .base import BaseMetric
from ..core.common.math import softmax


def pearson_correlation(a, b):
    """ computes Pearson correlation coefficient """

    # substitute for convenience
    q = a - np.mean(a)
    p = b - np.mean(b)
    corr = np.mean(p*q) / np.sqrt(np.mean(p**2) * np.mean(q**2))

    return corr


class CorrelationMetric(BaseMetric):

    def __init__(self, pred_range=None, gt_index=0, pred_index=0, name='Corr'):

        names = [name, name + '-cw']
        super().__init__(names, pred_range, gt_index, pred_index)

        self.predictions = []
        self.ground_truths = []

    def add(self, predictions, ground_truth):

        pred, gt = self._get_pred_gt(predictions, ground_truth)

        self.predictions += pred.detach().cpu().numpy().tolist()
        self.ground_truths += gt.detach().cpu().numpy().tolist()

    def value(self):
        preds = np.array(self.predictions)
        gts = np.array(self.ground_truths)

        return pearson_correlation(preds, gts), tuple(pearson_correlation(preds[:, i], gts[:, i]) for i in range(preds.shape[1]))


class OrdinalCorrelationMetric(BaseMetric):

    def __init__(self, weight, pred_range=None, gt_index=0, pred_index=0, apply_softmax=False, name='OrdCorr'):

        names = [name, name + '-cw']
        super().__init__(names, pred_range, gt_index, pred_index)

        self.apply_softmax = apply_softmax
        self.weight = np.array(weight)

        self.predictions = []
        self.ground_truths = []

    def add(self, predictions, ground_truth):

        pred, gt = self._get_pred_gt(predictions, ground_truth)

        self.predictions += pred.detach().cpu().numpy().tolist()
        self.ground_truths += gt.detach().cpu().numpy().tolist()

    def value(self):
        preds = np.array(self.predictions)
        gts = np.array(self.ground_truths)

        if self.apply_softmax:
            preds = softmax(preds)

        # project before correlation calculation
        preds = np.maximum(0, preds.dot(self.weight))
        gts = np.maximum(0, gts.dot(self.weight))

        return pearson_correlation(preds, gts), tuple(pearson_correlation(preds[:, i], gts[:, i]) for i in range(preds.shape[1]))
