### Training and Scoring

We differentiate two phases: training and scoring (or evaluation). Training will result in a
model and a model-args file being created that encode the obtained parameters (weights).

### Models, Datasets and Metrics

Models are trained and evaluated on datasets using a metric. All three components are 
defined separately and can be exchanged almost freely (given they are compatible). This
means that once an image classification model is designed, it can be trained on all
image classification datasets.

### Feature Extractors
These are re-usable components of the model.

### Transformations
Common operations such as image resizing.

### Inspection
Use `ava inspect <dataset> <dataset-args>` to explore datasets.

### Caching
Training and evaluation are expensive. Hence, many intermediate results are cached. 

