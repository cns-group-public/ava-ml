To install ava, first clone the git repository using

```bash
git clone https://gitlab.gwdg.de/cns-group-public/ava-ml
```

Now the package can be installed by:

```bash
python3 setup.py develop --user
```

The setup will create a folder `~/.ava` containing the file `paths.yaml`.

- `TRAINED_MODELS_PATH` location where trained models are stored. 
To prevent overwriting important models consider changing the access rights `chmod a-w <model-file>`.
- `CACHE_PATH` location where data is cached (sometimes required for fast training)
- `DATASETS` location of datasets
 
Also the path to every individual dataset can be set. E.g. by adding a line:

```yaml
MNIST: /path/to/mnist
```
