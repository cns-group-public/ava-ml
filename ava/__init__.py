import os
import yaml
from os.path import join, realpath, dirname, expanduser


class SettingDict(dict):

    def __init__(self, filename, notfound_cb=None, **kwargs):
        super().__init__(**kwargs)
        self.filename = filename
        self.notfound_cb = notfound_cb

        with open(filename) as f:
            super().update(yaml.load(f, Loader=yaml.BaseLoader))

    def __getitem__(self, key):

        if super().__contains__(key):
            return super().__getitem__(key)
        elif self.notfound_cb is not None:
            return self.notfound_cb(key)
        else:
            raise KeyError('No entry for ' + key + ' was found in file ' + self.filename)


path_filename = join(expanduser("~"), '.ava', 'paths.yaml')


if not os.path.isfile(path_filename):
    # create sample config
    with open(path_filename, 'w') as f2:
        f2.write("DATASETS: /datasets\n")
        f2.write("TRAINED_MODELS_PATH: " + join(expanduser('~'), '.ava', 'pretrained_models') + '\n')
        f2.write("CACHE: " + join(expanduser('~'), '.ava', 'cache') + '\n')

    print('paths.yaml was not found. Therefore, a new file was created from config.yaml.sample. '
          'Please check the newly created paths.yaml.')

PATHS = SettingDict(path_filename, notfound_cb=lambda x: os.path.join(PATHS['DATASETS'], x))
PATHS['ROOT'] = dirname(realpath(__file__))
