import os

import torch
import time
import collections

from ..metrics import init_metrics
from .logging import logger
# from .training.helper import TRAINING_INFO_KEYS


SCORE_PRINT_INTERVAL = 60


def score_simple(model_name, dataset_name, subset='test', batch_size=None,
                 visualize=False, print_output=False, ignore_gt=False,
                 max_samples=None, ignore_model_args=False,
                 threads=2, prediction_file=None, **cli_args):

    from .model import load_pretrained_model, initialize_model, count_parameters
    from ava.losses.set_loss import init_loss
    from .dataset import get_dataset
    from .visualize import Visualize

    model_data = None
    additional_args = {}

    if os.path.isfile(model_name):  # if pre-trained model, use the provided dataset arguments
        model_data = torch.load(model_name)

        if not ignore_model_args:
            additional_args.update(model_data['model_arguments'][-1])  # use arguments from last training

        if 'transfer_mode' in cli_args or 'transfer_mode' in model_data['model_arguments'][-1]:
            additional_args['transfer_mode'] = False  # transfer mode must be false for scoring to load weights
    else:
        logger.important('{} is not a file'.format(model_name))

    additional_args.update(cli_args)

    (dataset,), dataset_args, other_args = get_dataset(dataset_name, additional_args, subsets=(subset,),
                                                       samples=(max_samples,))

    loss_function = init_loss(None, dataset)

    logger.info('\nSCORE: Initialize dataset {} with arguments:\n   subset: {}\n   {}\n'.format(
        dataset.__class__.__name__, subset, '\n   '.join('{}: {}'.format(k, v) for k, v in dataset_args.items())))

    assert max_samples is None or len(dataset) <= max_samples
    # logger.important('Initialized dataset {} with subset {}'.format(dataset_type.__name__, subset))

    if model_data is not None:  # if pre-trained model, use the provided dataset arguments
        model = load_pretrained_model(model_data, other_args, dataset.model_config, cuda=True)
    else:
        model = initialize_model(model_name, other_args, dataset.model_config, cuda=True)

    assert len(dataset) > 0, 'Dataset is empty'
    model.train(False)
    model.eval()

    if visualize:
        Visualize(model=model)(dataset)
    else:
        if hasattr(dataset, 'without_ground_truth') and dataset.without_ground_truth:
            logger.important('SCORE: Can not evaluate model without ground truth')
            return None, None

        logger.important('SCORE: Number of network parameters (all/trainable)', count_parameters(model),
                         count_parameters(model, only_trainable=True))
        logger.important('SCORE: Number of threads for data generation', threads)

        metrics = init_metrics(cli_args['metrics'] if 'metrics' in cli_args else [], dataset)
        scorer = Scorer(model, loss_function, metrics)
        scorer.score(dataset, batch_size, ignore_gt, None, threads, prediction_file)

        if not ignore_gt:
            scores, metric_names = scorer.compute()
            if print_output:
                scorer.print_status(scores, metric_names)
            return scores, metric_names


# TODO: integrate Scorer.score with Scorer.__init__. No need for two methods?
class Scorer(object):

    def __init__(self, model, loss_function=None, metrics=(), no_metrics=False):

        from ..metrics.base import BaseMetric

        self.model = model
        self.is_baseline = hasattr(model, 'is_baseline') and model.is_baseline()
        self.loss_function = loss_function if loss_function is not None else lambda x, y: 0

        # metrics = model's default metrics + explicit metrics

        if hasattr(model, 'default_metrics'):
            rest_default_metrics = [m for m in model.default_metrics if m not in set(metrics)]
        else:
            rest_default_metrics = []

        self.metrics = rest_default_metrics + list(metrics)
        self.metrics = [m() for m in self.metrics] if not no_metrics else []
        self.metrics = sorted(list(set(self.metrics)), key=lambda m: m.__class__.__name__)

        self.losses = []
        self.running_variable = None
        # self.vis = get_visdom()

        assert all(isinstance(m, BaseMetric) for m in self.metrics)

    def score_sample_add(self, predictions, variables_y):

        if self.is_baseline:
            variables_y = [v.cuda(non_blocking=True) if v is not None else None for v in variables_y]

        loss = self.loss_function(predictions, variables_y)

        for i in range(len(self.metrics)):
            self.metrics[i].add(predictions, variables_y)

        return float(loss)

    def score(self, dataset, batch_size, ignore_ground_truth=False, csv_file=None, threads=None,
              prediction_file=None):

        from .collate import collate
        from torch.utils.data import DataLoader

        self.model.train(False)
        self.model.eval()

        self.model._i_episode = 0

        all_predictions = None

        with torch.no_grad():  # no need for gradients during scoring
            n_samples = len(dataset)

            assert n_samples > 0, 'can not evaluate without samples'

            logger.important('\nSCORE: Evaluate {} on {} ({} samples), batch size: {}, csv: {}\n'.format(
                self.model.__class__.__name__, dataset.__class__.__name__, len(dataset), batch_size, csv_file))

            data_loader = DataLoader(dataset, batch_size=batch_size, num_workers=4 if threads is None else threads,
                                     collate_fn=collate, pin_memory=True)

            t_start = time.time()
            t_last_print = time.time()
            t_last_intermediate_score = time.time()
            # last_sample_id = None

            self.losses = []
            for i_sample_batches, (variables_x, variables_y) in enumerate(data_loader):

                self.model._i_episode += 1

                if not self.is_baseline:
                    variables_x = [v.cuda(non_blocking=True) if v is not None else None for v in variables_x]
                    variables_y = [v.cuda(non_blocking=True) if v is not None else None for v in variables_y]

                if time.time() - t_last_print > 10:
                    logger.info('SCORE: sample', i_sample_batches * batch_size, '/', n_samples)
                    t_last_print = time.time()

                predictions = self.model(*variables_x)

                if prediction_file is not None:
                    pred = [p.cpu().numpy().tolist() for p in predictions]
                    if all_predictions is None:
                        all_predictions = pred
                    else:
                        for i in range(len(pred)):
                            all_predictions[i] += pred[i]

                if not ignore_ground_truth:
                    loss = self.score_sample_add(predictions, variables_y)
                    self.losses += [float(loss)]

                if time.time() - t_last_intermediate_score > SCORE_PRINT_INTERVAL:
                    metric_scores, metric_names = self.compute(is_intermediate=True)
                    self.print_status(metric_scores, metric_names)
                    t_last_intermediate_score = time.time()

                if i_sample_batches == 0 and hasattr(dataset, 'dataset_types') and not self.is_baseline:
                    predictions_cpu = [p.data.cpu().numpy() for p in predictions]
                    samples_x = [v.cpu().numpy() if v is not None else None for v in variables_x]
                    samples_y = [v.cpu().numpy() if v is not None else None for v in variables_y]
                    logger.plot_samples_predictions_visdom(self.model, samples_x + samples_y, predictions_cpu, dataset)

            # write predictions
            if prediction_file is not None:
                import pickle
                with open(prediction_file, 'wb') as fh:
                    if len(dataset.sample_ids) != len(all_predictions):
                        logger.warning('Sample id and prediction sizes do not match: {} / {}'.format(
                            len(dataset.sample_ids), len(all_predictions)))
                    out = (dataset.sample_ids, all_predictions)
                    pickle.dump(out, fh)

            logger.info('SCORE: done in {:.2f}sec'.format(time.time() - t_start))
            self.model.train(True)

            return self.losses

    def compute(self, is_intermediate=False, is_validation=False):
        all_scores, all_names = [], []
        for metric in self.metrics:
            # make sure the metric is supposed to be evaluated
            if (not is_intermediate or metric.eval_intermediate()) and (not is_validation or metric.eval_validation()):
                scores = metric.value()
                scores = prepare_scores(scores)  # make sure score has right format, see function below
                all_scores += scores
                all_names += metric.names()

        assert len(all_scores) == len(all_names), 'lengths: {} vs. {}'.format(len(all_scores), len(all_names))
        return all_scores, all_names

    def metric_names(self):
        return [m.name() for m in self.model.default_metrics]

    def print_status(self, metric_scores, metric_names):

        import numpy as np

        for i_metric, metric in enumerate(metric_names):
            print('{}: '.format(metric) + ' '.join('{:.3f}'.format(s) for s in metric_scores[i_metric]))

        if len(self.losses) > 0:
            logger.important('SCORE: loss: {:.4f}'.format(np.mean(self.losses)))


def prepare_scores(scores):
    assert isinstance(scores, collections.Iterable)
    return [prepare_score(score) for score in scores]


def prepare_score(score):
    """ Score can either be a 1-dimensional array or list (e.g. classwise) or a scalar. """

    import numpy as np

    if type(score) == np.ndarray and score.ndim == 1:
        assert score.ndim == 1
        return score
    elif type(score) in {list, tuple} and len(score) == 1:
        return score
    elif type(score) in {list, tuple} and len(score) > 1:
        assert type(score[0]) not in {list, tuple}
        return score
    elif type(score) in {float, int, np.float64, np.float32}:
        return [score]
    else:
        raise ValueError('Metric output has invalid type or dimension: {}'.format(type(score)))


def predict_single_sample(model, sample_x):
    """
    Simplified predictions on individual samples. Useful for qualitative plots, where speed is not essential.
    """

    from .collate import collate_element

    assert not model.training

    sample_x = [collate_element(x) for x in sample_x]  # collate
    sample_x = [x.unsqueeze(0) if x is not None else None for x in sample_x]  # and add batch dimension

    if next(model.parameters()).is_cuda:
        sample_x = [x.cuda() if x is not None else None for x in sample_x]
        model.cuda()

    model._i_episode = 0
    pred_y = model(*sample_x)

    # copy to CPU, detach and take 1st (and only) batch
    pred_y = [x.cpu().detach().numpy()[0] for x in pred_y] if next(model.parameters()).is_cuda else pred_y

    return pred_y
