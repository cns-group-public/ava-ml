import time
from random import shuffle as random_shuffle


def inspect_dataset(dataset_name, subset='train', shuffle=False, filter=None, whole_pass=False, **kwargs):

    from .dataset import get_dataset
    from .logging import logger

    (dataset,), dataset_args, other_args = get_dataset(dataset_name, kwargs, subsets=(subset,))

    assert len(other_args) == 0, 'Unused arguments: {}'.format(', '.join(other_args.keys()))

    if filter is not None:
        print(dataset.sample_ids[:5])

        def sample_id_to_str(sample_id):
            if type(sample_id) in {tuple, list}:
                return '-'.join(str(s) for s in sample_id)
            else:
                return str(sample_id)

        dataset.sample_ids = [s for s in dataset.sample_ids if filter in sample_id_to_str(s)]

    if whole_pass:
        import numpy as np

        def fancy_type(x):
            if type(x) == np.ndarray:
                return '{}-{}'.format(x.dtype, x.shape)
            else:
                return type(x)

        indices = list(range(len(dataset)))

        if shuffle:
            random_shuffle(indices)

        for i in indices:
            print(str(i).zfill(5), [fancy_type(x) for x in dataset[i][0]], [fancy_type(x) for x in dataset[i][1]])
    else:
        logger.important('visualize {} (subset {}, {} samples)'.format(dataset.name(), subset, len(dataset)))
        assert len(dataset) > 0, 'Dataset does not have any samples'

        vis_modes = dataset.dataset_types if hasattr(dataset, 'dataset_types') else None

        from .visualize import Visualize
        Visualize(modes=vis_modes, shuffle=shuffle)(dataset)


def benchmark_dataset(dataset_name, subset='train',
                      max_samples=10, shuffle=True, profile=True, batch_size=16, **kwargs):

    from .dataset import get_dataset
    from .logging import logger
    from line_profiler import LineProfiler

    # additional_args = additional_args if additional_args is not None else {}

    if max_samples <= batch_size:
       batch_size = max_samples
       logger.warning('Batch size is reduced to max_samples:', max_samples)

    # if augmentation:
    #    kwargs['augmentation'] = True
    # else:
    #    batch_size = 1  # because without augmentation,

    (dataset,), dataset_args, other_args = get_dataset(dataset_name, kwargs, subsets=(subset,), samples=(20,))

    logger.important('Dataset size:', len(dataset))
    logger.important('starting benchmark')
    t_start = time.time()

    profiler = LineProfiler()
    get_item_ref = dataset.__getitem__  # keep reference to original dataset.__getitem__
    dataset.__getitem__ = profiler(get_item_ref)

    for i in range(len(dataset)):
        samples_x, samples_y = dataset.__getitem__(i)

    t_diff = time.time() - t_start
    profiler.print_stats()
    logger.important('avg. sample computation time: {:.6f}s ({} samples)'.format(
        t_diff / len(dataset), len(dataset)))

    profiler = LineProfiler()
    dataset.__getitem__ = profiler(get_item_ref)

    t_start = time.time()

    for i in range(len(dataset)):
        samples_x, samples_y = dataset.__getitem__(i)

    profiler.print_stats()

    logger.important('avg. sample computation time (2nd epoch): {:.6f}s ({} samples, first run: {:.6f})'.format(
                     (time.time() - t_start) / len(dataset), len(dataset), t_diff / len(dataset)))


def benchmark_model(model_name, input_size, profile=True, additional_args=None):

    import torch
    from .model import load_pretrained_model
    from .logging import logger
    from torch.autograd import Variable
    from line_profiler import LineProfiler

    try:
        if '-' in input_size:
            input_sizes = tuple(s for s in input_size.split('-'))
        else:
            input_sizes = input_size,
        logger.important('input size', input_size)
    except ValueError:
        raise ValueError('Invalid input size')

    model = load_pretrained_model(model_name, additional_args)

    def random_input(batch_size, cuda):
        types = {'f': torch.FloatTensor(), 'l': torch.LongTensor(), 'i': torch.IntTensor(),
                 'b': torch.ByteTensor()}
        inp = []
        for s in input_sizes:
            if s[0] in types:
                inp_size = tuple(int(x) for x in s[1:].split(','))
                v = Variable(torch.zeros((batch_size,) + inp_size, out=types[s[0]]))
            else:
                inp_size = tuple(int(x) for x in s.split(','))
                v = Variable(torch.zeros((batch_size,) + inp_size))

            if cuda:
                v = v.cuda()
            inp += [v]

        return tuple(inp)

    logger.important('Profiler output for batch size 10')
    profiler = LineProfiler()
    out = profiler(model.forward)(*random_input(1, next(model.parameters()).is_cuda))
    profiler.print_stats()
    logger.important('output sizes: {}'.format(','.join(str(o.size()[1:]) for o in out)))

    del out
    torch.cuda.empty_cache()

    for batch_size in [1, 4, 6, 8, 10, 12, 16, 20, 30, 40, 50]:
        try:
            t_start = time.time()
            out = model(*random_input(batch_size, next(model.parameters()).is_cuda))
            t_fwd = time.time()

            fake_loss = torch.cat(out).mean()
            fake_loss.backward()

            t_stop = time.time()

            msg = 'time (batch size {:03}): {:.4f}s ({:.4f}s per sample) with backprop: {:.4f}s'
            msg = msg.format(batch_size, t_fwd - t_start, (t_fwd - t_start) / batch_size, t_stop - t_start)
            logger.important(msg)
        except RuntimeError as e:
            logger.important(
                'timing for batch size {} failed. This is the error message: {}'.format(batch_size, str(e)))
            break
