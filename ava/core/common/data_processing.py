from .functional import get
from itertools import groupby
from collections import Counter
from pandas import DataFrame


def tuple_list_to_count_dictionary(tuple_list):
    """ transforms an arbitrary long list of tuples into a count table """

    all_vals = list(set(v for _, v in tuple_list))

    count_dict = {k: list(map(get(1), v)) for k, v in groupby(sorted(tuple_list, key=get(0)), get(0))}
    count_dict = {k: Counter(v) for k, v in count_dict.items()}
    count_dict = {k: [counts[v] for v in all_vals] for k, counts in count_dict.items()}

    return count_dict, all_vals


def tuple_list_to_data_frame(tuple_list):
    """ transforms an arbitrary long list of tuples into a count table """

    count_dict, all_vals = tuple_list_to_count_dictionary(tuple_list)
    df = DataFrame(count_dict)
    df.index = all_vals

    return df
