from ..logging import logger


def get_prioritized(*list):
    for item in list:
        if item is not None:
            return item
    raise ValueError('All list elements are None')


def from_dicts(key, *dicts, default='<no_default>'):
    for d in dicts:
        if d is not None and key in d:
            return d[key]

    if default != '<no_default>':
        return default
    else:
        raise ValueError('Key {} could not be found in {} dictionaries'.format(key, len(dicts)))


def ordered_dict_merge(*dicts, verbose_context=''):
    """ highest priority first """

    out = dict()
    for d in dicts[::-1]:  # in reverse order
        if d is not None:

            overwritten_keys = ['{}->{}'.format(k, d[k]) for k in d if k in out and d[k] != out[k]]
            if verbose_context != '' and len(overwritten_keys) > 0:
                logger.info('{}: Overwritten keys: {}'.format(verbose_context, ', '.join(overwritten_keys)))

            out.update(d)
    return out


# def from_dict_or_config(key, dictionary, t, strict=True, default=None):
#     """ Lookup key in dictionary. If it is not found obtain value from CONFIG. """
#     assert type(t) == type
#     if key not in dictionary:
#         if key in CONFIG:
#             if CONFIG[key] == 'None':
#                 return None
#             else:
#                 return t(CONFIG[key])
#         else:
#             if strict:
#                 raise ValueError('No value for argument provided and no config key found for {}'.format(key))
#             else:
#                 return default
#     else:
#         return dictionary[key]


# def from_dict_or_model_or_config(key, dictionary, model, t, strict=True, default=None):
#     assert type(t) == type
#     if key not in dictionary:
#         try:
#             return model.get_default(key)
#         except KeyError:
#             if key in CONFIG:
#                 if CONFIG[key] == 'None':
#                     return None
#                 else:
#                     return t(CONFIG[key])
#             else:
#                 if strict:
#                     raise ValueError('No value for argument provided and no config key found for {}'.format(key))
#                 else:
#                     return default
#     else:
#         return dictionary[key]
