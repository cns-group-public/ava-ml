def single_parameter_to_str(p):
    if type(p) == bool:
        return 'y' if p else 'n'
    elif type(p) in {list, tuple}:
        return '_'.join(str(a) for a in p)
    elif p is None:
        return 'N'
    else:
        return str(p)


def class_config_str(class_name, parameters):
    param_str = '-'.join(single_parameter_to_str(p) for p in parameters)
    return '{}{}'.format(class_name, '-' + param_str if param_str != '' else '')


def hash_object(obj):
    import hashlib
    import base64

    hash = base64.b64encode(hashlib.sha1(str(obj).encode('utf-8')).digest())
    hash = hash.decode('utf-8').replace('/', '-').replace('+', '_')[:-1]
    return hash


def hash_dict(dictionary):
    return hash_object(tuple('{}:{}'.format(k, str(dictionary[k])) for k in sorted(dictionary.keys())))
