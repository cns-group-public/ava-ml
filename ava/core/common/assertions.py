import os


def assert_object_type(object, required_type, context=''):
    if type(required_type) not in {list, tuple, set}:
        required_type = set([required_type])

    if len(context) != 0:
        context += ': '

    msg = context + 'Object type must one of {}. Actual type: {}'
    msg = msg.format(', '.join([t.__name__ for t in required_type]), type(object).__name__)
    assert type(object) in required_type, msg


def assert_in_types(a_type, required_type, context=''):
    if type(required_type) not in {list, tuple, set}:
        required_type = set([required_type])

    if len(context) != 0:
        context += ': '

    msg = context + 'Object type must one of {}. Actual type: {}'
    msg = msg.format(', '.join([t.__name__ for t in required_type]), a_type.__name__)
    assert a_type in required_type, msg


def assert_same_types(a, b, context=''):
    if len(context) != 0:
        context += ': '
    assert type(a) == type(b), context + 'types are not the same: {} != {}'.format(type(a).__name__, type(b).__name__)


def assert_equal_shape(tuple_a, tuple_b, context=''):
    if len(context) != 0:
        context += ': '
    assert tuple_a == tuple_b, context + 'shapes are not equal. Actual sizes: ' + str(tuple_a) + ' and ' + str(tuple_b)


def assert_equal_length(a, b, context=''):
    if len(context) != 0:
        context += ': '
    assert len(a) == len(b), context + 'lengths are not equal. Actual lengths: {} {}'.format(len(a), len(b))


def assert_equal_elements(a, b, require_completeness_of_first=False, require_completeness_of_second=False, context=''):

    # assert_equal_length(a, b, context)
    # assert_same_types(a, b, context)

    if type(a) == dict:
        a, b = list(a.items()), list(b.items())

    if len(context) != 0:
        context += ': '

    # within a
    different_elements = []

    if not require_completeness_of_first and not require_completeness_of_second:
        max_index = min(len(a), len(b))
    elif require_completeness_of_first and not require_completeness_of_second:
        max_index = len(a)
    elif require_completeness_of_second and not require_completeness_of_first:
        max_index = len(b)
    else:
        max_index = max(len(a), len(b))

    for i in range(max_index):
        if i >= len(a):
            different_elements += ['{}: n/a != {}'.format(i, b[i])]
        elif i >= len(b):
            different_elements += ['{}: {} != n/a'.format(i, a[i])]
        elif a[i] != b[i]:
            different_elements += ['{}: {} != {}'.format(i, a[i], b[i])]

    msg = '{}Elements are not the same at some indices (index: element|element):\n{}'
    msg = msg.format(context, '\n'.join(different_elements))
    assert len(different_elements) == 0, msg


def assert_in_set(element, set, context=''):
    if len(context) != 0:
        context += ': '
    assert element in set, context + '{} not in {}'.format(element, ','.join(set) if len(set) < 10 else 'set')


def assert_opencv_image(img, context=''):

    if len(context) != 0:
        context += ': '

    assert img.shape[2] == 3, context + 'color channels must be in third dimensions. Actual shape: ' + str(img.shape)
    assert img.dtype.name in {'uint8', 'float32'}, 'datatype must be uint8 or float32. Actual dtype: ' + img.dtype.name


def assert_file_exists(filename, context=''):

    if len(context) != 0:
        context += ': '

    assert os.path.isfile(filename), context + 'File not found:' + filename
