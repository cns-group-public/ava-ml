import subprocess
import time
import inspect


# class Const(object):
#
#     def __init__(self, dictionary):
#         self.dict = dictionary
#
#     def __getattr__(self, item):
#         return self.dict[item]
#
#     def __setattr__(self, item, value):
#         raise LookupError('Const does not permit changes.')


# def split_arguments(function, arguments):
#     sig = set(inspect.signature(function).parameters.keys())
#     return {k: v for k, v in arguments.items() if k in sig}, {k: v for k, v in arguments.items() if k not in sig}

def download_file(url, target, exist_ok=False):
    from subprocess import run
    import os

    if not os.path.isfile(target):
        os.makedirs(os.path.dirname(target), exist_ok=True)
        run(f'wget {url} -P {target}'.split())
    else:
        if exist_ok:
            return
        else:
            raise FileExistsError


def extract_archive(filename, target_folder):
    from subprocess import run, PIPE

    if filename.endswith('tgz') or filename.endswith('.tar'):
        run(f'tar -xf {filename} -C {target_folder}'.split(), stdout=PIPE, stderr=PIPE)
    elif filename.endswith('zip'):
        run(f'unzip  {filename} -d {target_folder}'.split(), stdout=PIPE, stderr=PIPE)
    else:
        raise ValueError(f'unsuppored file ending of {filename}')


def find_file_in_folders(filename, folder_list):
    """ Try to find filename in different folders (provided by `folder_list`) """

    import os

    search_files = [filename] + [os.path.join(p, filename) for p in folder_list]

    print(search_files)
    found_files = [f for f in search_files if os.path.isfile(f)]

    if len(found_files) > 0:
        return found_files[0]
    else:
        return None


def elements_list_to_tuple(obj):

    def l2t(x): return tuple(x) if type(x) == list else x

    if type(obj) == dict: return {k: l2t(v) for k, v in obj.items()}
    if type(obj) == list: return [l2t(x) for x in obj]
    if type(obj) == tuple: return tuple(l2t(x) for x in obj)
    else: raise ValueError('obj has unsupported type: {} (supported: dict, list, tuple)'.format(type(obj)))


def matching_args(function, arguments):
    """ Sorts arguments into ones that are consumed by a function and those that aren't """
    sig = set(inspect.signature(function).parameters.keys())
    function_args = {k: v for k, v in arguments.items() if k in sig}
    non_function_args = {k: v for k, v in arguments.items() if k not in sig}
    return function_args, non_function_args


def parse_docstring(doc):
    doc = doc.strip()
    parts = doc.split(' ')
    doc = ' '.join([p.strip() for p in parts if len(p) > 0])
    return doc


def describe(obj):
    import numpy as np

    if type(obj) == list:
        el_types = ','.join(t.__name__ for t in set(type(el) for el in obj))
        return 'list of length ' + str(len(obj)) + ' with element types ' + el_types
    if type(obj) == tuple:
        el_types = ','.join(t.__name__ for t in set(type(el) for el in obj))
        return 'tuple of length ' + str(len(obj)) + ' with element types ' + el_types
    if type(obj) == np.ndarray:
        text = 'np.ndarray[' + obj.dtype.name + '] of shape' + str(obj.shape)
        text += ' and min/max of ' + str(obj.min()) + '/' + str(obj.max())
        return text
    else:
        return 'object of type ' + type(obj).__name__


def encode_train_params(train_params):

    def replace(p):
        if p is None:
            return ''
        else:
            return str(p)

    return '_'.join([replace(p) for p in train_params])


def decode_train_params(param_str):
    params = param_str.split('_')
    params = [None if p == '' else p for p in params]
    batch_size, lr, optim_name, weight_decay, max_episodes, max_epochs, fraction = params
    return batch_size, lr, optim_name, weight_decay, max_episodes, max_epochs, fraction


def enumerate_timed(iterable, start=0):
    counter = start
    while True:
        try:
            t_start = time.time()
            item = next(iterable)
            yield counter, time.time() - t_start, item
            counter += 1
        except StopIteration:
            break


def get_current_git():
    try:
        out = subprocess.run(["git", "rev-parse", "HEAD"], stdout=subprocess.PIPE)
        if out.returncode == 0:
            return out.stdout.decode('utf8')[:-1]
        else:
            return None
    except FileNotFoundError:
        return None
