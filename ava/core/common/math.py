import numpy as np


def softmax(x):
    """ apply softmax along last dimension """
    return np.exp(x) / np.exp(x).sum(-1)[..., None]
