""" A collection of functions that make the code more elegant and expressive """
from itertools import groupby


def get(*indices):
    """ getter that can me mapped on lists """
    def g(x):
        if len(indices) == 1:
            return x[indices[0]]
        else:
            return tuple(x[i] for i in indices)
    return g


def emap(f, a):
    return list(map(f, a))


def egroupby(a, key):
    """ conventional groupby requires sorted input and can only be evaluated once. """
    return {k: list(v) for k, v in groupby(sorted(a, key=key), key=key)}

