import time
import torch

from torch import nn
from .common.generic import get_current_git
from .common.value_assignment import ordered_dict_merge
from .common.serialization import class_config_str
from .logging import logger
# from .settings import CONFIG

MULTI_GPU_COPY_ATTRIBUTES = ('is_baseline', 'loss', 'metrics', 'prediction_names')

# also the standard normalization for torchvision models
IMAGENET_MEAN = [0.485, 0.456, 0.406]
IMAGENET_STD = [0.229, 0.224, 0.225]


def load_pretrained_model(model_name, cli_args=None, model_config=None, no_weights=False, multi_gpu=False,
                          prefer_dataset_args=False, load_state_dict_non_strict=False, cuda=False):

    # all_models = get_all_models()
    cli_args = dict() if cli_args is None else dict(cli_args)

    if type(model_name) == dict:
        model_file = model_name
        model_name = model_file['model'][-1]
    else:
        logger.important('\nload pre-trained model', model_name)
        model_file = torch.load(model_name)

    loaded_model_name = model_file['model'][-1]

    # there is no need to initialize the model with pretrained weights (e.g. ImageNet),
    # as the state_dict will be overwritten anyway.
    if 'pretrained' in cli_args:
        logger.warning('Pretrained is set to False as weights will be loaded later anyways.')
        cli_args['pretrained'] = False

    try:
        from .. import models
        model_type = getattr(models, loaded_model_name)
    except ImportError:
        raise ImportError('Model not found. Are you sure it is imported in models/__init__.py?')

    checkpoint_args = model_file['model_arguments'][-1]
    if prefer_dataset_args:
        model_args = ordered_dict_merge(cli_args, model_config, checkpoint_args, verbose_context='LOAD PRETRAINED MODEL (PREFER DATASET ARGS) ')
    else:
        model_args = ordered_dict_merge(cli_args, model_config, checkpoint_args, verbose_context='LOAD PRETRAINED MODEL ')

    # are there parameters missing?
    non_match_args = [k for k, v in checkpoint_args.items() if k not in model_args]
    msg = "LOAD PRETRAINED MODEL: These arguments required by {} were not provided: {}".format(model_name, ', '.join(non_match_args))
    assert len(non_match_args) == 0, msg

    logger.info(model_info_text(model_type, model_name, model_file, model_args, context='LOAD PRETRAINED MODEL'))

    model = model_type(**model_args)
    model_class = model.__class__.__name__

    # load weights
    if not no_weights:
        logger.important('LOAD PRETRAINED MODEL: Set pre-loaded weights for ', model.__class__.__name__)
        weights = model_file['state_dict']
        logger.info('LOAD PRETRAINED MODEL: Included submodules', set(['.'.join(k.split('.')[:2]) for k in weights]))

        # for some reason weight keys often start with "module.". This is fixed here:
        if all([k.startswith('module') for k in weights]):

            weights = {k[7:]: v for k, v in weights.items()}

        if load_state_dict_non_strict:
            param_names = {n: p.size() for n, p in model.named_parameters()}
            weight_keys = set(weights.keys())
            shared_params = weight_keys.intersection(param_names.keys())
            missing_from_model = weight_keys.difference(param_names.keys())
            missing_from_weights = set(param_names.keys()).difference(weight_keys)

            # weights must have same size to be used
            used_weights = {p: weights[p] for p in param_names if weights[p].size() == param_names[p]}
            logger.important('Non-strict import of Parameters: Number of used/same name keys: {}/{}, only in weights: {}, only in model: {}'.format(
                len(used_weights), len(shared_params), len(missing_from_model), len(missing_from_weights)
            ))

            model.load_state_dict(used_weights, strict=False)
        else:
            model.load_state_dict(weights, strict=True)

    logger.important('Model parameter checksum: {}'.format(float(sum([(p).sum() for p in model.parameters()]))))

    model = monkey_patch_model(model, multi_gpu)
    # if 'data_parallel' in model_file and model_file['data_parallel']:
    #     logger.info('Initialize DataParallel model')
    #
    #     # this monkey-patching is admittedly ugly, but it works
    #     model = monkey_patch_model(nn.DataParallel(model), model, MODEL_INTERFACE)

    # all entries are list, because they can be trained multiple times (e.g. pre-training)
    model.training_details = {
                              'model': [model_class],
                              'model_name': [model_name],
                              'model_arguments': model_file['model_arguments'] + [model_args],
                              'dataset_name': model_file['dataset_name'],
                              'dataset_arguments': model_file['dataset_arguments'],
                              'creation_time': model_file['creation_time'] + [time.strftime('%d.%m.%y %H:%M')],
                              'git_checksum': model_file['git_checksum'] + [get_current_git()],
                              'data_parallel': type(model) == nn.DataParallel,
                              }
    model.training_state = {'val_loss': model_file['val_loss'] + [], 'train_loss': model_file['val_loss'] + [],
                            'episode': model_file['episode'] + [], 'metrics': model_file['metrics'] + []}

    if cuda and (not hasattr(model, 'is_baseline') or not model.is_baseline()):
        model.cuda()

    return model


def initialize_model(model_name: str, cli_args=None, model_config=None, multi_gpu=False, cuda=False):

    from .. import models
    model_type = getattr(models, model_name)

    # the model priorizes arguments by these sources in descending order: CLI, dataset.model_config
    model_args = dict()

    if model_config is not None:
        model_args.update(model_config)
        logger.warning('Extra model arguments (e.g. by dataset): {}'.format(', '.join(model_config.keys())))

    if cli_args is None:
        cli_args = dict()

    overwritten_args = {k: cli_args[k] for k in model_args if k in cli_args}
    if len(overwritten_args) > 0:
        logger.warning('These arguments are overwritten by CLI:', ', '.join(overwritten_args))

    for k in cli_args:
        model_args[k] = cli_args[k]

    info_txt = ('Initialize model {}\n'
                '   model arguments:\n      {}\n')

    logger.info(info_txt.format(model_name, '\n      '.join('{}: {}'.format(k, v) for k, v in model_args.items())))

    try:

        model = model_type(**model_args)
    except TypeError as err:
        raise TypeError('Error while initializing the model {}. You might need to provide more arguments. This can '
                        'be done using the model_config dictionary of the dataset or via the CLI. '
                        'Error message: {}'.format(model_type.__name__, err))

    logger.important(torch.cuda.device_count(), 'available GPUs', 'using multi gpu' if multi_gpu else 'no multi gpu')

    model_class = model.__class__.__name__
    model = monkey_patch_model(model, multi_gpu)

    model.training_details = {'model': [model_class],
                              'model_arguments': [model_args],
                              'dataset_name': [],
                              'dataset_arguments': [],
                              'creation_time': [time.strftime('%d.%m.%y %H:%M')],
                              'git_checksum': [get_current_git()]}

    model.training_state = {'val_loss': [], 'train_loss': [], 'episode': [], 'metrics': []}

    if cuda and (not hasattr(model, 'is_baseline') or not model.is_baseline()):
        model.cuda()

    return model


# TODO: rewrite load_model such that all arguments are stored in the model checkpoint. Its proabably easiest to
def monkey_patch_model(model, multi_gpu=False):

    # no patching for baselines
    if hasattr(model, 'is_baseline') and model.is_baseline():
        return model

    if multi_gpu:
        DataParallel = nn.DataParallel
    else:
        DataParallel = lambda x: x

    original_model = model
    model = DataParallel(model)

    for m in MULTI_GPU_COPY_ATTRIBUTES:
        if hasattr(model, m):
            setattr(model, m, getattr(original_model, m))

    return model


def count_parameters(model, only_trainable=False):
    """ Count the number of parameters of a torch model. """
    import numpy as np
    return sum([np.prod(p.size()) for p in model.parameters() if (only_trainable and p.requires_grad) or not only_trainable])


def model_info_text(model_type, model_name, model_file, model_args, context=''):

    info_txt = ('{}Initialize pre-trained model of type {}\n'
                '   model file: {}\n'
                '   created: {}\n'
                '   validation/training loss: {:.5f} {:.5f}\n'
                '   metrics:\n      {}\n'
                '   model arguments:\n      {}\n'
                '   trained episodes: {}\n'
                '   training rounds: {}\n'
                '   data parallel: {}')

    return info_txt.format(context, model_type.__name__, model_name, model_file['creation_time'][-1],
                           model_file['val_loss'][-1], model_file['train_loss'][-1],
                           '\n      '.join('{}: {}'.format(k, ' '.join('{:.3f}'.format(s) for s in v)) for k, v in
                                           model_file['metrics'][-1]),
                           '\n      '.join('{}: {}'.format(k, v) for k, v in model_args.items()),
                           model_file['episode'][-1], len(model_file['episode']),
                           model_file['data_parallel'] if 'data_parallel' in model_file else False)