import os
from tkinter import ttk, Tk, N, W, E, S, StringVar, Label, Button, Frame, Entry, SUNKEN

import numpy as np

from .dataset import parse_dataset_def
from .collate import collate_element
from .logging import logger
from .dataset import DatasetBase
# from .plot import plot_samples_predictions_visdom
# from .selection import get_visualizers

from functools import partial

from .data_types import ImageData, TextData, SlicesData, LabelImageData

VISUALIZERS = {
    'image': partial(ImageData, ),
    'denseC': partial(LabelImageData),
    'denseM': partial(SlicesData, maps_dim=0, color=False),
    'video': partial(SlicesData, channel_dim=0, maps_dim=1, color=True),
    'videoGS': partial(SlicesData, maps_dim=0, color=False),  # GS = grayscale
    'C': partial(TextData),
    'M': partial(TextData)
}


def get_visualizers(samples, predictions, dataset, model, target_size):

    inp, out = parse_dataset_def(dataset)

    # overwrite visualizers if explicitly specified
    if dataset.visualizers is not None:
        visualizers = [vis(sample, target_size) for vis, sample in zip(dataset.visualizers, samples)]
        infos = [('a', i, t, s) for i, (t, s) in enumerate(inp + out)]
    else:
        visualizers, infos = [], []
        for i, (sample, (vis_type, vis_name)) in enumerate(zip(samples, inp + out)):

            if sample is not None:
                try:
                    sample = dataset.visualization_hooks[vis_name](sample) if vis_name in dataset.visualization_hooks else sample
                    visualizers += [VISUALIZERS[vis_type](sample, target_size)]
                    infos += [('input', i, vis_type, vis_name if vis_name is not None else vis_type)]
                except BaseException as e:
                    shape = sample.shape if hasattr(sample, 'shape') else 'no shape'
                    msg = (f'Failed to initialize visualizer type {vis_type}. Name: {vis_name}, Shape: {shape}\n'
                           'Available visualizers: ' + ', '.join(VISUALIZERS.keys()))
                    logger.warning(msg)
                    raise e
            else:
                visualizers += [TextData('None', target_size)]
                infos += [('input', i, vis_type, vis_name if vis_name is not None else vis_type)]

    if predictions is not None:
        for i, (prediction, (vis_type, vis_name)) in enumerate(zip(predictions, out)):
            try:
                vis_type = 'image'
                prediction = 1 / (1 + np.exp(prediction))
                # prediction -= prediction.min()
                # prediction /= prediction.max()
                prediction -= 0.5
                prediction*= 2

                visualizers += [VISUALIZERS[vis_type](prediction, target_size)]
                infos += [('prediction', i, vis_type, 'pred:' + vis_name if vis_name is not None else 'pred:' + vis_type)]
            except BaseException as e:
                logger.warning('Failed to initialize visualizer type {}. Name: {}'.format(vis_type, vis_name))
                raise e

    # additional visualizers
    additional = []
    if hasattr(model, 'additional_visualizations'): additional += model.additional_visualizations
    if hasattr(dataset, 'additional_visualizations'): additional += dataset.additional_visualizations

    if len(additional) > 0 or predictions is not None:

        names = [info[3] for info in infos]
        assert len(set(names)) == len(names), 'if additional visualizers are used, no duplicate names are allowed'

        data_by_name = {infos[i][3]: samples[i] for i in range(0, len(samples))}

        # if predictions is not None:
        #     infos += [infos[len(inp) + i] for i in range(0, len(predictions))]
        #     data_by_name.update({infos[len(samples) + i][3]: predictions[i] for i in range(0, len(predictions))})
        #

        for i, (data_names, visualizer, args) in enumerate(additional):

            missing_chunks = [n for n in data_names if n not in data_by_name]

            if len(missing_chunks) == 0:
                data = tuple(data_by_name[n] for n in data_names)
                visualizers += [visualizer(data, target_size, **args)]
                infos += [('additional', i, 'additional', 'additional')]
            else:
                logger.warning('Can not plot additional visualizations because these required '
                               'chunks are not available: {}'.format(', '.join(missing_chunks)))

    return visualizers, infos


class Visualize(object):
    """
    This class takes care of visualization using TkInter.

    # Arguments
        modes:
        save_images: Currently not being used.
        model: If specified, this model will be used to add predictions
    """

    def __init__(self, modes=None, save_images=None, save_images_path=None, start=0,
                 image_labels=None, chunk_names=None, max_height=200, model=None,
                 plot_visdom=True, shuffle=False):

        super().__init__()

        self.shuffle = shuffle
        self.image_labels = image_labels
        self.max_height = max_height
        self.save_images = save_images
        self.slice_labels = None
        self.chunk_names = chunk_names
        self.root = Tk()
        self.root.title("DataView")
        
        self.model = model
        self.predictions_cpu = None
        self.sample_batches = None

        self.plot_visdom = plot_visdom

        if save_images is not None:
            if save_images_path is None:
                self.save_images_path = 'image_output'

            if not os.path.isdir(self.save_images_path):
                os.mkdir(self.save_images_path)

        self.mainframe = ttk.Frame(self.root, padding="3 3 12 12")

        self.mainframe.grid(column=0, row=1, sticky=(N, W, E, S))
        self.mainframe.columnconfigure(0, weight=1)
        self.mainframe.rowconfigure(0, weight=1)
        self.modes = modes
        self.cursor = start
        self.widgets = []
        self.vis_modules = []

        self.height_var = StringVar(self.mainframe, value=max_height)
        self.height_var.trace("w", callback=self.size_cb)  # for the plus and minus buttons
        self.target_size = max_height

        self.sample_name_label = Label(self.mainframe, text='<no sample>')
        self.sample_name_label.grid(column=1, row=2, columnspan=3, sticky=(W, E))

    def size_cb(self, *args):
        self.target_size = max(100, int(self.height_var.get()))
        self.update_widgets()

    def __call__(self, dataset):

        assert isinstance(dataset, DatasetBase), 'dataset must be derived from DatasetBase'
        self.dataset = dataset

        self.draw_sample_batch()

        self.lane_labels, self.widgets = [], []

        self.sample_id_label = StringVar()
        self.sample_id_label_widget = Label(self.mainframe, textvariable=self.sample_id_label).grid(column=2, columnspan=2, row=0)
        self.set_waiting()

        f2 = Frame(self.mainframe, bd=1, relief=SUNKEN)
        f2.grid(column=1, row=1, columnspan=3, sticky=(W, E), ipady=7, ipadx=7, padx=7, pady=7)

        button_prev = Button(f2, text="< back")
        button_prev.pack(side='left')
        button_prev.bind("<Button-1>", self.prev)

        Label(f2, text='height: ').pack(side='left')

        image_height_plus = Button(f2, text='-')
        image_height_plus.pack(side='left')
        image_height_plus.bind('<Button-1>', self.zoom_out)

        entry_image_height = Entry(f2, textvariable=self.height_var, width=5)
        entry_image_height.pack(side='left')
        entry_image_height.bind('<FocusOut>', self.size_cb)

        image_height_plus = Button(f2, text='+')
        image_height_plus.pack(side='left')
        image_height_plus.bind('<Button-1>', self.zoom_in)

        Label(f2, text='width: ').pack(side='left')

        button_next = Button(f2, text="next >")
        button_next.pack(side='left')
        button_next.bind("<Button-1>", self.next)

        button_updt = Button(f2, text="refresh")
        button_updt.pack(side='left')
        button_updt.bind("<Button-1>", self.refresh)

        button_shuffle = Button(f2, text="shuffle")
        button_shuffle.pack(side='left')
        button_shuffle.bind("<Button-1>", self.shuffle_sample)

        f2.bind("<Left>", self.prev)
        f2.bind("<Right>", self.next)
        f2.bind("<space>", self.refresh)
        f2.bind("<plus>", self.zoom_in)
        f2.bind("<minus>", self.zoom_out)
        f2.focus_set()

        self.update_widgets()
        self.root.mainloop()

    def zoom_in(self, ev):
        self.height_var.set(int(self.height_var.get()) + 25)

    def zoom_out(self, ev):
        self.height_var.set(int(self.height_var.get()) - 25)

    def prev(self, event=None):
        if self.sample_batches is not None:
            if self.cursor_in_batch > 0:
                self.cursor_in_batch -= 1
            else:
                self.cursor -= 1
                self.draw_sample_batch(cursor_to_last=True)
        else:
            self.cursor -= 1
            self.draw_sample_batch(cursor_to_last=True)

        self.update_widgets()

    def next(self, event=None):
        if self.sample_batches is not None:
            if self.cursor_in_batch < len(self.sample_batches[0]) - 1:
                self.cursor_in_batch += 1
            else:
                self.cursor += 1
                self.draw_sample_batch()
        else:
            self.cursor += 1
            self.draw_sample_batch()

        self.update_widgets()

    def shuffle_sample(self, ev):
        self.draw_sample_batch(keep_cursor=False, shuffle=True)
        self.update_widgets()

    def refresh(self, event=None):
        self.draw_sample_batch(keep_cursor=True, shuffle=self.shuffle)
        self.update_widgets()

    def set_waiting(self):
        self.clear_widgets()
        #self.widgets = [Label(self.mainframe, text='loading...') for _ in self.input_data_generator.lane_names]
        self.widgets = [Label(self.mainframe, text='loading...')]

        for i, w in enumerate(self.widgets):
            w.grid(column=i + 2, row=4, sticky=(W,))

        self.root.update()

    def clear_widgets(self):
        for w in self.widgets:
            w.destroy()
        self.widgets = []

    def draw_sample_batch(self, cursor_to_last=False, keep_cursor=False, shuffle=False):

        if not keep_cursor:
            if self.cursor >= len(self.dataset) - 1:
                self.cursor = 0
            elif self.cursor < 0:
                self.cursor = len(self.dataset) - 1

            if shuffle:
                self.cursor = np.random.randint(0, len(self.dataset))


        samples_x, samples_y = self.dataset[self.cursor]
        samples_x = [[s] for s in samples_x]
        samples_y = [[s] for s in samples_y]
        self.sample_batches = samples_x + samples_y

        # sampler = BatchSampler(self.input_dataset, 1, start=self.cursor, shuffle_samples=False)
        if hasattr(self.dataset, 'sample_ids'):
            self.sample_name_label['text'] = 'sample id: {}  ({} / {})'.format(
                self.dataset.sample_ids[self.cursor], self.cursor, len(self.dataset))
        #
        # samples_x, samples_y, _, _ = sampler.get()
        #
        # self.sample_batches = samples_x + samples_y
        # assert_equal_length(self.sample_batches, self.chunk_names, 'Sample batches and chunk_names should have same length.')

        if self.model is not None:

            # import cv2

            variables_x = [collate_element(x) for x in samples_x]

            if next(self.model.parameters()).is_cuda:
                variables_x = [v.cuda() if v is not None else None for v in variables_x]

            predictions = self.model(*variables_x)

            print(variables_x[0].min(), variables_x[0].max(), variables_x[0].shape, variables_x[0].type())

            self.predictions_cpu = [p.data.cpu().numpy() for p in predictions]

        if self.plot_visdom:
            logger.plot_samples_predictions_visdom(self.model, self.sample_batches, self.predictions_cpu, self.dataset)

        msg = 'batch must have the same length for each element. Actual lengths:' + str([len(b) if b is not None else None for b in self.sample_batches])
        assert len(set([len(b) for b in self.sample_batches if b is not None])) == 1, msg
        self.cursor_in_batch = len(self.sample_batches[0]) - 1 if cursor_to_last else 0

    def update_widgets(self):
        self.set_waiting()
        self.clear_widgets()

        predictions_cpu = [p[0] for p in self.predictions_cpu] if self.predictions_cpu is not None else None

        samples = [s[0] if s is not None else None for s in self.sample_batches]
        size = (self.target_size, self.target_size)
        visualizers, info = get_visualizers(samples, predictions_cpu, self.dataset, self.model, size)

        for i, (name, j, mode, chunk_name) in enumerate(info):
            label = Label(self.mainframe, text=chunk_name)
            label.grid(column=i + 2, row=3, sticky=(W,))
            self.lane_labels += [label]

        for i, vis_module in enumerate(visualizers):
            self.vis_modules += [vis_module]
            try:
                w = vis_module.get_tcl_frame(self.mainframe, col=i)
                w.grid(column=i + 2, row=4, sticky=(N,), padx=5, pady=5)
                self.widgets += [w]
            except TypeError as err:
                raise TypeError('Could not build a widget from the provided data. You might want to check the '
                                'visualization mode of the dataset: Visualizer module {} at index {}. '
                                'Actual error message:\n {}'.format(vis_module.__class__.__name__, i, err))

        self.root.update()


