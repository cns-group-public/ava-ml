import torch
import numpy as np


def collate_element(batch, cuda=False):

    if batch is None:
        out = None
    elif type(batch) == list and batch[0] is None:
        out = None
    else:
        if type(batch[0]) == np.ndarray:
            # print(np.array(batch).dtype)
            # xx
            # print(np.array(batch).shape)
            out = torch.from_numpy(np.array(batch))
        elif type(batch[0]) in {str, np.str_}:
            out = str(batch[0])
        elif type(batch[0]) == float:
            out = torch.FloatTensor(batch)
        elif type(batch[0]) in {int, np.int64}:
            out = torch.LongTensor(batch)
        else:
            raise ValueError('Failed to convert data into tensor. Type: {}, Length: {}, Element types: {}'.format(
                type(batch), len(batch), [type(b) for b in batch]
            ))

    # if out is not None:
    #     print(type(out))
    #     out.pin_memory()

    if cuda and hasattr(out, 'cuda'):
        out = out.cuda()

    return out


def collate(batch, cuda=False):
    variables_x = [collate_element([batch[i][0][j] for i in range(len(batch))], cuda=cuda) for j in range(len(batch[0][0]))]
    variables_y = [collate_element([batch[i][1][j] for i in range(len(batch))], cuda=cuda) for j in range(len(batch[0][1]))]

    return variables_x, variables_y
