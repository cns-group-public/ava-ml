from collections import defaultdict
import numpy as np
import hashlib
import base64
from pandas import DataFrame, concat
import os

from .logging import logger


def train_output_to_pandas(scores, details, show=False):
    scores_flat = {k: {(ka+'-'+kb if len(v.keys()) > 1 else kb): v[ka][kb] for ka in v.keys() for kb in v[ka].keys()} for k, v in scores.items()}

    details_latest = {}

    score_keys = []
    for s in scores_flat.values():
        for k in s.keys():
            score_keys += [] if k in score_keys else [k]

    for k in details:
        details_latest[k] = {j: details[k][j][-1] for j in details[k].keys()
                             if j not in ('model_arguments', 'dataset_arguments') and
                             hasattr(details[k][j], '__getitem__')}
        details_latest[k].update(details[k]['model_arguments'][-1])
        details_latest[k].update(details[k]['dataset_arguments'][-1])

    combined = {k: {**scores_flat[k], **details_latest[k]} for k in scores_flat.keys()}

    # remove lists
    def clean_cell(x):
        if type(x) == np.ndarray:
            return clean_cell(x.tolist())
        # if type(x) in {list, tuple}:
        #     if len(x) == 1: return clean_cell(x[0])
        #     elif len(x) > 1: return x
        #     else: return ''
        elif type(x) in {float, np.float64, np.float32}: return float(x)
        else: return x

    combined = {i: {k: clean_cell(v) for k, v in d.items()} for i, d in enumerate(combined.values())}

    first_row = list(combined.values())[0]
    first_row = {k: v for k, v in combined[0].items() if k not in score_keys}

    sort_weights = defaultdict(lambda: 0, **{'model': -99, 'batch_size': 95, 'lr': 96, 'episode': 97, 'train_loss': 98, 'val_loss': 99})

    all_keys = set([kk for k in details_latest.keys() for kk in details_latest[k].keys()])
    details_keys = sorted(all_keys, key=lambda x: sort_weights[x])

    keys = details_keys + [k for k in first_row if k not in details_keys]  # makes sure that details come first

    key_vals = {k: [] for k in keys}
    for k in keys:
        for v in combined.values():
            if k not in v:
                pass
            elif type(v[k]) in {list, tuple}:
                key_vals[k] += [tuple(v[k])]
            elif type(v[k]) == dict:
                key_vals[k] += [str(v[k].items())]
            else:
                key_vals[k] += [v[k]]

    different_keys = [k for k in keys if len(set(key_vals[k])) > 1 or len(key_vals[k]) < len(combined)]
    different_keys += score_keys
    same_keys = [k for k in keys if k not in different_keys]

    common = DataFrame({k: [first_row[k]] for k in same_keys}).T
    # common = {k: [first_row[k]] for k in same_keys}
    table = DataFrame(DataFrame(combined).T, columns=different_keys)

    return table, common




def table_to_tex(table, common, experiments, filename_pre=None, show=False):

    default_ignore = ('train-loss-history', 'val-loss-history', 'git-checksum', 'creation-time', 'score-args', 'model-name')
    default_lookup = {'train-loss': 'lossT', 'val-loss': 'lossV', 'episode': 'epi'}

    experiments = defaultdict(lambda: None, **experiments)
    ignore_cols = default_ignore + tuple(experiments['ignore_cols']) if experiments['ignore_cols'] else default_ignore
    remap = dict(**default_lookup, **experiments['tex_remap']) if experiments['tex_remap'] else default_lookup

    if experiments['tex_percent_cols'] is not None:
        for hash_col in experiments['tex_percent_cols']:
            for i in table.index:
                if hash_col in table:
                    table[hash_col][i] = hash_element(table[hash_col][i])


    table.rename(columns={c: c.replace('_', '-') for c in table.columns}, inplace=True)
    if remap: table.rename(columns={k: v for k, v in remap.items() if k in table.columns}, inplace=True)

    logger.info('available columns: ', ', '.join(sorted(list(table.columns))))

    if 'tex_split_cols' in experiments:
        for split_col, names in experiments['tex_split_cols'].items():
            if split_col in table:
                splitted = DataFrame([table[split_col][i] for i in range(len(table[split_col]))])
                splitted.columns = names
                table = concat([table.drop(split_col, axis=1), splitted], axis=1)
            else:
                logger.warning('Split column {} not found'.format(split_col))

    # flatten entries and round
    for col in table.columns:
        for row in table[col].keys():
            if type(table[col][row]) == list and len(table[col][row]) == 1:
                table.at[row, col] = table[col][row][0]

            if type(table[col][row]) in {float, np.float16, np.float32, np.float64}:
                table.at[row, col] = np.round(table[col][row], 3)

            try:
                if type(table[col][row]) not in {list, tuple} and np.isnan(table[col][row]):
                    table.at[row, col] = np.nan
            except TypeError:
                pass


    if experiments['tex_collapse'] is not None:

        assert type(experiments['tex_collapse']) == list

        table['Parameters'] = ''

        for row in table.index:
            table['Parameters'][row] = ' '.join(str(table[col][row]) if table[col][row] != 'True' else col
                                                for col in experiments['tex_collapse']
                                                if col in table and table[col][row] not in {'', '-', None})

        for col in experiments['tex_collapse']:
            if col in table:
                table = table.drop(col, axis=1)

        # re-order table
        first_cols = (['model'] if 'model' in table else []) + ['Parameters']
        table = table[first_cols + [c for c in table.columns if c not in {'model', 'Parameters'}]]

        print('Collapsed version of the table:')
        print(table)

    if 'tex_model_cols' in experiments and 'model' in table.columns:
        for row in table.index:
            add = ' '.join(str(table[col][row]) if table[col][row] != 'True' else col
                           for col in experiments['tex_model_cols']
                           if col in table and table[col][row] not in {'', '-', None})
            table['model'][row] = table['model'][row] + ' ' + add

        for col in experiments['tex_model_cols']:
            if col in table:
                table = table.drop(col, axis=1)

    if experiments['tex_remap']:
        for col in table.columns:
            for row in table[col].keys():
                try:
                    table.at[row, col] = experiments['tex_remap'][table[col][row]]
                except (KeyError, TypeError):
                    pass


    if 'extra_output' in experiments:
        for k, setting in experiments['extra_output'].items():

            cols = setting['cols']
            rot = setting['rotate'] if 'rotate' in setting else 0

            missing_cols = [c for c in cols if c not in table]

            if len(missing_cols) > 0:
                logger.warning('Extra output: These columns are not in the table: {}'.format(missing_cols))
                cols = [c for c in cols if c not in missing_cols]

            # assert len(missing_cols) == 0, 'Extra output failed: These columns are not in the table: {}'.format(missing_cols)
            assert len(cols) == len(set(cols)), 'columns can only occur once'

            extra_tab = table[cols]
            # print(extra_tab)

            with open(os.path.join('{}_{}.tex'.format(filename_pre, k)), 'w') as fh:
                fh.write(data_frame_to_tex(extra_tab, rotate_head=rot, hlines=experiments['tex_hlines'], percent_cols=experiments['tex_percent_cols']))

            print('wrote extra output {}'.format(k))

    for ignore_col in ignore_cols:
        if ignore_col in table.columns:
            table = table.drop(ignore_col, axis=1)
            # print('dropped', ignore_col)

    if show:
        print(common, '\n\n', table.round(2))

    common.index = [to_tex(c) for c in common.index]
    for row in common[0].keys():
        common[0][row] = to_tex(common[0][row])

    # print(table.columns, 'model' in table.columns)

    transpose_table = 'tex_transpose' in experiments and experiments['tex_transpose']
    table_tex = data_frame_to_tex(table, multicols=experiments['tex_multi_cols'], hlines=experiments['tex_hlines'], percent_cols=experiments['tex_percent_cols'],
                                  transpose=transpose_table)
    common_tex = data_frame_to_tex(common, header=False)

    if filename_pre is not None:
        with open(os.path.join('{}_common.tex'.format(filename_pre)), 'w') as fh:
            fh.write(common_tex)

        with open(os.path.join('{}.tex'.format(filename_pre)), 'w') as fh:
            fh.write(table_tex)

    return common_tex, table_tex


def data_frame_to_tex(table, float_format='{:.3f}', multicols=None, header=True, hlines=None, rotate_head=0,
                      percent_cols=(), transpose=False):

    columns = list(table.columns)
    hlines = set(hlines) if hlines is not None else set()

    new_table = {col: {} for col in table.keys()}
    for col in table.keys():
        for row in table[col].keys():
            new_table[col][row] = to_tex(table[col][row], col=col, percent_cols=percent_cols)

    table = DataFrame(new_table)

    if transpose:
        table = table.T
        table.reset_index(level=0, inplace=True)

        out = '\\begin{{tabular}}{{{}}} \n'.format('l' + 'c' * (len(table.columns) - 1))
        out += '\\toprule \n'

    else:

        out = '\\begin{{tabular}}{{{}}} \n'.format('l' + 'c' * (len(table.columns) - 1))
        out += '\\toprule \n'

        if multicols is not None:
            # key, first-pos, length
            indices = [(k, columns.index(multicols[k][0][0]), len(multicols[k][0]), multicols[k][1]) for k in multicols.keys()]
            indices = sorted(indices, key=lambda x: x[1])

            i_pos = 0
            for k, pos, length, new in indices:
                out += ' & ' * (pos - i_pos)
                out += '\multicolumn{{{}}}{{c}}{{{}}}'.format(length, ' \\textbf{' + k + '} ')
                columns[pos:pos+length] = new
                i_pos = pos + length - 1

            out += ' \\\\ \n'

        if header:
            if rotate_head != 0:
                columns = [' \\rotatebox{{{}}}{{{}}} '.format(rotate_head, c) for c in columns]

            out += ' & '.join(columns) + ' \\\\ \n'

        out += '\\midrule \n'

    def val_to_str(val):
        if val != val:  # NaN
            return '-'
        if type(val) in {int, str}:
            return str(val)
        elif type(val) in {float, np.float_}:
            return float_format.format(val)
        else:
            return str(val)

    for i in table.index:
        line = ' & '.join([val_to_str(v) for v in table.loc[i].values]) + ' \\\\ \n'
        if i in hlines: line += '\\midrule \n'
        out += line

    out += '\\bottomrule \n'
    out += '\\end{tabular}'

    return out


# def cvs_table_to_tex(filename, config):
#
#     import csv
#     import re
#
#     replacements = config['replacements']
#     num_regexp = r'^[0-9]*\.[0-9][0-9][0-9]+$'
#
#     with open(filename, 'r') as csvfile:
#         csv_reader = csv.reader(csvfile, delimiter=';')
#
#         new_rows = []
#         if 'head' in config:
#             new_rows += ['\textbf{{{}}}'.format(x) for x in config['head']]
#
#
#         next(csv_reader)  # ignore first line
#         for row in csv_reader:
#             if row[0] != '':
#                 print(list(enumerate(row)))
#                 row = [row[i] if row[i] not in replacements else replacements[row[i]] for i in [0, 1, 8, 9, 10, 11, 7, 13, 14, 16, 17, 19, 20]]
#                 row = ['{:.3f}'.format(float(r)) if re.match(num_regexp, r) else r for r in row]
#                 row = [r.replace('_', '\_') for r in row]
#                 new_rows += [row]
#
#     if 'number_cols' in config:
#         for col in config['number_cols']:
#             max, max_idx = -999999, 0
#             current_dataset = new_rows[0][6]
#             for i in range(len(new_rows)):
#                 if new_rows[i][6] != current_dataset:
#                     new_rows[max_idx][col] = '\\bfseries ' + new_rows[max_idx][col] # mark bold
#                     max, max_idx = -999, 0  # reset
#                     current_dataset = new_rows[i][6]
#                 if float(new_rows[i][col]) > max:
#                     max = float(new_rows[i][col])
#                     max_idx = i
#
#     with open(filename + '.tex', 'w') as writefile:
#         writefile.write(' \\\\ \n'.join([' & '.join(r) for r in new_rows]) + '\\\\')


### HELPER FUNCTIONS

def print_losses(table, size=5):

    cols = ['train_loss_history', 'val_loss_history']
    table_new = {c: [None for _ in table.index] for c in cols}
    for col in cols:
        for row in table.index:
            seq = table[col][row]

            new_entry = np.interp(np.linspace(0, len(seq), size), np.arange(len(seq)), seq)
            new_entry = ['{:.3f}'.format(x) for x in new_entry]
            table_new[col][row] = new_entry


def to_tex(s, col=None, percent_cols=None):

    if s != s:
        return '-'
    elif type(s) == str:
        return s.replace('_', '-')
    elif type(s) in {float, np.float64}:
        if col is not None and percent_cols is not None and col in percent_cols:
            return '{:.1f}'.format(s*100)
        elif col == 'lr':
            return '{:.4f}'.format(s)
        else:
            return '{:.3f}'.format(s)
    elif type(s) in {list, tuple, np.ndarray}:
        return ', '.join([to_tex(x, col=col) for x in s])
    elif type(s) == bool:
        return '\\checkmark' if s else '-'
    else:
        return str(s)


def hash_element(element):


    if element != element:  # i.e. NaN
        return '-'
    if type(element) == bool and element is True:
        return 'y'
    if type(element) == bool and element is False:
        return 'n'
    if element is None:
        return 'N'
    if type(element) == str and len(element) < 5:
        return element
    else:
        return base64.b64encode(hashlib.sha1(str(element).encode('utf8')).digest()).decode('utf8')[:3]