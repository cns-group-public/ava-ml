import os
import numpy as np
import time


LOGGER_WRITER_IDLE_TIME = 60
VERBOSITY = 2


def get_visdom():

    import visdom
    try:
        _vis = visdom.Visdom(raise_exceptions=True)

        if not _vis.check_connection():
            _vis = None   # get_fake_visdom()
            logger.important('failed to connect with visdom')
        else:
            logger.important('visdom available')

        _vis.is_working = True

    except ConnectionError:
        _vis = None  # get_fake_visdom()
        logger.important('failed to connect with visdom')

    return _vis


# TODO: unify Logger with visualizer
class Logger(object):

    def __init__(self):
        self.file_handle = None
        self.filename = None
        self.env_name = None
        self.vis = None
        self.write_interval = 5  # in seconds
        self.last_write = 0
        self.creation_time = time.time()
        self.write_idle_time = LOGGER_WRITER_IDLE_TIME  # in seconds
        self.write_idle_cache = ''
        self.write_verbosity_level = 2

    def init_visdom(self, env_name):
        self.env_name = env_name
        self.vis = get_visdom()

    def set_file(self, filename):
        # if the filename was changed
        if self.filename is not None and self.filename != filename:
            self.close_file()

        if not os.path.isdir(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))

        if os.path.isfile(filename):
            fn = '{}-{}'.format(os.path.basename(filename), time.strftime('%d.%b-%H:%M'))
            filename = os.path.join(os.path.dirname(filename), fn)

        self.filename = filename

    def write(self, text, verbosity):
        if self.filename is not None and verbosity < self.write_verbosity_level:

            if time.time() - self.creation_time < self.write_idle_time:
                self.write_idle_cache += text + '\n'
            else:
                if self.file_handle is None:
                    self.file_handle = open(self.filename, 'w')

                self.clear_cache()

                self.file_handle.write(text + '\n')

            # reopen file such that contents are actually written to disk
            if time.time() - self.last_write > self.write_interval and self.file_handle is not None:
                self.file_handle.flush()
                self.last_write = time.time()

    def clear_cache(self):
        if self.write_idle_cache != '':

            if self.file_handle is None:
                self.file_handle = open(self.filename, 'w')

            self.file_handle.write(self.write_idle_cache + '\n')

    def close_file(self):
        if self.file_handle is not None:
            self.file_handle.close()
        self.file_handle = None

    def important(self, *text, color='\x1b[0;39;49m', end='\n'):
        self._log(*text, color=color, end=end, verbosity=0)

    def info(self, *text, color='\x1b[0;36;49m', end='\n'):
        self._log(*text, color=color, end=end, verbosity=1)

    def detail(self, *text, color='\x1b[2;35;49m', end='\n'):
        self._log(*text, color=color, end=end, verbosity=2)

    def hint(self, *text, color='\x1b[1;33;49m', end='\n'):
        self._log(*text, color=color, end=end, verbosity=0)

    def warning(self, *text, color='\x1b[1;31;49m', end='\n'):
        self._log(*text, color=color, end=end, verbosity=0)

    def warning_if(self, condition, *text, **kwargs):
        if condition:
            return self.warning(*text, **kwargs)

    def _log(self, *texts, verbosity=0, color='', end='\n'):
        text = ' '.join([str(x) for x in texts])
        if VERBOSITY > verbosity:
            print(color + text + '\x1b[0m', end=end)

        self.write(text, verbosity)

    def plot_samples_predictions_visdom(self, model, sample, predictions, dataset, max_visualization_samples=3):

        if self.vis is not None:
            # quick hack to avoid running into problems if sample[0] is None
            if sample[0] is None:
                sample[0] = []
                predictions[0] = []

            assert predictions is None or len(sample[0]) == len(predictions[0])

            for k in range(min(len(sample[0]), max_visualization_samples)):

                from .visualize.selection import get_visualizers
                visualizers, info = get_visualizers([s[k] if s is not None else None for s in sample], dataset,
                                                    [p[k] for p in predictions] if predictions is not None else None, model)

                for visualizer, (name, i, mode, chunk_name) in zip(visualizers, info):
                    img, vis_type = visualizer.get_visdom_data()
                    win_name = '{} ({}, {})'.format(name, k, i)
                    try:
                        if img is not None:
                            getattr(self.vis, vis_type)(img, win=win_name, env=self.env_name, opts={'title': win_name})
                    except TypeError as e:
                        msg = ('Failed to plot img created by visualizer "{}" in chunk name "{}" with vis_type '
                               '"{}" in window "{}")\nError message:\n{}')
                        raise TypeError(msg.format(mode, chunk_name, vis_type, win_name, e))

    def plot_metrics_visdom(self, metric_names, metric_histories, metric_episodes=None):
        if self.vis is not None:
            for metric_name, metric_h in zip(metric_names, metric_histories):

                metric_h = np.array(metric_h)

                if len(metric_h.shape) > 1 and metric_h.shape[1] > 10:
                    self.warning('VISDOM: Metric {} contains too many channels: {}'.format(metric_name, metric_h.shape[1]))
                elif len(metric_h) > 1:
                    self.vis.line(metric_h, X=np.arange(0, len(metric_h)), win=metric_name, env=self.env_name, opts={'title': metric_name})
                else:
                    self.warning('VISDOM: Metric {} can not be plotted because the array is too small'.format(metric_name))

    def plot_gradients_visdom(self, nn_model):
        if self.vis is not None:
            # GRADIENT MAGNITUDE VISUALIZATION
            grad_magnitudes = []
            parameter_names = []
            for p_name, p in nn_model.named_parameters():
                if hasattr(p, 'grad') and p.grad is not None:
                    grad_magnitudes += [float(p.grad.norm())]
                    parameter_names += [p_name]
            # vis.bar(grad_magnitudes, env=env_name, win='gradient magnitudes', opts={'title': 'gradient magnitudes'})

            if len(grad_magnitudes) > 1:
                getattr(self.vis, 'bar')(grad_magnitudes, Y=parameter_names, env=self.env_name,
                                         win='gradient magnitudes', opts={'title': 'gradient magnitudes'})
            else:
                self.warning('VISDOM: Not enough gradients to plot')

    def plot_text_visdom(self, text, title=None):
        if self.vis is not None:
            self.vis.text(text, win='info',  env=self.env_name, opts={'title': title} if title is not None else {})


logger = Logger()




