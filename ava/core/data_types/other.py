from tkinter import Frame, Label

import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

from .base import DataTypeBase


class HeatMapData(DataTypeBase):

    def resize(self, target_size):
        pass

    def __init__(self, data_item, target_size):
        super().__init__(data_item, target_size)
        assert type(data_item) == np.ndarray

    def get_tcl_frame(self, parent, col=None):

        frame = Frame(master=parent)

        text = "dtype: " + str(self.data_item.dtype)
        text += "\nshape: " + str(self.data_item.shape)
        text += '\nmin/max: ' + str(self.data_item.min()) + '/' + str(self.data_item.max()) + '\n'
        frame.label = Label(frame, text=text, anchor="w")
        frame.label.pack()

        f = Figure()
        f.set_figwidth(2)
        f.set_figheight(2)
        a = f.add_subplot(111)
        a.set_axis_off()
        a.imshow(self.data_item, vmax=25, vmin=0)
        # f.tight_layout()
        frame.canvas = FigureCanvasTkAgg(f, frame)
        frame.canvas.show()
        frame.canvas.get_tk_widget().pack()

        return frame


class FailData(DataTypeBase):

    def __init__(self, data_item, target_size):
        super().__init__(data_item, target_size)

    def get_tcl_frame(self, parent, col=None):
        frame = Frame(master=parent)
        frame.label = Label(frame, text="Intepreting failed", anchor="w")
        # label.pack(side="top", fill="x")
        return frame

    def resize(self, target_size):
        pass


class TextData(DataTypeBase):

    def __init__(self, data_item, target_size):
        super().__init__(data_item, target_size)

    def get_tcl_frame(self, parent, col=None):
        label = Label(parent, text=str(self.data_item), anchor="w")
        # label.pack(side="top", fill="x")
        return label

    def resize(self, target_size):
        pass
