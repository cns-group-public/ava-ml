from tkinter import Frame, StringVar, Entry, Button, Widget


class SaveImageComponent(Frame):

    def __init__(self, master, visualizer, col=None, **kw):
        # save image callback

        super().__init__(master, **kw)
        self.master = master
        self.visualizer = visualizer

        self.filename_var = StringVar(value='sample_{}_'.format('col_'+str(col) if col is not None else 'label_over'))
        self.filename = Entry(master=self, textvariable=self.filename_var)
        self.filename.grid(column=1, row=3)
        self.save = Button(master=self, text='save image')
        self.save.grid(column=1, row=4)
        self.save.bind('<Button-1>', self.save_image)

    def save_image(self, x):
        import os
        import cv2
        self.visualizer.resize()
        i = 0
        base_filename = self.filename_var.get()
        while True:
            filename = base_filename + str(i).zfill(3) + '.jpg'
            if not os.path.isfile(filename) or i > 100: break
            i += 1

        print('save to', filename)
        cv2.imwrite(filename, cv2.cvtColor(self.visualizer.data_transformed, cv2.COLOR_RGB2BGR))