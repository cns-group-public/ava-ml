from .image import *
from .distribution import *
from .label_image import *
from .label_image_overlay import *
from .other import *
from .slices import *


vis = {
    'C': DistributionData
}


class CategoricalDataType(DataTypeBase):

    visualizer = DistributionData