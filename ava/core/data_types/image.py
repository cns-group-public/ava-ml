from tkinter import Frame, Canvas, W, E, NW, Label, Button

import numpy as np
from PIL import Image, ImageTk

from ...transformations import tensor_resize
from .base import DataTypeBase, array_info_text
from .components import SaveImageComponent


class ImageData(DataTypeBase):
    def __init__(self, data_item, target_size, color=True, channel_dim=0, bgr=False, colormap=None):
        # Frame.__init__(self, parent.mainframe)
        super().__init__(data_item, target_size)

        assert not (color and (colormap is not None)), 'A color image can not be color-mapped'

        self.color = color
        self.channel_dim = channel_dim
        self.bgr = bgr

        assert type(data_item) == np.ndarray

        self.data_item = data_item
        self.resize(target_size)

    def resize(self, target_size=None):

        data_item_view = self.data_item.copy()
        # data_item_view[0, 0] = 0

        assert data_item_view.ndim in {3, 2}, 'Image must have 3 (color) or 2 (grayscale) dimensions.'

        if self.color:
            if self.channel_dim == 0:
                data_item_view = data_item_view.transpose([1, 2, 0])
            elif self.channel_dim == 1:
                data_item_view = data_item_view.transpose([0, 2, 1])

        # correct for negative values
        if -1 <= data_item_view.min() < 0 <= data_item_view.max() <= 1:
            data_item_view += 1
            data_item_view /= 2
        elif -100 <= data_item_view.min() < 0 <= data_item_view.max() <= 100:
            print('jo')
            data_item_view += data_item_view.min()
            data_item_view /= data_item_view.max()
        elif -255 <= data_item_view.min() < 0 <= data_item_view.max() <= 255:
            data_item_view += 255
            data_item_view /= 2

        if self.bgr:
            data_item_view = data_item_view[:, :, [2, 1, 0]]

        if target_size is not None:
            data_item_view = tensor_resize(data_item_view, target_size, interpret_as_max_bound=True)
        self.data_transformed = data_item_view
        # return data_item_view

    def get_visdom_data(self):
        data_item_view = self.data_transformed.copy()

        if data_item_view.max() <= 1 and data_item_view.dtype.name.startswith('float'):
            data_item_view = np.uint8(data_item_view*255)
        else:
            data_item_view = np.uint8(data_item_view)

        if data_item_view.ndim == 3:
            data_item_view = data_item_view.transpose([2, 0, 1])

        return data_item_view, 'image'

    def get_tcl_frame(self, parent, col=None):

        data_item_view = self.data_transformed.copy()

        if data_item_view.max() <= 1 and data_item_view.dtype.name.startswith('float'):
            data_item_view = np.uint8(data_item_view*255)
        else:
            data_item_view = np.uint8(data_item_view)

        frame = Frame(master=parent)
        frame.canvas = Canvas(master=frame, width=data_item_view.shape[1],
                              height=data_item_view.shape[0])
        frame.canvas.grid(column=1, row=1, sticky=(W, E))

        try:
            im = Image.fromarray(data_item_view)
        except TypeError:
            raise TypeError('Array (size: {}) to PIL image conversion failed. Note that images are expected to '
                            'be channel-first.'.format(data_item_view.shape))
        frame.photo = ImageTk.PhotoImage(image=im)
        frame.canvas.create_image(0, 0, image=frame.photo, anchor=NW)

        info = array_info_text(self.data_item)

        frame.info = Label(master=frame, text=info)
        frame.info.grid(column=1, row=2)

        frame.save_box = SaveImageComponent(frame, self, col)
        frame.save_box.grid(column=1, row=3)

        return frame