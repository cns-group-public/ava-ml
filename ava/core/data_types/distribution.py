from tkinter import Frame, Label

import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

from .base import DataTypeBase


class DistributionData(DataTypeBase):

    def resize(self, target_size):
        pass

    def __init__(self, data_item, target_size):
        super().__init__(data_item, target_size)
        text = "\ndtype: " + str(data_item.dtype)
        text += "\nsize: " + str(len(data_item))
        text += "\nsum: " + str(data_item.sum())
        text += "\nargmax: " + str(data_item.argmax())
        text += "\nnan/inf: " + str(np.any(np.isnan(data_item)) or np.any(np.isinf(data_item)))
        self.text = text
        assert type(data_item) == np.ndarray

    def get_visdom_data(self):
        return self.data_item, 'bar'

    def get_tcl_frame(self, parent, col=None):

        frame = Frame(master=parent)

        f = Figure()
        f.set_figwidth(2)
        f.set_figheight(2)
        a = f.add_subplot(111)
        a.set_ylim([0, max(1, self.data_item.max())])
        a.bar(np.arange(0, len(self.data_item)), self.data_item, 0.9)
        f.tight_layout()
        frame.canvas = FigureCanvasTkAgg(f, frame)
        frame.canvas.show()
        frame.canvas.get_tk_widget().pack()

        frame.info = Label(frame, text=self.text)
        frame.info.pack()

        return frame


# TODO: merge with DistributionData
class DistributionInfoData(DataTypeBase):

    def __init__(self, data_item, target_size):
        super().__init__(data_item, target_size)
        text = "Probability Distribution\nToo large to visualize"
        text += "\ndtype: " + str(data_item.dtype)
        text += "\nsize: " + str(data_item.shape if hasattr(data_item, 'shape') else len(data_item))
        text += "\nsum: " + str(data_item.sum())
        text += "\nargmax: " + str(data_item.argmax())
        text += "\nnan/inf: " + str(np.any(np.isnan(data_item)) or np.any(np.isinf(data_item)))
        text += "\nentropy: "
        self.text = text

    def get_visdom_data(self):
        return self.text, 'text'

    def get_tcl_frame(self, parent, col=None):
        label = Label(parent, text=self.text, anchor="w")
        # label.pack(side="top", fill="x")
        return label

    def resize(self, target_size):
        pass