from copy import deepcopy
from tkinter import Frame, Canvas, W, E, NW, Label

from PIL import Image, ImageTk

from ..common.assertions import assert_object_type
from ...transformations import tensor_resize
from .base import DataTypeBase
from .components import SaveImageComponent
from ..plots import color_overlay


class LabelImageColorOverlayData(DataTypeBase):

    # def __init__(self, parent, data_item, channel_dim=0, one_hot=False):
    def __init__(self, data_item, target_size, channel_dim=0, one_hot=False, indices=None, normalize=False,
                 intensities=None, start_color=0, n_colors=None, h_shift=0):
        # Frame.__init__(self, parent.mainframe)

        assert (indices is None or intensities is None) or len(intensities) == len(indices)

        self.h_shift = h_shift
        self.n_colors = n_colors
        self.start_color = start_color
        self.intensities = intensities
        self.normalize = normalize
        self.indices = indices
        self.channel_dim = channel_dim
        self.one_hot = one_hot
        super().__init__(data_item, target_size)

        assert type(data_item) == tuple
        self.data_item = data_item
        self.data_transformed = None
        self.target_size = target_size

        self.resize(target_size)

    def resize(self, target_size=None):
        img, maps = deepcopy(self.data_item)

        if self.one_hot:
            assert maps.ndim == 2
            assert_object_type(maps.dtype, {'int16', 'int32', 'int64', 'uint8'})

        if img.dtype == 'float32':
            img /= 255.0

        if target_size is not None:
            img = tensor_resize(img, target_size, channel_dim=self.channel_dim, interpret_as_max_bound=True,
                                keep_channels_last=True)
            maps = tensor_resize(maps, target_size, channel_dim=self.channel_dim, interpret_as_max_bound=True,
                                 keep_channels_last=True)
        else:
            img = img.transpose([1, 2, 0])
            maps = maps.transpose([1, 2, 0])


        if self.normalize:
            maps -= maps.min()
            maps /= maps.max()

        if self.indices is None:
            indices = list(range(maps.max() if self.one_hot else maps.shape[2]))
        else:
            indices = self.indices

        out = color_overlay(img, maps, self.h_shift, indices,
                            self.one_hot, self.n_colors, self.start_color, self.intensities)

        # n_colors = len(indices) if self.n_colors is None else self.n_colors
        # color_palette = hsv2rgb(np.dstack([
        #     np.arange(self.h_shift, 1, 1 / n_colors),
        #     0.7 * np.ones(n_colors),
        #     0.3 * np.ones(n_colors)
        # ]))[0]
        #
        # out = gray2rgb(rgb2gray(img))  # desaturate
        # out *= 0.5
        #
        # for i, idx in enumerate(indices):
        #     m = maps == idx if self.one_hot else maps[:, :, idx]
        #     # m = np.clip(m, 0, 1)
        #     col = color_palette[self.start_color + i]
        #
        #     if self.intensities is not None:
        #         col = col * self.intensities[i]
        #
        #     out = np.clip(out + col * m[:, :, None], 0, 1)
        #
        # out = (255 * out).astype('uint8')

        self.data_transformed = out
        # return out

    def get_visdom_data(self):
        data_item_view = self.data_transformed.copy()
        data_item_view = data_item_view.transpose([2, 0, 1])
        return data_item_view, 'image'

    def get_tcl_frame(self, parent, col=None):
        im = Image.fromarray(self.data_transformed)

        frame = Frame(master=parent)
        frame.canvas = Canvas(master=frame, width=self.data_transformed.shape[1], height=self.data_transformed.shape[0])
        frame.canvas.grid(column=1, row=1, sticky=(W, E))

        frame.photo = ImageTk.PhotoImage(image=im)
        frame.img = frame.canvas.create_image(0, 0, image=frame.photo, anchor=NW)

        info = 'shapes: ' + str(self.data_item[0].shape) + ' ' + str(self.data_item[1].shape) + '\n'
        info += 'dtypes: ' + str(self.data_item[0].dtype) + ' ' + str(self.data_item[1].dtype) + '\n'
        info += 'min/max: {:.4f}/{:.4f}\n'.format(self.data_item[0].min(), self.data_item[0].max())
        info += 'min/max: {:.4f}/{:.4f}\n'.format(self.data_item[1].min(), self.data_item[1].max())
        frame.info = Label(master=frame, text=info)
        frame.info.grid(column=1, row=5)

        frame.save_box = SaveImageComponent(frame, self, col)
        frame.save_box.grid(column=1, row=3)

        return frame