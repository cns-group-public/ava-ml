from tkinter import Frame, StringVar, Label, Button, NW, Canvas, W, E

import cv2
import numpy as np
from PIL import Image, ImageTk

from ..logging import logger
from ...transformations import tensor_resize
from .base import DataTypeBase


class SlicesData(DataTypeBase):
    """
    `maps_dim` indicates the dimension along which the maps are stored.
    `maps_dim` and `channel_dim` ignore the batch dimension
    """
    def __init__(self, data_item, target_size, maps_dim, slice_labels=None, # maps_dim=None,
                 channel_dim=None, color=False, normalize=False):
        super().__init__(data_item, target_size)

        self.normalize = normalize
        assert maps_dim is not None, 'maps_dim can not be None'
        assert channel_dim is not None and color or channel_dim is None

        self.cursor = 0
        self.maps_dim = maps_dim
        self.slice_labels = slice_labels
        if slice_labels is not None:
            logger.detail('Using slice', len(slice_labels), 'labels:', slice_labels)
        self.frame = None
        self.channel_dim = channel_dim

        assert (color and data_item.ndim == 4) or (not color and data_item.ndim == 3)

        self.target_size = target_size
        self.resize(self.target_size)
        # self.draw_slice()

    def init_frame(self, parent):
        self.frame = Frame(master=parent)
        self.frame.canvas = None
        self.slice_label_text = StringVar(value='No slice_labels provided')
        self.slice_label = Label(self.frame, textvariable=self.slice_label_text)
        self.slice_label.grid(column=1, row=2)

        self.button_up = Button(self.frame, text="Up")
        self.button_up.grid(column=1, row=3)
        self.button_up.bind("<Button-1>", self.next_slice)

        self.button_down = Button(self.frame, text="Down")
        self.button_down.grid(column=1, row=4)
        self.button_down.bind("<Button-1>", self.prev_slice)

        self.info_text = StringVar()
        self.info = Label(self.frame, textvariable=self.info_text)
        self.info.grid(column=1, row=5)

    def next_slice(self, e):
        self.cursor = self.cursor + 1 if self.cursor < self.data_item.shape[self.maps_dim] - 1 else 0
        # self.draw_slice()
        self.update_canvas()

    def prev_slice(self, e):
        self.cursor = self.cursor - 1 if self.cursor > 0 else self.data_item.shape[self.maps_dim] - 1
        # self.draw_slice()
        self.update_canvas()

    def get_current_slice(self):

        # self.current_slice = np.take(self.transformed_data, self.cursor, axis=self.maps_dim)

        # maps are always in first dimension in transformed data
        self.current_slice = self.transformed_data[self.cursor]

        self.info_text.set('original shape: ' + str(self.data_item.shape) + '\n')
        self.info_text.set(self.info_text.get() + 'map#: ' + str(self.cursor) + '\n')

        if self.current_slice.dtype == 'bool':
            self.info_text.set(self.info_text.get() + 'bool, coverage:' + str(np.round(self.current_slice.sum() / (self.current_slice.shape[0]*self.current_slice.shape[1]), 2)))
            self.info_text.set(self.info_text.get() + '\n shape:' + str(self.current_slice.shape))
        else:
            self.info_text.set(self.info_text.get() + 'min: ' + str(np.round(self.current_slice.min(), 2)) + '\n max: ' + str(np.round(self.current_slice.max(), 2)) + '\n dtype: ' + str(self.current_slice.dtype))
            self.info_text.set(self.info_text.get() + '\n shape:' + str(self.current_slice.shape))
            self.info_text.set(self.info_text.get() + "\nnan/inf: " + str(np.any(np.isnan(self.current_slice)) or np.any(np.isinf(self.current_slice))))
            self.info_text.set(self.info_text.get() + '\n h1:' + str(np.histogram(self.current_slice, [0, 0.00001, 0.5, 0.99999, 1])[0]))
            self.info_text.set(self.info_text.get() + '\n h255:' + str(np.histogram(self.current_slice, [0, 1, 128, 254, 255])[0]))

        if self.slice_labels is not None and self.data_item.shape[self.maps_dim] == len(self.slice_labels):

            self.slice_label_text.set(self.slice_labels[self.cursor])

        if self.current_slice.min() >= 0 and self.current_slice.max() <= 1 and self.current_slice.dtype.name.startswith('float'):
            self.current_slice = np.uint8(self.current_slice*255)
        elif self.current_slice.dtype == 'bool':
            self.current_slice = np.uint8(self.current_slice * 255)
        elif self.current_slice.min() >= 0:
            self.current_slice = np.uint8(self.current_slice)
        elif self.current_slice.min() < 0:
            self.current_slice = ((-self.current_slice.min() + self.current_slice) / (-self.current_slice.min() + self.current_slice.max()))*255
        else:
            ValueError('data has unsupported value range')

        # self.update_canvas()
        return self.current_slice

    # TODO: fuse this with the ImageVisualizer
    def resize(self, target_size):
        """ Resizes to `target size` but also sets channels last and maps first. """
        # treat the images as channels

        transformed_data = []
        for i in range(self.data_item.shape[self.maps_dim]):
            slice_map = np.take(self.data_item, i, self.maps_dim)
            # ipdb.set_trace()
            if self.channel_dim is not None:
                corrected_channel_dim = self.channel_dim - (1 if self.channel_dim > self.maps_dim else 0)
            else:
                corrected_channel_dim = None

            resized_map = tensor_resize(slice_map, target_size, interpret_as_max_bound=True,
                                        channel_dim=corrected_channel_dim, keep_channels_last=True)
            transformed_data += [resized_map]

        self.transformed_data = np.array(transformed_data)

        if self.normalize:
            self.transformed_data -= self.transformed_data.min()
            self.transformed_data /= self.transformed_data.max()

        if self.transformed_data.ndim == 4 and self.transformed_data.shape[3] == 2:  # e.g. for optical flow visualization
            hsv = np.zeros(self.transformed_data.shape[:3] + (3,), 'uint8')
            mag, ang = cv2.cartToPolar(self.transformed_data[..., 0], self.transformed_data[..., 1])
            hsv[..., 0] = ang * 180 / np.pi / 2
            hsv[..., 1] = 255
            hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)

            self.transformed_data = np.zeros(self.transformed_data.shape[:3] + (3,))
            for i in range(len(self.transformed_data)):
                self.transformed_data[i] = cv2.cvtColor(hsv[i], cv2.COLOR_HSV2RGB)

        self.update_canvas()

    def update_canvas(self):

        if self.frame is not None:
            current_slice = self.get_current_slice()
            # logger.info(describe(current_slice))
            im = Image.fromarray(current_slice.astype('uint8'))
            self.photo = ImageTk.PhotoImage(image=im)
            self.im_ref = self.frame.canvas.create_image(0, 0, image=self.photo, anchor=NW)
            self.frame.canvas.update()

    def get_visdom_data(self):

        d0 = np.ceil(np.sqrt(self.transformed_data.shape[0]))
        d1 = (d0-1) if d0 * (d0-1) >= self.transformed_data.shape[0] else d0

        slice_max_s = min(self.target_size[0] // d0, self.target_size[1] // d1)
        slice_max_s = (int(slice_max_s), int(slice_max_s))

        transformed_data_small = [tensor_resize(self.transformed_data[s], slice_max_s, interpret_as_max_bound=True)
                                  for s in range(self.transformed_data.shape[0])]

        slice_s = transformed_data_small[0].shape

        grid_image_size = (int(d0 * slice_s[0]), int(d1 * slice_s[1]))
        if len(self.transformed_data.shape) == 4:
            grid_image_size += (self.transformed_data.shape[3],)

        grid_image = np.zeros(grid_image_size)
        for i in range(len(transformed_data_small)):
            iy, ix = int(i // d1), int(i % d1)
            grid_image[iy*slice_s[0]: (iy+1)*slice_s[0], ix*slice_s[1]: (ix+1)*slice_s[1]] = transformed_data_small[i]

        if len(grid_image.shape) == 3:
            grid_image = grid_image.transpose([2, 0, 1])

        if grid_image.max() <= 1:
            grid_image *= 255

        return grid_image, 'image'

    def get_tcl_frame(self, parent, col=None):
        if self.frame is None:

            self.init_frame(parent)
            self.frame.canvas = Canvas(self.frame,
                                       width=self.transformed_data.shape[2],
                                       height=self.transformed_data.shape[1])
            self.frame.canvas.grid(column=1, row=1, sticky=(W, E))

        self.update_canvas()
        return self.frame
