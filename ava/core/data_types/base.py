import numpy as np


class DataTypeBase(object):

    def __init__(self, data_item, target_size):
        self.data_transformed = None
        self.data_item = data_item
        self.target_size = target_size

    def resize(self, target_size):
        raise NotImplementedError

    def get_visdom_data(self):
        return None, None

    def get_tcl_frame(self, parent, col=None):
        raise NotImplementedError

    # def save_image(self, path):
    #     raise NotImplementedError


def array_info_text(data_item):
    info = 'shape: {}\ndtype: {}\nmin/max: {} {}'
    info = info.format(data_item.shape, data_item.dtype, np.round(data_item.min(), 5), np.round(data_item.max(), 5))
    return info