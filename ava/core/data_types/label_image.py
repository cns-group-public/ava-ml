from tkinter import Frame, Canvas, W, E, NW, Label

import numpy as np
from PIL import Image, ImageTk
from skimage.color import lab2rgb

from ...transformations import tensor_resize
from .base import DataTypeBase, array_info_text


class LabelImageData(DataTypeBase):

    def __init__(self, data_item, target_size, maps_dim=None, n_colors=None):
        super().__init__(data_item, target_size)
        self.maps_dim = maps_dim
        self.n_colors = n_colors
        # self.one_hot = one_hot

        self.data_item = data_item
        self.transformed_data = None
        self.target_size = target_size
        self.data_item_view = None

        self.resize(target_size)

    def resize(self, target_size):
        data_item = self.data_item.copy()

        assert type(self.data_item) == np.ndarray

        if data_item.ndim == 3:
            data_item = np.argmax(data_item, axis=0)

        if self.maps_dim is None:
            assert len(data_item.shape) == 2
        else:
            assert len(data_item.shape) == 3 and 0 <= self.maps_dim <= 2

        data_item = data_item.astype('int16')
        self.data_item_view = tensor_resize(data_item, target_size, interpret_as_max_bound=True,
                                            channel_dim=self.maps_dim, keep_channels_last=True,
                                            interpolation='nearest')

        if self.maps_dim is not None:
            labels = self.data_item_view.argmax(2)
        else:
            labels = self.data_item_view

        n_colors = self.n_colors if self.n_colors is not None else max(20, labels.max() + 1)
        colors = np.uint8(lab2rgb(np.dstack([
            35 + np.sin(np.arange(0, 1, 1 / n_colors) * 2 * np.pi) * 20,
            np.sin(np.arange(-1, 1, 2 / n_colors) * 1.5 * np.pi) * 50,
            np.sin(np.arange(-1, 1, 2 / n_colors) * 3 * np.pi) * 50
        ])) * 255)[0]

        # gray for index 0
        colors = np.concatenate((np.array([[190, 190, 190]], 'uint8'), colors), 0)
        self.transformed_data = colors[labels.ravel()].reshape(labels.shape + (3,))
        # return self.label_image

    def get_visdom_data(self):
        return self.transformed_data.transpose([2, 0, 1]), 'image'

    def get_tcl_frame(self, parent, col=None):

        frame = Frame(master=parent)
        frame.canvas = Canvas(frame, width=self.transformed_data.shape[1], height=self.transformed_data.shape[0])
        frame.canvas.grid(column=1, row=1, sticky=(W, E))

        im = Image.fromarray(self.transformed_data)
        frame.photo = ImageTk.PhotoImage(image=im)
        frame.canvas.create_image(0, 0, image=frame.photo, anchor=NW)

        info = array_info_text(self.data_item)

        frame.info = Label(frame, text=info)
        frame.info.grid(column=1, row=2)

        self.current_pos = ''

        # add click callback
        def callback(e):
            val = str(self.data_item_view[e.y, e.x])
            val = val[:30] + ('...' if len(val) > 30 else '')
            self.current_pos = 'value at {}/{}:\n{}'.format(e.y, e.x, val)
            frame.info['text'] = info + '\n' + self.current_pos
            frame.info.update()

        frame.canvas.bind("<Button-1>", callback)

        return frame