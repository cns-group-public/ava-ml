Components of ava can be called within python scripts.

To load a pre-trained model:

```python
from ava.models import load_pretrained_model
model = load_pretrained_model('model_name')
```

It is also straightforward to use transformations:
```python
from ava.transformations import tensor_resize, random_crop
img = tensor_resize(img, (100, 100), interpret_as_min_bound=True)
img = random_crop(img, (100, 100))
```