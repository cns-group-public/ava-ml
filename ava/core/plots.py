import matplotlib.pyplot as plt
import re
import numpy as np
from skimage.color import hsv2rgb, gray2rgb, rgb2gray
from os.path import join
from collections import defaultdict


from ..core.logging import logger
from ..transformations import tensor_resize


def color_overlay(img, maps, h_shift, indices, one_hot, n_colors=None, start_color=0, intensities=None, scale_to_image=False):
    n_colors = len(indices) if n_colors is None else n_colors

    color_palette = hsv2rgb(np.dstack([
        np.arange(h_shift, 1, 1 / n_colors),
        0.7 * np.ones(n_colors),
        0.3 * np.ones(n_colors)
    ]))[0]

    out = gray2rgb(rgb2gray(img))  # desaturate
    out *= 0.5

    for i, idx in enumerate(indices):
        m = maps == idx if one_hot else maps[:, :, idx]

        if scale_to_image:
            m = tensor_resize(m, img.shape[:2])

        # m = np.clip(m, 0, 1)
        col = color_palette[start_color + i]

        if intensities is not None:
            col = col * intensities[i]

        out = np.clip(out + col * m[:, :, None], 0, 1)

    out = (255 * out).astype('uint8')
    return out


def plot_experiment(table, experiments, dirname_out, filename_pre):
    from matplotlib import pyplot as plt
    import seaborn as sns
    sns.set_context('paper')
    sns.set_style('whitegrid')

    for name, (metric, key_diff, key_plot, extra) in experiments['plot_outputs'].items():

        logger.info('generating plot output {}...'.format(name))

        extra = defaultdict(lambda: None, extra)

        if extra['figsize'] is not None:
            plt.figure(figsize=extra['figsize'])

        if extra['indices'] is not None:
            try:
                a, b = re.match(r'([0-9]+)-([0-9]+)', extra['indices']).groups()
                sel_indices = list(range(int(a), int(b)))
            except TypeError:
                sel_indices = extra['indices']

            table_out = table.loc[sel_indices]
        else:
            table_out = table

        if not key_diff in table_out:
            raise KeyError(f'The column {key_diff} is not in the table (maybe same for all experiments?)')

        all_diff = sorted(list(set(table_out[key_diff])), key=lambda x: str(x))
        all_x = sorted(list(set(table_out[key_plot])))

        tab = {(dif, k): m for m, dif, k in table_out[[metric, key_diff, key_plot]].values}
        tab = defaultdict(lambda: 0, tab)

        lines = [[tab[(d, x)] for x in all_x] for d in all_diff]

        # replace nan in all_x
        if extra['undefined'] is not None:
            all_x = [extra['undefined'] if np.isnan(x) else x for x in all_x]

        handles = []
        for l in lines:
            handle = plt.plot(all_x, l, marker='o')[0]
            handles += [handle]

        if extra['logx']: plt.gca().set_xscale('log')
        if extra['logy']: plt.gca().set_yscale('log')
        if extra['ylim']: plt.ylim(extra['ylim'])
        if extra['xlim']: plt.xlim(extra['xlim'])
        plt.xlabel(key_plot)

        if extra['hide-y-labels']:
            plt.gca().axes.get_yaxis().set_ticklabels([])
        else:
            plt.ylabel(metric)

        if extra['hide-y-axis']: plt.gca().axes.get_yaxis().set_visible(False)

        if extra['hide-x-axis']: plt.gca().axes.get_xaxis().set_visible(False)

        if extra['title']: plt.title(extra['title'])

        if extra['xticks'] == 'x':
            plt.xticks(all_x)
        elif type(extra['xticks']) == list:
            plt.xticks(extra['xticks'])

        if 'external-legend' not in extra or not extra['external-legend']:
            plt.legend(handles, all_diff, title=key_diff)

        plt.savefig(join(dirname_out, '{}-{}.pdf'.format(filename_pre, name)), bbox_inches="tight")

        if 'external-legend' in extra or extra['external-legend']:
            plt.close()
            plt.clf()
            plt.cla()
            plt.axis('off')
            plt.gca().set_axis_off()
            plt.legend(handles, all_diff, title=key_diff, loc='upper left', frameon=False)
            plt.savefig(join(dirname_out, '{}-{}-legend.pdf'.format(filename_pre, name)), bbox_inches="tight")


def plot_curves(details, show=False, filename=None):
    from matplotlib import pyplot as plt

    handles, names = [], []
    for k in details:
        handle, = plt.plot(details[k]['val_loss_history'][0])
        handles += [handle]
        names += [str(k) + ': ' + details[k]['model'][0]]

    if filename:
        plt.savefig(filename)

    if show:
        plt.legend(handles, names)
        plt.show()


def ax_image(img, ax):

    bx = ax.imshow(img)
    bx.axes.get_yaxis().set_visible(False)
    bx.axes.get_xaxis().set_visible(False)

    return ax


def ax_distribution(p, labels, ax, palette=None, fontsize=20):
    ax = ax if ax is not None else plt

    if palette is None:
        import seaborn as sns
        palette = sns.color_palette('pastel')

    ax.hlines(0, -1, len(labels), colors='lightgrey')
    ax.hlines(0.5, -1, len(labels), colors='lightgrey')
    ax.hlines(1, -1, len(labels), colors='lightgrey')

    for i, l in enumerate(labels):
        ax.text(i, 0.02 + 0.02 * np.max(p), l, rotation='vertical', va='bottom', ha='center', weight='bold', fontsize=fontsize, color='#333333')

    ax.bar(range(len(p)), height=p, color='grey')
    # ax.axes.get_yaxis().set_visible(False)
    ax.axes.get_xaxis().set_visible(False)

    # ax.axes.set_ylim(0,0.4)
    plt.gca().set_ylim(0, 0.4)
    # plt.yticks([0,0.4,0.8])
    # plt.ylim(0, 0.8)
    # ax.frameon(True)
    # plt.xticks(range(5), labels,  rotation='vertical', ha='left',)
    # bx.xticks([])

    return ax


def plot_image_over_distribution(img, p, labels, fontsize=10, filename=None, figsize=(3, 5)):
    plt.figure(figsize=figsize)

    ax1 = plt.subplot2grid((7, 5), (0, 0), colspan=5, rowspan=5, frameon=False)
    ax_image(img, ax1)

    ax2 = plt.subplot2grid((7, 5), (5, 0), colspan=5, frameon=False)
    ax_distribution(p, labels, ax2, fontsize=fontsize)

    # asdasdasda
    plt.ylim(0, 1)
    # plt.subplots_adjust(left=.02, right=.98, top=.98, bottom=.02)

    if filename is not None:
        plt.savefig(filename, bbox_inches="tight")


def plot_counts(df, filename=None, figsize=(6, 5), log_xaxis=False, colors=None):
    """ Take DataFrame `df` of counts and visualize in a bar plot. """

    palette = plt.get_cmap('Set1')

    plt.figure(figsize=figsize)
    plt.vlines(x=0, ymin=0, ymax=10)
    for i, a in enumerate(df.columns):
        plt.subplot(len(df.columns), 1, i + 1, frameon=False)
        plt.title(a, position=(-0.02, 0.3), horizontalalignment='right')

        plt.xscale("log", nonposx='clip')
        plt.xlim(0, int(1.05 * df.values.max()))
        if i != len(df.columns) - 1:
            plt.xticks([], [])
        plt.yticks([], [])  # plt.yticks(np.linspace(0,1,3), df.index)

        s = len(df[a])
        ax = plt.barh(y=np.linspace(0, 1, s), height=1 / (s - 1), width=df[a],
                      color=[colors[j] if colors else palette(j) for j in range(3)])

    plt.legend(ax, df.index, ncol=3, bbox_to_anchor=(1, -1.2), fontsize=12)

    if filename is not None:
        plt.savefig(filename, bbox_inches="tight")