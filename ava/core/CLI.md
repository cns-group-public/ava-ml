The command line interface of ava offers several options:

- `ava train <model> <dataset> <options>`
- `ava score <model> <dataset> <options>`
- `ava inspect <dataset>` check the dataset implemention
- `ava benchmark <dataset>` assess the speed and potential bottlenecks in datasets
- `ava experiment <experiment-definition>` run experiment

### Customized Datasets

You can define customized datasets outside the ava source code tree by defining 
datasets or models in python modules ending with `_dataset.py` or `_model.py` in your 
project folder. When these are available the defined datasets and models are 
available to be used in the CLI or experiment files.

See the `examples/sample_project` folder for an example.

