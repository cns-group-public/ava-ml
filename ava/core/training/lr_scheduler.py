import numpy as np
from torch.optim.lr_scheduler import _LRScheduler


class CyclicalLR(_LRScheduler):
    def __init__(self, optimizer, step_size, start_lr, end_lr=None, type='triangle', last_epoch=-1):
        self.end_lr = end_lr if end_lr is not None else start_lr * 5
        # self.start_lr = start_lr
        self.step_size = step_size

        st = step_size
        if type == 'triangle':
            self.get_x = lambda i: (i % (2*st) if i % (2*st) < st else -i % (2*st)) / st
        elif type == 'sine':
            self.get_x = lambda i: (np.sin(1/st * np.pi * i))
        else:
            raise ValueError('Unknown cyclical lr type: {}'.format(type))
        super().__init__(optimizer, last_epoch)

    def step(self, epoch=None):
        pass

    def iteration_step(self, i_episode):
        x = self.get_x(i_episode)
        learning_rates = [base_lr * max(0, 1 - x) + self.end_lr * min(1, x) for base_lr in self.base_lrs]
        # learning_rates = [base_lr + (self.max_lr - base_lr) * max(0, 1 - x) for base_lr in self.base_lrs]
        for param_group, lr in zip(self.optimizer.param_groups, learning_rates):
            param_group['lr'] = lr

