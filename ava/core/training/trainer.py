import os
import time
import signal

from collections import defaultdict
from os.path import isfile, join, isdir, dirname

import ipdb

from ..logging import logger

import numpy as np

SCORE_CHAR_LIMIT = 100
PLOT_START_TIME = 30

DEFAULT_CONFIG = dict()


def train_high_level(model_name, dataset_name,
                     # additional_args=None,

                     optimizer='adam',
                     lr=0.001,
                     weight_decay=0.0,
                     batch_size=16,
                     grad_clip=None,

                     max_epochs=None,
                     max_episodes=None,
                     early_stop_epochs=None,
                     plot_episode_interval=250,
                     early_plot_episode_interval=5,
                     scores_per_epoch=1,

                     metrics=None,

                     subset='train',
                     subset_val='val',
                     max_samples_train=None,
                     max_samples_val=None,
                     checkpoint_folder=None,
                     checkpoint_filename=None,
                     overwrite_checkpoint=False,
                     multi_gpu=False,
                     prefer_dataset_args=False,
                     load_state_dict_non_strict=False,
                     keyboard_interrupt_exit=False,

                     threads_train=4,
                     threads_score=4,

                     no_checkpoints=False,
                     testrun=False,
                     **kwargs):
    """ Only for the CLI """

    from ..dataset import get_dataset
    from ..model import load_pretrained_model, initialize_model
    from ... import PATHS

    # SPECIAL MODES
    if testrun:  # use a minimal configuration to check if everything works
        plot_episode_interval, score_episode_interval, scores_per_epoch = 1, 2, 1
        max_samples_val, max_samples_train = batch_size, batch_size
        max_episodes = 2
        checkpoint_filename = None
        no_checkpoints = True

    (d_train, d_val), dataset_args, other_args = get_dataset(dataset_name, kwargs,
                                                             subsets=(subset, subset_val),
                                                             samples=(max_samples_train, max_samples_val))

    logger.info('\nTRAIN: Initialized dataset {} with arguments:\n   subset: {}\n   {}'.format(
        d_train.__class__.__name__, subset, '\n   '.join('{}: {}'.format(k, v) for k, v in dataset_args.items())))

    # INITIALIZE MODEL
    model = None

    if checkpoint_folder is None:
        potential_model_folders = [join(PATHS['TRAINED_MODELS_PATH']),
                                   join(os.getcwd())]
        potential_model_folders += [f for f in os.listdir(join(PATHS['TRAINED_MODELS_PATH'])) if isdir(f)]
    else:
        potential_model_folders = [PATHS['TRAINED_MODELS_PATH'], os.getcwd(),
                                   join(PATHS['TRAINED_MODELS_PATH'], checkpoint_folder)]

    force_load_model = type(model_name) == dict and 'load' in model_name
    model_name = model_name if type(model_name) == str else model_name['load']

    filename = None
    for folder in potential_model_folders:

        if isfile(join(folder, model_name)):
            filename = join(folder, model_name)
        elif force_load_model and isdir(folder):
            all_files = [f for f in os.listdir(folder) if f.startswith(model_name) and f.endswith('-args')]
            assert len(all_files) <= 1, 'Too many pretrained models were found for {}'.format(model_name)
            filename = join(folder, all_files[0][:-5]) if len(all_files) > 0 else None

    if filename is not None:
        model = load_pretrained_model(filename, other_args, d_train.model_config,
                                      multi_gpu=multi_gpu,
                                      prefer_dataset_args=prefer_dataset_args,
                                      load_state_dict_non_strict=load_state_dict_non_strict, cuda=True)
        pretrained_model_name = model_name
    elif force_load_model:
        raise FileNotFoundError(f'Could not load pretrained model {model_name} in {potential_model_folders}')

    if model is None:
        logger.info('TRAIN: Could not find a pretrained model "{}" in {}'.format(model_name, potential_model_folders))
        model = initialize_model(model_name, other_args, d_train.model_config, multi_gpu=multi_gpu, cuda=True)
        training_id = '{}-{}'.format(model.__class__.__name__, d_train.name())
    else:
        training_id = '{}++{}-{}'.format(pretrained_model_name, model.__class__.__name__, d_train.name())

    if checkpoint_filename is None:
        checkpoint_filename = join(PATHS['TRAINED_MODELS_PATH'],
                                   checkpoint_folder if checkpoint_folder is not None else '', training_id)

    if os.path.isfile(checkpoint_filename) and not overwrite_checkpoint:
        checkpoint_filename = '{}-{}'.format(checkpoint_filename, time.strftime('%d.%b-%H:%M'))

    if checkpoint_folder is not None and not os.path.isdir(os.path.dirname(checkpoint_folder)):
        os.makedirs(os.path.dirname(checkpoint_folder))

    model.training_details['dataset_name'] += [d_train.name()]
    model.training_details['dataset_arguments'] += [dataset_args]
    model.train(True)

    # assert '(' not in model.name() and ')' not in model.name()
    # assert '(' not in d_train.name() and ')' not in d_train.name()

    if hasattr(model, 'device_ids'):
        logger.important('TRAIN: Training on ', len(model.device_ids), 'GPUs')
    else:
        logger.important('TRAIN: Training on a single device')

    return trainer(model, d_train, d_val,
                   optimizer=optimizer,
                   lr=lr,
                   weight_decay=weight_decay,
                   batch_size=batch_size,
                   grad_clip=grad_clip,

                   metrics=metrics,

                   max_epochs=max_epochs,
                   max_episodes=max_episodes,
                   early_stop_epochs=early_stop_epochs,
                   plot_episode_interval=plot_episode_interval,
                   early_plot_episode_interval=early_plot_episode_interval,
                   scores_per_epoch=scores_per_epoch,

                   checkpoint_filename=checkpoint_filename,
                   checkpoint_folder=checkpoint_folder,
                   keyboard_interrupt_exit=keyboard_interrupt_exit,

                   threads_train=threads_train,
                   threads_score=threads_score,

                   testrun=testrun,
                   no_checkpoints=no_checkpoints,

                   **kwargs)


def trainer(model, d_train, d_val, **kwargs):

    from ..common.generic import enumerate_timed
    from .optimizer import get_optimizer
    from .lr_scheduler import CyclicalLR
    from ..collate import collate
    from ..model import count_parameters
    from ..logging import logger
    from ..score import Scorer
    from torch.nn.utils import clip_grad_norm
    from torch.utils.data import DataLoader
    from ...metrics import init_metrics
    from ava.losses.set_loss import init_loss

    is_baseline = hasattr(model, 'is_baseline') and model.is_baseline()

    args = defaultdict(lambda: None, DEFAULT_CONFIG)
    args.update(kwargs)

    if args['batch_size_val'] is None:
        args['batch_size_val'] = args['batch_size']

    # DATA LOADER
    def worker_init(x): signal.signal(signal.SIGINT, signal.SIG_IGN)  # hack to avoid unnecessary messages
    data_loader = DataLoader(d_train, shuffle=True, batch_size=args['batch_size'], collate_fn=collate,
                             num_workers=args['threads_train'], drop_last=True,
                             pin_memory=True, worker_init_fn=worker_init)

    # LOSS AND METRICS
    metrics = init_metrics(args['metrics'], d_train)
    loss_function = init_loss(args['loss'], d_train)

    # OPTIMIZER
    if not is_baseline:
        optim, lr_scheduler = get_optimizer(args['optimizer'], model, args['lr'], args['weight_decay'])
    else:
        optim, lr_scheduler = None, None

    assert hasattr(model, 'training_details')
    # assert 'model_name' in model.training_details

    # INFO
    train_msg = (f'\nTRAIN: model {model.__class__.__name__} on {d_train.name()}\n' +
                 '   optimizer:        {}\n'.format(
                     '{} (weight decay: {})'.format(args['optimizer'], args['weight_decay'])) +
                 '   batch_size:       {}\n'.format(args['batch_size']) +
                 '   learning rate:    {}\n'.format(args['lr']) +
                 '   dataset size:     {}/{} (train/val)\n'.format(len(d_train),
                                                                   len(d_val) if d_val is not None else 0) +
                 '   score interval:   {}\n'.format(args['score_episode_interval']) +
                 '   score per epoch:  {}\n'.format(args['scores_per_epoch']) +
                 '   max episode:      {}\n'.format(args['max_episodes']) +
                 '   max epoch:        {}\n'.format(args['max_epochs']) +
                 '   # parameters:     {} / {} (all/trainable)\n'.format(
                     '{}M'.format(count_parameters(model) / 1e6), '{}M'.format(
                         count_parameters(model, only_trainable=True) / 1e6)) +
                 '   threads:          {}\n'.format(args['threads_train']) +
                 '   checkpoint file:  {}\n'.format(args['checkpoint_filename'], logger.filename) +
                 '   log file:         {}\n'.format(logger.filename))

    logger.info(train_msg)
    logger.plot_text_visdom(train_msg)

    continue_training = True
    timings = defaultdict(lambda: [])
    histories = defaultdict(lambda: [])

    predictions = None
    model_saved = False
    i_episode, current_epoch = 0, 0
    new_losses, early_stop = [], {'scores': defaultdict(lambda: []), 'best_score': None}
    training_start_time = time.time()

    while continue_training:
        try:
            t_start_epoch = time.time()
            losses_epoch = []
            for _, data_gen_time, (variables_x, variables_y) in enumerate_timed(iter(data_loader)):

                # if not is_baseline:
                variables_x = [v.cuda(non_blocking=True) if v is not None else None for v in variables_x]
                variables_y = [v.cuda(non_blocking=True) if v is not None else None for v in variables_y]

                if not is_baseline:
                    optim.zero_grad()

                variables_x = [v.float() if v is not None else None for v in variables_x]

                timings['new_episode'] += [data_gen_time / args['batch_size']]
                network_start_time = time.time()

                loss = None

                if not args['copy_only'] or predictions is None:
                    predictions = model(*variables_x)
                    assert type(predictions) == tuple, 'predictions must be a tuple'

                    loss = loss_function(predictions, variables_y)

                    timings['forward'] += [time.time() - network_start_time]
                    new_losses += [float(loss)]

                    if not is_baseline:
                        loss.backward()

                losses_epoch += [float(loss)]

                # gradient clipping
                if args['grad_clip'] is not None:
                    total_norm = clip_grad_norm(model.parameters(), args['grad_clip'])
                    if total_norm > args['grad_clip']:
                        logger.important('Gradients were clipped:', float(total_norm))

                if not is_baseline:
                    optim.step()

                if lr_scheduler is not None and type(lr_scheduler) == CyclicalLR:
                    lr_scheduler.iteration_step(i_episode)

                timings['backward'] += [time.time() - network_start_time]

                ############
                ### PLOT ###
                ############
                if is_plot_episode(i_episode, args) and not is_baseline:
                    t = time.time()

                    scorer = Scorer(model, loss_function, metrics, no_metrics=args['no_metrics'])
                    scorer.score_sample_add(predictions, variables_y)
                    scores, metric_names = scorer.compute(is_intermediate=True)

                    logger.info('time for score: {:.5f}'.format(time.time() - t))

                    # update histories
                    current_loss = np.array(new_losses).mean()
                    histories['loss'] += [current_loss]  # add the mean of the current losses
                    histories['loss_episode'] += [i_episode]

                    avg_time_data = np.array(timings['new_episode']).mean()
                    avg_time_net_fwd = np.array(timings['forward']).mean()
                    avg_time_net_bp = np.array(timings['backward']).mean()

                    info_text = []
                    for i_metric, metric_name in enumerate(metric_names):
                        info_line = '{}: '.format(metric_name) + ' '.join('{:.4f}'.format(s) for s in scores[i_metric])
                        info_line = (info_line[:SCORE_CHAR_LIMIT] + '...') if len(info_line) > SCORE_CHAR_LIMIT else info_line
                        info_text += [info_line]

                        if len(histories['train_metric']) <= i_metric:  # dynamically inc
                            histories['train_metric'].append([])

                        histories['train_metric'][i_metric] += [scores[i_metric]]

                    if time.time() - training_start_time > PLOT_START_TIME and hasattr(d_train, 'dataset_types'):

                        predictions_cpu = [p.data.cpu().numpy() for p in predictions]
                        samples_x = [v.cpu().numpy() if v is not None else None for v in variables_x]
                        samples_y = [v.cpu().numpy() if v is not None else None for v in variables_y]
                        logger.plot_samples_predictions_visdom(model, samples_x + samples_y, predictions_cpu, d_train)

                        logger.plot_metrics_visdom(['train ' + m for m in metric_names], histories['train_metric'])
                        logger.plot_metrics_visdom(['loss', 'loss-tail', 'loss-1k'],
                                                   [histories['loss'], histories['loss'][5:],
                                                    histories['loss'][-1000:]],
                                                   [histories['loss_episode'], histories['loss_episode'][5:],
                                                    histories['loss_episode'][-1000:]])
                        logger.plot_gradients_visdom(model)

                    if sum([len(i) for i in info_text]) < 20:
                        info_text = ', '.join(info_text)
                    else:
                        info_text = '\n   ' + '\n   '.join(info_text)

                    msg = ('epoch {:03}, batch {:05}/{:05} {:.3f}G: time (sample/batch): {:.4f}/{:.4f}, '
                           'time (network): {:.4f}+{:.4f}, loss: {:.5f}, {}')
                    n_episode_samples = len(d_train) // args['batch_size']
                    msg = msg.format(current_epoch, i_episode % n_episode_samples, n_episode_samples,
                                     0.00, avg_time_data, avg_time_data * args['batch_size'],
                                     avg_time_net_fwd, avg_time_net_bp,
                                     current_loss, info_text)

                    param_groups = optim.state_dict()['param_groups']
                    opt_name = optim.name() if hasattr(optim, 'name') else optim.__class__.__name__
                    if opt_name == 'SGD':
                        opt_name += ' (momentum: {})'.format(param_groups[0]['momentum'])
                    logger.info('TRAIN: opt: {}, learning rate: {} ({} groups)'.format(opt_name, param_groups[0]['lr'], len(param_groups)))
                    logger.important(msg)

                    new_losses, timings = [], {k: [] for k in timings.keys()}  # reset

                # GUESS TIME
                if i_episode == 10:
                    iter_per_epoch = len(d_train) // args['batch_size']
                    logger.info(
                        'Expected time for epoch: {:.0f}s'.format(np.mean(timings['backward']) * iter_per_epoch))

                ##################
                ### VALIDATION ###
                ##################
                episodes_per_epoch = len(d_train) // args['batch_size']
                if d_val is not None and is_validation_episode(i_episode, len(losses_epoch), episodes_per_epoch, args) and not args['no_validation']:
                    del predictions, variables_x, variables_y
                    if loss is not None: del loss

                    new_best, stop_score = validation_score(model, loss_function, metrics, d_val, histories, early_stop, current_epoch, args)

                    if new_best:
                        d_lengths = len(d_train), len(d_val) if d_val is not None else 0
                        saved_cp = save_checkpoint(model, metrics, histories, i_episode, d_lengths, args)
                        model_saved = model_saved or saved_cp

                    if lr_scheduler is not None:
                        if type(lr_scheduler).__name__ == 'ReduceLROnPlateau':
                            lr_scheduler.step(stop_score)
                        else:
                            lr_scheduler.step()

                if stop_now(i_episode, current_epoch, early_stop, histories, args):
                    continue_training = False
                    break

                i_episode += 1

            if hasattr(model, 'on_epoch_end'):
                model.on_epoch_end(current_epoch)

            if hasattr(d_train, 'on_epoch_end'):
                d_train.on_epoch_end(current_epoch)

            if hasattr(optim, 'on_epoch_end'):
                optim.on_epoch_end()

            logger.important('TRAIN: completed epoch {} in {:.2f}s (mean loss of epoch: {:.5f})'.format(
                current_epoch, time.time() - t_start_epoch, np.mean(losses_epoch)))

            current_epoch += 1

        except KeyboardInterrupt:
            # time.sleep(0.2)
            if args['keyboard_interrupt_exit']:
                logger.warning('Experiment stopped by keyboard interrupt!')
                import sys
                sys.exit(0)
            else:

                logger.important('TRAIN: Stopped by keyboard interrupt!')

                save, optim, lr_scheduler = interrupt(model, lr_scheduler, args['lr'], args)

                if save:
                    save_checkpoint(model, metrics, histories, i_episode, (len(d_train), len(d_val)), args)
                else:
                    model_saved = True

                if optim is None: break

    logger.info('training complete')
    if not model_saved and not args['no_checkpoints']:  # save now if it was not saved before
        logger.warning('Model was not saved before, so it is saved now (without considering validation error)')
        save_checkpoint(model, metrics, histories, i_episode, (len(d_train), len(d_val)), args)

    return True


def is_plot_episode(i_episode, args):
    if i_episode == 0:
        return True

    if i_episode < 25:
        return i_episode % args['early_plot_episode_interval'] == args['early_plot_episode_interval'] - 1

    if args['plot_episode_interval'] is not None:
        return i_episode % args['plot_episode_interval'] == args['plot_episode_interval'] - 1

    return False


def is_validation_episode(i_episode, i_episode_epoch, episodes_per_epoch, args):

    # episodes_per_epoch = len(self.d_train) // self.args['batch_size']

    if args['score_episode_interval'] is not None:
        score_interval_hit = i_episode % args['score_episode_interval'] == args['score_episode_interval'] - 1
    else:
        score_interval_hit = False

    scores_per_epoch_hit = args['scores_per_epoch'] > 1 and i_episode_epoch == (episodes_per_epoch // args['scores_per_epoch'])
    # if self.score_freq is not None and self.score_freq > 0:
    #     scores_per_epoch_hit = self.i_episode % self.score_freq == self.score_freq - 1
    # else:
    #     scores_per_epoch_hit = False

    score_start = args['start_with_score'] and i_episode == 1

    is_last_episode_of_epoch = i_episode_epoch == episodes_per_epoch - 1
    end_of_epoch = args['scores_per_epoch'] > 0 and is_last_episode_of_epoch

    return score_interval_hit or scores_per_epoch_hit or score_start or end_of_epoch


def is_early_stop(early_stop, args):

    if args['early_stop_epochs'] is not None and len(early_stop['scores']) > 0:

        # view epoch scores as a reversed list.
        epochs = sorted(list(early_stop['scores'].keys()))
        epoch_scores_rev = [s for ep in epochs for s in early_stop['scores'][ep]][::-1]

        best_idx = (lambda x: x.argmax()) if args['stop_metric_maximize'] else (lambda x: x.argmin())

        # print(epoch_scores_rev, best_idx(np.array(epoch_scores_rev)), self.stop_metric_maximize)
        # print('early stop debug:', best_idx(np.array(epoch_scores_rev)))

        if best_idx(np.array(epoch_scores_rev)) >= args['early_stop_epochs'] * args['scores_per_epoch']:
            msg = ('TRAIN: Stopping because the validation metric {} did not {} for {} epochs'
                   ' (with {} scores per epoch).\nLatest scores were: {}')
            msg = msg.format(args['stop_metric'], 'increase' if args['stop_metric_maximize'] else 'decrease',
                             args['early_stop_epochs'], args['scores_per_epoch'],
                             ', '.join('{:.5f}'.format(s) for s in epoch_scores_rev[:10]))
            logger.important(msg)
            return True

    return False


def stop_now(i_episode, current_epoch, early_stop, histories, args):

    if args['max_episodes'] is not None and i_episode >= args['max_episodes']:
        logger.important('TRAIN: Reached maximal number of episodes: {}'.format(args['max_episodes']))
        return True

    if args['max_time'] is not None and (time.time() - args['train_start_time']) / 60 > args['max_time']:
        logger.important('TRAIN: Reached maximal training time: {}min'.format(args['max_time']))
        return True

    if np.isnan(histories['loss']).any() or np.isinf(histories['loss']).any():
        logger.important('TRAIN: Stopping because of Nan or Inf loss.')
        return True

    if args['max_epochs'] is not None and current_epoch >= args['max_epochs']:
        logger.important('TRAIN: Reached maximal number of epochs: {} (after {} episodes)'.format(args['max_epochs'], i_episode + 1))
        return True

    if is_early_stop(early_stop, args):
        logger.important('TRAIN: Stopping because of early stopping of {} epochs'.format(args['early_stop_epochs']))
        return True

    return False


def validation_score(model, loss_function, metrics, d_val, histories, early_stop, current_epoch, args):

    from ..score import Scorer

    logger.info('TRAIN: Compute validation error')
    info_text = []

    scorer = Scorer(model, loss_function, metrics, no_metrics=args['no_metrics'])
    score_batch_size = args['batch_size_val']
    losses = scorer.score(d_val, score_batch_size, threads=args['threads_score'])
    computed_scores, metric_names = scorer.compute(is_validation=True)

    val_loss = np.mean(losses)
    # metric_names, scores, val_loss

    # update histories
    for i_metric, metric_name in enumerate(metric_names):

        info_line = '   {}: '.format(metric_name) + ' '.join('{:.4f}'.format(s) for s in computed_scores[i_metric])
        info_line = (info_line[:SCORE_CHAR_LIMIT] + '...') if len(info_line) > SCORE_CHAR_LIMIT else info_line
        info_text += [info_line]

        if len(histories['val_metric']) <= i_metric:  # dynamically inc
            histories['val_metric'].append([])

        histories['val_metric'][i_metric] += [computed_scores[i_metric]]

    # this needs to be done before save_checkpoint()
    histories['val_loss'] += [val_loss]

    logger.info('TRAIN: val loss history: {}'.format(' -> '.join(['{:.4f}'.format(v) for v in histories['val_loss']])))
    logger.plot_metrics_visdom(metric_names, histories['val_metric'])
    logger.plot_metrics_visdom(['val_loss'], [histories['val_loss']])

    logger.important('TRAIN: {}\nvalidation loss: {:.5}:\n{:<}\n{}'.format('#' * 30, val_loss, '\n'.join(info_text), '#' * 30))

    # EARLY STOPPING
    if args['stop_metric'] is not None:
        stop_score = computed_scores[metric_names.index(args['stop_metric'])][0]
        stop_name = 'score {}'.format(args['stop_metric'])
    else:
        stop_score = val_loss
        stop_name = 'val loss'

    cmp = (lambda new, old: new > old) if args['stop_metric_maximize'] else (lambda new, old: new < old)
    early_stop['scores'][current_epoch] += [stop_score]  # track for early stopping

    best = early_stop['best_score']

    if best is None or cmp(stop_score, early_stop['best_score']):

        logger.info('TRAIN: new best {}: {} (old {})'.format(
            stop_name,  f'{stop_score:.5f}' if stop_score else '-',  f'{best:.5f}' if best else '-'))
        early_stop['best_score'] = stop_score
        early_stop['best_epoch'] = current_epoch
        # self.save_checkpoint()
        return True, stop_score
    else:
        logger.info('TRAIN: {} worse than best: {} (old {})'.format(
            stop_name,  f'{stop_score:.5f}' if stop_score else '-',  f'{best:.5f}' if best else '-'))
        return False, stop_score


def save_checkpoint(model, metrics, histories, i_episode, d_lengths, args):

    if args['checkpoint_filename'] is not None:

        if dirname(args['checkpoint_filename']) != '':
            os.makedirs(dirname(args['checkpoint_filename']), exist_ok=True)

        logger.clear_cache()  # makes sure the log is written

        checkpoint = {'state_dict': model.state_dict()}

        #  TODO: this needs to be fixed: If a pre-trained model is used, this overwrites values.
        #  See the transfer_learning experiment.
        checkpoint.update(model.training_details)

        checkpoint['val_loss'] = [histories['val_loss'][-1] if len(histories['val_loss']) > 0 else 99999]
        checkpoint['train_loss'] = [np.array(histories['loss'][-1]).mean() if len(histories['loss']) > 0 else -99]

        checkpoint['val_loss_history'] = [histories['val_loss']]
        # sample train_loss_history of 20 times the length of val_loss_history
        checkpoint['train_loss_history'] = [histories['loss'][::1 + (len(histories['loss']) // max(1, len(histories['val_loss']) * 20))]]

        checkpoint['episode'] = [i_episode]
        checkpoint['dataset_size'] = [d_lengths[0]]
        checkpoint['val_dataset_size'] = [d_lengths[1]]
        checkpoint['metrics'] = [tuple((m.name(), histories['val_metric'][j][-1])
                                       for j, m in enumerate(metrics)
                                       if j in histories['val_metric'] and len(histories['val_metric'][j]) > 0)]

        checkpoint['batch_size'] = [args['batch_size']]
        checkpoint['lr'] = [args['lr']]
        checkpoint['optim'] = [args['optim_name']]
        checkpoint['weight-decay'] = [args['weight_decay']]
        checkpoint['grad_clip'] = [args['grad_clip']]

        from torch import save
        save(checkpoint, args['checkpoint_filename'])

        detail_args = {k: checkpoint[k] for k in checkpoint.keys() if k not in {'state_dict'}}

        # save details additionally in a separate file to avoid loading the large file if only arguments are needed
        from pickle import dump
        dump(detail_args, open(args['checkpoint_filename'] + '-args', 'wb'))

        logger.important('TRAIN: saved model to {} with these arguments:\n{}'.format(
            args['checkpoint_filename'],
            ''.join('  {}: {}\n'.format(k, v) for k, v in model.training_details['model_arguments'][-1].items())))
        return True

    return False


def interrupt(model, lr_scheduler, lr, args):

    from .optimizer import get_optimizer

    ans = None
    while ans not in {'y', 'n'}:
        ans = input('\n\nRe-initialize optimizer with new learning rate ("no" will quit the training)? (y/n) ')

    if ans in {'y', 'Y'}:
        while True:  # prevents the program from crashing if the input is invalid
            try:
                lr = lr_scheduler.get_lr() if lr_scheduler is not None else lr
                ans_lr = input('New learning rate (current is {}): '.format(lr))
                # do not get a new lr_scheduler here
                optim, lr_scheduler = get_optimizer(args['optim_name'], model, ans_lr, args['weight_decay'])
                return False, optim, lr_scheduler
            except (ValueError, AttributeError):
                pass

    ans = None
    while ans not in {'y', 'n'}:
        ans = input('TRAIN: Save model checkpoint (filename: {})? (y/n) '.format(args['checkpoint_filename']))

    if ans == 'y':
        return True, None, None
    else:
        return False, None, None
