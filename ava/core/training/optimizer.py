import re

from torch.optim import SGD, Adam, RMSprop, Optimizer, LBFGS
from torch.optim.lr_scheduler import ReduceLROnPlateau, StepLR
from torch.optim.optimizer import required

from ..logging import logger
from ..training.lr_scheduler import CyclicalLR


class TwoPhaseOpt(Optimizer):
    r""" First Adam or RMSprop, later switch to SGD. Should improve generalization.
    This class fakes the PyTorch's optimizer API and adds the new methods on_epoch_end and name.
    """

    def __init__(self, params, lr=required, switch_epoch=5, switch_step=None, momentum=0.9, dampening=0,
                 weight_decay=0.0, nesterov=False, opt1='adam'):

        if opt1 == 'adam':
            self.opt1 = Adam(params, lr, weight_decay=weight_decay)
        elif opt1 == 'rmsprop':
            self.opt1 = RMSprop(params, lr, weight_decay=weight_decay)
        else:
            raise ValueError('Invalid opt')

        self.sgd = SGD(params, lr, momentum, dampening, weight_decay, nesterov)
        self.param_groups = self.sgd.param_groups

        self.switch_epoch = switch_epoch
        self.switch_step = switch_step
        self.pointer = self.opt1
        self.step_counter = 0
        self.epoch_counter = 0

    def step(self, closure=None):
        self.step_counter += 1
        self.pointer.step(closure)

        if self.switch_step is not None and self.step_counter >= self.switch_step:
            self.pointer = self.sgd
            self.opt1 = None

    def zero_grad(self):
        self.pointer.zero_grad()

    def on_epoch_end(self):
        self.epoch_counter += 1

        if self.switch_epoch is not None and self.epoch_counter >= self.switch_epoch:
            self.pointer = self.sgd
            self.param_groups = self.pointer.param_groups

    def state_dict(self):
        return self.pointer.state_dict()

    def load_state_dict(self, state_dict):
        self.pointer.load_state_dict(state_dict)

    def name(self):
        if self.switch_step is not None:
            return '2PhaseSGD(@{}, switch at step {})'.format(self.pointer.__class__.__name__, self.switch_step)
        else:
            return '2PhaseSGD(@{}, switch at epoch {})'.format(self.pointer.__class__.__name__, self.switch_epoch)

    def __getstate__(self):
        return self.pointer.__getstate__()

    def __setstate__(self, state):
        return self.pointer.__setstate__(state)

    def __repr__(self):
        return self.pointer.__repr__()

    def add_param_group(self, param_group):
        raise NotImplementedError
        # return self.pointer.add_param_group(param_group)


def get_optimizer(optim_name, model, lr, weight_decay):

    try:
        learning_rate = float(lr)
        lr_scheduler_type, lr_scheduler_args = None, None
    except ValueError:
        m = re.match(r'^(\w*)(:(.*))?$', lr)
        if m:
            lr_type_name, _, lr_params = m.groups()
            lr_params = lr_params.split(':')
            lr_scheduler_args = dict()

            learning_rate = float(lr_params[0])

            if lr_type_name == 'plateau':
                lr_scheduler_type = ReduceLROnPlateau
                lr_scheduler_args['patience'] = int(lr_params[1])
                lr_scheduler_args['factor'] = float(lr_params[2])
            elif lr_type_name == 'step':
                lr_scheduler_type = StepLR
                lr_scheduler_args['step_size'] = int(lr_params[1])
                lr_scheduler_args['gamma'] = float(lr_params[2])
            elif lr_type_name == 'cyclical':
                lr_scheduler_type = CyclicalLR
                lr_scheduler_args['start_lr'] = learning_rate
                lr_scheduler_args['end_lr'] = float(lr_params[1])
                lr_scheduler_args['step_size'] = int(lr_params[2])
            else:
                raise ValueError('Invalid lr schedule type: {}'.format(lr_type_name))
        else:
            raise ValueError('Invalid learning rate: {}'.format(lr))

    if hasattr(model, 'opt_policies'):
        logger.warning('Using optimization policies')
        valid_parameters = model.opt_policies()

        # avoid generators in valid_parameters because they can only be evaluated once
        for i in range(len(valid_parameters)):
            valid_parameters[i]['params'] = list(valid_parameters[i]['params'])

        for group in valid_parameters:
            # non_grad_params = [p for p in group['params'] if not p.requires_grad]
            if any([not p.requires_grad for p in group['params']]):
                logger.warning('There is a parameter that does not require gradients in group {}'.format(group['name']))
    else:
        valid_parameters = [p for p in model.parameters() if p.requires_grad]

    weight_decay = float(weight_decay)

    two_phase_regexp = r'^(adam|rmsprop)_([0-9]+)(e|s)_sgd_m$'

    if optim_name == 'rmsprop':
        optim = RMSprop(valid_parameters, lr=learning_rate, weight_decay=weight_decay)
    elif optim_name == 'adam':
        optim = Adam(valid_parameters, lr=learning_rate, weight_decay=weight_decay)
    elif optim_name == 'sgd':
        optim = SGD(valid_parameters, lr=learning_rate, weight_decay=weight_decay)
    elif optim_name == 'sgd_m':
        optim = SGD(valid_parameters, lr=learning_rate, momentum=0.9, weight_decay=weight_decay)
    elif optim_name == 'lbfgs':
        optim = LBFGS(valid_parameters)
    elif re.match(two_phase_regexp, optim_name):
        opt1, num, epoch_or_step = re.match(two_phase_regexp, optim_name).groups()
        switch_epoch, switch_step = (int(num), None) if epoch_or_step == 'e' else (None, int(num))

        optim = TwoPhaseOpt(valid_parameters, switch_epoch=switch_epoch, switch_step=switch_step,
                            lr=learning_rate, momentum=0.9, weight_decay=weight_decay, opt1=opt1)
    else:
        raise ValueError('invalid optimizer: {}'.format(optim_name))

    if lr_scheduler_type is not None:
        lr_scheduler = lr_scheduler_type(optim, **lr_scheduler_args)
    else:
        lr_scheduler = None

    return optim, lr_scheduler
