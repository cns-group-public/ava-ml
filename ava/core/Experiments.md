Experiments can be defined in yaml by specifying a pairs of models 
and datasets as well as their parameters. You can find examples of experiment definitions ins the `experiments` folder, 
e.g. run 

```bash
ava experiment experiments/mnist.yaml
```

Important variables are:

- `common_train_args`: arguments that hold for all model-dataset pairs for training (e.g. batch size)
- `common_test_args`: arguments that hold for all model-dataset pairs for evaluation
- `configurations`: A list of individual (model, dataset, parameter) tuples for training.
- `test_dataset`: A list of datasets (with parameters) for evaluation.
- `individual_test_dataset`: A list of individual evaluation datasets (with parameters) for each configuration.

The experiment can generate tables which can be directly used in tex files. 
These are the relevant parameters:

- `tex_transpose`

for more parameters see the example experiment files


