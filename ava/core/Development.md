This folder and its subfolder contain modules that are relevant 
for the core functions of the library, e.g. training and scoring models.

### General Structure

Ava is strcutured into several sub-packages.

- `core` (see below)
- `datasets`: Dataset defintions
- `metrics`: Metrics to measure performance
- `models` (see below)
- `scripts`: Useful tools
- `third_party`: Code that was not written by us
- `transformations`: Data preprocessing routines


### Core

This sub-package contains core functions such as for launching training and experiments. 
It involves the following subpackages:

- `common`: 
- `data_types`: Definitions of data types for visualization
- `training`: Training logic
- `visualize`: Framework for visualizing input data and predictions

Additionally these modules exist:

- `arguments`: The `argparse` configuration for the CLI
- `benchmarks`: 
- `dataset`: 
- `experiment`: 
- `model`:
- `plots`:
- `logging`: Logging to console, log file and visdom
- `score`:
- `table`:
- `visualize`:


### Model

The models directory contains implementations of several models. 

### Folders
* `video:` Models for video classification.
* `dense:` Models that predict densely, e.g. for semantic segmentation.
* `deprecated:` Old models that should no longer be used. These models will not be available on the CLI.
