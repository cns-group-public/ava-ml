import inspect
import os
import pickle
import shutil
import time
from collections import OrderedDict, defaultdict
from os.path import join, isfile, isdir, splitext

import numpy as np
import yaml

# Metrics can be ignored since only stop-metric is relevant.
HASH_IGNORE_PARAMS = ('multi_gpu', 'gpu_id', 'threads', 'plot_episode_interval', 'score_episode_interval',
                      'scores_per_epoch', 'start_with_score', 'no_log', 'log_file',
                      'checkpoint_filename', 'checkpoint_folder', 'overwrite_checkpoint', 'metrics')


# use multiprocessing for distributed training
def maybe_train(experiment_config, common_args, gpu_dict=None, gpu_lock=None, retrain=False, testrun=False, threads=None,
                multi_gpu=False):
    """ Train only if necessary. """

    from .logging import logger
    from .. import PATHS
    from .training import trainer
    from .common.generic import find_file_in_folders
    import torch

    logger.info('next configuration:', experiment_config)

    _, training_dataset_name, this_training_args = experiment_config

    # determine GPU id
    if gpu_dict is not None:
        with gpu_lock:
            logger.info('Trying to obtain a free GPU. Available: {}'.format(gpu_dict.keys()))
            gpu_id = list(gpu_dict.keys())[0]
            del gpu_dict[gpu_id]
    else:
        gpu_id = None

    # start with these default arguments
    train_args = dict({
        'model_name': experiment_config[0],
        'dataset_name': training_dataset_name,
        'overwrite_checkpoint': True,
        'checkpoint_filename': None,
        'keyboard_interrupt_exit': True,
    })

    # individual arguments have highest priority
    train_args.update(common_args)
    train_args.update(this_training_args)

    if threads is not None:
        train_args['threads'] = threads

    train_args['multi_gpu'] = multi_gpu
    train_args = {k.replace('-', '_'): v for k, v in train_args.items()}

    if train_args['checkpoint_filename'] is None:

        sig = set(inspect.signature(trainer.train_high_level).parameters.keys()).union(trainer.DEFAULT_CONFIG.keys())
        training_args2 = defaultdict(lambda: None, {k: v for k, v in train_args.items() if k in sig})
        non_train_args2 = {k: v for k, v in train_args.items() if k not in sig}

        complete_hash = training_hash(training_args2, non_train_args2)

        folder = training_args2['checkpoint_folder'] if training_args2['checkpoint_folder'] is not None else ''
        checkpoint_filename = os.path.join(PATHS['TRAINED_MODELS_PATH'], folder, complete_hash)
        train_args['checkpoint_filename'] = checkpoint_filename
    else:
        checkpoint_filename = train_args['checkpoint_filename']

    found_checkpoint_filename = find_file_in_folders(checkpoint_filename, ['pretrained', PATHS['TRAINED_MODELS_PATH']])

    # only train a new model if the target filename does not exists yet
    if found_checkpoint_filename is not None and not retrain and not testrun:
        checkpoint_filename = found_checkpoint_filename
        logger.important('{} already exists. Skipping training.'.format(checkpoint_filename))
    else:
        logger.important('{} does not exist yet or retrain is set. Starting training.'.format(checkpoint_filename))

        if testrun:
            train_complete = trainer.train_high_level(**train_args, no_checkpoints=True, testrun=True)
            return checkpoint_filename if train_complete else checkpoint_filename + '_FAILED'
        else:
            train_args['checkpoint_filename'] += '_TRAIN'   # to indicate that training is not completed
            train_complete = trainer.train_high_level(**train_args)

            # rename to original name if training successful
            if train_complete:
                assert train_args['checkpoint_filename'].endswith('_TRAIN')
                shutil.move(train_args['checkpoint_filename'], train_args['checkpoint_filename'][:-6])
                shutil.move(train_args['checkpoint_filename'] + '-args', train_args['checkpoint_filename'][:-6] + '-args')
            else:
                raise SystemError('No best epoch was recorded. It seems the training did not complete.')

    import gc
    gc.collect()
    torch.cuda.empty_cache()

    if gpu_dict is not None:
        with gpu_lock:
            gpu_dict[gpu_id] = None

    # replace the model name with the checkpoint name
    return checkpoint_filename


def experiment(experiment_file, testrun=False, parallel=None, nums=None, recalc=False,
               threads_train=None, threads_score=None, only_tex=False, delete_models=False, with_modelfiles=False,
               curves=False, **kwargs):
    """
    Run an experiment defined in `filename`.
    Models can either be passed as Triples of model, dataset and arguments or
    It is important that model name and dataset name yield a unique identifier for the specific training.
    """

    import torch
    from .plots import plot_experiment, plot_curves
    from .common.value_assignment import from_dicts, ordered_dict_merge
    from .dataset import get_dataset
    from .model import load_pretrained_model
    from .table import train_output_to_pandas, table_to_tex
    from .logging import logger
    from .score import Scorer
    from .. import PATHS
    from .common.generic import elements_list_to_tuple
    from .common.serialization import hash_dict
    from .. import models
    from ..metrics import init_metrics

    # def write_tex(scores, details):
    #     table, common = train_output_to_pandas(scores, details)
    #     table_to_tex(table, common, experiments, os.path.join(dirname_out, filename_pre), show=True)

    dirname_out = join(os.path.abspath(os.path.dirname(experiment_file)), 'out')
    if not isdir(dirname_out): os.makedirs(dirname_out)

    filename_pre = os.path.basename(os.path.splitext(experiment_file)[0])

    with open(experiment_file) as fh:
        experiments = yaml.load(fh)

    if only_tex:
        exp_file = pickle.load(open(os.path.join(dirname_out, '{}-experiment.pickle').format(filename_pre), 'rb'))
        table, common = train_output_to_pandas(exp_file['table'], exp_file['details'])
        table_to_tex(table, common, experiments, os.path.join(dirname_out, filename_pre), show=True)
        return

    if not isdir(os.path.join(dirname_out, 'cached')): os.makedirs(os.path.join(dirname_out, 'cached'))

    validate_experiment_file(experiments)

    if nums is not None:
        try:
            start, end = int(nums), int(nums) + 1
        except ValueError:
            assert type(nums) == str and ':' in nums
            start, end = nums.split(':')
            start, end = int(start) if len(start) > 0 else None, int(end) if len(end) > 0 else None

        experiments['configurations'] = experiments['configurations'][start:end]
        if 'individual_test_datasets' in experiments:
            experiments['individual_test_datasets'] = experiments['individual_test_datasets'][start:end]

    if 'retrain' in kwargs and kwargs['retrain']:
        recalc = True

    for experiment_config in experiments['configurations']:

        if type(experiment_config[0]) != dict:
            assert hasattr(models, experiment_config[0]), 'Model {} not found'.format(experiment_config[0])
        else:
            model_folder = experiments['model_folder'] if 'model_folder' in experiments else ''
            if not os.path.isfile(join(PATHS['TRAINED_MODELS_PATH'], model_folder, experiment_config[0]['load'])):
                logger.warning('Checkpoint to be loaded does not exist yet: {}'.format(experiment_config[0]['load']))

        if len(experiment_config) == 3:
            from .. import datasets
            assert hasattr(datasets, experiment_config[1]), 'Dataset {} not found'.format(experiment_config[1])

    # Experiment control parameters
    # ignore_model_training_arguments = 'ignore_indices_for_test' in experiments and experiments['ignore_indices_for_test']
    ignore_indices_for_test = {} if 'ignore_indices_for_test' not in experiments else experiments['ignore_indices_for_test']
    common_train_args = experiments['common_train_args'] if 'common_train_args' in experiments else {}
    common_test_args = experiments['common_test_args'] if 'common_test_args' in experiments else {}

    common_train_args.update({
        'checkpoint-folder': experiments['model_folder'] if 'model_folder' in experiments else join(PATHS['TRAINED_MODELS_PATH'], splitext(experiment_file)[0]),
        # 'log-folder': experiments['log_folder'] if 'log_folder' in experiments else join(dirname(realpath(experiment_file)), 'log'),
    })

    if 'metrics' in common_test_args and 'metrics' not in common_test_args:
        logger.warning('There are no metrics defined for testing.')

    if threads_train is not None: common_train_args['threads'] = threads_train
    # if threads_score is not None: common_test_args['threads'] = threads_score

    # if models need to be trained, start with that and save them to disk
    model_filenames = []

    # expand configurations and individual_test_datasets for cross validation or repetitions
    if 'cross_validation' in experiments and experiments['cross_validation'] > 1:
        n_repetitions = int(experiments['cross_validation'])
        del experiments['cross_validation']
        config_new = []
        for c in experiments['configurations']:
            for k in range(n_repetitions):
                config_new += [[c[0], c[1], dict(**c[2], **{'seed': k})]]
        experiments['configurations'] = config_new

        indiv_test = []
        if 'individual_test_datasets' in experiments:
            for c in experiments['individual_test_datasets']:
                for k in range(n_repetitions):
                    indiv_test += [[c[0], dict(**c[1], **{'seed': k})]]
            experiments['individual_test_datasets'] = indiv_test

    if parallel is not None:  # train in parallel
        assert type(parallel) == int
        for i in range(len(experiments['configurations']) // parallel):

            from multiprocessing.pool import ThreadPool
            from threading import Lock
            from functools import partial
            pool = ThreadPool(parallel)  # Threads must be used because Pool creates daemonic processes that can't spawn

            gpu_dict = dict()
            gpu_dict.update({i: None for i in range(torch.cuda.device_count())})
            gpu_lock = Lock()

            train_partial = partial(maybe_train, **{'common_args': common_train_args, 'gpu_dict': gpu_dict,
                                                    'gpu_lock': gpu_lock,
                                                    'testrun': testrun, **kwargs})

            # avoid training the same configurations in parallel
            def exp_key(k):
                items = k[2].items()
                items = {k: tuple(v) if hasattr(v, '__len__') else v for k, v in items}
                return k[0], k[1], frozenset(items)

            from itertools import groupby
            non_duplicate_experiments = [x for x, _ in groupby(sorted(experiments['configurations'], key=exp_key))]

            out = pool.map(train_partial, non_duplicate_experiments)

    training_states = []
    # this needs to be done for parallel, too, because model_filenames must be provided.
    for i, experiment_config in enumerate(experiments['configurations']):
        this_filename = maybe_train(experiment_config, common_train_args, testrun=testrun, **kwargs)
        if this_filename is None: break  # stop whole experiment if one training fails
        model_filenames += [this_filename]

    # now all models should exist as checkpoints
    invalid_models = [m for m in model_filenames if not os.path.isfile(m)]

    if delete_models:
        for m in model_filenames:
            print('remove', m)
            os.remove(m)
        return

    if testrun:
        print('Failed models:')
        print('\n'.join(os.path.basename(m) for m in model_filenames if m.endswith('_FAILED')))
        logger.important('Testrun completed.')
        return
    else:
        assert len(invalid_models) == 0, 'Invalid model names:\n{}'.format('\n'.join(invalid_models))

    #############

    logger.important('\n\n### Training completed ###\n\n')

    t_start = time.time()
    enumerated_models = [(i, model_str) for i, model_str in enumerate(model_filenames) if i not in ignore_indices_for_test]

    # assign test datasets for each trained model
    all_test_datasets = [[] for _ in enumerated_models]

    if 'test_datasets' in experiments and experiments['test_datasets'] is not None:
        for i, _ in enumerated_models:
            all_test_datasets[i] = experiments['test_datasets']

    if 'individual_test_datasets' in experiments and experiments['individual_test_datasets']:
        # for i, _ in enumerated_models:
        for i in range(len(experiments['individual_test_datasets'])):
            all_test_datasets[i] += [experiments['individual_test_datasets'][i]]

    table, details, model_strs = {i: dict() for i, _ in enumerated_models}, dict(), []

    for (i_conf, model_str), test_datasets in zip(enumerated_models, all_test_datasets):
        model_filename, model_args = model_str if len(model_str) == 2 else (model_str, {})
        model_strs += [model_filename]

        details[i_conf] = pickle.load(open(model_filename + '-args', 'rb'))
        details[i_conf]['score_args'] = []

        logger.important('Experiment number {}: Testing on {} datasets'.format(i_conf, len(test_datasets)))

        assert len(test_datasets) > 0

        for j, dataset in enumerate(test_datasets):

            err = 'Dataset entry has wrong format (must be either tuple or str): {}'
            assert type(dataset) == str or (type(dataset) in {list, tuple} and len(dataset) == 2), err.format(dataset)

            score_dataset_name, score_args = dataset if len(dataset) == 2 else (dataset, {})

            logger.important('Next evaluation setting: {} {} {}'.format(model_filename, score_dataset_name, score_args))
            full_score_args = ordered_dict_merge(score_args, common_test_args)

            # normalize keys
            full_score_args = {k.replace('-', '_'): v for k, v in full_score_args.items()}

            batch_size = from_dicts('batch_size', full_score_args, score_args, default=16)
            max_samples = from_dicts('max_samples', full_score_args, score_args, default=None)
            metrics = init_metrics(full_score_args['metrics']) if 'metrics' in full_score_args else []

            # remove keys to avoid contradicting values
            if 'batch_size' in full_score_args.keys(): del full_score_args['batch_size']
            if 'max_samples' in full_score_args.keys(): del full_score_args['max_samples']
            if 'metrics' in full_score_args.keys(): del full_score_args['metrics']

            model_train_args = pickle.load(open(model_str + '-args', 'rb'))

            # if a model key is not specified yet, use it from the trained model
            full_score_args.update({k: v for k, v in model_train_args['model_arguments'][-1].items() if k not in full_score_args})

            subset = from_dicts('subset', full_score_args, score_args, default='test')
            if 'subset' in full_score_args: del full_score_args['subset']

            hashed_name = hash_dict(dict({'batch_size': batch_size, 'subset': subset, 'max_samples': max_samples},
                                         **model_train_args, **full_score_args))
            cache_filepath = os.path.join(dirname_out, 'cached', hashed_name)

            # FROM CACHE: try to load cached file first
            if isfile(cache_filepath) and not recalc:
                logger.info('Use cached scores: {}'.format(cache_filepath))

                with open(cache_filepath, 'rb') as fh:
                    metric_names, metric_scores = pickle.load(fh)
                    assert all([type(s) == str for s in metric_names])

            # RECOMPUTE SCORE
            else:
                assert isfile(model_filename)

                (dataset,), dataset_args, other_args = get_dataset(score_dataset_name, full_score_args,
                                                                   subsets=(subset,), samples=(max_samples,))

                logger.important('EXPERIMENT: Arguments for dataset {}:\n{}'.format(
                    dataset.name(), '\n'.join(['   {}: {}'.format(k, v) for k, v in dataset_args.items()])))

                model_data = torch.load(model_filename, map_location=torch.device('cuda'))

                # no need to initialize with pretrained weights since we are loading custom weights
                if 'pretrained' in other_args: del other_args['pretrained']

                model = load_pretrained_model(model_data, other_args, model_config=dataset.model_config, cuda=True)
                scorer = Scorer(model, metrics=metrics)
                scorer.score(dataset, batch_size=batch_size, threads=threads_score)
                metric_scores, metric_names = scorer.compute()

                with open(cache_filepath, 'wb') as fh:
                    assert all([type(s) == str for s in metric_names])
                    pickle.dump((metric_names, metric_scores), fh)
                    print('WROTE\n{}\n\n\n'.format(cache_filepath))

            table[i_conf][score_dataset_name] = {m: s for m, s in zip(metric_names, metric_scores)}
            details[i_conf]['score_args'] += [full_score_args]

    # merge experiments that differ only in seed
    groups = OrderedDict()
    seed_arg_count = 0  # to keep track if groups are actually needed

    for i, k in enumerate(details):

        model_args, dataset_args = details[k]['model_arguments'][-1], details[k]['dataset_arguments'][-1]

        # lists to tuples to allow sorting
        model_args, dataset_args = elements_list_to_tuple(model_args), elements_list_to_tuple(dataset_args)

        key = tuple(sorted([x for x in model_args.items()] + [x for x in dataset_args.items() if x[0] != 'seed']))
        key = (details[k]['model'][-1],) + key

        seed_arg_count += 1 if 'seed' in dataset_args else 0

        if key in groups:  # only add to group if seed occurs in dataset arguments
            groups[key] += [i]
        else:
            groups[key] = [i]

    # only reduce
    if seed_arg_count > 0 and len(groups) < len(details):
        model_strs_new = {}
        details_new, table_new = {i: {} for i in range(len(groups))}, {i: {} for i in range(len(groups))}
        for i, indices in enumerate(groups.values()):

            i0 = indices[0]

            # just copy the details from first entry as all should be identical and remove seed
            details_new[i] = dict(details[i0])

            if 'seed' in details_new[i]['dataset_arguments'][-1]:
                del details_new[i]['dataset_arguments'][-1]['seed']

            model_strs_new[i] = enumerated_models[i0][1]

            for dataset in table[i0]:
                table_new[i][dataset] = {}
                for metric in table[i0][dataset]:

                    table_new[i][dataset][metric + '_mean'] = np.array([table[j][dataset][metric] for j in indices]).mean(0)
                    table_new[i][dataset][metric + '_std'] = np.array([table[j][dataset][metric] for j in indices]).std(0)
                    table_new[i][dataset][metric + '_N'] = len(indices)

        table = table_new
        details = details_new
        model_strs = model_strs_new
        logger.important('\nReduced number of groups by seed from {} to {}'.format(len(details), len(details_new)))

    logger.important('Evaluation completed in {:.1f}s\n\n\n'.format(time.time() - t_start))

    with open(join(dirname_out, '{}-experiment.pickle').format(filename_pre), 'wb') as fh:
        pickle.dump({'table': table, 'details': details}, fh)

    # for i, k in enumerate(details):
    #    print('{}: {} {}'.format(i, details[k]['model'][-1], ', '.join(['{:.4f}'.format(x) for x in details[k]['val_loss_history'][-1]])))

    if with_modelfiles:
        for k in details:
            modelfile = [os.path.basename(model_strs[k])]
            details[k].update({'modelname': modelfile})

    # SHOW RESULTS
    table, common = train_output_to_pandas(table, details)
    table_to_tex(table, common, experiments, os.path.join(dirname_out, filename_pre), show=True)

    if 'plot_outputs' in experiments:
        plot_experiment(table, experiments, dirname_out, filename_pre)

    if curves:  # plot curves
        plot_curves(details, show=True)


def training_hash(training_args, non_train_args):

    from .common.generic import matching_args, elements_list_to_tuple
    from .common.serialization import hash_object
    from .. import datasets

    dataset_name = training_args['dataset_name']
    model_name = training_args['model_name']
    model_name = model_name if type(model_name) == str else 'load-{}'.format(model_name['load'])
    dataset_type = getattr(datasets, dataset_name)
    dataset_args, other_args = matching_args(dataset_type.__init__, non_train_args)

    dataset_args, other_args = elements_list_to_tuple(dataset_args), elements_list_to_tuple(other_args)

    # sort to be invariant w.r.t argument order
    training_args_items = sorted(training_args.items(), key=lambda x: x[0])
    dataset_args_items = sorted(dataset_args.items(), key=lambda x: x[0])
    other_args_items = sorted(other_args.items(), key=lambda x: x[0])

    # This representation should be invariant to parameters that are False or None. Then adding new parameters
    # in default False/None setting won't alter the hash and the code remains extendable.
    train_params = tuple(('{}:{}'.format(k, str(v)) for k, v in training_args_items
                          if k not in HASH_IGNORE_PARAMS + ('dataset_name', 'model_name')))

    dataset_params = tuple(
        '{}:{}'.format(k, str(v)) for k, v in dataset_args_items)  # used to be: if v not in {False, None}
    other_params = tuple('{}:{}'.format(k, str(v)) for k, v in other_args_items)

    return hash_object((dataset_name, model_name) + dataset_params + other_params + train_params)


def validate_experiment_file(experiments):
    valid_fields = ['configurations', 'individual_test_datasets', 'test_datasets', 'common_train_args',
                    'common_test_args', 'model_folder', 'log_folder', 'ignore_model_training_args',
                    'ignore_indices_for_test', 'ignore_cols', 'tex_hash_cols', 'tex_remap_cols', 'tex_percent_cols',
                    'tex_multi_cols', 'tex_collapse', 'tex_remap', 'tex_hlines', 'tex_split_cols', 'tex_model_cols',
                    'extra_output', 'plot_outputs', 'tex_transpose', 'cross_validation']

    invalid_keys = set(experiments.keys()).difference(valid_fields)
    assert len(invalid_keys) == 0, 'Invalid experiment fields: {}'.format(','.join(invalid_keys))

    n_ignore = len(experiments['ignore_indices_for_test']) if 'ignore_indices_for_test' in experiments else 0

    # check validity of models and datasets first
    if 'individual_test_datasets' in experiments:
        assert len(experiments['configurations']) == len(experiments['individual_test_datasets']) + n_ignore, '{} vs. {}'.format(
            len(experiments['configurations']), len(experiments['individual_test_datasets']))

    # maybe too restrictive...
    for config in experiments['configurations']:
        assert len(config) == 3 and type(config[2]) == dict

    if 'test_datasets' in experiments:
        for dataset in experiments['test_datasets']:
            assert type(dataset) == str or (type(dataset) in {list, tuple} and len(dataset) == 2)
    elif 'individual_test_datasets' in experiments:
        for dataset in experiments['individual_test_datasets']:
            assert type(dataset) == str or (type(dataset) in {list, tuple} and len(dataset) == 2)