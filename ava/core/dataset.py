import importlib
import re
from os import listdir
from os.path import isdir

import numpy as np
from random import shuffle as random_shuffle
from .common.assertions import assert_equal_elements
from .logging import logger
from .common.generic import matching_args
from .common.serialization import class_config_str


def get_dataset(dataset_name, arguments=None, subsets=('train',), samples=None, shuffle=None,
                no_argument_normalization=False):
    """ Simple dataset loader """

    assert samples is None or len(subsets) == len(samples)
    assert shuffle is None or len(subsets) == len(shuffle)

    samples = tuple(None for _ in subsets) if samples is None else samples
    shuffle = tuple(None for _ in subsets) if shuffle is None else shuffle

    try:
        dataset_type = None

        # # search in folder first
        # for module in listdir('.'):
        #     if module.endswith('_dataset.py'):
        #         mod = importlib.import_module(module[:module.rindex('.py')])
        #         dataset_type = getattr(mod, dataset_name)

        if dataset_type is None:
            from .. import datasets
            dataset_type = getattr(datasets, dataset_name)
    except AttributeError:
        raise ImportError('The dataset module was not found. Are you sure it is imported in datasets/__init__.py?')

    arguments = arguments if arguments is not None else {}

    if not no_argument_normalization:
        arguments = {k.replace('-', '_'): v for k, v in arguments.items()}

    dataset_args, other_args = matching_args(dataset_type.__init__, arguments)

    datasets = [dataset_type(sub, **dataset_args) for sub in subsets]

    # resize to desired size
    for dataset, n_samples, shuf in zip(datasets, samples, shuffle):

        if shuf:
            dataset.shuffle()

        if n_samples is not None:
            dataset.resize(n_samples)

    return tuple(datasets), dataset_args, other_args


class DatasetBase(object):
    """
    Represents a datasets.
    Required attributes:
    - `chunk_names`: a tuple of strings with the same length as the output of `get_sample`.
    - `sample_ids`: list of strings of length of the dataset which names each sample.

    Optional attributes:
    - `chunk_hooks`: can be optionally specified to map the values of specific chunks. E.g. a numeric label
    can be converted to a text label.

    """

    def __init__(self, data_types, visualizers=None):
        # self.chunk_names = chunk_names
        self.data_types = data_types
        self.visualizers = visualizers

        self.visualization_hooks = dict()
        self.model_config = dict()
        # self.parameter_variables = []
        self._n_active_samples = None
        self.sample_ids = None

    def __getitem__(self, index):
        raise NotImplementedError

    def name(self):
        return self.__class__.__name__

    def shuffle(self):
        random_shuffle(self.sample_ids)

    def resize(self, n_elements):

        if n_elements is not None and n_elements > len(self.sample_ids):
            raise ValueError('n_element must be smaller or equal the number of samples in the dataset.')

        self._n_active_samples = n_elements

        # self.sample_ids = self.sample_ids[:n_elements]

    def __len__(self):
        if self._n_active_samples is not None:
            return self._n_active_samples
        else:
            return len(self.sample_ids)

    def download(self):
        pass


class Interleaved(DatasetBase):
    """
    Merge two datasets such that samples are interleaved. Without shuffling they will simply be after each other
    """

    def __init__(self, dataset1, dataset2, data_types=None, model_config=None):

        if data_types is None:
            assert dataset1.data_types == dataset2.data_types
            data_types = dataset1.data_types

        super().__init__(data_types)

        self.dataset1 = dataset1
        self.dataset2 = dataset2

        len1 = len(dataset1)
        len2 = len(dataset2)

        # indices1 = np.floor(np.arange(0, len1 + len2, (1 + len2 / len1))).astype('int')

        indices1 = (np.linspace(0, 1, len1)*(len1 + len2 - 1)).astype('int')

        self.from1 = np.zeros(len1 + len2, dtype='bool')
        self.from1[indices1] = True

        assert np.sum(self.from1) == len(dataset1)

        self.sample_ids, i1, i2 = [], 0, 0
        self.rel_index = []  # the actual indices inside the datasets
        for f in self.from1:
            if f:
                self.sample_ids += [self.dataset1.sample_ids[i1]]
                self.rel_index += [i1]
                i1 += 1
            else:
                self.sample_ids += [self.dataset2.sample_ids[i2]]
                self.rel_index += [i2]
                i2 += 1

        self.sample_ids = tuple(self.sample_ids)
        self.rel_index = tuple(self.rel_index)
        assert len(self.sample_ids) == len1 + len2

        if model_config is None:
            assert_equal_elements(dataset1.model_config, dataset2.model_config,
                                  context='Interleaved datasets model config')
            self.model_config = dataset1.model_config
        else:
            self.model_config = model_config

        self.sample_ids = tuple(self.sample_ids)
        self.rel_index = tuple(self.rel_index)
        assert len(self.sample_ids) == len1 + len2

    def __getitem__(self, index):
        dataset = self.dataset1 if self.from1[index] else self.dataset2
        return dataset[self.rel_index[index]]


def parse_dataset_def(dataset):

    inp, out = dataset.data_types.split('->')
    inp = inp.split(',')
    out = out.split(',')

    regexp = r'^(\w*)(\((\w*)\))?$'

    try:
        inp = [re.match(regexp, s).groups() for s in inp]
        inp = [(s[0], s[2] if s[2] is not None else s[0]) for s in inp]
        # inp = [(s[1], s.group(2) if s is not None else None) for s in inp]

        out = [re.match(regexp, s).groups() for s in out]
        out = [(s[0], s[2] if s[2] is not None else s[0]) for s in out]
        # out = [(s.group(1), s.group(2) if s is not None else None) for s in out]

        return inp, out
    except AttributeError as e:
        logger.warning('Failed to parse datatype string: {}'.format(dataset.data_types))
