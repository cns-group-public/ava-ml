## What is Ava ML?

Ava ML is a toolkit that simplifies supervised training of neural networks for research purposes. 
It is designed facilitate many tasks common in applying deep neural networks.

Features of Ava ML include:

- **Standard models, datasets and metrics:** Avoids re-implementations and allows for a quick start. 
Datasets can be downloaded with a single command and shared with other users on the same system.

- **Feature Extractors:** A large number implemented feature extractors pre-trained on ImageNet
(via the `pretrainedmodels` python package)

- **Command Line Interface:** Train and evaluate models by running simple instructions in the command line.

- **Experiment definitions:** Facilitates reproducibility and describes hyperparameters 
in a transparent way. Direct output of latex tables is possible.

- **Visual inspection of datasets:** Enables verification that datasets (including their parameters) 
are properly implemented.

- **Standard Structure:** The division into models, datasets and metrics facilitates 
collaboration in projects when multiple persons are involved.

Note, this is an early preview version not meant to be used in production.