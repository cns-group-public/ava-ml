import os
import random
import re
from collections import Counter

import numpy as np

from os.path import join

from ..transformations import random_crop, tensor_resize, imread
from .. import PATHS
from ..core.dataset import DatasetBase


class ImageNet(DatasetBase):

    def __init__(self, subset, image_size=(224, 224), binary_loss=False, moving_sequence=None, shuffle=False):
        super().__init__('image->C')
        # self.lock = threading.Lock()

        self.image_size = image_size
        self.binary_loss = binary_loss
        self.moving_sequence = moving_sequence

        try:
            all_files = open(join(PATHS['IMAGE_NET'], 'list_of_files.txt')).read().split()
        except FileNotFoundError:
            raise FileExistsError('The list of files could not be found. You might need to run: '
                                  '"ava tool install-dataset ImageNet"')

        self.classes = sorted(list(set(f[:9] for f in all_files)))

        if subset == 'train':
            all_files = [s for s in all_files if hash(s) % 15 < 10]
        elif subset == 'val':
            all_files = [s for s in all_files if 10 <= hash(s) % 15 < 12]
        elif subset == 'test':
            all_files = [s for s in all_files if 12 <= hash(s) % 15 < 14]

        elif subset == 'train+':
            all_files = [s for s in all_files if hash(s) % 15 < 14]
        elif subset == 'val+':
            all_files = [s for s in all_files if 14 <= hash(s) % 15]
        elif subset == 'test+':
            raise ValueError('subset test+ does not exist')

        if shuffle:
            random.shuffle(all_files)

        class_counts = Counter(s[:9] for s in all_files)
        print('class frequencies, min: {}, max: {}'.format(min(class_counts.values()), max(class_counts.values())))

        some_classes = [sorted(class_counts.keys())[i] for i in [0, 1, 2, 3, 50, 100, 200, 300]]
        print(some_classes)
        print([class_counts[c] for c in some_classes])

        self.sample_ids = tuple(all_files)

        self.model_config.update({'n_classes': len(self.classes)})

        if self.moving_sequence is not None:
            self.model_config['n_frames'] = self.moving_sequence
            self.dataset_types = (('ImageSlice', {'maps_dim': 1, 'channel_dim': 0, 'color': True}), None)

        if binary_loss:
            self.model_config['multiclass'] = 1

    def __getitem__(self, index):
        filename = self.sample_ids[index]

        label_name = filename[:filename.index('/')]

        img = imread(join(PATHS['IMAGE_NET'], filename))

        if self.moving_sequence is not None:
            img = tensor_resize(img, (self.image_size[0] + 25, self.image_size[0] + 25), interpret_as_min_bound=True)
            d = np.random.uniform(0, 1), np.random.uniform(0, 1)

            space = img.shape[0] - self.image_size[0], img.shape[1] - self.image_size[1]
            pad = space[0] if d[0] < 0 else 0, space[1] if d[1] < 0 else 0

            images = []
            for i in range(self.moving_sequence):
                y = int(pad[0] + (i / self.moving_sequence) * d[0] * space[0])
                x = int(pad[1] + (i / self.moving_sequence) * d[1] * space[1])
                img_local = img[y: y + self.image_size[0], x: x+self.image_size[1]]
                # print(y, x, img_local.shape)
                images += [img_local]

            img = np.array(images)
            img = img.transpose([3, 0, 1, 2])
            img = img[[2, 1, 0]]
            img = np.ascontiguousarray(img, np.uint8)
        else:
            img = tensor_resize(img, self.image_size, interpret_as_min_bound=True)
            img = random_crop(img, self.image_size)
            img = img[:, :, [2, 1, 0]]
            img = img.transpose([2, 0, 1])

        # print(img.shape)

        if self.binary_loss:
            label = np.zeros(len(self.classes), dtype='float32')
            label[self.classes.index(label_name)] = 1
        else:
            label = self.classes.index(label_name)

        return (img,), (label,)

    @staticmethod
    def install():

        assert os.listdir(PATHS['IMAGE_NET']), 'you need to download ImageNet by yourself'

        with open(join(PATHS['IMAGE_NET'], 'list_of_files.txt'), 'w') as fh:
            for dirpath, dirnames, filenames in os.walk(PATHS['IMAGE_NET']):
                dirpath = os.path.relpath(dirpath, PATHS['IMAGE_NET'])

                if dirpath != '.':
                    for f in filenames:
                        fh.write(f'{dirpath}/{f}\n')
            fh.close()


class ImageNetMulti(DatasetBase):

    def __init__(self, subset, image_size=(224, 224), n_objects_range=(1, 3)):
        super().__init__('image->M')

        self.image_size = tuple(image_size)
        self.n_objects_range = n_objects_range

        self.classes = [f for f in os.listdir(join(PATHS['IMAGENET'])) if len(f) == 9 and f[0] == 'n']
        all_files = open(join(PATHS['IMAGENET'], 'list_of_files.txt'))
        all_files = [f.strip() for f in all_files if re.match(r'^n[0-9]*', f)]

        if subset == 'train':
            all_files = [s for s in all_files if hash(s) % 15 < 10]
        elif subset == 'val':
            all_files = [s for s in all_files if 10 <= hash(s) % 15 < 12]
        elif subset == 'test':
            all_files = [s for s in all_files if 12 <= hash(s) % 15 < 14]

        self.all_files = all_files
        self.sample_ids = tuple(range(10000))

        self.model_config.update({'n_classes': len(self.classes), 'multiclass': 1})

    def __getitem__(self, index):

        label_vec = np.zeros(len(self.classes), dtype='float32')
        full_img = np.zeros(self.image_size + (3,), dtype='float32')
        bboxes = []

        n_images = np.random.choice(range(self.n_objects_range[0], self.n_objects_range[1] + 1))

        for k in range(n_images):
            index = np.random.choice(len(self.sample_ids))
            filename = self.all_files[index]
            label = filename[:filename.index('/')]

            img = imread(join(PATHS['IMAGENET'], filename))

            half_size = int(self.image_size[0] / 2.6), int(self.image_size[1] / 2.6)
            img = tensor_resize(img, half_size, interpret_as_min_bound=True)
            # img = random_crop(img, half_size)

            if self.image_size[0] - img.shape[0] <= 0 or self.image_size[1] - img.shape[1] <= 0:
                break

            this_mask = np.zeros((self.image_size[0] - img.shape[0], self.image_size[1] - img.shape[1]), dtype='bool')

            for y_min, y_max, x_min, x_max in bboxes:
                # print(y_min, y_max, x_min, x_max, part_img.shape)
                this_mask[y_min: y_max, x_min: x_max] = 1
                this_mask[max(0, y_min - img.shape[0]): y_min, x_min: x_max] = 1
                this_mask[max(0, y_min - img.shape[0]): y_min, max(0, x_min - img.shape[1]): x_min] = 1
                this_mask[y_min: y_max, max(0, x_min - img.shape[1]): x_min] = 1

            indices = np.argwhere(this_mask == 0)

            if len(indices) == 0:
                # logger.warning('Not enough space for another image')
                break

            pos_y, pos_x = indices[np.random.choice(len(indices))]

            bboxes += [[pos_y, pos_y + img.shape[0], pos_x, pos_x + img.shape[1]]]

            full_img[pos_y:pos_y + img.shape[0], pos_x:pos_x + img.shape[1]] = img

            label_vec[self.classes.index(label)] = 1

        full_img = full_img[:, :, [2, 1, 0]]
        full_img = full_img.transpose([2, 0, 1])

        return (full_img,), (label_vec,)


class ImageNetMoving(ImageNet):

    def __init__(self, subset, sequence_length=10, shuffle=False, **kwargs):
        super().__init__(subset, moving_sequence=sequence_length, shuffle=shuffle, **kwargs)
        self.data_types = 'video->C'

