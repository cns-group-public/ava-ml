import numpy as np

from os.path import join

from ..transformations.io import imread
from ..transformations import tensor_resize, random_crop
from ..core.dataset import DatasetBase
from .. import PATHS
from ..core.logging import logger


class CUB200_2011(DatasetBase):

    def __init__(self, subset, image_size=352):
        super().__init__('image->C')

        self.images = []
        self.labels = []
        self.augmentation = subset != 'test'
        self.image_size = image_size

        with open(join(PATHS['CUB200_2011'], 'CUB_200_2011', 'train_test_split.txt'), 'r') as fh:
            lines = fh.read().split('\n')
            split = [l.split(' ')[1] for l in lines if len(l) > 0]
      
        with open(join(PATHS['CUB200_2011'], 'CUB_200_2011', 'images.txt'), 'r') as fh:
            lines = fh.read().split('\n')
            images = [l.split(' ')[1] for l in lines if len(l) > 0]

        with open(join(PATHS['CUB200_2011'], 'CUB_200_2011', 'image_class_labels.txt'), 'r') as fh:
            lines = fh.read().split('\n')
            labels = [l.split(' ')[1] for l in lines if len(l) > 0]

        assert len(split) == len(images) == len(labels)

        logger.warning('There is an intersection between CUB200_2011 test and ImageNet training dataset')

        target_split = {
            'train': '1',
            'train_train': '1',
            'train_val': '1',
            'test': '0'
        }[subset]

        valid = {
            'train_train': lambda i: i % 20 != 0,
            'train_val': lambda i: i % 20 == 0,
            'train': lambda i: True,
            'test': lambda i: True
        }[subset]

        self.sample_ids = tuple((img, label) for i, (img, split, label) in enumerate(zip(images, split, labels))
                                if split == target_split and valid(i))

        self.default_loss = 'cross_entropy'
        self.model_config = {'n_classes': 200}

    def __getitem__(self, index):
        img_filename, label = self.sample_ids[index]

        img = imread(join(PATHS['CUB200_2011'], 'CUB_200_2011', 'images', img_filename))

        if self.augmentation:
            img_size = int(self.image_size * np.random.uniform(0.9, 1.1))
            img_size = max(self.image_size, img_size + 10)
            img = tensor_resize(img, (img_size, img_size), interpret_as_min_bound=True)
            img = random_crop(img, (self.image_size, self.image_size))

        img = img[:, :, [2, 1, 0]]
        img = img.transpose([2, 0, 1])
        img = img.astype('float32')

        return (img,), (int(label)-1,)

    @staticmethod
    def install():
        from ..core.common.generic import download_file, extract_archive
        import os

        target = join(PATHS['CUB200_2011'], 'cub.tgz')
        download_file('http://www.vision.caltech.edu/visipedia-data/CUB-200-2011/CUB_200_2011.tgz', target)
        extract_archive(target, PATHS['CUB200_2011'])
        os.unlink(target)
