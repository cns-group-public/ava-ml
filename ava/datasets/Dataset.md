Custom implementations for several dataset are provided. A list can be found below.

### Writing a Dataset

#### Data Type

The data type describes the input and ground truth of the dataset. 
For image classification this would be `image(input_image)->C(class)`. 
Names can be optionally provided in brackets.

```python
super().__init__('image(input_image)->C(class)')
```

- **`C`**: categorical
- **`M`**: multi-label
- **`denseC`**: pixel-wise categorical
- **`denseM`**: pixel-wise multi-label
- **`image`**: RGB image
- **`video`**: RGB video
- **`videoGS`**: greyscale video

#### Required Methods
- `__init__(subset, seed)`: Initialization of the dataset
    - `subset`: Defines the subset.
    - `seed`: Can be used to randomly generate different subsets. Useful for cross-validation.
- `__getitem__(idx)`:
    - `idx`: index into the samples array, i.e. draws the `idx`-th sample.

#### Special Attributes
- `sample_ids`: List of sample identifiers. Its length determines the dataset length. 
This attribute should be set in the `__init__` method.
- `default_loss`: Default loss function to be used.
- `default_metrics`: List of default metrics.
- `model_config`: Arguments to be passed to the model. E.g. number of classes
- `visualization_hooks`: Dictionary of functions to apply on input and outputs. 
E.g. convert classification id back to name.
- `additional_visualizations`: Additional visualizations, combining different inputs and outputs.
