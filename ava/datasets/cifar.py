import os
import pickle

import numpy as np

from ..transformations import apply_gamma_offset
from ..core.dataset import DatasetBase
from .. import PATHS


class Cifar10(DatasetBase):

    def __init__(self, subset, greyscale=False):
        super().__init__('image->C')

        self.images = []
        self.labels = []
        self.augmentation = False
        self.greyscale = greyscale

        if subset == 'train':
            sets = ['data_batch_1', 'data_batch_2', 'data_batch_3', 'data_batch_4']
            self.augmentation = True
        elif subset == 'val':
            sets = ['data_batch_5']
        elif subset == 'test':
            sets = ['test_batch']
        else:
            raise ValueError('invalid subset')

        for s in sets:
            with open(os.path.join(PATHS['CIFAR10'], s), 'rb') as fh:
                obj = pickle.load(fh, encoding='bytes')
                print(obj.keys())
                self.images += [obj[b'data']]
                self.labels += [obj[b'labels']]

        self.images = np.concatenate(self.images)
        self.labels = np.concatenate(self.labels)
        self.images = self.images.reshape((len(self.images), 3, 32, 32))
        self.sample_ids = tuple(range(len(self.images)))

        self.images = self.images.astype('uint8')
        self.labels = self.labels.astype('int64')

        self.default_loss = 'cross_entropy'
        self.default_metrics = ['Accuracy']
        self.model_config = {'n_classes': 10}

        if self.greyscale:
            self.model_config['input_channels'] = 1

    def __getitem__(self, index):

        img = self.images[index]

        if self.greyscale:
            img = img[1:2, :, :]

        if self.augmentation:
            if np.random.random() > 0.5:
                img = img[:, :, :-1]

            img = np.pad(img, [(0, 0), (2, 2), (2, 2)], mode='reflect')
            offset_x, offset_y = np.random.randint(0, 4), np.random.randint(0, 4)
            img = img[:, offset_y:offset_y+32, offset_x: offset_x+32]

            if not self.greyscale:
                img = img.transpose([1, 2, 0])
                gamma_all = 2 / (np.random.normal(1, 0.5, 1) + np.random.normal(1, 0.1, 3))
                img = apply_gamma_offset(img, gamma=gamma_all, offset=np.random.normal(0, 2, 3), channel_dim=2)
                img = img.transpose([2, 0, 1])

        img = img.astype('float32')

        return (img,), (self.labels[index],)

    @staticmethod
    def install():
        from ..core.common.generic import download_file, extract_archive
        from os.path import join

        url = 'https://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz'
        download_file(url, join(PATHS['CIFAR10'], 'cifar.tar.gz'))
        extract_archive(join(PATHS['CIFAR10'], 'cifar.tar.gz'), PATHS['CIFAR10'])
