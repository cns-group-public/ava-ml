import os
from collections import Counter, defaultdict
from os.path import join

import cv2
import pickle
import numpy as np

from ..core.logging import logger
from ..transformations import random_crop, tensor_resize, cache_write_pickle, cache_write_numpy, cache_read
from ..core.dataset import DatasetBase
from .. import PATHS


def load_class_names(class_description_filename):
    with open(class_description_filename, 'r') as fp:
        classes = fp.read().split('\n')
        classes = [c.split(',') for c in classes if len(c) > 0]
        identifiers, class_names = [c[0] for c in classes], [c[1] for c in classes]
        identifiers_to_idx = {ident: i for i, ident in enumerate(identifiers)}
    
    return class_names, identifiers_to_idx




class OpenImagesCount(OpenImagesBase):

    def __init__(self, subset, count=0, image_size=500, min_objects=1):
        super().__init__(subset, count, image_size)

        self.model_config.update({'n_classes': len(self.class_names), 'multiclass': count if count else 1})

        # remove scenes with less than N objects
        self.img_indices = {k: v for k, v in self.img_indices.items() if v[1] >= min_objects}
        self.sample_ids = tuple(self.img_indices.keys())

        self.visualization_hooks['bboxes'] = lambda x: ', '.join([self.class_names[i] + '({:.0f})'.format(x[i])
                                                                  for i in np.argwhere(x > 0)[:, 0]])

    def __getitem__(self, index):

        img_id = self.sample_ids[index]
        start, length = self.img_indices[img_id]

        labels = self.bbox_labels[start: start + length]

        if self.count == 0:
            indicator_vec = np.zeros(len(self.class_names), dtype='float32')
            occ_label_ids = list(set(labels))

            # print([self.class_names[i] for i in occ_label_ids])
            indicator_vec[np.array(occ_label_ids)] = 1

        elif self.count == 4:
            indicator_vec = np.zeros(len(self.class_names), dtype='int64')
            occ_label_counts = Counter(labels)
            for l, count in occ_label_counts.items():
                if count < 2:
                    indicator_vec[l] = count
                elif count < 7:
                    indicator_vec[l] = 2
                else:
                    indicator_vec[l] = 3

        elif self.count == 10:
            indicator_vec = np.zeros(len(self.class_names), dtype='int64')
            occ_label_counts = Counter(labels)
            for l, count in occ_label_counts.items():
                indicator_vec[l] = min(count, 9)
        else:
            raise ValueError('Invalid count value')

        image_filename = join(PATHS['OPEN_IMAGES'], self.images_subfolder, img_id + '.jpg')
        img = cv2.imread(image_filename, flags=cv2.IMREAD_COLOR)

        assert img is not None, 'image not found: {}'.format(image_filename)

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        # print(img.shape)
        if min(img.shape[:2]) < self.image_size:
            img = tensor_resize(img, (self.image_size, self.image_size), interpret_as_min_bound=True)
        else:
            factor = (self.image_size + 20) / min(img.shape[:2])
            img = tensor_resize(img, factor)

        img = random_crop(img, (self.image_size, self.image_size))

        # img = tensor_resize(img, (self.image_size, self.image_size))

        img = img.transpose([2, 0, 1])
        img = img.astype('float32')

        return (img,), (indicator_vec,)


class OpenImagesEpic(OpenImagesCount):

    def __init__(self, subset, count=0, image_size=500, min_objects=1):
        super().__init__(subset, count, image_size, min_objects)

        valid_classes = ['Coin', 'Light bulb', 'Toy', 'Doll', 'Balloon', 'Dice', 'Flying disc', 'Kite',
                         'Teddy bear', 'Home appliance', 'Washing machine', 'Toaster', 'Oven', 'Blender', 'Gas stove',
                         'Mechanical fan', 'Heater', 'Kettle', 'Hair dryer', 'Refrigerator', 'Wood-burning stove',
                         'Humidifier', 'Mixer', 'Coffeemaker', 'Microwave oven', 'Dishwasher', 'Sewing machine',
                         'Hand dryer', 'Ceiling fan', 'Plumbing fixture', 'Sink', 'Bidet', 'Shower', 'Tap', 'Bathtub',
                         'Toilet', 'Office supplies', 'Scissors', 'Calculator', 'Box', 'Stapler',
                         'Whiteboard', 'Pencil sharpener', 'Eraser', 'Fax', 'Adhesive tape', 'Ring binder',
                         'Pencil case', 'Plastic bag', 'Paper cutter', 'Toilet paper', 'Envelope', 'Pen', 'Paper towel',
                         'Pillow', 'Kitchenware', 'Kitchen utensil', 'Chopsticks', 'Ladle', 'Spatula', 'Can opener',
                         'Cutting board', 'Whisk', 'Drinking straw', 'Knife', 'Bottle opener', 'Measuring cup',
                         'Pizza cutter', 'Spoon', 'Fork', 'Tableware', 'Chopsticks', 'Teapot', 'Mug', 'Coffee cup',
                         'Salt and pepper shakers', 'Mixing bowl', 'Saucer', 'Cocktail shaker', 'Bottle', 'Bowl',
                         'Plate', 'Pitcher', 'Kitchen knife', 'Jug', 'Platter', 'Wine glass', 'Spoon', 'Fork',
                         'Serving tray', 'Cake stand', 'Frying pan', 'Wok', 'Spice rack', 'Kitchen appliance', 'Oven',
                         'Blender', 'Slow cooker', 'Food processor', 'Refrigerator', 'Waffle iron', 'Mixer',
                         'Coffeemaker', 'Microwave oven', 'Pressure cooker', 'Dishwasher', 'Fireplace', 'Countertop',
                         'Book', 'Furniture', 'Chair', 'Cabinetry', 'Desk', 'Wine rack', 'Couch', 'Sofa bed',
                         'Loveseat', 'Wardrobe', 'Nightstand', 'Bookcase', 'Bed', 'Infant bed', 'Studio couch',
                         'Filing cabinet', 'Table', 'Coffee table', 'Kitchen & dining room table', 'Chest of drawers',
                         'Cupboard', 'Bench', 'Drawer', 'Stool', 'Shelf', 'Wall clock', 'Bathroom cabinet', 'Closet',
                         'Dog bed', 'Cat furniture', 'Lantern', 'Clock', 'Alarm clock', 'Digital clock', 'Wall clock',
                         'Vase', 'Window blind', 'Curtain', 'Bust',
                         'Picture frame', 'Candle', 'Lamp', 'Bathroom accessory', 'Towel',
                         'Toilet paper', 'Soap dispenser', 'Facial tissue holder', 'Beehive', 'Tent', 'Parking meter',
                         'Traffic light', 'Billboard', 'Traffic sign', 'Stop sign', 'Fire hydrant', 'Fountain',
                         'Street light', 'Jacuzzi', 'Tree house', 'Lighthouse', 'Skyscraper', 'Castle',
                         'Tower', 'House', 'Office building', 'Convenience store', 'Tool', 'Container', 'Tin can', 'Barrel', 'Bottle',
                         'Picnic basket', 'Jug', 'Waste container', 'Beaker', 'Flowerpot', 'Ladder', 'Toothbrush',
                         'Screwdriver', 'Drill', 'Chainsaw', 'Wrench', 'Flashlight', 'Scissors', 'Ratchet',
                         'Kitchen utensil', 'Chopsticks', 'Ladle', 'Spatula', 'Can opener', 'Cutting board', 'Whisk',
                         'Drinking straw', 'Knife', 'Bottle opener', 'Measuring cup', 'Pizza cutter', 'Spoon', 'Fork',
                         'Hammer', 'Scale', 'Snowplow', 'Nail', 'Tripod', 'Torch', 'Chisel', 'Axe', 'Camera', 'Grinder',
                         'Ruler', 'Binoculars', 'Cassette deck', 'Headphones', 'Laptop', 'Computer keyboard', 'Printer',
                         'Computer mouse', 'Computer monitor', 'Light switch',
                         'Musical keyboard', 'Television', 'Telephone', 'Mobile phone', 'Corded phone',
                         'Tablet computer', 'Microphone', 'Ipod', 'Remote control', 'Swimming pool', 'Food',
                         'Fast food', 'Hot dog', 'French fries', 'Waffle', 'Pancake', 'Burrito', 'Snack', 'Pretzel',
                         'Popcorn', 'Cookie', 'Dessert', 'Muffin', 'Cookie', 'Ice cream', 'Cake', 'Candy', 'Guacamole',
                         'Fruit', 'Apple', 'Grape', 'Common fig', 'Pear', 'Strawberry', 'Tomato', 'Lemon', 'Banana',
                         'Orange', 'Peach', 'Mango', 'Pineapple', 'Grapefruit', 'Pomegranate', 'Watermelon',
                         'Cantaloupe', 'Egg', 'Baked goods', 'Pretzel', 'Bagel', 'Muffin', 'Cookie', 'Bread', 'Pastry',
                         'Doughnut', 'Croissant', 'Tart', 'Pasta', 'Pizza', 'Seafood', 'Squid', 'Shellfish',
                         'Oyster', 'Lobster', 'Shrimp', 'Crab', 'Taco', 'Cooking spray', 'Vegetable', 'Cucumber',
                         'Radish', 'Artichoke', 'Potato', 'Tomato', 'Asparagus', 'Squash', 'Pumpkin', 'Zucchini',
                         'Cabbage', 'Carrot', 'Salad', 'Broccoli', 'Bell pepper', 'Winter melon',
                         'Sandwich', 'Hamburger', 'Submarine sandwich', 'Dairy', 'Cheese', 'Milk', 'Sushi',
                         'Land vehicle', 'Cart', 'Bicycle', 'Bicycle wheel',
                         'Shorts', 'Dress', 'Swimwear', 'Brassiere', 'Tiara', 'Shirt', 'Coat',
                         'Suit', 'Hat', 'Cowboy hat', 'Fedora', 'Sombrero', 'Sun hat', 'Scarf', 'Skirt', 'Miniskirt',
                         'Jacket', 'Fashion accessory', 'Glove', 'Baseball glove', 'Belt', 'Sunglasses', 'Tiara',
                         'Necklace', 'Sock', 'Earrings', 'Tie', 'Goggles', 'Hat', 'Cowboy hat', 'Fedora', 'Sombrero',
                         'Sun hat', 'Scarf', 'Handbag', 'Watch', 'Umbrella', 'Glasses', 'Crown',

                         'Backpack', 'Suitcase', 'Briefcase', 'Handbag', 'Helmet', 'Bicycle helmet',
                         'Football helmet', 'Cosmetics', 'Face powder', 'Hair spray', 'Lipstick', 'Perfume',
                         'Personal care', 'Toothbrush', 'Sunglasses', 'Goggles', 'Crutch', 'Cream', 'Diaper', 'Glasses',
                         'Wheelchair', 'Musical instrument', 'Organ', 'Banjo', 'Cello', 'Drum', 'Horn', 'Guitar',
                         'Harp', 'Harpsichord', 'Harmonica', 'Musical keyboard', 'Oboe', 'Piano', 'Saxophone',
                         'Trombone', 'Trumpet', 'Violin', 'Chime', 'Flute', 'Accordion', 'Maracas', 'Tool', 'Container',
                         'Tin can', 'Barrel', 'Bottle', 'Picnic basket', 'Jug', 'Waste container', 'Beaker',
                         'Flowerpot', 'Ladder', 'Toothbrush', 'Screwdriver', 'Drill', 'Chainsaw', 'Wrench',
                         'Flashlight', 'Scissors', 'Ratchet',
                         'Kitchen utensil', 'Chopsticks', 'Ladle', 'Spatula',
                         'Can opener', 'Cutting board', 'Whisk', 'Drinking straw', 'Knife', 'Bottle opener',
                         'Measuring cup', 'Pizza cutter', 'Spoon', 'Fork', 'Hammer', 'Scale', 'Snowplow', 'Nail',
                         'Tripod', 'Torch', 'Chisel', 'Axe', 'Camera', 'Grinder', 'Ruler', 'Binoculars',
                         'Cassette deck', 'Headphones', 'Laptop', 'Computer keyboard', 'Printer', 'Computer mouse',
                         'Computer monitor', 'Light switch', 'Television',
                         'Telephone', 'Mobile phone', 'Corded phone', 'Tablet computer', 'Microphone', 'Ipod',
                         'Remote control', 'Drink', 'Beer', 'Cocktail', 'Coffee', 'Juice', 'Tea', 'Wine',
                         'Door handle', 'Door', 'Door handle', 'Window', 'Stairs',
                         'Seat belt', 'Coconut']

        avoid_classes = ['Person', 'Human face', 'Human head', 'Building', 'House', 'Truck', 'Car', 'Motorcycle', 'Train',
                         'Woman', 'Man', 'Guitar', 'Boy', 'Girl', 'Sculpture', 'Tree', 'Land vehicle', 'Shorts', 'Human leg',
                         'Human leg', 'Mammal', 'Skyscraper', 'Bicycle', 'Tower', 'Street light', 'Traffic sign', 'Window',
                         'Jeans', 'Boot', 'High heels', 'Suit', 'Wheel', 'Roller skates']

        valid_class_ids = {self.class_names.index(c) for c in valid_classes}
        avoid_class_ids = {self.class_names.index(c) for c in avoid_classes}

        # at least one object in the image needs to belong the classes defined above
        self.img_indices = {k: v for k, v in self.img_indices.items()
                            if any([self.bbox_labels[j] in valid_class_ids for j in range(v[0], v[0] + v[1])]) and
                            all([self.bbox_labels[j] not in avoid_class_ids for j in range(v[0], v[0] + v[1])])}

        self.sample_ids = tuple(self.img_indices.keys())



class OpenImagesComposition(DatasetBase):

    # TODO: make sure the classes are sampled with equal likelihood

    def __init__(self, subset, image_size=(300, 300), con=0.2, scale=1.0, background='random', n_objects_range=(0, 3),
                 center=False, full_extent=False):
        super().__init__(('img', 'labels'), dataset_types=('Image', None))

        self.center = center
        self.n_objects_range = n_objects_range
        self.image_size = tuple(image_size)
        self.con = con
        self.background = background
        self.full_extent = full_extent

        oa = OpenImagesBase('train')
        self.oa = oa

        print('load rot and subsets...')
        self.rot_subset = pickle.load(open(join(PATHS['OPEN_IMAGES'], 'dict_rotations_subset.pickle'), 'rb'))
        print('done')

        # shortcut
        ii, bl, bb = oa.img_indices, oa.bbox_labels, oa.bboxes

        # speed-up TODO: remove
        # ii = ii[:10000], bl[:10000], bb[:10000]

        small_objs = ['Mug', 'Coffee cup', 'Knife', 'Fork', 'Spoon', 'Apple', 'Orange', 'Banana', 'Plate', 'Tableware',
                      'Chopsticks', 'Teapot', 'Salt and pepper shakers', 'Mixing bowl', 'Saucer', 'Cocktail shaker',
                      'Bottle', 'Bowl', 'Plate', 'Pitcher', 'Kitchen knife', 'Jug', 'Platter', 'Wine glass',
                      'Serving tray', 'Cake stand', 'Frying pan', 'Wok', 'Blender',
                      'Mixer', 'Toaster']

        large_objs = [
            # appliances
            'Dishwasher', 'Refrigerator', 'Washing machine', 'Gas stove', 'Oven',

            # furniture
            'Chair', 'Cabinetry', 'Desk', 'Wine rack', 'Couch', 'Sofa bed', 'Loveseat', 'Wardrobe',
            'Nightstand', 'Bookcase', 'Bed', 'Infant bed', 'Studio couch', 'Filing cabinet', 'Table',
            'Coffee table', 'Kitchen & dining room table', 'Chest of drawers', 'Cupboard', 'Drawer', 'Closet',
            'Shelf',
        ]

        self.foreground_class_names = small_objs + large_objs
        self.sizes = defaultdict(lambda: (0.15 * scale, 0.4 * scale), {})

        # large objects
        self.sizes.update({o: (0.3 * scale, 0.4 * scale) for o in large_objs})

        self.fg_classes = list(oa.class_names.index(n) for n in self.foreground_class_names)

        # remove rare classes
        counts = Counter(bl)
        self.fg_freq = [counts[i] for i in self.fg_classes]
        valid_classes = [i for i in range(len(self.fg_classes)) if counts[self.fg_classes[i]] > 1000]
        self.foreground_class_names = [self.foreground_class_names[i] for i in valid_classes]
        self.fg_classes = [self.fg_classes[i] for i in valid_classes]

        self.fg_classes_set = set(self.fg_classes)
        print('found {} frequent classes'.format(len(self.fg_classes)))


        # without foreground classes
        self.bg_scenes = [s for s in oa.sample_ids if len(self.fg_classes_set.intersection(bl[ii[s][0]: ii[s][0] + ii[s][1]])) == 0]
        self.fg_scenes = [s for s in oa.sample_ids if len(self.fg_classes_set.intersection(bl[ii[s][0]: ii[s][0] + ii[s][1]])) > 0]

        # self.fg_scenes = [s for s in self.fg_scenes if self.rot_subset[s][0] == '90']
        print('found {} fg scenes and {} bg scenes'.format(len(self.fg_scenes), len(self.bg_scenes)))

        if subset == 'train':
            self.fg_scenes = [s for s in self.fg_scenes if hash(s) % 15 < 10]
        elif subset == 'val':
            self.fg_scenes = [s for s in self.fg_scenes if 10 <= hash(s) % 15 < 12]
        elif subset == 'test':
            self.fg_scenes = [s for s in self.fg_scenes if 12 <= hash(s) % 15 < 14]

        # does not matter as samples are randomly generated
        self.sample_ids = tuple(range(5000))

        self.visualization_hooks['labels'] = lambda x: ', '.join([self.foreground_class_names[i] + '({:.0f})'.format(x[i])
                                                                  for i in np.argwhere(x > 0)[:, 0]])

        self.model_config.update({'n_classes': len(self.fg_classes), 'multiclass': 1})

    def correct_rotation(self, scene_id, img, bbox):
        if self.rot_subset[scene_id][0] == '270':
            img = img.transpose([1, 0, 2])
            bbox = [bbox[2], bbox[3], bbox[0], bbox[1]]
        elif self.rot_subset[scene_id][0] == '90':
            img = img.transpose([1, 0, 2])[::-1]
            bbox = [bbox[2], bbox[3], bbox[0], bbox[1]]
        elif self.rot_subset[scene_id][0] == '180':
            img = img.transpose([1, 0, 2])

        return img, bbox


    def __getitem__(self, index):

        ii, bl, bb = self.oa.img_indices, self.oa.bbox_labels, self.oa.bboxes

        bg_idx = np.random.choice(len(self.bg_scenes))
        bg = self.bg_scenes[bg_idx]

        n_fg_objects = np.random.choice(range(self.n_objects_range[0], self.n_objects_range[1] + 1))

        fg_scene_indices = np.random.choice(len(self.fg_scenes), n_fg_objects, replace=False)
        fg_object_scenes = [self.fg_scenes[i] for i in fg_scene_indices]

        # all possible labels/bbox indices for each scene
        fg_object_scenes = [(s, [i for i, l in enumerate(bl[ii[s][0]: ii[s][0] + ii[s][1]]) if l in self.fg_classes])
                            for s in fg_object_scenes]

        # for s, indices in fg_object_scenes:
        #     print(s)
        #     for index in indices:
        #         label = [bl[ii[s][0]: ii[s][0] + ii[s][1]][index] for index in indices]
        #

        fg_object_scenes = [(s, np.random.choice(l)) for s, l in fg_object_scenes]

        if self.background == 'random':
            img = cv2.imread(join(PATHS['OPEN_IMAGES'], self.oa.images_subfolder, bg + '.jpg'))
            img = tensor_resize(img, self.image_size, interpret_as_min_bound=True)
            img = random_crop(img, self.image_size)
        elif self.background == 'black':
            img = np.zeros(self.image_size + (3,), dtype='float32')
        else:
            raise ValueError('invalid choice for background')

        bboxes = []
        labels = []

        for s, index in fg_object_scenes:
            label = bl[ii[s][0]: ii[s][0] + ii[s][1]][index]
            bbox = bb[ii[s][0]: ii[s][0] + ii[s][1]][index]
            part_img = cv2.imread(join(PATHS['OPEN_IMAGES'], self.oa.images_subfolder, s + '.jpg'))
            h, w = part_img.shape[:2]

            con_h, con_w = int(self.con * self.image_size[0]), int(self.con * self.image_size[1])
            a, b = max(0, int(bbox[2] * h) - con_h), int(bbox[3] * h) + con_h
            c, d = max(0, int(bbox[0] * w) - con_w), int(bbox[1] * w) + con_w

            part_img = part_img[a:b, c: d]
            label_name = self.foreground_class_names[self.fg_classes.index(label)]

            part_img, _ = self.correct_rotation(s, part_img, bbox)

            if not self.full_extent:
                min_size, max_size = self.sizes[label_name]

                img_size = int(np.random.uniform(min_size * self.image_size[0], max_size * self.image_size[1]))
                # img_size = min(img_size, max(img.shape[:2]) - int(0.5 * max(self.image_size)))

                part_img = tensor_resize(part_img, (img_size, img_size), interpret_as_max_bound=True)
            else:
                part_img = tensor_resize(part_img, self.image_size, interpret_as_max_bound=True)

            if self.center:
                pos_y, pos_x = self.image_size[0] // 2 - part_img.shape[0] // 2, self.image_size[1] // 2 - part_img.shape[1] // 2
            else:

                this_mask = np.zeros((self.image_size[0] - part_img.shape[0], self.image_size[1] - part_img.shape[1]),
                                     dtype='bool')
                for y_min, y_max, x_min, x_max in bboxes:
                    # print(y_min, y_max, x_min, x_max, part_img.shape)
                    this_mask[y_min: y_max, x_min: x_max] = 1
                    this_mask[max(0, y_min - part_img.shape[0]): y_min, x_min: x_max] = 1
                    this_mask[max(0, y_min - part_img.shape[0]): y_min, max(0, x_min - part_img.shape[1]): x_min] = 1
                    this_mask[y_min: y_max, max(0, x_min - part_img.shape[1]): x_min] = 1

                indices = np.argwhere(this_mask == 0)

                if len(indices) == 0:
                    break

                pos_y, pos_x = indices[np.random.choice(len(indices))]


            bboxes += [[pos_y, pos_y + part_img.shape[0], pos_x, pos_x + part_img.shape[1]]]

            labels += [label]
            # mask[pos_y:pos_y + part_img.shape[0], pos_x:pos_x + part_img.shape[1]] = 0
            img[pos_y:pos_y + part_img.shape[0], pos_x:pos_x + part_img.shape[1]] = part_img


        indicator_vector = np.zeros(len(self.fg_classes), dtype='float32')
        for l in labels:
            indicator_vector[self.fg_classes.index(l)] = 1

        img = img[:, :, [2, 1, 0]]
        img = img.transpose([2, 0, 1]).astype('float32')

        return (img,), (indicator_vector,)

