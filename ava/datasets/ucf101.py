import random
import re
from itertools import groupby
from os.path import join

import cv2
import numpy as np
from numpy.random import uniform, normal

from ..transformations import sample_equidistant, tensor_resize, cache_read, cache_write_jpg_sequence, \
    random_crop_slices, get_gamma_offset_lut, apply_gamma_offset
# from ..core.cache import DiskCachedFunctionImage, DiskCachedFunctionNumpy
from ..core.dataset import DatasetBase
from .. import PATHS


class UCF101(DatasetBase):
    """
    http://crcv.ucf.edu/data/UCF101.php

    Installation:
      mkdir UCF101; cd UCF101
      wget http://crcv.ucf.edu/data/UCF101/UCF101.rar
      unrar UCF101.rar
      http://crcv.ucf.edu/data/UCF101/UCF101TrainTestSplits-RecognitionTask.zip
      unzip UCF101TrainTestSplits-RecognitionTask.zip
    """

    def __init__(self, subset, augmentation=False, image_size=(100, 150), n_frames=5):
        super().__init__('video,video(optical_flow)->C(class)')

        self.image_size = image_size
        self.augmentation = augmentation
        self.n_frames = n_frames

        if subset in {'train', 'val'}:
            sample_names = open(join(PATHS['UCF101'], 'ucfTrainTestlist', 'trainlist01.txt')).read().split('\n')
            sample_names = tuple(re.sub(r'^(.*/)?([\w0-9_.]*) [0-9]+$', r'\2', l) for l in sample_names if len(l) > 0)

            sample_ids = []
            for _, samples in groupby(sample_names, lambda x: x[3:-11]):
                samples = sorted(list(samples))
                k = int(0.95 * len(samples))
                sample_ids += samples[:k] if subset == 'train' else samples[k:]

        elif subset == 'test':
            sample_ids = open(join(PATHS['UCF101'], 'ucfTrainTestlist', 'testlist01.txt')).read().split('\n')
            sample_ids = [re.sub(r'^(.*/)?([\w0-9_.]*)$', r'\2', l) for l in sample_ids if len(l) > 0]
        else:
            raise ValueError('invalid subset')

        # fix of the dataset
        self.sample_ids = tuple(s.replace('HandStand', 'Handstand') for s in sample_ids)

        self.labels = [re.sub(r'^v_(.*)_g[0-9]+_(.*)\.avi\s?[0-9]*$', r'\1', s) for s in self.sample_ids]
        self.labels = [l.lower() for l in self.labels]

        classes = open(join(PATHS['UCF101'], 'ucfTrainTestlist', 'classInd.txt')).read().split('\n')
        classes = [c.split(' ') for c in classes if len(c) > 0]
        self.classes = {c[1].lower(): int(c[0])-1 for c in classes}

        assert all([self.labels[i] in self.classes for i in range(len(self.sample_ids))])

        self.default_loss = 'cross_entropy'
        self.default_metrics = ['Accuracy']
        self.model_config['n_classes'] = len(self.classes)
        self.model_config['n_frames'] = self.n_frames
        self.model_config['image_size'] = self.image_size
        self.cache_name = 'ucf101_{}_{}x{}_{}_jpg_sequence'.format(self.n_frames, self.image_size[0], self.image_size[1], subset)

    def extract_frames(self, sample_name):
        cap = cv2.VideoCapture(join(PATHS['UCF101'], sample_name))
        n_video_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        assert n_video_frames > 0, sample_name

        all_frames = []
        for i in range(n_video_frames):
            _, frame = cap.read()
            if frame is not None:
                all_frames += [frame]

        out = np.zeros((self.n_frames, self.image_size[0], self.image_size[1], 3), dtype='uint8')
        indices = sample_equidistant(len(all_frames) - 2, self.n_frames)

        for i, j in enumerate(indices):
            frame = tensor_resize(all_frames[j], self.image_size)
            out[i] = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            j += 1

        return out

    def __getitem__(self, index):

        assert self.labels[index] in self.classes, 'not found: {}'.format(self.labels[index])
        label = self.classes[self.labels[index]]

        cache_file = self.cache_name + '/' + self.sample_ids[index][:-4]

        try:
            frames = cache_read(cache_file)
        except FileNotFoundError:
            frames = self.extract_frames(self.sample_ids[index])
            cache_write_jpg_sequence(frames, cache_file)

        if self.augmentation:
            s = np.random.randint(0, 20)
            new_size = (self.image_size[0] + s, self.image_size[1] + s)
            slices_y, slices_x = random_crop_slices(new_size, self.image_size)
            lut = get_gamma_offset_lut(normal(1, 0.2) + normal(0, 0.05, 3), normal(1, .2) + normal(0, 0.05, 3))

            for i in range(len(frames)):
                f = tensor_resize(frames[i], new_size)
                f = f[slices_y, slices_x]
                frames[i] = apply_gamma_offset(f, lut=lut)

        out = np.array(frames)

        if self.augmentation and np.random.rand() > 0.5:
            out = out[:, :, ::-1]

        out = out.transpose([3, 0, 1, 2])
        out = out.astype('float32')

        return (out, None), (label,)
