import os
from os.path import join

import cv2
import numpy as np
from PIL import Image
from scipy.io import loadmat

from ..core.dataset import DatasetBase
from ..core.logging import logger
from .. import PATHS
from ..transformations import tensor_resize, sample_random_patch, apply_gamma_offset, MemoryCachedFunction


class PascalVoc(DatasetBase):

    def __init__(self, subset, cache=False, augmentation=False, image_size=(224, 224), multiscale=None):
        super().__init__('image->denseC,None')

        self.multiscale = multiscale
        self.augmentation = augmentation
        self.image_size = image_size
        if subset == 'train':
            prefix_file = join(PATHS['PASCAL_VOC2012'], 'ImageSets/Segmentation', 'train.txt')
        elif subset == 'val':
            prefix_file = join(PATHS['PASCAL_VOC2012'], 'ImageSets/Segmentation', 'val.txt')
        elif subset == 'test':
            prefix_file = join(PATHS['PASCAL_VOC2012'], 'ImageSets/Segmentation', 'val.txt')  # just a random file
        else:
            raise ValueError('Invalid subset')

        with open(prefix_file) as f:
            prefixes = [p for p in f.read().split('\n') if len(p) > 0]

        self.sample_ids = tuple(prefixes)

        if subset == 'test':
            self.sample_ids = tuple([])

        self.model_config.update({'out_channels': 21})

        if self.multiscale:
            self.chunk_names = ('image',) + tuple('segmentation_scale{}'.format(s) for s in self.multiscale) + ('mask',)
            self.dataset_types = ('Image',) + tuple('LabelImage' for _ in self.multiscale) + (None,)

        if cache:
            self.prepare_sample = MemoryCachedFunction(self.prepare_sample)

    def prepare_sample(self, index):

        prefix = self.sample_ids[index]
        img = cv2.imread(join(PATHS['PASCAL_VOC2012'], 'JPEGImages',  prefix + '.jpg'), cv2.IMREAD_UNCHANGED)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        # the Pascal VOC segmentation images are indexed pngs. Since opencv can not read the index let's use PIL
        seg = np.array(Image.open(join(PATHS['PASCAL_VOC2012'], 'SegmentationClass', prefix + '.png')))
        # print(seg.shape, set(seg.flatten()))
        seg[seg == 255] = 0

        img = tensor_resize(img, (int(self.image_size[0]*1.2), int(self.image_size[1]*1.2)), interpret_as_min_bound=True)
        seg = tensor_resize(seg, (int(self.image_size[0]*1.2), int(self.image_size[1]*1.2)), interpret_as_min_bound=True, interpolation='nearest')

        return img, seg

    def __getitem__(self, index):

        img, seg = self.prepare_sample(index)

        if self.augmentation:
            slice_indices = sample_random_patch(img.shape, (self.image_size[0], self.image_size[1]))
            seg = seg[slice_indices]
            img = img[slice_indices]

            if np.random.random() > 0.5:
                img = img[:, ::-1]
                seg = seg[:, ::-1]

            img = apply_gamma_offset(img, gamma=np.random.normal(1, 0.08, 3), offset=np.random.randint(-5, 5, 3))

        img = img.transpose([2, 0, 1])

        img = img.astype('float32')

        if self.multiscale is not None:
            all_seg = []
            for s in self.multiscale:
                s = (s, s) if type(s) not in {tuple, list} else s
                all_seg += [tensor_resize(seg, s, interpolation='nearest').astype('int64')]
            return (img,), tuple(all_seg) + (None,)
        else:
            return (img,), (seg.astype('int64'), None)

    def download(self):
        pass


class SBD(DatasetBase):
    """
    http://home.bharathh.info/pubs/codes/SBD/download.html
    """

    def __init__(self, subset, cache=False, augmentation=False, image_size=(224, 224), multiscale=None):
        super().__init__('image->denseC')

        self.multiscale = multiscale
        self.augmentation = augmentation
        self.image_size = image_size

        if subset == 'train':
            prefix_file = join(PATHS['SBD'], 'dataset', 'train.txt')
        elif subset == 'train_noval':
            prefix_file = join(PATHS['SBD'], 'dataset', 'train_noval.txt')
        elif subset in {'val', 'test'}:
            prefix_file = join(PATHS['SBD'], 'dataset', 'val.txt')
        else:
            raise ValueError('Invalid subset')

        with open(prefix_file) as f:
            prefixes = [p for p in f.read().split('\n') if len(p) > 0]

        if subset == 'val':
            prefixes = prefixes[:2000]
        elif subset == 'test':
            prefixes = prefixes[2000:]

        self.sample_ids = tuple(prefixes)

        if self.multiscale:
            self.chunk_names = ('image',) + tuple('segmentation_scale{}'.format(s) for s in self.multiscale) + ('mask',)
            self.dataset_types = ('Image',) + tuple('LabelImage' for _ in self.multiscale) + (None,)

        if cache:
            logger.info('Dataset uses cache')
            self.prepare_sample = MemoryCachedFunction(self.prepare_sample)

    def prepare_sample(self, index):
        prefix = self.sample_ids[index]

        img = cv2.imread(join(PATHS['SBD'], 'dataset', 'img',  prefix + '.jpg'), cv2.IMREAD_UNCHANGED)

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        seg = loadmat(join(PATHS['SBD'], 'dataset', 'cls',  prefix + '.mat'))['GTcls'][0][0][1]

        img = tensor_resize(img, (int(self.image_size[0]*1.2), int(self.image_size[1]*1.2)), interpret_as_min_bound=True)
        seg = tensor_resize(seg, (int(self.image_size[0]*1.2), int(self.image_size[1]*1.2)), interpret_as_min_bound=True, interpolation='nearest')

        # seg[seg == 255] = 0

        return img, seg

    def __getitem__(self, index):

        img, seg = self.prepare_sample(index)

        if self.augmentation:
            slice_indices = sample_random_patch(img.shape, (self.image_size[0], self.image_size[1]))
            seg = seg[slice_indices]
            img = img[slice_indices]

            if np.random.random() > 0.5:
                img = img[:, ::-1]
                seg = seg[:, ::-1]

            # img = apply_gamma_offset(img, gamma=np.random.normal(1, 0.08, 3), offset=np.random.randint(-5, 5, 3))

        img = img.transpose([2, 0, 1])
        img = img.astype('float32')

        if self.multiscale is not None:
            all_seg = []
            for s in self.multiscale:
                s = (s, s) if type(s) not in {tuple, list} else s
                all_seg += [tensor_resize(seg, s, interpolation='nearest').astype('int64')]
            return (img,), tuple(all_seg) + (None,)
        else:
            return (img,), (seg.astype('int64'), None)


class PascalSBD(DatasetBase):
    """
    Combines PascalVOC2012 and SBD (http://home.bharathh.info/pubs/codes/SBD/download.html)

    """

    def __init__(self, subset, cache=False, augmentation=False, image_size=(224, 224), multiscale=None,
                 center_crop=False):
        super().__init__('image->denseC(segmentation),denseM(mask)')
        # super().__init__(('image', 'segmentation', 'mask'), dataset_types=('Image', 'LabelImage', None))

        self.multiscale = multiscale
        self.augmentation = augmentation
        self.image_size = image_size
        self.center_crop = center_crop

        selector = lambda x: True

        if subset == 'sbd_trainaug':  # full sbd_trainaug
            prefix_file = join(PATHS['SBD'], 'dataset', 'trainaug.txt')
        elif subset == 'sbd_trainaug_train':
            prefix_file = join(PATHS['SBD'], 'dataset', 'trainaug.txt')
            selector = lambda x: x % 9 < 3
        elif subset == 'sbd_trainaug_val':
            prefix_file = join(PATHS['SBD'], 'dataset', 'trainaug.txt')
            selector = lambda x: x % 9 >= 3
        elif subset == 'pascal_val':
            prefix_file = join(PATHS['PASCAL_VOC2012'], 'ImageSets/Segmentation', 'val.txt')
        elif subset == 'pascal_test':
            prefix_file = join(PATHS['PASCAL_VOC2012'], 'ImageSets/Segmentation', 'test.txt')
            logger.warning('There should not be ground truth data for pascal test set available.')
        else:
            raise ValueError(f'Invalid subset: {subset}')

        with open(prefix_file) as f:
            prefixes = [p for p in f.read().split('\n') if len(p) > 0]
            prefixes = [p for p in prefixes if selector(int(p[5:]))]

        self.sample_ids = tuple(prefixes)

        self.model_config.update({'out_channels': 21})
        self.default_loss = 'dense_cross_entropy'

        if cache:
            logger.info('Dataset uses cache')
            self.prepare_sample = MemoryCachedFunction(self.prepare_sample)

    def prepare_sample(self, index):
        prefix = self.sample_ids[index]

        if os.path.isfile(join(PATHS['PASCAL_VOC2012'], 'SegmentationClass', prefix + '.png')):
            img = cv2.imread(join(PATHS['PASCAL_VOC2012'], 'JPEGImages', prefix + '.jpg'), cv2.IMREAD_UNCHANGED)
            seg = np.array(Image.open(join(PATHS['PASCAL_VOC2012'], 'SegmentationClass', prefix + '.png')))
            # print(seg.shape, set(seg.flatten()))
            seg[seg == 255] = 0
        else:
            img = cv2.imread(join(PATHS['SBD'], 'dataset', 'img', prefix + '.jpg'), cv2.IMREAD_UNCHANGED)
            assert img is not None, f'failed for {prefix}'
            seg = loadmat(join(PATHS['SBD'], 'dataset', 'cls', prefix + '.mat'))['GTcls'][0][0][1]

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        return img, seg

    def __getitem__(self, index):

        img, seg = self.prepare_sample(index)

        if self.augmentation:

            # scale to be larger
            img = tensor_resize(img, (int(self.image_size[0] * 1.2), int(self.image_size[1] * 1.2)),
                                interpret_as_min_bound=True)
            seg = tensor_resize(seg, (int(self.image_size[0] * 1.2), int(self.image_size[1] * 1.2)),
                                interpret_as_min_bound=True, interpolation='nearest')

            slice_indices = sample_random_patch(img.shape, (self.image_size[0], self.image_size[1]))
            seg = seg[slice_indices]
            img = img[slice_indices]

            if np.random.random() > 0.5:
                img = img[:, ::-1]
                seg = seg[:, ::-1]

            # img = apply_gamma_offset(img, gamma=np.random.normal(1, 0.08, 3), offset=np.random.randint(-5, 5, 3))

        if self.center_crop:
            img = tensor_resize(img, self.image_size, interpret_as_min_bound=True)
            seg = tensor_resize(seg, self.image_size, interpret_as_min_bound=True, interpolation='nearest')

            offset_y, offset_x = (img.shape[0] - self.image_size[0]) // 2, (img.shape[1] - self.image_size[1]) // 2
            img = img[offset_y: offset_y + self.image_size[0], offset_x: offset_x + self.image_size[0]]
            seg = seg[offset_y: offset_y + self.image_size[0], offset_x: offset_x + self.image_size[0]]

        img = img.transpose([2, 0, 1])
        img = img.astype('float32')

        if self.multiscale is not None:
            all_seg = []
            for s in self.multiscale:
                s = (s, s) if type(s) not in {tuple, list} else s
                all_seg += [tensor_resize(seg, s, interpolation='nearest').astype('int64')]
            return (img,), tuple(all_seg) + (None,)
        else:
            return (img,), (seg.astype('int64'), None)
