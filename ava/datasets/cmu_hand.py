import json
import os
from random import shuffle

import cv2
import numpy as np

from ..core.dataset import DatasetBase
from ..core.logging import logger
from .. import PATHS
from ..transformations import tensor_resize, apply_gamma_offset


class CMUHand(DatasetBase):

    def __init__(self, subset, augmentation=False, image_size=(352, 352), intermediate_size=None,
                 label_offset=0):
        super().__init__('image->denseC,none')

        self.label_offset = label_offset
        self.augmentation = augmentation
        self.root = PATHS['CMU_HANDS']
        self.image_size = image_size
        self.intermediate_size = self.image_size if intermediate_size is None else intermediate_size

        self.selected_part_indices = [0, 1, 3, 7, 19]
        self.point_size = (self.image_size[0] + self.image_size[1]) // 15

        if subset == 'train':
            self.sub_folder_postfixes = [
                # (('hand_labels', 'manual_train'), '_l'),
                ## (('hand_labels_synth', 'synth1'), ''),
                 (('hand_labels_synth', 'synth2'), ''),
                 (('hand_labels_synth', 'synth3'), ''),
                 (('hand_labels_synth', 'synth4'), ''),
            ]

        else:
            self.sub_folder_postfixes = [(('hand_labels', 'manual_test'), '_l')]

        self.sample_ids = tuple((f[:-len(postfix + '.jpg')], sub_folder, postfix)
                                for sub_folder, postfix in self.sub_folder_postfixes
                                for f in os.listdir(os.path.join(self.root, *sub_folder))
                                if f.endswith(postfix + '.jpg'))

        self.sample_ids = list(self.sample_ids)
        shuffle(self.sample_ids)
        self.sample_ids = tuple(self.sample_ids)

        assert len(self.sample_ids) > 0, 'no samples found'

        n_parts = len(self.selected_part_indices)
        self.model_config.update({'with_mask': False, 'out_channels': n_parts + 1,
                                  'out_channel_weights': [0.01] + [.99/n_parts]*n_parts})

        self.visualization_hooks['predicted_segmentation'] = np.exp

    def __getitem__(self, index):
        prefix, sub_folder, postfix = self.sample_ids[index]
        path = os.path.join(self.root, *sub_folder)

        with open(os.path.join('{}/{}{}.json'.format(path, prefix, postfix)), 'r') as f:
            hand_seg = json.load(f)
        body_pts = [np.array(hand_seg['hand_pts'])[:, [1, 0, 2]]]

        if postfix == '_l' and os.path.isfile(os.path.join('{}/{}{}.json'.format(path, prefix, '_r'))):
            with open(os.path.join('{}/{}{}.json'.format(path, prefix, '_r')), 'r') as f:
                hand_seg = json.load(f)
            body_pts += [np.array(hand_seg['hand_pts'])[:, [1, 0, 2]].astype('float')]

        img = cv2.imread('{}/{}{}.jpg'.format(path, prefix, postfix))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        # scale to intermediate size
        img_shape_old = img.shape
        img = tensor_resize(img, (self.image_size[0], self.image_size[1]), interpret_as_min_bound=True)
        scale = img.shape[0] / img_shape_old[0]
        logger.detail('resized from ', img_shape_old, img.shape)
        for i in range(len(body_pts)):
            body_pts[i] *= scale

        offset_x, offset_y = 0, 0
        if self.augmentation:
            body_pts_mean = body_pts[0].mean(0)
            center_y, center_x = np.random.normal(body_pts_mean[0], 3), np.random.normal(body_pts_mean[1], 3)
            offset_y = min(max(0, int(center_y - self.image_size[0] / 2)), img.shape[0] - self.image_size[0])
            offset_x = min(max(0, int(center_x - self.image_size[0] / 2)), img.shape[1] - self.image_size[1])

            img = img[offset_y: offset_y + self.image_size[0], offset_x: offset_x + self.image_size[1]]
            img = apply_gamma_offset(img, gamma=np.random.normal(1, 0.08, 3), offset=np.random.randint(-5, 5, 3))

        img = img.transpose([2, 0, 1]).astype('float32')
        # part_seg = np.zeros(img.shape[:2], dtype='uint8')
        part_seg = np.zeros(img.shape[1:], dtype='int64')

        grid_y, grid_x = np.ogrid[0: part_seg.shape[0], 0:part_seg.shape[1]]
        for body_pts_object in body_pts:
            body_pts_object = body_pts_object.astype('int')

            for i, part_i in enumerate(self.selected_part_indices):
                c1, c2 = body_pts_object[part_i][0], body_pts_object[part_i][1]
                part_mask = (grid_y - c1 + offset_y)**2 + (grid_x - c2 + offset_x)**2 <= self.point_size
                # img[0, mask] = 255
                part_seg[part_mask] = i+1 + self.label_offset

        # to be compatible with other DenseNets
        mask = None

        return (img,), (part_seg, mask)
