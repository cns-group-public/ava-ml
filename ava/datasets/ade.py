import os
from collections import Counter
from functools import partial
from os.path import join, realpath, dirname

import cv2
import numpy as np

from ..losses import dense_binary_cross_entropy
from ..metrics import IoUMetric
from ..core.common.assertions import assert_file_exists
from ..core.data_types import LabelImageColorOverlayData
from ..core.logging import logger
from .. import PATHS
from ..transformations import tensor_resize, scale_to_bound, sample_random_patch, random_crop_slices, \
    apply_gamma_offset, remap, cache_write, cache_read, cache_write_jpg
from ..core.dataset import DatasetBase
from ..transformations import MemoryCachedFunction


ADE_SUBSETS = {
    'train_train': ('train', 0, 0, 19210),
    'train_val': ('train', 0, 19210, 20210),
    'train': ('train', 0, 0, 20210),
    'val': ('val', 20210, 0, 2000),
}


def load_property_map(filename, add_bg_col_zero=False, second_row_weights=False, delimiter=','):
    """
    Parse a property remapping csv file according to this scheme:
    1 means known presence of a property
    0 means known absence of a property
    An empty cell means uncertainty about the presence of the property (hence mask will be zero).
    """
    object_to_property, object_to_mask = dict(), dict()
    zero_col = [0.0] if add_bg_col_zero else []
    weights = None

    with open(filename, 'r') as f:
        lines = f.read().split('\n')
        properties = [p.strip() for p in lines[0].split(delimiter)[2:]]

        if second_row_weights:
            cols = lines[1].split(delimiter)
            weights = ([1.0] if add_bg_col_zero else []) + [float(c) if c != '' else float(0) for c in cols[2:]]

        for line in lines[1:]:
            cols = line.split(delimiter)
            if len(cols[2:]) == len(properties):
                object_to_property[cols[0].strip()] = zero_col + [float(c) if c != '' else float(0) for c in cols[2:]]
                object_to_mask[cols[0].strip()] = zero_col + [1 if c != '' else 0 for c in cols[2:]]

    if add_bg_col_zero:
        properties = ['bg'] + properties

    return object_to_property, object_to_mask, properties, weights


class ADE(DatasetBase):
    """ 
    Generic ADE dataset.
    `important_sampling_labels` Provide a list or filename that contains labels which are used to sample crops
    `remap` Filename that contains a csv file which maps labels from the original ADE names to a new space.
    `with_coverage_mask` Use first map to indicate coverage
    `no_exclusive_labels` A pixel can have more than one active map
    """

    def __init__(self, subset, remap=None, importance_sampling_labels=None,
                 importance_sampling_offset=0.0, augmentation=False,
                 no_exclusive_labels=False, remap_with_coverage_mask=False, show_hints=False, image_size=352, cache=False,
                 select_scenes=False):

        super().__init__('image->denseC(segmentation),None')

        self.augmentation = augmentation
        self.cache = cache
        self.remap = remap
        self.no_exclusive_labels = no_exclusive_labels
        self.importance_sampling_labels = importance_sampling_labels
        self.importance_sampling_offset = importance_sampling_offset  # probability of non-matching pixels
        self.select_scenes = select_scenes  # select scenes
        self.remap_with_coverage_mask = remap_with_coverage_mask
        self.target_size = image_size

        # self.intermediate_size_range = (370, 550)
        self.intermediate_size_range = (image_size, 750)
        # self.intermediate_size_range = (750, 850, 550)
        self.root_path = PATHS['ADE20K']
        self.data_root = join(PATHS['ROOT'], 'datasets', 'ade')

        print(self.data_root)

        with open(join(self.data_root, 'scene_folders.txt'), 'r') as fh:
            self.scene_paths = fh.read().split('\n')[:-1]
            self.scene_paths = ['/'.join(s.split('/')[1:]) for s in self.scene_paths]

        assert len(self.scene_paths) == 22210

        # import object info from objects.txt
        with open(os.path.join(self.data_root, 'objects.txt'), 'r') as fh:
            objects = [a.split(';') for a in fh.read().split('\n')]

        objects = [(name, int(count), float(ispart)) for name, count, ispart in objects]
        self.objects, self.object_counts, self.object_ispart = zip(*objects)

        duplicates = [o for o, c in Counter(self.objects).most_common() if c > 1]
        assert len(self.objects) == len(set(self.objects)), 'There are duplicates in objects: ' + ', '.join(duplicates)

        self.object_index = {o: i for i, o in enumerate(self.objects)}
        self.subset_path, self.offset, start, end = ADE_SUBSETS[subset]
        self.sample_ids = tuple((self.subset_path, i) for i in range(1 + start, 1 + end))

        if self.remap is not None:
            logger.important('ADE: Using remapping from file:', remap)

            property_map = load_property_map(remap, add_bg_col_zero=not self.no_exclusive_labels,
                                             second_row_weights=True, delimiter=';')
            label_remapping_in, label_remapping_mask_in, self.properties, self.property_weights = property_map

            label_remapping_in = {k: v for k, v in label_remapping_in.items() if not k.startswith('_')}
            assert all([w in {0,1} for v in label_remapping_in.values() for w in v])

            self.label_remapping, self.label_remapping_mask = dict(), dict()
            fail_due_to_unknown_object = False

            for k in label_remapping_in.keys():

                if k.startswith('_'):  # ignore this key
                    pass
                elif '/' in k:  # assume hierarchical item
                    k_tuple = []
                    for key in k.split('/'):
                        if key in self.object_index:
                            k_tuple += [self.object_index[key]]
                        elif key == '*':
                            k_tuple += ['*']
                        else:
                            logger.important('object "{}" was not found in object_indices'.format(key))
                            fail_due_to_unknown_object = True

                    self.label_remapping[tuple(k_tuple)] = label_remapping_in[k]
                    self.label_remapping_mask[tuple(k_tuple)] = label_remapping_mask_in[k]
                else:
                    if k in self.object_index:
                        self.label_remapping[self.object_index[k]] = label_remapping_in[k]
                        self.label_remapping_mask[self.object_index[k]] = label_remapping_mask_in[k]
                    else:
                        logger.important('object "{}" was not found in object_indices'.format(k))
                        fail_due_to_unknown_object = True

            if not self.remap_with_coverage_mask:
                self.label_remapping_mask = None

            if fail_due_to_unknown_object:
                raise ValueError('Aborted due to unknown objects in the remap labels.')

            if show_hints:
                sorted_objects = sorted(list(zip(self.object_counts, self.objects)), key=lambda x:int(x[0]), reverse=True)
                missing_objects = [o for c, o in sorted_objects if self.object_index[o] not in self.label_remapping and
                                   '_' + o not in label_remapping_in][:50]
                logger.info('100 frequent, but missing object classes', '\n'.join(missing_objects))

                part_freqs = [float(self.object_counts[i]) * float(self.object_ispart[i]) for i in range(len(self.objects))]
                missing_part_indices = np.argsort(part_freqs)[::-1][:50]

                logger.info('100 frequent, but missing part classes (hierarchical labels not included)',
                            '\n'.join(self.objects[i] for i in missing_part_indices
                                      if i not in self.label_remapping and
                                      all([k[-1] != i for k in self.label_remapping.keys() if type(k) == tuple])))

            self.model_config.update({
                'out_channels': len(self.properties),
            })

        if self.cache:
            logger.important('Using in memory cache. Be careful if you do not have a lot of RAM.')
            self.load_and_remap_ade_parts = MemoryCachedFunction(self.load_and_remap_ade_parts)
            # self.downsample_ade_image = MemoryCachedFunction(self.downsample_ade_image)

    def downsample_ade_image(self, index):
        index, subset_path = index
        path = self.scene_paths[index + self.offset - 1]

        sample_num = str(index).zfill(8)

        img_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '.jpg')

        img = cv2.imread(img_path)
        assert img is not None

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = tensor_resize(img, (self.intermediate_size_range[1],)*2, interpret_as_min_bound=True)
        return img

    def load_ade_parts(self, index):

        index, subset_path = index
        path = self.scene_paths[index + self.offset - 1]
        sample_num = str(index).zfill(8)

        interm_size = (self.intermediate_size_range[1],) * 2
        seg_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '_seg.png')
        part1_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '_parts_1.png')
        part2_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '_parts_2.png')
        part3_path = os.path.join(self.root_path, path, 'ADE_' + self.subset_path + '_' + sample_num + '_parts_3.png')

        seg = cv2.imread(seg_path)
        seg = tensor_resize(seg, interm_size, interpret_as_min_bound=True, interpolation='nearest')
        seg = seg.astype('int16')
        seg = np.maximum(0, (seg[:, :, 2] // 10) * 256 + seg[:, :, 1] - 1).astype('uint16')  # convert to label ids

        part_segs = []
        for p in [part1_path, part2_path, part3_path]:
            if os.path.isfile(p):
                part_seg = cv2.imread(p)
                if part_seg is not None:
                    part_seg = tensor_resize(part_seg, interm_size, interpret_as_min_bound=True, interpolation='nearest')

                part_seg = part_seg.astype('int16')
                part_seg = np.maximum(0, (part_seg[:, :, 2] // 10) * 256 + part_seg[:, :, 1] - 1).astype('uint16')
                part_segs += [part_seg]
            else:
                part_segs += [None]

        return seg, part_segs[0], part_segs[1], part_segs[2]

    def load_and_remap_ade_parts(self, index):
        # seg, part0, part1, part2 = self.intermediate_part_cache(index)
        seg, part0, part1, part2 = self.load_ade_parts(index)
        part_seg = [part0, part1, part2]

        occurring_objects = np.argwhere(np.bincount(seg.flatten()) > 0)[:, 0]
        # print('occurring objects:', '  |  '.join(self.objects[j] for j in occurring_objects))

        occurring_parts = []
        for i in range(len(part_seg)):
            if part_seg[i] is not None:
                occurring_parts += [np.argwhere(np.bincount(part_seg[i].flatten()) > 0)[:, 0]]
            else:
                occurring_parts += [[]]

        # print('occurring parts 0:', '  |  '.join(self.objects[j] for j in occurring_parts[0]))
        # print('occurring parts 1:', '  |  '.join(self.objects[j] for j in occurring_parts[1]))

        if self.importance_sampling_labels is not None:

            importance_map = [(seg == i).astype('uint8') for i in self.desired_indices if i in occurring_objects]
            for l in range(3):
                if part_seg[l] is not None:
                    importance_map += [(part_seg[l] == i).astype('uint8') for i in self.desired_indices
                                       if i in occurring_parts[l]]

            if len(importance_map) > 0:
                importance_map = np.dstack(importance_map).sum(2).astype('float32')
                importance_map = np.clip(importance_map, 0, 1)
                importance_map = self.importance_sampling_offset + (1-self.importance_sampling_offset)*importance_map
            else:
                importance_map = None
                # logger.warning('No importance sampling labels found in the actual maps '
                #             '(this could be due to strong down-sizing).')
        else:
            importance_map = None

        seg_shape = seg.shape

        mask = None
        if self.remap:
            seg, mask = remap([seg] + part_seg, [occurring_objects] + occurring_parts,
                              self.label_remapping, self.label_remapping_mask, self.no_exclusive_labels)

        assert seg.shape[:2] == seg_shape[:2], '{}  vs. {}'.format(seg.shape[:2], seg_shape[:2])

        if seg.ndim == 3 and self.cache:
            seg = seg.astype('bool')
            seg = seg.shape, np.packbits(seg)
            mask = mask.shape, np.packbits(mask)

        return seg, mask, importance_map

    def __getitem__(self, index):

        subset_path, real_index = self.sample_ids[index]

        try:
            img = cache_read('ade20k_images/{}-{}.jpg'.format(real_index, self.subset_path))
        except FileNotFoundError:
            img = self.downsample_ade_image((real_index, subset_path))
            cache_write_jpg(img, 'ade20k_images/{}-{}.jpg'.format(real_index, self.subset_path))

        seg, mask, importance_map = self.load_and_remap_ade_parts((real_index, subset_path))

        if type(seg) == tuple:
            seg = np.unpackbits(seg[1]).reshape(seg[0]).astype('float32')

        if type(mask) == tuple:
            mask = np.unpackbits(mask[1]).reshape(mask[0]).astype('float32')
        # importance_map = Noney

        if not self.augmentation:

            if self.target_size is not None:
                seg = seg.astype('float32')
                img = tensor_resize(img, (self.target_size,)*2)
                seg = tensor_resize(seg, (self.target_size,)*2, interpolation='nearest')
                if mask is not None:
                    mask = mask.astype('float32')
                    mask = tensor_resize(mask, (self.target_size,)*2, interpolation='nearest')

            img = img.transpose([2, 0, 1]).astype('float32')

            if seg.ndim == 3:
                seg = seg.transpose([2, 0, 1])
                seg = seg.astype('float32')
            else:
                seg = seg.astype('int64')

            if mask is not None:
                mask = mask.transpose([2, 0, 1]).astype('float32')

            return (img,), (seg, mask,)

        else:
            if seg.dtype == 'bool' and self.cache:
                # this is computationally expensive but saves memory, which is useful for caching
                seg = seg.astype('float32')

            intermediate_size = np.random.uniform(self.intermediate_size_range[0], self.intermediate_size_range[1])
            intermediate_size = int(np.clip(intermediate_size, max(intermediate_size - 150, self.target_size), intermediate_size + 150))
            intermediate_size = (intermediate_size, intermediate_size)

            # Depending on input size, the output of scale_to_bound might vary by a pixel. Therefore, do it only once.
            intermediate_size = scale_to_bound(img.shape, intermediate_size, interpret_as_max_bound=False)

            img = tensor_resize(img, intermediate_size)
            seg = tensor_resize(seg, intermediate_size, interpolation='nearest')

            assert img.shape[:2] == seg.shape[:2], '{} vs. {}'.format(img.shape, seg.shape)

            if mask is not None:
                # print(mask.shape)
                mask = tensor_resize(mask.astype('float32'), intermediate_size, interpret_as_min_bound=True,
                                     interpolation='nearest', channel_dim=2)

            if importance_map is not None:
                assert importance_map.sum() > 0
                slice_indices = sample_random_patch(img.shape, (self.target_size, self.target_size), importance_map)
            else:
                slice_indices = random_crop_slices(img.shape, (self.target_size, self.target_size))

            assert img.shape[:2] == img.shape[:2]

            seg_crop = seg[slice_indices]
            img_crop = img[slice_indices]
            mask_crop = mask[slice_indices] if mask is not None else None
            img_crop = apply_gamma_offset(img_crop, gamma=np.random.normal(1, 0.08, 3), offset=np.random.randint(-5, 5, 3))

            assert seg_crop.shape[0] == self.target_size, seg_crop.shape
            assert seg_crop.shape[1] == self.target_size, seg_crop.shape

            if np.random.rand() > 0.5:  # random horizontal flip
                img_crop = img_crop[:, ::-1]
                seg_crop = seg_crop[:, ::-1]
                if mask_crop is not None:
                    mask_crop = mask_crop[:, ::-1]

            # channels first
            img_crop = img_crop.transpose([2, 0, 1])

            if self.no_exclusive_labels:
                seg_crop = seg_crop.transpose([2, 0, 1])

                if mask_crop is not None:
                    mask_crop = mask_crop.transpose([2, 0, 1])
                    mask_crop = mask_crop

            img_crop = img_crop.astype('float32')

            if seg_crop.dtype.name in {'float64', 'uint8'}:
                seg_crop = seg_crop.astype('float32')

            if seg_crop.dtype.name == 'uint16':
                seg_crop = seg_crop.astype('int64')

            return (img_crop,), (seg_crop, mask_crop)

    @staticmethod
    def install():
        from ..core.common.generic import download_file, extract_archive
        zip_file = join(PATHS['ADE20K'], 'ade.zip')
        download_file('http://groups.csail.mit.edu/vision/datasets/ADE20K/ADE20K_2016_07_26.zip', zip_file)
        extract_archive(zip_file, PATHS['ADE20K'])
        os.unlink(zip_file)


class ADEAff12(ADE):
    def __init__(self, subset, show_hints=False, augmentation=False, image_size=352, cache=False,
                 importance_sampling_labels=None, select_scenes=False):
        super().__init__(subset, augmentation=augmentation, cache=cache,
                         remap=join(PATHS['ROOT'], 'datasets', 'ade', 'affordances12.csv'),
                         importance_sampling_labels=importance_sampling_labels,
                         importance_sampling_offset=0.1,
                         select_scenes=select_scenes,
                         no_exclusive_labels=True,
                         remap_with_coverage_mask=True,
                         image_size=image_size,
                         show_hints=show_hints)

        self.data_types = 'image->denseM(segmentation),denseM(mask)'

        self.default_loss = partial(dense_binary_cross_entropy, use_mask=True, apply_sigmoid=True)
        self.default_metrics = [partial(IoUMetric, binary=True, n_classes=12)]

        self.additional_visualizations = [
            (['image', 'predicted_segmentation'],
             LabelImageColorOverlayData,
             dict(normalize=True,
                  indices=[5, 2, 3],
                  intensities=[10, 2, 5])),
            (['image', 'predicted_segmentation'],
             LabelImageColorOverlayData,
             dict(normalize=True,
                  h_shift=0.1,
                  indices=[9, 10, 11],
                  intensities=[2, 2, 1])),
        ]


class ADE150(DatasetBase):

    def __init__(self, subset, augmentation=False, image_size=(352, 352), image_maxbox=False,
                 intermediate_size=None, bgr=False):

        super().__init__('image->denseC,denseM')

        self.image_maxbox = image_maxbox
        self.bgr = bgr
        self.additional_visualizations = [
            (['image', 'predicted_segmentation'],
             LabelImageColorOverlayData,
             dict(normalize=False,
                  indices=[2],
                  intensities=[1]))
        ]

        self.augmentation = augmentation
        # self.subsets = get_subset_names(subset)

        self.subset_path, self.offset, start, end = ADE_SUBSETS[subset]
        self.subset_path_long = {'train': 'training', 'val': 'validation'}[self.subset_path]

        self.image_size = image_size

        if intermediate_size is not None:
            self.intermediate_size = intermediate_size
        else:
            self.intermediate_size = self.image_size

        self.root_path = PATHS['ADE150']

        classes = open(os.path.join(PATHS['ADE150'], 'objectInfo150.txt')).read().split('\n')
        classes = [c.split('\t') for c in classes[1:]]
        classes = [c[4] for c in classes if len(c) == 5]

        scene_categories = open(os.path.join(PATHS['ADE150'], 'sceneCategories.txt')).read().split('\n')
        scene_categories = [c.split(' ') for c in scene_categories]
        if self.subset_path in {'train', 'val'}:
            sample_names = [c[0] for c in scene_categories if c[0][4: 4 + len(self.subset_path)] == self.subset_path]
        else:
            sample_names = open(os.path.join(PATHS['ADE150'], 'release_test', 'list.txt')).read().split('\n')
            sample_names = [s[:-4] for s in sample_names]  # remove the '.jpg'

        self.sample_ids = tuple(sample_names[start: end])

        self.default_loss = 'dense_cross_entropy'
        self.model_config = {'out_channels': len(classes)}

        print('samples', len(self.sample_ids))

    def __getitem__(self, index):
        img_file = os.path.join(self.root_path, 'images', self.subset_path_long, self.sample_ids[index] + '.jpg')
        seg_file = os.path.join(self.root_path, 'annotations', self.subset_path_long, self.sample_ids[index] + '.png')

        assert_file_exists(img_file)
        img = cv2.imread(img_file)

        assert_file_exists(seg_file)
        seg = cv2.imread(seg_file, cv2.IMREAD_GRAYSCALE)

        if self.augmentation:
            img = tensor_resize(img, self.intermediate_size, interpret_as_min_bound=True)
            seg = tensor_resize(seg, self.intermediate_size, interpolation='nearest', interpret_as_min_bound=True)

            slice_indices = random_crop_slices(img.shape, self.image_size)

            img = img[slice_indices]
            seg = seg[slice_indices]

            img = apply_gamma_offset(img, gamma=np.random.normal(1, 0.08, 3), offset=np.random.randint(-5, 5, 3))
        elif self.image_size is not None:
            img = tensor_resize(img, self.image_size, interpret_as_max_bound=self.image_maxbox)
            seg = tensor_resize(seg, self.image_size, interpret_as_max_bound=self.image_maxbox, interpolation='nearest')

        seg = seg.astype('int64')
        seg = seg - 1  # to get the correct labels, -1 can be ignored

        if not self.bgr:
            img = img[:, :, [2, 1, 0]]  # BGR -> RGB
            
        img = img.transpose([2, 0, 1])  # channels-first
        img = img.astype('float32')

        # set mask to None to match the ADE API
        mask = None

        return (img,), (seg, mask)

    @staticmethod
    def install():
        from subprocess import run
        target = PATHS['DATASETS']
        url = 'http://groups.csail.mit.edu/vision/datasets/ADE20K/ADE20K_2016_07_26.zip'

        run(f'wget {url} -P {target}'.split())
        run(f'unzip {target}/ADE20K_2016_07_26.zip'.split())

