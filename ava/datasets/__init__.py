from .mnist import MNIST
from .cub200 import *
from .imagenet import *
from .ucf101 import *
from .cifar import Cifar10
from .pascal import *
from .ade import *
from .something import *
from .epic_kitchens import *
from .cmu_hand import *
from .affordances import AffHQ12

from os import listdir

from .. import PATHS

# the current folder must be in PYTHONPATH in order to find _dataset.py modules
import sys
sys.path.append('.')
sys.path.append(join(PATHS['CONTRIB_PATH'], 'datasets'))


# search in local folder first
for module in listdir('.'):
    if module.endswith('_dataset.py'):
        mod = __import__(module[:module.rindex('.py')], globals=globals())
        for attr in dir(mod):
            globals()[attr] = getattr(mod, attr)
