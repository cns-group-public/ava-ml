import os
from os.path import join

import cv2
import numpy as np


from ..core.dataset import DatasetBase, Interleaved
from .. import PATHS
from ..transformations import tensor_resize

from .ade import load_property_map


class AffHQ12(DatasetBase):
    """
    Expert-corrected affordances.
    """

    def __init__(self, subset, cache=False):
        filename = join(PATHS['ROOT'], 'datasets', 'ade', 'affordances12.csv')
        _, _, self.parts, _ = load_property_map(filename, add_bg_col_zero=False, second_row_weights=True, delimiter=';')
        super().__init__('image->denseM(segmentation),denseM(mask)')

        def threshold(x):
            x = x.astype('float32')
            return np.uint8(x.mean(2) > 20)

        self.threshold = threshold
        self.parts = [p.replace('/', '_').replace('clean_dry', 'dry') for p in self.parts]

        self.image_fp = lambda folder: '{}/{}_img.jpg'.format(folder, folder)
        self.part_fp = lambda folder, part: '{}/{}_{}.png'.format(folder, folder, part)

        self.path = PATHS['HQ_AFF']
        self.sample_ids = tuple(d for d in os.listdir(self.path) if os.path.isdir(os.path.join(self.path, d)))

        self.model_config.update({'binary': True, 'with_mask': True, 'out_channels': len(self.parts)})

    def __getitem__(self, index):

        img = cv2.imread(os.path.join(self.path, self.image_fp(self.sample_ids[index])))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        seg = []
        for part in self.parts:
            if part == 'pull':
                filename = os.path.join(self.path, self.part_fp(self.sample_ids[index], 'hook_pull'))
                prop_img1 = cv2.imread(os.path.join(self.path, self.part_fp(self.sample_ids[index], 'hook_pull')))
                prop_img2 = cv2.imread(os.path.join(self.path, self.part_fp(self.sample_ids[index], 'pinch_pull')))
                seg_img = cv2.add(prop_img1, prop_img2)
                assert seg_img is not None, 'image not found for {}'.format(filename)
            else:
                filename = os.path.join(self.path, self.part_fp(self.sample_ids[index], part))
                seg_img = cv2.imread(filename)
                assert seg_img is not None, 'image not found: {}'.format(filename)

            seg += [self.threshold(seg_img)]

        seg = np.array(seg, dtype='float32')
        img = img.transpose([2, 0, 1])
        img = img.astype('float32')

        return (img,), (seg, np.ones(seg.shape, dtype='float32'))

    @staticmethod
    def install():

        from subprocess import run, PIPE
        from ..core.common.generic import download_file, extract_archive

        target = join(PATHS['HQ_AFF'])
        download_file('https://gitlab.gwdg.de/cns-group-public/aff-seg/raw/master/data/aff_expert.zip', target)
        extract_archive(join(target, 'aff_expert.zip'), PATHS['HQ_AFF'])
        os.unlink(join(target, 'aff_expert.zip'))

        fol = join(target, 'aff_expert')
        run(f'mv {fol}/* {target}/', stdout=PIPE, stderr=PIPE, shell=True)
        os.rmdir(fol)

        all_ade = list(os.walk(PATHS['ADE20K']))

        folders = [f for f in os.listdir(PATHS['HQ_AFF'])]

        for f in folders:
            path = [a for a, b, c in all_ade if len(c) > 1 and any([f in r for r in c]) and 'validation' in a][0]

            img = cv2.imread(join(path, 'ADE_val_' + f + '.jpg'))
            img = tensor_resize(img, (352, 352))
            # print(os.path.join('data/aff_expert/', f, f + '_img.jpg'))
            print('.')
            cv2.imwrite(join(PATHS['HQ_AFF'], f, f + '_img.jpg'), img)
