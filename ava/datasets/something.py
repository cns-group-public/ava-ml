import os
from random import seed, shuffle

import cv2
import numpy as np
from math import ceil

from ..transformations import sample_binned, sample_equidistant, MemoryCachedFunction
from ..transformations import tensor_resize, get_gamma_offset_lut, apply_gamma_offset, random_crop
from ..core.dataset import DatasetBase
from .. import PATHS


class _SomethingBase(DatasetBase):

    subset_to_filename = {
        'train-s': [('something-something-v1-train.csv', 0, -5000)],
        'val-s': [('something-something-v1-train.csv', -5000, None)],
        'train-s2': [('something-something-v1-train.csv', -80000, None)],
        'val-s2': [('something-something-v1-train.csv', 0, -80000)],
        'train-v': [('something-something-v1-validation.csv', 0, -5000)],
        'val-v': [('something-something-v1-validation.csv', -5000, None)],

        'train': [('something-something-v1-train.csv', 0, None)],
        'trainval': [('something-something-v1-train.csv', 0, None), ('something-something-v1-validation.csv', 0, None)],
        'val': [('something-something-v1-validation.csv', 0, None)],
        'test': [('something-something-v1-test.csv', 0, None)]}

    def __init__(self, data_types, subset, resize_factor=None, augmentation=False):

        super().__init__(data_types)

        self.augmentation = augmentation
        # if subset in self.subset_to_filename:
        #     filename = self.subset_to_filename[subset]
        # else:
        #     raise ValueError('invalid subset')

        samples = []
        for filename, start, end in self.subset_to_filename[subset]:
            with open(os.path.join(PATHS['SomethingV1'], filename), 'r') as fh:
                data = fh.read()

            add = [l.split(';') for l in data.split('\n') if len(l) > 0]

            seed(12345)
            shuffle(add)

            add = add[start:end]
            samples += add

        samples = samples[:int(len(samples)*resize_factor)] if resize_factor is not None else samples

        with open(os.path.join(PATHS['SomethingV1'], 'category.txt'), 'r') as fh:
            actions = fh.read().split('\n')

        self.action_to_id = {a: i for i, a in enumerate(actions)}
        self.id_to_action = {i: a for i, a in enumerate(actions)}

        self.undirectional_classes = set([i for i, a in self.id_to_action.items() if 'left' not in a and 'right' not in a])

        self.sample_ids = tuple(s[0] for s in samples)

        self.labels = [self.action_to_id[s[1]] for s in samples]

        self.default_loss = 'cross_entropy'
        self.visualization_hooks['label'] = lambda i: '{} ({})'.format(self.id_to_action[i], i)

        # enable augmentation on the training dataset
        # self.use_augmentation = subset == 'train'

    def __getitem__(self, index):
        raise NotImplementedError

    def mode(self):
        from scipy.stats import mode
        return mode(self.labels).mode,


class SomethingFrames(_SomethingBase):
    """
    Frames of 20BN's SomethingSomething dataset.
        `feature_extractor`: Extract features in the dataset such that they can be cached. Augmentation is not
        possible in that case.
    """

    def __init__(self, subset, n_frames, resize_factor=None, image_size=(100, 150),
                 crop_padding=20, early=None, cache=False, with_optical_flow=False,
                 feature_extractor=None, no_avgpool=False, augmentation=False, multi_seq=None, fixed_frame=None,
                 sampling='binned'):

        super().__init__('video,video(optical_flow)->C(class)', subset, resize_factor, augmentation=augmentation)

        self.sampling = sampling
        self.fixed_frame = fixed_frame
        self.early = early
        self.n_frames = n_frames
        self.multi_seq = multi_seq

        assert self.early is None or 0 < self.early < 100

        self.with_optical_flow = with_optical_flow
        self.cache_processed_images = cache

        self.image_size = image_size
        self.crop_padding = crop_padding

        if cache:
            self.load_img = MemoryCachedFunction(self.load_img)

        if self.multi_seq:
            self.model_config.update({'n_seqs': self.multi_seq[0], 'seq_size': self.multi_seq[1]})

        self.model_config['n_classes'] = 174

    def load_and_process(self, sample_id):
        img_stack = self.load_img(sample_id)
        # img_stack = self.process_img(images)
        img_stack = np.array(img_stack)
        return img_stack

    def load_img(self, sample_id):
        path = os.path.join(PATHS['SomethingV1'], '20bn-something-something-v1', sample_id)
        n_images = len(list(os.listdir(path)))

        if self.augmentation:
            intermediate_size = self.image_size[0] + self.crop_padding, self.image_size[1] + self.crop_padding
        else:
            intermediate_size = self.image_size

        if self.early is None:
            load_range = range(1, n_images+1, 2)
        else:
            load_range = range(1, int((self.early / 100) * n_images))

        images = []
        for i in load_range:
            img = cv2.imread(os.path.join(path, str(i).zfill(5) + '.jpg'))
            img = tensor_resize(img, intermediate_size, interpret_as_min_bound=self.augmentation, channel_dim=2, interpolation='bilinear')
            img = img[:, :, [2, 1, 0]]
            assert img is not None
            images += [img]

        images = np.array(images)
        return images

    def get_optical_flow(self, images):
        flow = []
        self.logger.detail('calculate optical flow')
        tvl = cv2.DualTVL1OpticalFlow_create(epsilon=0.1, innnerIterations=10, outerIterations=6, nscales=3, medianFiltering=0)
        for i in range(len(images)-1):
            flow += [tvl.calc(images[i, :, :, 0], images[i+1, :, :, 0], None)]
        return np.array(flow)

    def img_augmentation(self, img_stack, with_flip=False):
        # crop
        img_stack = random_crop(img_stack, self.image_size, image_dimensions=(1, 2))

        gamma_all = 2 / (np.random.normal(1, 0.5, 1) + np.random.normal(1, 0.1, 3))
        gamma_offset_lut = get_gamma_offset_lut(gamma_all, np.random.normal(0, 2, 3))
        for i in range(img_stack.shape[0]):
            img_stack[i] = apply_gamma_offset(img_stack[i], lut=gamma_offset_lut, channel_dim=2)

        # flip
        if with_flip and np.random.rand() > 0.5:
            img_stack = img_stack[:, :, ::-1, :]

        return img_stack

   #@profile
    def __getitem__(self, index):
        sample_id = self.sample_ids[index]  # use sample_id to disk-cache under the right name
        label = self.labels[index]

        img_stack = self.load_and_process(sample_id)

        if self.multi_seq is not None:

            n_roots, seq_length = self.multi_seq

            root_choices = list(range(int(ceil(seq_length / 2)), len(img_stack) - seq_length // 2))

            assert len(root_choices) > 0

            splits = np.array_split(root_choices, n_roots)
            roots = [np.random.choice(s) for s in splits]

            image_ids = [j for i in range(n_roots) for j in range(roots[i] - ceil(seq_length / 2), roots[i] + seq_length // 2)]
        elif self.fixed_frame is not None:
            image_ids = [self.fixed_frame]
        else:

            if self.sampling == 'binned':
                image_ids = np.array(sample_binned(len(img_stack), self.n_frames, bin_randomness=0.03, fill_gaps='prev'))
            elif self.sampling == 'equidistant':
                image_ids = sample_equidistant(len(img_stack), self.n_frames, offset=0)

                # replace None with preceding index
                for i in range(len(image_ids)):
                    image_ids[i] = image_ids[i] if image_ids[i] is not None else image_ids[i-1]

                image_ids = np.array(image_ids)
            else:
                raise ValueError('Invalid sampling method')

        img_stack = img_stack[image_ids]

        if self.augmentation:
            img_stack = self.img_augmentation(img_stack, with_flip=label in self.undirectional_classes)

        img_stack = img_stack.transpose([3, 0, 1, 2])
        img_stack = img_stack.astype('float32')

        if self.with_optical_flow:
            flow = self.get_optical_flow(img_stack)
            flow = flow.transpose([3, 0, 1, 2])  # channels x depth x height x width
        else:
            flow = None

        return (img_stack, flow), (label,)
