from .preprocessing import *
from ...core.dataset import DatasetBase
from ...core.logging import logger
from ...transformations.cache import cache_read, cache_write_pickle, cache_write_numpy


class OpenImagesBase(DatasetBase):

    def __init__(self, subset, count=0, image_size=500, filter_classes=None, filter_min_objects=None,
                 no_file_exist_check=False):
        super().__init__('image->bboxes')
        # self.lock = threading.Lock()

        self.image_size = image_size
        self.count = count
        assert subset in {'train', 'val', 'test'}
        assert count in {0, 4, 10}
        subset_long = subset.replace('val', 'validation')

        self.class_names, identifiers_to_idx = load_class_names(join(PATHS['OPEN_IMAGES'], 'class-descriptions-boxable.csv'))
        # with open(join(PATHS['OPEN_IMAGES'], 'class-descriptions-boxable.csv')) as fp:
        #     classes = fp.read().split('\n')
        #     classes = [c.split(',') for c in classes if len(c) > 0]
        #     identifiers, self.class_names = [c[0] for c in classes], [c[1] for c in classes]
        #     identifiers_to_idx = {ident: i for i, ident in enumerate(identifiers)}

        if os.path.isdir(join(PATHS['OPEN_IMAGES'], 'images_w500')):
            self.images_subfolder = 'images_w500'  # try to use smaller images
        else:
            self.images_subfolder = 'images'

        existing_str = '-with-non-existing' if no_file_exist_check else ''

        try:
            self.img_indices = cache_read('open_images/{}{}_img_indices.pickle'.format(subset, existing_str))
            self.bboxes = cache_read('open_images/{}{}_bboxes.npy'.format(subset, existing_str))
            self.bbox_extra = cache_read('open_images/{}{}_bbox_extra.npy'.format(subset, existing_str))
            self.bbox_labels = cache_read('open_images/{}{}_bbox_labels.npy'.format(subset, existing_str))
        except (FileNotFoundError, ValueError):

            print('build cache for {}...'.format(subset))
            with open(join(PATHS['OPEN_IMAGES'], '{}-annotations-bbox.csv'.format(subset_long))) as fp:
                lines = fp.read().split('\n')

            header = lines[0]
            lines = lines[1:-1]

            self.img_indices = dict()
            self.bboxes = []
            self.bbox_extra = []
            self.bbox_labels = []

            current_id = None
            current_start = 0
            current_id_count = 0
            current_i = 0

            # must be a loop due to memory constraints
            for line in lines:
                line = line.split(',')
                img_id = line[0]

                assert len(line) == 13

                if os.path.isfile(join(PATHS['OPEN_IMAGES'], 'images', img_id + '.jpg')) or no_file_exist_check:
                    if img_id != current_id or current_id is None:
                        # if image_id changes write the mapping from image_id to (start index, number of images)
                        assert img_id not in self.img_indices
                        self.img_indices[current_id] = (current_start, current_id_count)
                        current_id = img_id
                        current_start = current_i
                        current_id_count = 1
                    else:
                        current_id_count += 1

                    current_i += 1
                    self.bbox_labels += [identifiers_to_idx[line[2]]]
                    self.bboxes += [np.array(line[4:8], dtype='float32')]

                    extra = [int(x) for x in line[8:]]
                    self.bbox_extra += [np.array(extra, dtype='int8')]
                else:
                    raise ValueError('File not found: ', subset, join(PATHS['OPEN_IMAGES'], 'images', img_id + '.jpg'))

            if None in self.img_indices:
                del self.img_indices[None]

            self.bbox_labels = np.array(self.bbox_labels)
            self.bboxes = np.array(self.bboxes)

            cache_write_pickle(self.img_indices, 'open_images/{}{}_img_indices'.format(subset, existing_str))
            cache_write_numpy(self.bboxes, 'open_images/{}{}_bboxes'.format(subset, existing_str))
            cache_write_numpy(self.bbox_extra, 'open_images/{}{}_bbox_extra'.format(subset, existing_str))
            cache_write_numpy(self.bbox_labels, 'open_images/{}{}_bbox_labels'.format(subset, existing_str))

        if filter_classes:
            classes = np.loadtxt(join(PATHS['AVA_DATA'], filter_classes), dtype=object)

            valid_class_ids = {self.class_names.index(c) for c in classes}

            n_old = len(self.img_indices)

            # at least one object in the image needs to belong the classes defined above
            self.img_indices = {k: v for k, v in self.img_indices.items()
                                if any([self.bbox_labels[j] in valid_class_ids for j in range(v[0], v[0] + v[1])])}

            logger.important('Reduce from {} to {} elements by filtering.'.format(n_old, len(self.img_indices)))

        self.sample_ids = tuple(self.img_indices.keys())

        self.parameter_variables = [count, image_size]

        self.augmentation = subset == 'train'

        # arity refers to the implied probability distribution (we count up to ten classes)
