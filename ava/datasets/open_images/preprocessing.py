import os
import sqlite3
from collections import Counter, defaultdict
from itertools import combinations, product, groupby
from os.path import join

import cv2
import pickle
import numpy as np
import yaml

from ...core.common.functional import get
from .. import PATHS
from ...metrics.intersection_over_union import bbox_iou


ALL_RATINGS = ('imposs', 'implausible', 'plausible')

BBOX_MIN = 0.02
BBOX_MAX = 0.7
BBOX_MAX_OVERLAP = 0.2
MIN_N_OBJECTS = 1
MIN_N_OBJECT_CLASSES = 1
MAX_N_OBJECTS = 3
ORDINAL_WEIGHTS = [-1, 0.2, 0.8]


def find_all_subentities(node, search_name, path=tuple()):
    """ For traversing the object entity hierarchy. Returns branches that belong to search_name. """

    res = []
    if 'children' in node:
        for child_node in node['children']:
            res += find_all_subentities(child_node, search_name, path + (node['name'],))
        return res
    else:
        # print(node['name'])
        if search_name in path or node['name'] == search_name:
            return [node['name']]
        else:
            return []


def load_class_names(class_description_filename):
    with open(class_description_filename, 'r') as fp:
        classes = fp.read().split('\n')
        classes = [c.split(',') for c in classes if len(c) > 0]
        identifiers, class_names = [c[0] for c in classes], [c[1] for c in classes]
        identifiers_to_idx = {ident: i for i, ident in enumerate(identifiers)}

    return class_names, identifiers_to_idx


def get_action_defs(filename, hierarchy_filename, class_names):
    import yaml
    import json

    hierarchy = json.load(open(join(PATHS['OPEN_IMAGES_ACTION'], 'data/hierarchy.json')))
    definitions = yaml.load(open(filename))

    action_defs = {}
    for action_name, objects in definitions.items():
        if type(objects[0]) != list:  # unary
            action_defs[action_name] = (
            list(set(class_names.index(c) for o in objects for c in find_all_subentities(hierarchy, o))), None)
        else:
            action_defs[action_name] = (
            list(set(class_names.index(c) for o in objects[0] for c in find_all_subentities(hierarchy, o))),
            list(set(class_names.index(c) for o in objects[1] for c in find_all_subentities(hierarchy, o))))

    return action_defs


def entropy(x):
    x /= np.array(x).sum()
    return -1 * (x * np.log(x)).sum()


def extract_scenes(open_images):
    iids = open_images.img_indices
    return [(sid, open_images.bbox_labels[iids[sid][0]: iids[sid][0] + iids[sid][1]],
             open_images.bboxes[iids[sid][0]: iids[sid][0] + iids[sid][1]],
             open_images.bbox_extra[iids[sid][0]: iids[sid][0] + iids[sid][1]])
            for sid in open_images.sample_ids]


def keep_only_k_occurrences(list_of_tuples, k, same_rating_only=False):
    """ removes all tuples where the element occurs less than k times"""

    # sort by first element
    grouped = [list(v) for k, v in groupby(sorted(list_of_tuples, key=lambda x: x[0]), key=lambda x: x[0])]

    if same_rating_only:
        out = [x for g in grouped for x in g if len(g) >= k and len(set(y[1] for y in g)) == 1]
    else:
        out = [x for g in grouped for x in g if len(g) >= k]

    # count = Counter(x[0] for x in list_of_tuples)
    # out = [x for x in list_of_tuples if count[x[0]] >= k]

    return out


def build_actions(max_objects_per_class=1000, filename='scenes'):
    open_images_train = OpenImagesBase('train')
    scenes = extract_scenes(open_images_train) + extract_scenes(OpenImagesBase('val')) + extract_scenes(
        OpenImagesBase('test'))

    class_names = open_images_train.class_names
    images_subfolder = open_images_train.images_subfolder

    print('re-compute scenes for actions...')

    # define limit per action_name to compensate for imbalance (introduced by the acton-completeness correction)
    action_defs = get_action_defs('data/action_definitions.yaml', 'data/hierarchy.json', class_names)

    # filter persons (not all, since many are not annotated)
    invalid_class_names = {'Person', 'Boy', 'Human ear', 'Human hand', 'Human face', 'Human arm', 'Human leg',
                           'Human hair', 'Human head',
                           'Man', 'Woman', 'Dog', 'Cat', 'Hamster', 'Suit', 'Shirt'}
    # invalid_class_names = {}
    invalid_classes = [class_names.index(o) for o in invalid_class_names]
    required_classes = set(a for k in action_defs.keys() for a in action_defs[k][0])

    scenes = [(sid, objs, set(objs), bboxes, extra) for sid, objs, bboxes, extra in scenes]

    pickle.dump(scenes, open('data/all_instances.pickle', 'wb'))

    # remove scenes without required classes
    scenes = [(sid, objs, objs_set, bboxes, extra) for sid, objs, objs_set, bboxes, extra in scenes if
              any([i in objs_set for i in required_classes])]

    # remove scenes with invalid classes
    scenes = [(sid, objs, bboxes.tolist(), extra) for sid, objs, objs_set, bboxes, extra in scenes if
              not (any([i in objs_set for i in invalid_classes]))]

    # compute object class frequencies
    obj_class_count = Counter([o for s in scenes for o in s[1]])

    # scenes = [(sid, objs, bboxes.tolist()) for sid, objs, bboxes in objects]

    # # requires at least two object classes
    # scenes = [s for s in scenes if len(set(s[1])) > 1]

    print('found {} objects and {} scenes'.format(sum([len(s[1]) for s in scenes]), len(scenes)))

    removed_obj_count = 0

    # -----------------------------------------
    # Filter small and redundant bounding boxes

    for i_scene in range(len(scenes)):
        _, obj_classes, bboxes, extra = scenes[i_scene]
        obj_ids = list(range(len(obj_classes)))

        assert extra.min() >= -1 and extra.max() <= 1, 'min: {}, max: {}'.format(extra.min(), extra.max())
        assert len(obj_classes) == len(bboxes)

        # obj_ids to maintain original object ids
        objects_new, bboxes_new, obj_ids_new = [], [], []
        n_objects = len(obj_classes)
        n_object_classes = len(set(obj_classes))

        n_obj_crit = n_objects >= MIN_N_OBJECTS and n_object_classes >= MIN_N_OBJECT_CLASSES

        for object_cls, obj_id, bbox, ex in zip(obj_classes, obj_ids, bboxes, extra):
            area = (bbox[1] - bbox[0]) * (bbox[3] - bbox[2])

            # no (known) occlusion, truncation, group and depiction.
            valid_annotation = (ex[0] != 1 and ex[1] != 1 and ex[2] != 1)  # and ex[3] != 1)

            valid_class = object_cls in required_classes

            if BBOX_MIN < area < BBOX_MAX and n_obj_crit and valid_annotation and valid_class:
                objects_new += [object_cls]
                bboxes_new += [bbox]
                obj_ids_new += [obj_id]

        # check for iou
        all_indices = set(range(len(bboxes_new)))
        rm_indices = set()

        # if bk is contained in bj -> remove k (otherwise there would be too many very small objects)
        for j, k in product(all_indices, repeat=2):
            bj, bk = bboxes_new[j], bboxes_new[k]

            # area_j = (bj[1] - bj[0]) * (bj[3] - bj[2])
            # overlap = (min(bj[1], bk[1]) - max(bj[0], bk[0])) * ((min(bj[3], bk[3]) - max(bj[2], bk[2])))
            # print(j, k, bj, area_j, overlap)

            EPS = 0.02

            if j != k and bj[0] - EPS < bk[0] < bk[1] < bj[1] + EPS and bj[2] - EPS < bk[2] < bk[3] < bj[3] + EPS:
                rm_indices.add(k)

        all_indices = all_indices.difference(rm_indices)

        for j, k in combinations(all_indices, 2):
            bj, bk = bboxes_new[j], bboxes_new[k]  # shortcuts
            if bbox_iou(bj, bk) > BBOX_MAX_OVERLAP:
                class_j, class_k = objects_new[j], objects_new[k]
                rm_index = k if obj_class_count[class_j] < obj_class_count[class_k] else j
                rm_indices.add(rm_index)

        # rm_indices = rm_indices.union([k for k in all_indices if extra_new[k][0] == 1 or extra_new[k][1] == 1])  # if occluded or truncated -> remove

        removed_obj_count += len(rm_indices)

        # if scenes[i_scene][0] == '00dc0baf74f95e45':
        #     print([class_names[objects_new[j]] for j in rm_indices])
        #     print([obj_class_count[objects_new[j]] for j in rm_indices])
        #     import ipdb; ipdb.set_trace()

        valid_indices = list(all_indices.difference(rm_indices))

        if len(valid_indices) > MAX_N_OBJECTS:
            sizes = [(bboxes_new[j][1] - bboxes_new[j][0]) * (bboxes_new[j][3] - bboxes_new[j][2]) for j in
                     valid_indices]
            valid_indices = np.argsort(sizes)[::-1][:MAX_N_OBJECTS]

        objects_new = [objects_new[j] for j in valid_indices]
        bboxes_new = [bboxes_new[j] for j in valid_indices]
        obj_ids_new = [obj_ids_new[j] for j in valid_indices]

        sid = scenes[i_scene][0]
        scenes[i_scene] = (sid, objects_new, set(objects_new), bboxes_new, obj_ids_new, extra)

    # remove empty scenes
    scenes = [s for s in scenes if len(s[1]) > 0]

    print('Removed {} objects'.format(removed_obj_count))
    print('{} scenes and {} objects after BBox size filtering and duplicate removal'.format(
        len(scenes), sum([len(s[1]) for s in scenes])))

    # filter ignored scenes
    # ignore_scenes = set(json.load(open('data/ignore_scenes.json')))
    # scenes = [s for s in scenes if s[0] not in ignore_scenes]
    # print('{} scenes and {} objects after ignoring scenes'.format(
    #    len(scenes), sum([len(s[1]) for s in scenes])))

    all_actions = sorted(k for k in action_defs.keys())
    valid_object_classes = set(a for k in action_defs.keys() for a in action_defs[k][0])
    all_instances = [(s[0], s[1][i], s[3][i], i, s[5][i]) for s in scenes for i in range(len(s[1])) if
                     s[1][i] in valid_object_classes]

    # filter out very frequent object classes
    class_count = defaultdict(lambda: 0)
    keep_indices = []

    def inst_order(args):
        sid, obj_class, bbox, oid, extra = args
        return extra[0] - (bbox[1] - bbox[0]) * (bbox[3] - bbox[2])  # size of bbox

    all_instances_sorted = sorted(all_instances, key=inst_order, reverse=True)
    for i, (sid, obj_class, bbox, oid, extra) in enumerate(all_instances_sorted):
        class_count[obj_class] += 1
        if class_count[obj_class] <= max_objects_per_class: keep_indices += [i]

    all_instances = [all_instances_sorted[i] for i in keep_indices]

    # import ipdb; ipdb.set_trace()

    final_scenes = {}
    for action_name in all_actions:
        valid_obj_classes = set(action_defs[action_name][0])
        final_scenes[action_name] = [(sid, obj_cls, bbox, obj_id, extra) for sid, obj_cls, bbox, obj_id, extra in
                                     all_instances if obj_cls in valid_obj_classes]

    print('-----------------------------\nAfter all-action correction')

    def calc_entropy(x):
        x /= np.array(x).sum()
        return -1 * (x * np.log(x)).sum()

    print(all_actions)
    print([len(final_scenes[a]) for a in all_actions])

    print('#Scenes / Instances:', len(set(x[0] for x in all_instances)), len(all_instances))
    print('Action entropy:', calc_entropy([len(final_scenes[a]) for a in all_actions]))
    print('Object entropy:', calc_entropy(list(Counter([x[1] for x in all_instances]).values())))

    pickle.dump(final_scenes, open('data/' + filename + '.pickle', 'wb'))

    # ----------------
    # Image dimensions

    print('compute image dimensions...')

    try:
        image_dims = pickle.load(open('data/image_dimensions.pickle', 'rb'))
    except IOError:
        image_dims = {}
        print('failed to load image_dims.pickle')

    for s in all_instances:
        if s[0] not in image_dims:
            img = cv2.imread(os.path.join(PATHS['OPEN_IMAGES'], images_subfolder, s[0] + '.jpg'))
            assert img is not None, 'image {} was not found'.format(
                os.path.join(PATHS['OPEN_IMAGES'], images_subfolder, s[0] + '.jpg'))
            image_dims[s[0]] = img.shape[:2][::-1]

    pickle.dump(image_dims, open('data/image_dimensions.pickle', 'wb'))


def compute_action_object_entropies(valid_samples, object_classes, plaus_only=False):
    from collections import defaultdict
    from scipy.stats import entropy

    instance_action_to_ratings = defaultdict(lambda: [])

    for (sid, iid, _), ratings in valid_samples.items():
        for action, rating, user in ratings:
            obj_cls = object_classes[(sid, iid)]
            instance_action_to_ratings[(obj_cls, action)] += [rating]

    if plaus_only:
        # ignore impossible for entropy calculation
        instance_action_entropies = [(k, entropy(np.bincount([ALL_RATINGS.index(r) for r in ratings], minlength=3)[1:]))
                                     for k, ratings in instance_action_to_ratings.items()]
    else:
        instance_action_entropies = [(k, entropy(np.bincount([ALL_RATINGS.index(r) for r in ratings], minlength=3)))
                                     for k, ratings in instance_action_to_ratings.items()]

    instance_action_entropies = sorted(instance_action_entropies, key=lambda x: x[1], reverse=True)

    return instance_action_entropies


def load_action_samples(metadata, required, full_annotation, valid_raters, min_ratings, same_rating_only,
                        object_classes,
                        min_action_object_entropy=None):
    critical_scenes = yaml.load(open(join(PATHS['OPEN_IMAGES_ACTION'], 'data/critical_scenes.yaml')))
    sample_scenes = yaml.load(open(join(PATHS['OPEN_IMAGES_ACTION'], 'data/sample_scenes.yaml')))

    db = sqlite3.connect(join(PATHS['OPEN_IMAGES_ACTION'], 'data/annotations.sqlite'))
    annotation_db = db.execute('SELECT * FROM annotations').fetchall()

    # filter annnotations by raters
    if valid_raters is not None:
        annotation_db = [r for r in annotation_db if r[0] in valid_raters]

    print('found {} rows for {} valid raters'.format(len(annotation_db),
                                                     len(valid_raters) if valid_raters is not None else '-'))

    required_set = set(required.keys())
    samples = defaultdict(lambda: [])
    for rater_code, _, _, action, sid, obj_id, plausibility, repetition_set in annotation_db:
        if (sid, obj_id) in required_set:  # not necessarily the case, as train and val differ
            samples[(sid, obj_id, repetition_set)] += [(action, plausibility, rater_code)]

    # filter samples that did not receive enough ratings
    samples = {k: keep_only_k_occurrences(v, min_ratings, same_rating_only=same_rating_only) for k, v in
               samples.items()}

    # filter low-entropy samples
    if min_action_object_entropy is not None:
        print('FILTER BY ENTROPY')
        instance_action_entropies = compute_action_object_entropies(samples, object_classes, plaus_only=False)
        valid_object_actions = set(oa for oa, ent in instance_action_entropies if ent > min_action_object_entropy)
        samples = {k: [r for r in ratings if (object_classes[k[:2]], r[0]) in valid_object_actions] for k, ratings in
                   samples.items()}
        samples = {k: ratings for k, ratings in samples.items() if len(ratings) > 0}

    print(len([1 for s in samples.values() if len(s) > 0]), 'samples')

    if full_annotation == 'strict':
        # identify samples that have all actions annotated
        valid_samples = {(s, o, rs): v for (s, o, rs), v in samples.items() if
                         len(set(x[0] for x in v)) == len(set(required[k[:2]])) and len(v) > 0}
    elif full_annotation == 'strict_test':
        # a sample is valid if it is either not from test set or if all required actions are annotated.
        valid_samples = {(s, o, rs): v for (s, o, rs), v in samples.items() if len(v) > 0 and
                         (metadata[s][0] != 'test' or
                          len(set(x[0] for x in v)) == len(set(required[(s, o)])))}
    elif full_annotation == 'full':
        # identify samples with at least one annotated action
        valid_samples = {k: v for k, v in samples.items() if len(v) > 0}
    else:
        raise ValueError

    # remove sample scenes
    sample_scenes = set((s[0], s[1]) for s in sample_scenes)
    valid_samples = {(s, o, rs): v for (s, o, rs), v in valid_samples.items() if (s, o) not in sample_scenes}
    print(len(valid_samples), 'valid samples')

    count_test = Counter([x[0] for k, v in valid_samples.items() for x in v if metadata[k[0]][0] == 'test'])
    count_train = Counter([x[0] for k, v in valid_samples.items() for x in v if metadata[k[0]][0] == 'train'])

    all_actions = sorted(count_train.keys())
    print('train', sum(count_train.values()), [count_train[a] for a in all_actions])
    print('test', sum(count_test.values()), [count_test[a] for a in all_actions])

    duplicate_scenes = [s for (s, o, rs), v in valid_samples.items()
                        if len(set(map(get(0, 2), v))) != len(list(map(get(0, 2), v)))]

    print([s for s in duplicate_scenes if s not in critical_scenes])
    assert all(s in critical_scenes for s in duplicate_scenes)

    return valid_samples


def build_rating_count_mat(k, v, all_actions, required):
    mat = np.zeros((len(all_actions), len(ALL_RATINGS)))
    # mask = np.zeros(len(all_actions))

    annotated_actions = set(a for a, _, _ in v)

    # XXXXXXXXXXXXXXX
    # Here is some problem!!!
    # shouldn't actions be impossible by default? This means mat[:,0]=1 if an action is not required?
    # Also, the test set should be complete but apparently is not.

    # set mask according to required actions
    mask = np.array([a in annotated_actions or a not in required[k] for a in all_actions])

    for action, rating, code in v:
        action = action.replace(' ', '_')
        # print(rating)
        mat[all_actions.index(action), ALL_RATINGS.index(rating)] += 1

    # set mask according to available annotations
    # mask = mat.sum(1) > 0

    # print(required[k], v)
    # print(mask)

    return mat, mask


def build_labels(action_mat, mask, output_mode, ordinal_weights=None):
    if output_mode == 'scalar_prob':
        # old: to_plaus = np.array([-1, -2, 1, 3])
        to_plaus = np.array(ordinal_weights)
        action_vec = np.maximum(0, action_mat.dot(to_plaus / to_plaus[-1]) / (0.00001 + action_mat.sum(1)))
        return action_vec.astype('float32')

    # elif self.output_mode == 'dual':
    #     # old: to_plaus = np.array([-1, -2, 1, 3])
    #     to_plaus = np.array([0, -2, 0.3, 3])
    #     action_vec = np.maximum(0, action_mat.dot(to_plaus / to_plaus[-1]) / (0.00001 + action_mat.sum(1)))
    #     return action_vec.astype('float32')

    elif output_mode == 'prob_dist':
        labels = np.zeros((len(action_mat), len(ALL_RATINGS)), dtype='float32')
        labels[:, 0] = 1  # set all valid actions to impossible by default
        sum_per_row = action_mat[:].sum(1)

        for i in np.where(mask > 0)[0]:
            if sum_per_row[i] > 0:
                labels[i] = (action_mat[i] / sum_per_row[i])
            else:
                labels[i] = np.array([1, 0, 0])

        return labels.astype('float32')