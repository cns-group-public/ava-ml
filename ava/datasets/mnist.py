import random
import cv2
import numpy as np

from os.path import join
from .. import PATHS
from ..core.dataset import DatasetBase


class MNIST(DatasetBase):

    def __init__(self, subset, factor=1, augmentation=None, resize_factor=None, seed=None):
        # super().__init__(('image', 'label',), dataset_types=('Image', None))
        super().__init__('image->C')
        # self.lock = threading.Lock()

        self.augmentation = augmentation

        x_filenames = ['train-images-idx3-ubyte', 't10k-images-idx3-ubyte']
        y_filenames = ['train-labels.idx1-ubyte', 't10k-labels-idx1-ubyte']

        if subset == 'train':
            interval = 0, 50000
        elif subset == 'val':
            interval = 50000, 60000
        elif subset == 'test':
            interval = 60000, None
        else:
            raise ValueError('Invalid subset:' + subset)

        data_x, data_y = [], []
        for x_filename in x_filenames:
            with open(join(PATHS['MNIST'], x_filename), 'rb') as f:
                data_x += [np.frombuffer(f.read(), np.uint8, offset=16).reshape(-1, 1, 28, 28) / np.float32(256)]

        for y_filename in y_filenames:
            with open(join(PATHS['MNIST'], y_filename), 'rb') as f:
                data_y += [np.frombuffer(f.read(), np.uint8, offset=8)]

        self.data_x = np.concatenate(data_x, axis=0)
        self.data_y = np.concatenate(data_y, axis=0)

        if seed is not None:  # shuffle data
            indices = list(range(len(self.data_x)))
            random.seed(seed)
            random.shuffle(indices)
            self.data_x = [self.data_x[i] for i in indices]
            self.data_y = [self.data_y[i] for i in indices]

        self.data_x = self.data_x[interval[0]:interval[1]]
        self.data_y = self.data_y[interval[0]:interval[1]]

        # this is just bullshit for testing
        self.data_x *= factor

        if resize_factor is not None:  # reduce according to n_samples
            self.data_x = self.data_x[:int(resize_factor * len(self.data_x))]
            self.data_y = self.data_y[:int(resize_factor * len(self.data_y))]

        self.sample_ids = tuple(range(0, len(self.data_x)))
        self.sample_scene_ids = self.sample_ids

        self.default_loss = 'cross_entropy'
        self.default_metrics = ['accuracy']
        self.model_config.update({'k': 5, 'input_channels': 1, 'n_classes': 10})

    def __getitem__(self, index):

        img = self.data_x[index]

        if self.augmentation:
            tx, ty = np.random.randint(-3, 4), np.random.randint(0, 4)
            img = np.roll(img, tx, axis=0)
            img = np.roll(img, ty, axis=1)

        return (img,), (self.data_y[index].astype('int'),)


class MNISTT(DatasetBase):
    """ Pairs of MNIST images with """

    def __init__(self, subset, factor=1, augmentation=None, resize_factor=None):
        super().__init__(('image1', 'image2', 'rot', 'tx', 'ty'),
                         dataset_types=(('Image', {'color': True}), ('Image', {'color': True}), None, None, None))

        self.mnist = MNIST(subset)
        self.sample_ids = self.mnist.sample_ids
        self.model_config = self.mnist.model_config

    def __getitem__(self, index):
        (x,), _ = self.mnist[index]
        x = x[0]
        img_orig = x.reshape((1,) + x.shape).copy()

        translation_x = np.random.randint(-3, 4)
        translation_y = np.random.randint(-3, 4)
        # rotation = np.random.uniform(-np.pi/6, np.pi/6)
        rotation = np.random.uniform(-30, 30)

        x = np.roll(x, translation_x, axis=0)
        x = np.roll(x, translation_y, axis=1)

        rows, cols = x.shape
        M = cv2.getRotationMatrix2D((cols/2, rows/2), rotation, 1)
        dst = cv2.warpAffine(x, M, (cols, rows), borderMode=0)

        rotation_bin = int(10 + rotation // 3)
        t_x_bin = 3 + int(translation_x)
        t_y_bin = 3 + int(translation_y)

        x = x.reshape((1,) + x.shape)
        dst = dst.reshape((1,) + dst.shape)

        # dst = (dst > 0.2).astype('float32')

        return (img_orig, dst), (rotation_bin, t_x_bin, t_y_bin)
