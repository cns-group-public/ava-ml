import json
import os
import cv2
import numpy as np
from PIL import Image
from ..core.dataset import DatasetBase


class FolderwiseSegmentation(DatasetBase):

    def __init__(self, subset, path, parts, image_fp=lambda folder: '{}/{}_img.jpg'.format(folder, folder),
                 part_fp=lambda folder, part: '{}/{}_{}.png'.format(folder, folder, part),
                 mask_fp=None,
                 process_seg=lambda x: cv2.cvtColor(x, cv2.COLOR_BGR2GRAY),
                 binary=False):
        super().__init__('image->denseM(segmentation),denseM(mask)')

        self.mask_fp = mask_fp
        self.process_seg = process_seg
        self.binary = binary
        self.part_fp = part_fp
        self.image_fp = image_fp
        self.parts = parts
        self.path = path
        self.sample_ids = tuple(d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d)))

        if not self.binary:
            self.dataset_types = self.dataset_types[0], 'LabelImage'

    def __getitem__(self, index):
        img = cv2.imread(os.path.join(self.path, self.image_fp(self.sample_ids[index])))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        seg = []
        for part in self.parts:
            filename = os.path.join(self.path, self.part_fp(self.sample_ids[index], part))
            seg_img = cv2.imread(filename)
            assert seg_img is not None, 'image not found: {}'.format(filename)
            seg += [self.process_seg(seg_img)]

        seg = np.array(seg, dtype='int64' if not self.binary else 'float32')

        mask = None
        if self.mask_fp is not None:
            mask = cv2.imread(os.path.join(self.path, self.mask_fp(self.sample_ids[index])), cv2.IMREAD_GRAYSCALE)

        img = img.transpose([2, 0, 1])

        if not self.binary:
            seg = seg.argmax(0)

        return (img,), (seg, mask)



