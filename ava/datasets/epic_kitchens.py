import time
import os
from bisect import bisect_left, bisect_right
from collections import defaultdict
from functools import partial
from itertools import groupby
from scipy.stats import mode
from random import shuffle

import cv2
import numpy as np

from os.path import join

import torch
import re

from ava.losses import splitted_cross_entropy
from ava.metrics import AccuracyMetric
from ..transformations.io import imread
from ..transformations.sample import sample_binned, sample_equidistant
from ..core.dataset import DatasetBase
from ..core.logging import logger
from .. import PATHS
from ..transformations import tensor_resize, pad_to_square, random_crop, random_crop_slices, get_gamma_offset_lut, apply_gamma_offset

# useful settings: anticipation=1.0, gap=0.1 (otherwise too much overlap)


EPIC_RESOLUTIONS = defaultdict(lambda: (1080, 1920), {
    'P12_01': (720, 1280), 'P12_02': (720, 1280), 'P12_03': (720, 1280), 'P12_04': (720, 1280),
    'P12_05': (1440, 2560), 'P12_06': (1440, 2560),
})

EPIC_FPS = defaultdict(lambda: 59.94, {
        'P09_07': 29.97, 'P09_08': 29.97, 'P10_01': 29.97, 'P10_04': 29.97, 'P11_01': 29.97, 'P18_02': 29.97,
        'P18_03': 29.97,
        'P17_01': 48, 'P17_02': 48, 'P17_03': 48, 'P17_04': 48,
        'P18_09': 90
})


def load_epic(subset, bboxes=False):

    with open(join(PATHS['EPIC_KITCHENS'], 'annotations', 'EPIC_verb_classes.csv')) as fh:
        verb_classes = [l.split(',') for l in fh.read().split('\n')[1:] if len(l) > 0]
        assert tuple(l[0] for l in verb_classes) == tuple(str(i) for i in range(125))
        all_verbs = [l[1] for l in verb_classes]

    with open(join(PATHS['EPIC_KITCHENS'], 'annotations', 'EPIC_noun_classes.csv')) as fh:
        noun_classes = [l.split(',') for l in fh.read().split('\n')[1:] if len(l) > 0]
        assert tuple(l[0] for l in noun_classes) == tuple(str(i) for i in range(352))
        all_nouns = [l[1] for l in noun_classes]

    with open(join(PATHS['EPIC_KITCHENS'], 'annotations', 'EPIC_train_object_labels.csv')) as fh:

        if bboxes:
            objects = [re.split(r',(?!\s)', l) for l in fh.read().split('\n')[1:] if len(l) > 0]

            # ugly hack, thanks to shitty data representation
            objects = [x[:5] + [eval(x[5][1:-1]) if x[5] != '[]' else None] for x in objects]
            objects = [(o[3], int(o[4]), int(o[0]), o[1], o[5]) for o in objects]
        else:
            objects = [l.split(',') for l in fh.read().split('\n')[1:] if len(l) > 0]
            objects = [(o[3], int(o[4]), int(o[0]), o[1]) for o in objects]

        objects = sorted(objects, key=lambda x: (x[0], x[1]))
        all_objects = dict(set((o[2], o[3]) for o in objects))

    if subset in {'train', 'val', 'train-s', 'val-s', 'train-ez', 'val-ez', 'train-ez-s', 'val-ez-s', 'all'}:
        with open(join(PATHS['EPIC_KITCHENS'], 'annotations', 'EPIC_train_action_labels.csv')) as fh:
            samples = [l.split(',') for l in fh.read().split('\n')[1:] if len(l) > 0]

        # extract uid, video_id, start_frame, stop_frame, verb_class, noun_class (in that order)
        original_samples = [(None, s[2], int(s[6]), int(s[7]), int(s[9]), int(s[11])) for s in samples]
        folder_name = 'train'

    elif subset == 'test':
        samples = []

        for st in {'test_s1', 'test_s2'}:
            with open(join(PATHS['EPIC_KITCHENS'], 'annotations', 'EPIC_{}_timestamps.csv'.format(st))) as fh:
                samples += [l.split(',') for l in fh.read().split('\n')[1:] if len(l) > 0]

        # first is uid which only exists in test set
        original_samples = [(s[0], s[2], int(s[5]), int(s[6]), None, None) for s in samples]
        folder_name = 'test'

    else:
        raise ValueError('Invalid subset')

    return all_verbs, all_nouns, all_objects, objects, original_samples, folder_name


def get_valid_indices(subset, scene_ids):

    # split based on index
    if subset in {'train-ez', 'val-ez', 'train-ez-s', 'val-ez-s'}:
        all_scene_ids = sorted(list(set(scene_ids)))

        if subset == 'train-ez':
            valid_scenes = set(s for s in all_scene_ids if 0 < hash(s) % 15 < 11)
        elif subset == 'val-ez':
            valid_scenes = set(s for s in all_scene_ids if 11 < hash(s) % 15)
        elif subset == 'train-ez-s':
            valid_scenes = set(s for s in all_scene_ids if 0 < hash(s) % 15 < 9)
        elif subset == 'val-ez-s':
            valid_scenes = set(s for s in all_scene_ids if 9 < hash(s) % 15 < 11)
        else:
            raise ValueError('invalid subset: ' + subset)

        return [i for i, s in enumerate(scene_ids) if s in valid_scenes]

    # split based on person
    elif subset in {'train', 'train-s', 'val-s', 'val'}:

        if subset == 'train':
            indices = [i for i, s in enumerate(scene_ids) if 0 < int(s[1:3]) < 27]
        elif subset == 'val':
            indices = [i for i, s in enumerate(scene_ids) if int(s[1:3]) > 27]
        elif subset == 'train-s':
            indices = [i for i, s in enumerate(scene_ids) if 0 < int(s[1:3]) < 24]
        elif subset == 'val-s':
            indices = [i for i, s in enumerate(scene_ids) if 24 < int(s[1:3]) < 27]
        else:
            raise ValueError('invalid subset: ' + subset)

        return indices

    elif subset == 'all':
        return list(range(len(scene_ids)))
    else:
        raise ValueError('Invalid subset: ' + subset)


class EpicKitchens(DatasetBase):

    def __init__(self, subset, n_frames=10, image_size=(100, 150), crop_padding=30, pad_square=False,
                 anticipate=None, input_data='images', shuffle=False, sampling='binned', multi_actions=None,
                 clean=False, shuffle_order=False, lookahead=None, no_aug=False, ):
        """
        :param subset:
        :param n_frames:
        :param image_size:
        :param crop_padding:
        :param anticipate: Tuple (anticipation duration, gap)
        :param input_data: Data used to make predictions, e.g. images or object matrix
        """
        # super().__init__(('frames', 'flow', 'prev', 'verb_label', 'obj_label'),
        #                  dataset_types=(('ImageSlice', {'channel_dim': 0, 'maps_dim': 1, 'color': True}),
        #                                 None, None, None, None))

        super().__init__('video(frames),None,None->C(verb_label),C(obj_label)')

        # assert (history and anticipate) or not history, 'history only works with anticipation'

        # self.subsequent_labels = subsequent_labels
        self.pad_square = pad_square
        assert not self.pad_square or image_size[0] == image_size[1]

        self.lookahead = lookahead
        self.shuffle_order = shuffle_order
        self.n_frames = n_frames
        self.sampling = sampling
        self.multi_actions = multi_actions
        self.input_data = input_data
        self.image_size = tuple(image_size)

        self.augmentation = subset.startswith('train') and not no_aug

        self.all_verbs, self.all_nouns, self.all_objects, objects, self.original_samples, self.folder_name = load_epic(subset)
        objects = {g: sorted(v, key=lambda x: x[1]) for g, v in groupby(objects, key=lambda x: x[0])}

        if anticipate:
            assert type(anticipate) in {tuple, list}, 'anticipate has invalid type: {}'.format(anticipate)
            assert type(anticipate[0]) in {float, str} and type(anticipate[1]) == float, 'anticipate has invalid form: {}'.format(anticipate)

            # ant_duration = int(anticipate[0] * 60) if anticipate[0] != 'all' else 'all'  # fps is always 60
            # ant_gap = int(anticipate[1] * 60)  # = offset to the actual action
            
            if anticipate[0] == 'all':
                begin_at = lambda x: 0
                condition = lambda x: True
            else:
                # begin = current_time - anticipation_time - gap_size
                begin_at = lambda fps, x: max(0, x - int(anticipate[0] * fps) - int(anticipate[1] * fps))
                condition = lambda fps, x: x > int(anticipate[0] * fps) + int(anticipate[1] * fps)

            samples = [(s[0], s[1],
                        begin_at(EPIC_FPS[s[0]], int(s[2])),
                        int(s[2]) - int(anticipate[1] * EPIC_FPS[s[0]]), s[4], s[5])
                       for s in self.original_samples if condition(EPIC_FPS[s[0]], s[2])]

            # if self.lookahead is not None:
            #     samples = [samples[i] + (np.array([samples[j][4] for j in range(i, i+self.lookahead)]),
            #                              np.array([samples[j][5] for j in range(i, i+self.lookahead)]))
            #                for i in range(0, len(samples) - self.lookahead - 1)]
            #     self.chunk_names = self.chunk_names + ('next_verbs', 'next_objects')
            #     self.dataset_types = self.dataset_types + (None, None)
            #     self.chunk_hooks.update({'next_verbs': lambda x: str(x), 'next_objects': lambda x: str(x)})

        else:
            samples = self.original_samples

        if multi_actions is not None:

            samples_grouped = {g: list(s) for g, s in groupby(samples, key=lambda x: x[1])}
            samples_new = []
            for g, gs in samples_grouped.items():
                sid, vid, starts, ends, verbs, nouns = zip(*gs)
                samples_new += [(sid[i], g, starts[i:i + multi_actions], ends[i:i + multi_actions], verbs[i:i + multi_actions], nouns[i:i + multi_actions]) for i in range(len(gs) - multi_actions - 1)]

            # convert to binary indicator vectors and set start and end
            samples = []
            for sid, vid, starts, ends, verbs, nouns in samples_new:

                verbs_binary = np.zeros(len(self.all_verbs), 'float32')
                verbs_binary[list(verbs)] = 1

                nouns_binary = np.zeros(len(self.all_nouns), 'float32')
                nouns_binary[list(nouns)] = 1

                samples += [(sid, vid, starts[0], ends[-1], verbs_binary, nouns_binary)]

        t_start = time.time()
        # determine all actions that fall in the interval of each sample. Store them as list of tuples in sample_events
        samples = sorted(samples, key=lambda x: (x[1], x[2]))

        orig_samples_sorted = sorted(self.original_samples, key=lambda x: (x[1], x[2]))
        sg = {g: list(s) for g, s in groupby(orig_samples_sorted, key=lambda x: x[1])}
        starts = {g: [x[2] for x in s] for g, s in sg.items()}

        sample_events, sample_objects = [], []  # all events during each sample interval
        for g, group_samples in groupby(samples, key=lambda x: x[1]):

            _, object_frames, object_labels, _ = zip(*objects[g])

            for i_sample, (_, group, start, end, v, n) in enumerate(group_samples):
                i = max(0, bisect_left(starts[group], start) - 1)
                events = []
                while True:
                    if start <= sg[group][i][2] and sg[group][i][3] <= end and i != i_sample:
                        verb, noun = sg[group][i][4:6]
                        events += [(verb, noun, (sg[group][i][2] - start) / (end - start +1))]

                    if sg[group][i][2] > end: break
                    if i == len(sg[group]) - 1: break

                    i += 1
                sample_events += [events]

                # same for objects
                i = bisect_left(object_frames, start)
                j = bisect_right(object_frames, end)

                sample_objects += [list(zip(object_labels[i:j], [(f - start) / (end - start + 1) for f in object_frames[i:j]]))]

        print('time for events', time.time() - t_start)

        assert len(samples) == len(sample_events)

        # only consider videos that actually exist on disk
        if self.input_data == 'images':
            available_videos = set(os.listdir(join(PATHS['EPIC_KITCHENS'], 'frames/{}'.format(self.folder_name))))
            invalid_indices = set(i for i, s in enumerate(samples) if s[1] not in available_videos)
        else:
            invalid_indices = set()

        if clean:
            samples = [s for s in samples if s[3] > 2]

        valid_indices = get_valid_indices(subset, [s[1] for s in samples])

        self.samples = [samples[i] for i in valid_indices if i not in invalid_indices]
        self.sample_events = [sample_events[i] for i in valid_indices if i not in invalid_indices]
        self.sample_objects = [sample_objects[i] for i in valid_indices if i not in invalid_indices]

        # do not use verb and noun for sample id as they can get big in binary case
        self.sample_ids = tuple([s[:4] for s in self.samples])

        print(self.sample_ids[0])

        if 'EPIC_KITCHENS_FRAMES' in PATHS:
            self.frame_path = PATHS['EPIC_KITCHENS_FRAMES']
        else:
            self.frame_path = os.path.join(PATHS['EPIC_KITCHENS'], 'frames')

        # augmentation
        self.intermediate_size = image_size[0] + crop_padding, image_size[1] + crop_padding

        if multi_actions is not None:
            # deal with binary variables
            self.visualization_hooks['verb_label'] = lambda vec: ','.join(self.all_verbs[i] for i in np.where(vec > 0)[0])
            self.visualization_hooks['obj_label'] = lambda vec: ','.join(self.all_nouns[i] for i in np.where(vec > 0)[0])
            self.model_config.update({'multiclass': 1})
        else:
            self.visualization_hooks['verb_label'] = lambda i: '{} ({})'.format(self.all_verbs[i] if i is not None else None, i)
            self.visualization_hooks['obj_label'] = lambda i: '{} ({})'.format(self.all_nouns[i] if i is not None else None, i)

        # self.parameter_variables = [n_frames, image_size, anticipate, sampling[0], self.shuffle_order]
        # aux_size = None

        self.model_config.update({
            'n_classes': len(self.all_verbs) + len(self.all_nouns),
            # 'n_classes': (len(self.all_verbs), len(self.all_nouns)),
            'image_size': self.image_size,
            'n_frames': self.n_frames,
            # 'auxiliary': aux_size,
            # 'lookahead': self.lookahead
        })

        self.default_metrics = [
            partial(AccuracyMetric, name='AccVerb', pred_range=(0, len(self.all_verbs))),
            partial(AccuracyMetric, name='AccNoun', pred_range=(len(self.all_verbs), len(self.all_verbs) + len(self.all_nouns))),
        ]

        # split ranges: list of (gt_index, start, end)
        self.default_loss = partial(splitted_cross_entropy, split_ranges=[
            (0, 0, len(self.all_verbs)),
            (1, len(self.all_verbs), len(self.all_verbs) + len(self.all_nouns))
        ])

        if self.input_data in {'actions', 'objects', 'actions+objects'}:
            self.dataset_types = (('Image', {'color': False}),) + self.dataset_types[1:]
            n_actions = len(self.all_verbs) + len(self.all_nouns)
            n_objects = max(self.all_objects) + 1
            self.model_config['vector_input'] = {
                'actions': n_actions, 'objects': n_objects, 'actions+objects': n_actions + n_objects}[self.input_data]

    def mode(self):
        verb, noun = zip(*[s[4:6] for s in self.samples])
        print('modes:', self.all_verbs[mode(verb).mode[0]], self.all_nouns[mode(noun).mode[0]])

        return mode(verb).mode, mode(noun).mode

    def get_previous_indices(self, point_in_time, action_times, action_acc):
        end = bisect_left(action_times, point_in_time)
        start = bisect_left(action_times, point_in_time - 60*self.history_time)  # 60 FPS
        return action_acc[start:end]

    def __getitem__(self, index):

        if self.lookahead is not None:
            _, video_id, start, end, verb_class, object_class, next_verbs, next_objects = self.samples[index]
            target = (verb_class, object_class, next_verbs, next_objects)
        else:
            _, video_id, start, end, verb_class, object_class = self.samples[index]
            target = (verb_class, object_class)

        # print([self.all_verbs[v] + ' ' + self.all_nouns[n] + ' ' + (str(i)) for v, n, i in self.sample_events[index]])

        if 'EPIC_KITCHENS_FRAMESxxx' in PATHS:
            sample_folder = join(PATHS['EPIC_KITCHENS_FRAMES'], self.folder_name, video_id)
        else:
            sample_folder = join(PATHS['EPIC_KITCHENS'], 'frames', self.folder_name, video_id)

        # print('->', past_encoding.sum(), start, end, len(indices), video_id)

        if end <= 2:  # INVALID SAMPLE
            logger.important('Invalid sample: {} {}'.format(video_id, index))
            return (np.zeros((3, self.n_frames, self.image_size[0], self.image_size[1]), 'float32'), None, None), target

        # images = sorted([f for f in os.listdir(sample_folder)])

        # frame ids start with 1
        if self.sampling == 'binned':
            image_ids = sample_binned(np.arange(start + 1, end), self.n_frames, bin_randomness=0.3)
        elif self.sampling == 'equidistant':
            image_ids = sample_equidistant(np.arange(start + 1, end), self.n_frames, offset=0)
        else:
            raise ValueError('Invalid sampling')

        if self.shuffle_order:
            shuffle(image_ids)

        # image_ids = np.arange(start + 1, end)
        # image_ids = np.array_split(image_ids, self.n_frames)
        # #assert all(len(s) > 0 for s in image_ids), '{} {}'.format(str(image_ids), len(images))
        # image_ids = np.array([np.random.choice(s) if len(s) > 0 else None for s in image_ids])

        # print(old_start, old_end, image_ids)

        img_stack = []
        slices = None

        if self.augmentation:
            gamma_all = 2 / (np.random.normal(1, 0.5, 1) + np.random.normal(1, 0.1, 3))
            gamma_offset_lut = get_gamma_offset_lut(gamma_all, np.random.normal(0, 2, 3))
        else:
            gamma_offset_lut = None

        if self.input_data in {'actions', 'objects', 'actions+objects'}:

            if self.input_data in {'actions', 'actions+objects'}:
                mat_verb = np.zeros((self.n_frames, len(self.all_verbs)), dtype='float32')
                mat_noun = np.zeros((self.n_frames, len(self.all_nouns)), dtype='float32')
                for v, n, i in self.sample_events[index]:
                    frame = int(i * self.n_frames)
                    mat_verb[frame, v] = 1
                    mat_noun[frame, n] = 1

            if self.input_data in {'objects', 'actions+objects'}:
                mat_objects = np.zeros((self.n_frames, max(self.all_objects)+1), dtype='float32')
                for o, i in self.sample_objects[index]:
                    frame = int(i * self.n_frames)
                    mat_objects[frame, o] = 1

            if self.input_data == 'actions+objects':
                img_stack = np.concatenate([mat_verb, mat_noun, mat_objects], axis=1)
            elif self.input_data == 'actions':
                img_stack = np.concatenate([mat_verb, mat_noun], axis=1)
            else:
                img_stack = np.concatenate([mat_objects], axis=1)

        elif self.input_data == 'images':
            for i in image_ids:

                if i is None:
                    # ImageNet mean
                    shape = (self.image_size[0], self.image_size[1], 3)
                    img_stack += [np.ones(shape, dtype='float32') * [124, 116, 104]]
                else:
                    img = cv2.imread(join(sample_folder, 'frame_{}.jpg'.format(str(i).zfill(10))))
                    assert img is not None, 'not found: {}'.format(
                        join(sample_folder, 'frame_{}.jpg'.format(str(i).zfill(10))))

                    if img.ndim == 2:
                        img = np.dstack([img, img, img])
                        logger.warning(
                            'non-rgb image: {}'.format(join(sample_folder, 'frame_{}.jpg'.format(str(i).zfill(10)))))

                    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

                    if self.pad_square:
                        img = pad_to_square(img)

                    if self.augmentation:
                        img = tensor_resize(img, self.intermediate_size, interpret_as_min_bound=True, channel_dim=2,
                                            interpolation='bilinear')

                        img = apply_gamma_offset(img, lut=gamma_offset_lut, channel_dim=2)

                        if slices is None:  # set slices only once (per video)
                            slices = random_crop_slices(img.shape[:2], self.image_size)

                        img = img[slices]
                    else:
                        img = tensor_resize(img, self.image_size, channel_dim=2, interpolation='bilinear')

                    img_stack += [img]

            img_stack = np.array(img_stack)
            img_stack = img_stack[:, :, :, [2, 1, 0]]  # BGR -> RGB
            img_stack = img_stack.transpose([3, 0, 1, 2]).astype('float32')
        else:
            img_stack = None

        return (img_stack, None, None), target


class EpicObjects(DatasetBase):

    def __init__(self, subset, image_size=(100, 180), no_image=False, augmentation=1, show_bboxes=False):
        super().__init__(('img', 'labels'), dataset_types=('Image', None))

        self.image_size = image_size
        self.no_image = no_image
        self.augmentation = augmentation
        self.show_bboxes = show_bboxes

        all_verbs, all_nouns, self.all_objects, objects, original_samples, folder_name = load_epic(subset, bboxes=True)

        objects_grouped = {g: list(o) for g, o in groupby(objects, key=lambda x: (x[0], x[1]))}
        frames = [(g, [x[2] for x in o], [x[4][0] for x in o if x[4] is not None]) for g, o in objects_grouped.items()]

        # ignore frames that do not exist
        available_videos = set(os.listdir(join(PATHS['EPIC_KITCHENS'], 'frames/{}'.format(folder_name))))
        video_lengths = {vid: len(os.listdir(join(PATHS['EPIC_KITCHENS'], 'frames/{}'.format(folder_name), vid))) for vid in available_videos}
        invalid_indices = set(i for i, f in enumerate(frames) if f[0][0] not in available_videos or f[0][1] >= video_lengths[f[0][0]])

        # if bboxes:
        #     invalid_indices.union(i for i, f in enumerate(frames) if len(f[1]) != len(f[2]))

        valid_indices = get_valid_indices(subset, [f[0][0] for f in frames])
        frames = [frames[i] for i in valid_indices if i not in invalid_indices]

        # remove frames that do not have bounding boxes
        frames = [f for f in frames if len(f[1]) == len(f[2])]

        self.folder_name = 'train'
        self.sample_ids = tuple(frames)

        self.model_config.update({'n_classes': max(self.all_objects) + 1, 'multiclass': 1})

        self.visualization_hooks['labels'] = lambda vec: ', '.join(self.all_objects[o] for o in np.where(vec > 0)[0])

    def __getitem__(self, index):

        (video_id, frame), labels, bboxes = self.sample_ids[index]

        label_vec = np.zeros(max(self.all_objects) + 1, dtype='float32')
        label_vec[labels] = 1

        sample_folder = join(PATHS['EPIC_KITCHENS'], 'frames', self.folder_name, video_id)
        filename = join(sample_folder, 'frame_' + str(frame).zfill(10) + '.jpg')

        if self.no_image:
            assert os.path.isfile(filename), 'file does not exist: {}'.format(filename)
            return (np.zeros(self.image_size + (3,), dtype='float32'),), (None,)

        img = imread(filename)

        if self.augmentation > 0:
            img = tensor_resize(img, (self.image_size[0] + 10, self.image_size[1] + 10), interpret_as_min_bound=True)
            img = random_crop(img, self.image_size)

            gamma = 3.4 / (np.random.normal(1, 0.5 * self.augmentation, 1) + np.random.normal(1, 0.1 * self.augmentation, 3))
            img = apply_gamma_offset(img, gamma=gamma, offset=np.random.normal(0, 2, 3), channel_dim=2)

        else:
            img = tensor_resize(img, (self.image_size[0], self.image_size[1]))

        if self.show_bboxes:
            for top, left, height, width in bboxes:

                orig_h, orig_w = EPIC_RESOLUTIONS[video_id]

                top, height = int(self.image_size[0] * top / orig_h), int(self.image_size[0] * height / orig_h)
                left, width = int(self.image_size[1] * left / orig_w), int(self.image_size[1] * width / orig_w)
                img = cv2.rectangle(img, (left, top), (left + width, top + height), color=(255, 0, 0), thickness=5)

            # img = cv2.rectangle(img, (50, 10), (70, 20), color=(255, 0, 0))

        img = img[:, :, [2, 1, 0]]
        img = img.transpose([2, 0, 1])

        # print([self.all_objects[o] for o in labels])

        img = img.astype('float32')

        return (img,), (label_vec,)


class MiniEpic(EpicKitchens):

    def __init__(self, subset, max_samples, crop_padding=30, anticipate=None,
                 augmentation=False, image_size=None, sampling='binned', n_frames=24):
        image_size = (150, 225) if image_size is None else image_size
        super().__init__(subset, max_samples, n_frames, image_size, crop_padding, anticipate, augmentation, shuffle=True, sampling=sampling)


class EpicKitchensHistory(EpicKitchens):
    def __init__(self, subset, n_frames=10, image_size=(100, 150), crop_padding=30,
                 anticipate=None, augmentation=False, history=None, history_time=60, ignore_images=False, oracle=False):
        super().__init__(subset, n_frames, image_size, crop_padding, anticipate, augmentation)
        self.history = history
        self.history_time = history_time
        self.oracle = oracle

        original_samples = sorted(self.original_samples, key=lambda x: (x[0], x[1]))  # sort by id and start frame

        self.prev_actions_ends = {}
        self.prev_actions_acc = {}
        for video_id, group_samples in groupby(original_samples, lambda x: x[0]):

            self.prev_actions_ends[video_id] = []
            self.prev_actions_acc[video_id] = []

            for _, start, end, verb, noun in group_samples:
                self.prev_actions_acc[video_id] += [(verb, noun)]
                self.prev_actions_ends[video_id] += [end]

        self.parameter_variables += [history, oracle]

        if self.history in {'matrix', 'matrix_time'}:
            aux_size = len(self.all_verbs) * len(self.all_nouns)
        elif self.history == 'vector':
            aux_size = len(self.all_verbs) + len(self.all_nouns)
        else:
            aux_size = None                

        self.model_config['auxiliary'] = aux_size

    def matrix_encode_history(self, indices):
        """ Matrix indicating if a certain object action combination already occurred """
        past_encoding = np.zeros((len(self.all_verbs), len(self.all_nouns)), dtype='float32')
        past_encoding[[x[0] for x in indices], [x[1] for x in indices]] = 1
        return past_encoding

    def matrix_time_encode_history(self, indices):
        """ Matrix indicating if a certain object action combination already occurred """
        past_encoding = np.zeros((len(self.all_verbs), len(self.all_nouns)), dtype='float32')
        past_encoding[[x[0] for x in indices], [x[1] for x in indices]] = [i for i, _ in enumerate(indices)]

        return past_encoding

    # TODO: history encoding that considers uncertainty of previous predictions

    def vector_encode_history(self, indices):
        """ Vector indicating if action or object already occurred """

        past_verbs = np.zeros(len(self.all_verbs), dtype='float32')
        past_nouns = np.zeros(len(self.all_nouns), dtype='float32')

        past_verbs[[x[0] for x in indices]] = 1
        past_nouns[[x[1] for x in indices]] = 1
        return np.hstack([past_verbs, past_nouns])

    def running_var_reset(self, sample, sample_prev):
        if sample_prev is None or sample[0] != sample_prev[0]:
            print('reset', sample, sample_prev)
        return sample_prev is None or sample[0] != sample_prev[0]

    def running_var_incorporate(self, running_var, variables_x):

        if running_var is not None and self.history is not None:

            # print(running_var)
            # the large number makes sure all indices in running_var['times'] are considered
            indices = self.get_previous_indices(999999, running_var['times'], running_var['acc'])
            if self.history == 'matrix':
                var = self.matrix_encode_history(indices)
            if self.history == 'matrix_time':
                var = self.matrix_time_encode_history(indices)
            elif self.history == 'vector':
                var = self.vector_encode_history(indices)

            var = torch.from_numpy(var).unsqueeze(0).cuda()
            variables_x[2] = var

        return variables_x

    def running_var_add(self, running_var, predictions, sample_id):

        if self.history is not None:
            assert predictions[0].size(0) == 1, 'assume batch size=1'

            if running_var is None:
                running_var = dict(acc=[], times=[], counter=0)

            if not self.oracle:
                idx1 = int(predictions[0][:, :len(self.all_verbs)].argmax(1)[0])
                idx2 = int(predictions[0][:, len(self.all_verbs):].argmax(1)[0])

            # print('qqq', idx1, idx2)
            # print(sample_id)
            # xxx

            # use ground truth instead of prediction
            else:
                _, _, _, idx1, idx2 = sample_id

            running_var['acc'] += [(idx1, idx2)]
            running_var['times'] += [sample_id[1]]  # this contains start time of action
            # running_var['times'] += [running_var['counter']]
            # running_var['counter'] += 1

            return running_var

    def __getitem__(self, index):

        _, video_id, start, end, verb_class, object_class = self.samples[index]

        (img_stack, _, _), (verb_class, object_class) = super().__getitem__(index)

        # Different history encodings
        if self.history == 'matrix':
            indices = self.get_previous_indices(start, self.prev_actions_ends[video_id], self.prev_actions_acc[video_id])
            past_encoding = self.matrix_encode_history(indices)
        elif self.history == 'matrix_time':
            indices = self.get_previous_indices(start, self.prev_actions_ends[video_id], self.prev_actions_acc[video_id])
            past_encoding = self.matrix_time_encode_history(indices)
        elif self.history == 'vector':
            indices = self.get_previous_indices(start, self.prev_actions_ends[video_id], self.prev_actions_acc[video_id])
            past_encoding = self.vector_encode_history(indices)

        elif self.history == 'temporal':
            # Tensor of time x object x noun (sum of each slice is one)?
            # Then apply CNN in model
            raise NotImplementedError

        else:
            past_encoding = None

