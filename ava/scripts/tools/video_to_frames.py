#!/usr/bin/python3

import argparse
import os
import re
from os.path import join

pattern = r'^(.*)\.(avi|mpg|mpeg|mp4)$'


def process_video(video_filename, folder_name, new_folder_name, skip=0, mode=None, length=None, jpg_quality=95,
                  overwrite=False):

    from ava.transformations import resize_by_mode
    import cv2

    match = re.match(pattern, video_filename)
    if match:
        prefix = match.groups()[0]
        filename = join(folder_name, video_filename)
        output_path = join(new_folder_name, prefix)

        if not os.path.isdir(output_path) or overwrite:
            cap = cv2.VideoCapture(filename)

            assert cap.isOpened(), 'failed to open: {}'.format(filename)

            if not os.path.isdir(output_path): os.makedirs(output_path)

            fps = cap.get(cv2.CAP_PROP_FPS)
            n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

            assert n_frames < 10e6  # otherwise zfill will cause problems

            with open(join(output_path, 'info.txt'), 'w') as fp:
                fp.write('fps,{}\nframes,{}\nduration,{}'.format(fps, n_frames))

            frame_valid, frame_i = True, 0
            while frame_valid:
                frame_valid, img = cap.read()

                if frame_valid:
                    img = resize_by_mode(img, mode, length)

                    print(join(output_path, str(frame_i).zfill(6) + '.jpg'), img.shape)
                    params = [cv2.IMWRITE_JPEG_QUALITY, jpg_quality] if jpg_quality is not None else []
                    cv2.imwrite(join(output_path, str(frame_i).zfill(6) + '.jpg'), img, params)
                    frame_i += 1

                    for _ in range(skip):
                        frame_valid, img = cap.read()
                        frame_i += 1
                else:
                    print('invalid frame', frame_i)
                    frame_i += 1
        else:
            print('{} already exists'.format(output_path))


def videos_to_frames(folder_name, length=None, mode=None, threads=False, skip=0, limit=None,
                     filter=None, **kwargs):

    if length is None:
        length_str = ''
    else:
        try:
            length = int(length)
            length_str = str(length)
        except ValueError:
            length = tuple(int(l) for l in length.split(','))
            length_str = 'x'.join(str(l) for l in length)

    mode_shortcuts = {'max_side': 's', 'max_width': 'w', 'max_height': 'h', 'size': '', None: 'orig'}

    folder_name += '/' if not folder_name.endswith('/') else ''

    new_folder_name = '{}_frames_{}{}{}'.format(folder_name[:-1], mode_shortcuts[mode], length_str,
                                                '_skip'+str(skip) if skip is not None else '')

    all_videos = os.listdir(folder_name)
    all_videos = all_videos[:limit]

    if filter is not None:
        all_videos = [v for v in all_videos if v.startswith(filter)]

    if threads == 0:
        for video in all_videos:
            process_video(video, folder_name, new_folder_name, length=length, mode=mode, skip=skip, **kwargs)
    else:
        from multiprocessing import Pool
        from functools import partial
        pool = Pool(processes=threads)
        pool.map(partial(process_video, folder_name=folder_name, new_folder_name=new_folder_name,
                         mode=mode, skip=skip, length=length, **kwargs),
                 all_videos)


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('video_folder', type=str, help='mode')
    parser.add_argument('--skip', type=int, default=0, help='skip')
    parser.add_argument('--limit', type=int, default=None, help='limit')
    parser.add_argument('--length', type=str, default=None, help='input_folder')
    parser.add_argument('--mode', default=None, type=str, help='mode')
    parser.add_argument('--filter', default=None, type=str)
    parser.add_argument('--threads', default=0, type=int)
    parser.add_argument('--jpg-quality', default=None, type=int, help='jpg quality (0-100)')
    parser.add_argument('--overwrite', default=False, action='store_true', help='overwrite existing files')
    # parser.add_argument('--max-videos', type=int, default=None, help='Number of processed videos')
    # parser.add_argument('--frame-skip', type=int, default=4, help='Number of skipped frames between cap
    return parser


if __name__ == '__main__':
    videos_to_frames(**vars(get_parser().parse_args()))
