#!/usr/bin/python3

"""
This script transforms images into features using the feature_extractor API. The directory structure is not changed.
"""

import os
import time
import argparse
import numpy as np
from os.path import join, isdir


def compute_features(folder_name, network_name, size=224, multi=1, skip=1, crop_margin=20, batch_size=16,
                     overwrite=False):
    """
    Conducts feature extraction for all images in folder_name
    if `batch_size` is provided, all images should have the same aspect ratio. If `multi` is > 1, the effective batch
    size will at least be `multi`.
    """

    import cv2
    import torch
    from ava.transformations import tensor_resize
    from ava.models import get_feature_extractor

    assert isdir(folder_name)
    folder_name += '/' if not folder_name.endswith('/') else ''

    net = get_feature_extractor(network_name).cuda()

    skip_str = '' if skip == 1 else 'sk{}_'.format(skip)

    if folder_name == './':
        new_folder_name = 'features_' + skip_str + net.name()
    else:
        new_folder_name = folder_name[:-1] + '_feat_' + skip_str + net.name()

    if not isdir(new_folder_name):
        os.mkdir(new_folder_name)

    img_size = size if multi == 1 else size + crop_margin

    t_start = time.time()

    for dirname, b, filenames in os.walk(folder_name):

        rel_dirname = dirname[len(folder_name):]

        if not isdir(join(new_folder_name, rel_dirname)):
            os.makedirs(join(new_folder_name, rel_dirname))

        batch = []
        target_filenames = []
        for filename in sorted(filenames)[::skip]:

            point_idx = filename.rindex('.')
            prefix = filename[:point_idx]
            suffix = filename[point_idx+1:]

            # print(prefix, suffix, filename)

            if suffix in {'jpg', 'jpeg', 'png', 'bmp', 'gif'}:

                if multi > 1:
                    new_filenames = [join(new_folder_name, rel_dirname, prefix + '_' + str(i).zfill(3) + '.npy')
                                     for i in range(multi)]
                else:
                    new_filenames = [join(new_folder_name, rel_dirname, prefix + '.npy')]

                if all([not os.path.isfile(fn) for fn in new_filenames]) or overwrite:
                    img = cv2.imread(join(folder_name, dirname, filename))
                    img = tensor_resize(img, (img_size, img_size), interpret_as_min_bound=True)

                    if multi > 1:
                        for i in range(multi):
                            offsets = np.random.randint(img.shape[0] - size), np.random.randint(img.shape[1] - size)
                            img2 = img[offsets[0]:offsets[0] + size, offsets[1]:offsets[1] + size]
                            img2 = img2.transpose([2, 0, 1]).astype('float32')
                            batch += [img2]

                    else:
                        offsets = (img.shape[0] - size) // 2, (img.shape[1] - size) // 2
                        img = img[offsets[0]:offsets[0] + size, offsets[1]:offsets[1] + size]
                        img = img.transpose([2, 0, 1]).astype('float32')
                        batch += [img]

                    target_filenames += new_filenames
                else:
                    print('skipped {}'.format(filename))

            if len(batch) == batch_size or len(batch) > batch_size - multi:
                print('run on {} batch'.format(len(batch)))
                print([b.shape for b in batch])
                img_var = torch.from_numpy(np.array(batch)).cuda()
                feat = net(img_var)
                feat = feat.cpu().detach().numpy()

                for fn, feat in zip(target_filenames, feat):
                    np.save(fn, feat)

                batch = []
                target_filenames = []
                # if len(feat) == 1:
                #     np.save(join(new_folder_name, rel_dirname, prefix), feat)
                # else:
                #     for i in range(len(feat)):
                #         np.save(join(new_folder_name, rel_dirname, prefix + '_' + str(i).zfill(3)), feat[i])

                # print(join(folder_name, dirname, filename), '->', join(new_folder_name, rel_dirname, prefix))
                # print(b, new_folder_name, dirname, join(new_folder_name, dirname, filename))

    print('took {:.2f}s'.format(time.time() - t_start))


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('folder_name', type=str, help='input_folder')
    parser.add_argument('network_name',  type=str, help='name of network (e.g. resnet50)')
    parser.add_argument('--size', default=224, type=int, help='input image size')
    parser.add_argument('--crop-margin', default=20, type=int, help='crop margin')
    parser.add_argument('--multi', default=1, type=int, help='extract features for multiple crops')
    parser.add_argument('--batch-size', default=5, type=int, help='batch_size. If multi > 1, batch size will be multiple')
    parser.add_argument('--overwrite', default=False, action='store_true')
    parser.add_argument('--skip', default=1, type=int, help='extract features for multiple crops')
    return parser


if __name__ == '__main__':
    compute_features(**vars(get_parser().parse_args()))
