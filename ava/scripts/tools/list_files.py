#!/usr/bin/python3

import fire
import os
from os.path import normpath, join, isfile


def list_files(folder_name):

    assert not isfile('list_of_files.txt')

    # check for permission
    with open('list_of_files.txt', 'w') as fh:
        fh.write('')

    out = ''
    for dirpath, dirnames, filenames in os.walk(folder_name):

        for filename in filenames:
            out += normpath(join(dirpath, filename)) + '\n'

    with open('list_of_files.txt', 'w') as fh:
        fh.write(out)


if __name__ == '__main__':
    fire.Fire(list_files)

