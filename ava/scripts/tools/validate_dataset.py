#!/usr/bin/python3

import fire
from itertools import combinations
from ava.core.logging import logger
import ava.datasets as ava_datasets


def validate_dataset(dataset, splits='train,val,test', show_intersection=False, **kwargs):

    dataset_type = getattr(ava_datasets, dataset)
    dataset_args = kwargs

    if type(splits) == str:
        splits = splits.split(',')
        assert 1 <= len(splits) <= 3

    datasets = [(dataset_type(s, None, **dataset_args), s) for s in splits]

    for dataset, name in datasets:
        print('{}: {} samples'.format(name, len(dataset.sample_ids)))

    for (a, a_name), (b, b_name) in combinations(datasets, 2):
        if a is not None and b is not None:
            intersection = set(a.sample_ids).intersection(b.sample_ids)
            if len(intersection) == 0:
                logger.important('{}/{} do not intersect'.format(a_name, b_name))
            else:
                if show_intersection:
                    print(intersection)
                logger.warning('{}/{} intersect ({} samples)'.format(a_name, b_name, len(intersection)))


if __name__ == '__main__':
    fire.Fire(validate_dataset)
