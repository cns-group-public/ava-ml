import fire

import ava.datasets as ava_datasets
from ava.core.logging import logger


def install_dataset(dataset_name):
    dataset_type = getattr(ava_datasets, dataset_name)

    if hasattr(dataset_type, 'install'):
        dataset_type.install()
    else:
        logger.warning('Dataset {dataset_name} provides no install method')


if __name__ == '__main__':
    fire.Fire(install_dataset)
