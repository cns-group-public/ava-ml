import fire
from os.path import join


def download_video(yt_link, output_path):
    import youtube_dl
    from youtube_dl import DownloadError

    download_complete = False
    for format in ['134', '18', '43']:

        try:
            ydl_opts = {
                'format': 'noaudio/{}'.format(format),
                'outtmpl': join(output_path, '%(id)s.%(ext)s')
            }
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                ydl.download([yt_link])

            download_complete = True
            break
        except DownloadError:
            print('format {} does not exist for {}'.format(format, yt_link))

    if not download_complete:
        raise ValueError('No matching format found for {}'.format(yt_link))


if __name__ == '__main__':
    fire.Fire(download_video)
