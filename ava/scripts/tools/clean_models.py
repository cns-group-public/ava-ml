#!/usr/bin/python3

import os
from os.path import isdir

import fire


def clean_models():
    from ava import PATHS
    import torch

    for filename in os.listdir(PATHS['TRAINED_MODELS_PATH']):
        filepath = os.path.join(PATHS['TRAINED_MODELS_PATH'], filename)

        if not filepath.endswith('-args') and not isdir(filepath):
            try:
                model_data = torch.load(filepath)
                episodes = model_data['episode'][0]
                if episodes < 1000:
                    do_remove = input('Should {} be removed ({} episodes)? (y/n): '.format(filename, episodes))
                    if do_remove == 'y':
                        os.remove(filepath)

            except BaseException as err:
                print(filename, err)


if __name__ == '__main__':
    fire.Fire(clean_models)
