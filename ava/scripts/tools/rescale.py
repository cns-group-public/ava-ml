#!/usr/bin/python3

import os
import argparse
import random
from os.path import join, isdir


def rescale_image(filenames):

    from ava.transformations import resize_by_mode
    import cv2

    input_filename, target_filename, length, mode, jpg_quality = filenames
    img = cv2.imread(input_filename)
    if img is None:
        print('Not an image: {}'.format(input_filename))
    else:
        shape_inp = img.shape

        if not os.path.isdir(os.path.dirname(target_filename)):
            os.makedirs(os.path.dirname(target_filename))

        img = resize_by_mode(img, mode, length)
        params = [cv2.IMWRITE_JPEG_QUALITY, jpg_quality] if jpg_quality is not None else []
        ret = cv2.imwrite(target_filename, img, params)

        if random.random() > 0.99:
            print(input_filename, '->', target_filename, shape_inp, '->', img.shape, ret)

        assert os.path.isfile(target_filename)


def rescale_folder(folder_name, length, mode, overwrite=False, ext='jpg', jpg_quality=None):

    try:
        length = int(length)
        length_str = str(length)
    except ValueError:
        length = tuple(int(l) for l in length.split(','))
        length_str = 'x'.join(str(l) for l in length)

    assert isdir(folder_name)
    folder_name += '/' if not folder_name.endswith('/') else ''

    mode_shortcuts = {'max_side': 's', 'max_width': 'w', 'max_height': 'h', 'size': ''}
    assert mode in mode_shortcuts

    if folder_name == './':
        new_folder_name = 'rescaled_' + mode_shortcuts[mode] + length_str
    else:
        new_folder_name = folder_name[:-1] + '_' + mode_shortcuts[mode] + length_str

    if not isdir(new_folder_name):
        os.mkdir(new_folder_name)

    all_images = []

    print('scanning folder...(this might take a few minutes)')

    for dirname, b, filenames in os.walk(folder_name):

        if dirname != './' + new_folder_name:

            rel_dirname = dirname[len(folder_name):]

            for filename in filenames:

                target_filename = join(new_folder_name, rel_dirname, filename)
                target_filename = target_filename[:target_filename.rindex('.') + 1] + ext
                input_filename = join(folder_name, rel_dirname, filename)

                if not os.path.isfile(target_filename) or overwrite:
                    all_images += [(input_filename, target_filename, length, mode, jpg_quality)]
                else:
                    print('will not write, target file exists: {}'.format(target_filename))

    from multiprocessing import Pool
    import time

    print('sorting...')
    all_images = sorted(all_images, key=lambda x: x[0])
    print('{} images will be rescaled'.format(len(all_images)))

    t_start = time.time()
    pool = Pool(10)
    pool.map(rescale_image, all_images)

    print('took {}s'.format(time.time() - t_start))


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('folder_name', type=str, help='input_folder')
    parser.add_argument('length', type=str, help='input_folder')
    parser.add_argument('--mode', default='max_side', type=str, help='modes: max_side, max_height, max_width, size')
    parser.add_argument('--ext', default='jpg', type=str, help='file extension (defines image format)')
    parser.add_argument('--jpg-quality', default=None, type=int, help='jpg quality (0-100)')
    parser.add_argument('--overwrite', default=False, action='store_true', help='overwrite existing files')
    return parser


if __name__ == '__main__':
    rescale_folder(**vars(get_parser().parse_args()))
