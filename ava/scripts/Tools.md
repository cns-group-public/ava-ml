There are some useful preparation tools available using

```bash
ava tool <tool>
```

Here `<tool>` can be one of these:

- **`rescale`** Rescale Images
- **`extract-features`** extract features from images
- **`extract-frames`** extract frames from videos
- **`list-files`** writes an index of text files
- **`validate-dataset`** check if subsets overlap
- **`clean-models`** remove models
- **`download-video`** downloads video
