import torch
import torch.nn.functional as nnf
from torch.nn.functional import cross_entropy as torch_cross_entropy
from torch.nn.functional import binary_cross_entropy as torch_binary_cross_entropy


def cross_entropy(y_pred, y_gt, with_mask=False):
    y_pred, = y_pred
    y_gt, mask = (y_gt[0], y_gt[1]) if with_mask else (y_gt[0], None)
    return torch_cross_entropy(y_pred, y_gt, weight=mask)


def binary_cross_entropy(y_pred, y_gt, with_mask=False):
    y_pred, = y_pred
    y_gt, mask = (y_gt[0], y_gt[1]) if with_mask else (y_gt[0], None)
    return torch_binary_cross_entropy(y_pred, y_gt, weight=mask)


def prob_cross_entropy(y_pred, y_gt, with_mask=False):
    y_pred, = y_pred
    y_gt, mask = (y_gt[0], y_gt[1]) if with_mask else (y_gt[0], None)

    y_pred = nnf.softmax(y_pred, dim=2)
    element_loss = -1 * (y_gt * torch.log(y_pred)).sum(2)

    if mask is not None:
        return (element_loss * mask).sum() / mask.sum()
    else:
        return element_loss.mean()


def splitted_cross_entropy(y_pred, y_gt, split_ranges):
    loss = 0

    for gt_index, start, end in split_ranges:
        loss += torch_cross_entropy(y_pred[0][:, start:end], y_gt[gt_index])

    return loss


def binary_cross_entropy_masked(input, target, channel_weights, mask, class_mean=False):

    eps = 10e-6
    loss = -0.5 * (target * torch.log(eps + input) + (1-target) * torch.log(eps + 1-input))

    if channel_weights is not None:
        mask *= channel_weights.view(1, -1, 1, 1)

    loss = loss * mask

    if class_mean:
        # mean over classes
        loss = loss.sum(dim=3).sum(dim=2).sum(dim=0) / (1 + mask.sum(dim=3).sum(dim=2).sum(dim=0))
        loss = loss.mean()
        loss = loss / mask.size(0)  # to normalize sum over batch dimension
    else:
        # loss over image-dimensions
        loss = loss.sum(dim=3).sum(dim=2).sum(dim=1) / (1 + mask.sum(dim=3).sum(dim=2).sum(dim=1))
        # loss = loss.sum() / (eps + mask.sum())
        loss = loss.mean()
        # assert mask.sum(dim=3).sum(dim=2).sum(dim=1).data.cpu().numpy() > 0

    return loss


def focal(y_pred, y_gt, lam):

    y_pred, = y_pred
    y_gt, = y_gt

    eps = 0.0000001
    y_pred = torch.sigmoid(y_pred)
    bce_focal = -y_gt * (1 - y_pred + eps) ** lam * torch.log(y_pred + eps)
    bce_focal -= (1 - y_gt) * y_pred ** lam * torch.log(1 - y_pred + eps)

    return bce_focal.mean()
