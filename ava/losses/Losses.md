The loss function tells us how good the predictions of the model match the ground truth provided by
the dataset.
Several loss functions have been implemented.