import torch
import torch.nn.functional as nnf
from .standard import binary_cross_entropy_masked


def dense_cross_entropy(y_pred, y_gt, loss_weights=None, out_channel_weights=None):
    """ Cross entropy for pixel-wise predictions. """

    y_gt = y_gt[:-1]

    assert len(y_pred) == len(y_gt), ('Number of predictions must match the number of provided ground truth '
                                      'elements. If you use multiscale, are you sure, the number of predictions'
                                      'matches the number of ground truth elements?')
    loss = 0
    for i in range(len(y_pred)):
        weight = loss_weights[i] if loss_weights is not None else 1

        # pred = .view(y_pred[i].size(0), y_pred[i].size(1), -1)
        # gt = .view(y_gt[i].size(0), -1)

        loss += weight * nnf.cross_entropy(y_pred[i], y_gt[i], weight=out_channel_weights, ignore_index=-1)

    return loss


def dense_binary_cross_entropy(y_pred, y_gt, loss_weights=None, class_mean=False, use_mask=False,
                               out_channel_weights=None, apply_sigmoid=False):
    """ Binary cross entropy for pixel-wise predictions. """

    assert len(y_gt) >= 2, 'Ground truth must contain segmentation and mask'

    mask = y_gt[-1] if use_mask else None
    y_gt = y_gt[:-1]

    assert len(y_pred) == len(y_gt), ('Number of predictions must match the number of provided ground truth '
                                      'elements. If you use multiscale, are you sure, the number of predictions'
                                      'matches the number of ground truth elements?')

    loss = 0
    for i in range(len(y_pred)):
        weight = loss_weights[i] if loss_weights is not None else 1

        pred = torch.sigmoid(y_pred[i]) if apply_sigmoid else y_pred[i]
        loss += weight * binary_cross_entropy_masked(pred, y_gt[i], out_channel_weights, mask, class_mean)

    return loss
