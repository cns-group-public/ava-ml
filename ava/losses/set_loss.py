from functools import partial
from .. import losses


def init_loss(loss_str, dataset):

    if loss_str is None and hasattr(dataset, 'default_loss'):
        loss_str = dataset.default_loss
    else:
        raise ValueError('No loss is specified (neither explicitly nor by dataset)')

    if type(loss_str) == tuple:
        assert len(loss_str) == 2
        loss_name, loss_args = loss_str[0], loss_str[1]
        return partial(getattr(losses, loss_name), **loss_args)
    elif type(loss_str) == str:
        return getattr(losses, loss_str)
    elif callable(loss_str):
        return loss_str
    else:
        raise ValueError('Loss has invalid type')
