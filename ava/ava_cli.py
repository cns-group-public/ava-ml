#!/usr/bin/env python3

import fire
from ava.core.benchmark import benchmark_dataset as bench_dataset
from ava.core.benchmark import benchmark_model as bench_model
from ava.core.benchmark import inspect_dataset
from ava.core.training.trainer import train_high_level
from ava.core.score import score_simple
from ava.core.experiment import experiment

from ava.scripts.tools import list_files
from ava.scripts.tools.rescale import rescale_folder
from ava.scripts.tools.video_to_frames import videos_to_frames
from ava.scripts.tools.list_files import list_files
from ava.scripts.tools.extract_features import compute_features
from ava.scripts.tools.download_video import download_video
from ava.scripts.tools.validate_dataset import validate_dataset
from ava.scripts.tools.clean_models import clean_models
from ava.scripts.tools.install_dataset import install_dataset


class Tool(object):
    list_files = staticmethod(list_files)
    rescale = staticmethod(rescale_folder)
    videos_to_frames = staticmethod(videos_to_frames)
    extract_features = staticmethod(compute_features)
    clean_models = staticmethod(clean_models)
    validate_dataset = staticmethod(validate_dataset)
    download_video = staticmethod(download_video)
    install_dataset = staticmethod(install_dataset)


class AVACli(object):

    def __init__(self):
        self.tool = Tool()

    benchmark_dataset = staticmethod(bench_dataset)
    benchmark_model = staticmethod(bench_model)
    inspect = staticmethod(inspect_dataset)
    train = staticmethod(train_high_level)
    score = staticmethod(score_simple)
    experiment = staticmethod(experiment)


def main():
    fire.Fire(AVACli)
    
# if __name__ == '__main__':
#     fire.Fire(AVACli)
