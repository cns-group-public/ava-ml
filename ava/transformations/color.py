import cv2
import numpy as np
from ..core.common.assertions import assert_opencv_image


def get_gamma_offset_lut(gamma, offset):
    if offset is None:
        if type(gamma) in {float, int}:
            offset = 0
        else:
            offset = np.zeros_like(gamma)

    if type(gamma) in {float, int}:
        inv_gamma = 1 / gamma
        lut = np.arange(0, 1, 1/256)
        lut = (lut ** inv_gamma) * 256 + offset
        lut = np.clip(lut, 0, 255)

    elif len(gamma) == 3:
        inv_gamma = 1 / np.array(gamma)

        lut = np.arange(0, 1, 1 / 256)
        lut = (lut[:, np.newaxis] ** inv_gamma) * 255 + offset
        lut = np.clip(lut, 0, 255)
        lut = lut.reshape((256, 1, 3))
    else:
        raise ValueError('gamma must be either an array or list of three gamma values or a single float value')

    return np.array(lut).astype('uint8')


def apply_gamma_offset(img, gamma=None, offset=None, lut=None, channel_dim=2, keep_channels_last=False):
    assert img.dtype == 'uint8'

    assert (gamma is not None) or (lut is not None), 'either gamma/offset or lut must be provided'

    if gamma is not None:
        lut = get_gamma_offset_lut(gamma, offset)

    # if channel index is not 2 then transpose such that it is.
    if channel_dim == 0:
        img = img.transpose([1, 2, 0])
    elif channel_dim == 1:
        img = img.transpose([0, 2, 1])

    img = cv2.LUT(img, lut)

    if not keep_channels_last:
        if channel_dim == 0:
            img = img.transpose([2, 0, 1])
        elif channel_dim == 1:
            img = img.transpose([0, 2, 1])

    return img


def add_noise(img, std, pos_noise=None, neg_noise=None):
    """
    Adds noise to an image in the range of 0-255.
    Generating random numbers is very time-consuming. It can be avoided by providing a pre-computed noise template
    from which a crop is sampled
     """
    assert img.dtype == 'uint8'

    # values = np.arange(5, dtype='uint8')
    # pdf = normal_dist.pdf(values, scale=std)
    # pdf = pdf / pdf.sum()
    #
    # pos_noise = np.random.choice(values, img.shape, p=pdf)
    # neg_noise = np.random.choice(values, img.shape, p=pdf)

    if pos_noise is None and neg_noise is None:
        pos_noise = np.empty(img.shape, dtype='uint8')
        neg_noise = np.empty(img.shape, dtype='uint8')

        cv2.randn(pos_noise, 0, std)
        cv2.randn(neg_noise, 0, std)

        pos_noise = np.maximum(0, pos_noise).astype('uint8')
        neg_noise = np.maximum(0, neg_noise).astype('uint8')

    else:
        assert pos_noise.dtype == 'uint8' and neg_noise.dtype == 'uint8'

        offsets = [np.random.randint(0, pos_noise.shape[i] - img.shape[i] + 1) for i in range(4)]
        pos_noise = pos_noise[offsets[0]: offsets[0] + img.shape[0], offsets[1]: offsets[1] + img.shape[1],
                    offsets[2]: offsets[2] + img.shape[2], offsets[3]: offsets[3] + img.shape[3]]
        offsets = [np.random.randint(0, pos_noise.shape[i] - img.shape[i] + 1) for i in range(4)]
        neg_noise = neg_noise[offsets[0]: offsets[0] + img.shape[0], offsets[1]: offsets[1] + img.shape[1],
                    offsets[2]: offsets[2] + img.shape[2], offsets[3]: offsets[3] + img.shape[3]]

    img = img + np.minimum(pos_noise, 255 - img)
    img = img - np.minimum(neg_noise, img)
    return img


def randomize_color_hs(img, max_h_shift, max_s_shift, max_v_shift):

    assert_opencv_image(img)
    h_shift = np.random.randint(-max_h_shift, max_h_shift) if max_h_shift > 0 else 0
    v_shift = np.random.randint(-max_v_shift, max_v_shift) if max_v_shift > 0 else 0
    s_shift = np.random.uniform(-1 * max_s_shift, max_s_shift)
    #
    # print('shifts', h_shift, s_shift, v_shift)
    # print(img.dtype)

    x_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

    x_hsv = np.dstack([
        cv2.add(x_hsv[..., 0], h_shift) % 360,
        np.clip(x_hsv[..., 1]**(1 + s_shift), 0, 1),
        x_hsv[..., 2],
    ])

    # print('#####', x_hsv[..., 1].min(), x_hsv[..., 1].max())

    x = cv2.cvtColor(x_hsv, cv2.COLOR_HSV2RGB)
    return x


def rescale_intensities(img, data_type, in_intensity_range, out_intensity_range):
    assert data_type in {'uint8', 'float32', 'float64'}
    assert in_intensity_range in {'0-255', '0-1'}
    assert out_intensity_range in {'0-255', '0-1'}

    if in_intensity_range == '0-255':
        assert img.min() >= 0 and img.max() <= 255


# old
def randomize_gamma(img, max_gamma, per_channel=False, channels_first=False):
    # gamma = np.random.uniform(1 - max_gamma, 1 + max_gamma)
    from logging import warning

    warning('Too slow, use adjust_gamma from skimage instead')
    max_value = 255 if img.max() > 1 else 1
    if per_channel:
        x = np.zeros_like(img)
        for c in range(3):
            gamma = np.random.normal(1, max_gamma)
            if channels_first:
                x[c, :, :] = np.minimum(np.maximum(((img[c, :, :] / max_value) ** gamma) * max_value, 0), max_value)
            else:
                x[:, :, c] = np.minimum(np.maximum(((img[:, :, c] / max_value) ** gamma) * max_value, 0), max_value)
    else:
        gamma = np.random.normal(1, max_gamma)
        x = np.minimum(np.maximum(((img / max_value) ** gamma) * max_value, 0), max_value)

    x = x * max_value
    return x
