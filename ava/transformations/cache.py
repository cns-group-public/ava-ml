import os
import cv2
import pickle
from functools import partial
from multiprocessing import Manager
import numpy as np
import time

from .. import PATHS
from ..core.logging import logger


# SIMPLE CACHE API
def cache_write(object, filename, backend='pickle'):
    """
    Simple cache writing.
    Write `object` to `filename` (within CACHE path).
    `index` is only required for sequences
    """

    # add cache dir as prefix
    filename = os.path.join(PATHS['CACHE'], filename)

    if backend == 'jpg_sequence':
        filename = filename.split('/')
        filename[-2] = filename[-2] + '_jpg_sequence' if not filename[-2].endswith('_jpg_sequence') else filename[-2]
        filename = '/'.join(filename)

    os.makedirs(os.path.dirname(filename), exist_ok=True)

    if backend == 'pickle':
        filename = filename + '.pickle' if not filename.endswith('.pickle') else filename
        pickle.dump(object, open(filename, 'wb'))
    elif backend == 'json':
        from json import dump
        filename = filename + '.json' if not filename.endswith('.json') else filename
        dump(object, open(filename, 'w'))
    elif backend == 'yaml':
        filename = filename + '.yaml' if not filename.endswith('.yaml') else filename
        from yaml import dump
        dump(object, open(filename, 'w'))
    elif backend == 'numpy':
        filename = filename + '.npy' if not filename.endswith('.npy') else filename
        np.save(filename, object)
    elif backend == 'jpg':
        filename = filename + '.jpg' if not filename.endswith('.jpg') else filename
        cv2.imwrite(filename, object)
    elif backend == 'jpg_sequence':
        for i, img in enumerate(object):
            filename_ = f'{filename}_{i:04d}.jpg'
            assert img is not None
            cv2.imwrite(filename_, img)
    else:
        raise ValueError('File has unsupported extension: {}'.format(filename))


cache_write_pickle = partial(cache_write, backend='pickle')
cache_write_yaml = partial(cache_write, backend='yaml')
cache_write_json = partial(cache_write, backend='json')
cache_write_numpy = partial(cache_write, backend='numpy')
cache_write_jpg = partial(cache_write, backend='jpg')
cache_write_jpg_sequence = partial(cache_write, backend='jpg_sequence')


def cache_read(filename):
    """
    Simple cache reading.
    The type is inferred from the file extension.
    """

    filename = os.path.join(PATHS['CACHE'], filename)

    if not os.path.isfile(filename) and not filename.split('/')[-2].endswith('jpg_sequence'):
        raise FileNotFoundError('File does not exist: {}'.format(filename))

    if filename.endswith('.pickle'):
        return pickle.load(open(filename, 'rb'))
    elif filename.endswith('.json'):
        from json import load
        return load(open(filename, 'r'))
    elif filename.endswith('.yaml'):
        from yaml import load
        return load(open(filename, 'r'))
    elif filename.endswith('.npy'):
        return np.load(filename)
    elif filename.endswith('.jpg'):
        return cv2.imread(filename)
    elif filename.split('/')[-2].endswith('jpg_sequence'):
        images = []
        i = 0
        while True:
            filename_ = f'{filename}_{i:04d}.jpg'
            if os.path.isfile(filename_):
                images += [cv2.imread(filename_)]
                i += 1
            else:
                break

        if len(images) == 0:
            raise FileNotFoundError('No images found in the sequence')

        return images
    else:
        raise ValueError('File has unsupported extension: {}'.format(filename))


_global_cache_manager = Manager()


class MemoryCachedFunction(object):
    """ Wrapper for functions such that results are cached in working memory """

    def __init__(self, transform, cache_size=None):
        self.transform = transform
        self.cache_size = cache_size
        self.storage = {}
        self.last_info_time = 0
        self.info_interval = 90  # in seconds
        self.storage = _global_cache_manager.dict()
        logger.info('Memory cache initialized: Size', 'Infinite' if cache_size is None else cache_size)

    def __call__(self, key):
        if key in self.storage:
            item = self.storage[key]
            # print('found in cache', key, 'size', len(self.storage), 'time', time.time() - t_start)
            return item
        else:
            x = self.transform(key)

            if self.cache_size is None or len(self.storage) < self.cache_size:
                self.storage[key] = x
                if time.time() - self.last_info_time > self.info_interval:
                    print('cache: ', len(self.storage), 'elements')
                    self.last_info_time = time.time()

            return x

