import numpy as np
import re
from skimage.draw import polygon, line_aa


def render_polygons(image_instance, object_id_to_index, first_depth_index_is_bg=False, normalize=False, target_size=None, target_dtype='bool'):
    """
    Computes a tensor from polygons. Expects input to be in the this form:
    `((img_height, img_width), [(object_id, [[x1, y1, x2, y2, ...], [optional further polygon]], (object_id, [[...]])])`
    `object_id_to_index` needs to map every occurring object_id to an index and there should be gaps in object_id_to_index.
    """
    img_size, ann = image_instance

    if target_size is None:
        target_size = img_size
        scale = 1
    else:
        scale = target_size[0] / img_size[0], target_size[1] / img_size[1]
        raise NotImplementedError

    tensor = np.zeros((target_size[0], target_size[1], len(object_id_to_index)), target_dtype)

    for object_class_id, polygons in ann:
        for p in polygons:
            x = np.array(p[0::2])
            y = np.array(p[1::2])
            rr, cc = polygon(y, x, shape=target_size)
            tensor[rr, cc, object_id_to_index[object_class_id]] = True

    if first_depth_index_is_bg:  # if a pixel has no class it is background
        tensor[:, :, 0] = 1 - tensor[:, :, 1:].sum(2)

    if normalize:
        tensor = tensor / tensor.sum(2)[:, :, np.newaxis]

    return tensor


def svg_path_to_polygon(svg_path):
    """" Take an SVG path and transform it to a sequence of polygon points. """
    co = re.split('(M|L)', svg_path)[2::2]
    polygon = [[float(c.split(',')[0]), float(c.split(',')[1])] for c in co[:-1]]
    return np.array(polygon)


def svg_path_to_lines(svg_path, scale=1.0, max_extent=None):
    """ Take an SVG path and transform it to outline point coordinates"""
    poly = svg_path_to_polygon(svg_path)
    poly = poly[:, ::-1] * scale
    poly = poly.astype('int')

    if max_extent is not None:
        assert type(max_extent) == tuple and len(max_extent) == 2
        poly[:, 0] = np.clip(poly[:, 0], 0, max_extent[0] - 1)
        poly[:, 1] = np.clip(poly[:, 1], 0, max_extent[1] - 1)

    rr0, cc0, val = line_aa(poly[0, 0], poly[0, 1], poly[1, 0], poly[1, 1])
    rr1, cc1, val = line_aa(poly[1, 0], poly[1, 1], poly[2, 0], poly[2, 1])
    rr2, cc2, val = line_aa(poly[2, 0], poly[2, 1], poly[3, 0], poly[3, 1])
    rr3, cc3, val = line_aa(poly[3, 0], poly[3, 1], poly[0, 0], poly[0, 1])

    return np.hstack([rr0, rr1, rr2, rr3]), np.hstack([cc0, cc1, cc2, cc3])
