import cv2
import torch
import numpy as np


# def one_hot_torch(index, size):
#     idx = torch.from_numpy(np.array([index]))
#     a = torch.zeros(size)
#     a.scatter_(0, idx, 1)
#     return a
#
#
# def one_hot(pos_dictionary, item):
#     """
#     Creates a one-hot vector of length `len(pos_dictionary)`.
#     `pos_dictionary` maps items to positions, for item a one-hot vector is returned.
#     """
#     x = np.zeros(len(pos_dictionary), dtype='bool')
#     if item in pos_dictionary:
#         x[pos_dictionary[item]] = 1
#     return x
#
#
# def np_one_hot(x, n_classes):
#     s = x.shape
#     out = np.zeros((np.prod(s), n_classes))
#     x = x.flatten()
#     out[np.arange(len(x)), x] = 1
#     out = out.reshape(s + (n_classes,))
#     return out


# def rotation(img, rotation_angle):
#     # assert_opencv_image(img)
#     M = cv2.getRotationMatrix2D((img.shape[1] / 2, img.shape[0] / 2), rotation_angle, 1)
#     x = cv2.warpAffine(img, M, (img.shape[1], img.shape[0]))
#     return x
