import cv2
import os


def imread(filename):

    if not os.path.isfile(filename):
        raise ValueError('Image file not found: {}'.format(filename))

    if os.path.getsize(filename) == 0:
        raise ValueError('Image file exists, but size is 0 Kb: {}'.format(filename))

    img = cv2.imread(filename)

    if img is None:
        raise ValueError('Image file exists, but failed to load: {}'.format(filename))

    return img
