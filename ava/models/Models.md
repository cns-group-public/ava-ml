### Writing a Model
  
##### Required Methods
- `__init__()`: Initialization of the model (e.g. initialize the neural network)
- `forward(*x)`: Receive inputs `*x` and do forward pass
  

### Baselines

Baselines are special models that are not trained but have direct access to the Dataset. 
They are indicated by returning `True` in the method `is_baseline`. Using the access to the
dataset, e.g. the mode of the label distribution can be computed.