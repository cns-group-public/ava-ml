import torch
from torch import nn
import torch.nn.functional as nnf


class _TemporalPyramidPooling(nn.Module):
    """ Just the pyramid pooling module """

    def __init__(self, in_features, hidden_size, pool_functions='all', reduction=None,
                 final_pooling=False, additional_size=0):
        super().__init__()

        self.hidden_size = hidden_size
        # self.out_features = out_features
        self.in_features = in_features
        self.final_pooling = final_pooling
        self.additional_size = additional_size

        self.pool_functions = [
            lambda x: nnf.adaptive_avg_pool1d(x, 1),
            lambda x: nnf.adaptive_avg_pool1d(x, 2),
            lambda x: nnf.adaptive_avg_pool1d(x, 4),
            lambda x: nnf.adaptive_max_pool1d(x, 1),
            lambda x: nnf.adaptive_max_pool1d(x, 2),
            lambda x: nnf.adaptive_max_pool1d(x, 4),
            lambda x: x.min(2)[0].unsqueeze(2),
            lambda x: x.median(2)[0].unsqueeze(2),
        ]

        pool_function_indices = {
            'all': None,
            'global': [0, 3, 6, 7],
            'classic': [0, 3],
            'classic_min': [0, 3, 6]
        }[pool_functions]

        if pool_function_indices is not None:
            self.pool_functions = [self.pool_functions[i] for i in pool_function_indices]

        self.pyramid = nn.ModuleList([
            nn.Sequential(
                nn.Conv1d(self.in_features, self.hidden_size, 1, bias=False),
                nn.BatchNorm1d(self.hidden_size),
                nn.ReLU()
            ) for _ in self.pool_functions
        ])

        if reduction is not None:
            self.reduction = nn.Conv1d(self.pyramid_output_size(), reduction, kernel_size=1)
        else:
            self.reduction = None

    def pyramid_output_size(self):
        return self.in_features + self.hidden_size * len(self.pool_functions) + self.additional_size

    def feature_size(self):
        return self.hidden_size * (len(self.pool_functions) + 1)

    def forward(self, x, add=None):
        # batch_size x features x length
        target_length = x.size(2)

        x_pooled = [x] + [nnf.interpolate(pyr(pool_f(x)), target_length, mode='linear')
                          for pyr, pool_f in zip(self.pyramid, self.pool_functions)]

        if add is not None:
            x_pooled += [nnf.interpolate(add, target_length, mode='linear')]

        x = torch.cat(x_pooled, dim=1)
        # batch_size x more features x length

        if self.reduction is not None:
            x = self.reduction(x)

        if self.final_pooling:
            # batch_size x features_in+hidden_size*len(pool_functions)
            x = nnf.adaptive_avg_pool1d(x, 1).squeeze(2)

        return x


class _TempConvPoolBlock(nn.Module):
    """ The temporal convolutions """

    def __init__(self, in_features, out_features, features_inside=128, depth=1, pyramid_position=None,
                 pool_functions='all'):
        super().__init__()

        self.pyramid_position = pyramid_position
        assert depth > 0

        # feat_inside_pyr = features_inside * 2
        # fac = 3 if not self.multi_pool else 8

        first_conv = nn.Conv1d(in_features, 2 * features_inside, kernel_size=2)
        final_conv = nn.Conv1d(features_inside, out_features, kernel_size=1)

        if self.pyramid_position is None:
            self.pyramid = None
        elif self.pyramid_position == 'parallel':
            self.pyramid = _TemporalPyramidPooling(in_features, features_inside, reduction=out_features,
                                                   final_pooling=True, pool_functions=pool_functions)
        elif self.pyramid_position == 'pre':
            self.pyramid = _TemporalPyramidPooling(in_features, features_inside, reduction=features_inside,
                                                   pool_functions=pool_functions)
            first_conv = nn.Conv1d(features_inside, 2 * features_inside, kernel_size=2)  # from pyramid
        elif self.pyramid_position == 'post':
            self.pyramid = _TemporalPyramidPooling(features_inside, features_inside, reduction=out_features,
                                                   pool_functions=pool_functions)
            final_conv = nn.Conv1d(features_inside, features_inside, kernel_size=1)

        conv_blocks = [first_conv]
        conv_blocks += [
            nn.BatchNorm1d(2 * features_inside), nn.ReLU(),
            nn.Conv1d(2 * features_inside, features_inside, kernel_size=2, dilation=2), nn.BatchNorm1d(features_inside),
            nn.ReLU()]

        for i in range(1, depth):
            conv_blocks += [nn.Conv1d(features_inside, features_inside, kernel_size=2, dilation=2 ** (i + 1)),
                            nn.BatchNorm1d(features_inside), nn.ReLU()]

        conv_blocks = conv_blocks + [final_conv]
        self.convs = nn.Sequential(*conv_blocks)

    def forward(self, x):
        # x: batch_size x features x length
        # pre pyramid

        if self.pyramid_position == 'pre':
            pyr_out = self.pyramid(x)
            out = self.convs(pyr_out)
            out = nnf.adaptive_avg_pool1d(out, 1).squeeze(2)
        elif self.pyramid_position == 'parallel':
            pyr_out = self.pyramid(x)
            conv_out = self.convs(x)
            conv_out = nnf.adaptive_avg_pool1d(conv_out, 1).squeeze(2)
            out = conv_out + pyr_out
        elif self.pyramid_position == 'post':
            conv_out = self.convs(x)
            out = self.pyramid(conv_out)
            out = nnf.adaptive_avg_pool1d(out, 1).squeeze(2)
        elif self.pyramid_position is None:
            out = self.convs(x)
            out = nnf.adaptive_avg_pool1d(out, 1).squeeze(2)
        else:
            raise ValueError('invalid pyramid_position: {}'.format(self.pyramid_position))

        return out

