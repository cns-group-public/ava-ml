import torch
from torch import nn

IMAGENET_MEAN = [0.485, 0.456, 0.406]
IMAGENET_STD = [0.229, 0.224, 0.225]


class DatasetNormalize(nn.Module):

    def __init__(self, in_range, mean, std, dims=4, color_dim=1):
        super().__init__()

        sizes = tuple(1 if i != color_dim else 3 for i in range(dims))
        self.input_range = nn.Parameter(torch.FloatTensor([in_range]).view(*(1 for _ in range(dims))),
                                        requires_grad=False)
        self.input_mean = nn.Parameter(torch.FloatTensor(mean).view(*sizes), requires_grad=False)
        self.input_std = nn.Parameter(torch.FloatTensor(std).view(*sizes), requires_grad=False)

    def forward(self, x):
        if any([x is not None for x in [self.input_range, self.input_std, self.input_mean]]):
            x = x.float()

            x = x * (self.input_range / 255.0)
            x = x - self.input_mean
            x = x / self.input_std

            return x

        else:
            raise ValueError('DatasetNormalize block is not properly specified.')
