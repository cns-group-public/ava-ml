import torch
from torch import nn
from torch.autograd import Variable
import torch.nn.functional as nnf


class DecoderBlock(nn.Module):

    def __init__(self, input_channels, output_channels, dropout=None):
        super().__init__()

        self.input_channels = input_channels
        self.output_channels = output_channels

        self.conv = nn.Conv2d(input_channels, output_channels, kernel_size=3, padding=1)
        self.dropout = nn.Dropout2d(p=dropout) if dropout is not None else None

    def forward(self, x, skip_x):
        x_up = nnf.interpolate(x, size=(skip_x.size(2), skip_x.size(3)), mode='bilinear', align_corners=False)
        x = torch.cat([x_up, skip_x], dim=1)
        x = self.conv(x)
        if self.dropout is not None:
            x = self.dropout(x)

        x = nnf.relu(x)
        return x


class EncoderBlock(nn.Module):

    def __init__(self, input_channels, k, repetitions, pool=True, batch_norm=False):
        super().__init__()
        layers = []
        inp_k = input_channels
        for _ in range(repetitions):
            layers += [nn.Conv2d(inp_k, k, kernel_size=3, padding=1)]
            if batch_norm:
                layers += [nn.BatchNorm2d(k)]
            layers += [nn.ReLU()]
            inp_k = k

        if pool:
            layers += [nn.MaxPool2d(2)]

        self.block = nn.Sequential(*layers)

    def forward(self, x):
        return self.block(x)


class PredictionOutput(nn.Module):

    def __init__(self, input_channels, n_classes, with_softmax=False, upsample_mode='bilinear', dropout=None):
        super().__init__()
        self.upsample_mode = upsample_mode
        self.with_softmax = with_softmax
        self.n_classes = n_classes
        self.input_channels = input_channels

        self.dropout = nn.Dropout2d()

        n_features = int(0.5 * input_channels + 0.5 * n_classes)

        seq = [nn.Conv2d(input_channels, n_features, kernel_size=3, padding=1)]
        if dropout is not None:
            seq += [nn.Dropout2d(p=dropout)]
        seq += [nn.Conv2d(n_features, n_classes, kernel_size=3, padding=1)]

        self.convs = nn.Sequential(*seq)

    def forward(self, x, target_size):
        x_up = nnf.interpolate(x, target_size, mode=self.upsample_mode)
        pred = self.convs(x_up)

        if self.with_softmax:
            pred = nnf.softmax(pred, dim=1)

        return pred
