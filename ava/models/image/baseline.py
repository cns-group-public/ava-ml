import numpy as np
import torch
from torch import nn


class ImageClassifierBaseline(nn.Module):
    """ Baseline """

    def __init__(self, multiclass=None, pretrained=False, n_classes=10, freeze_level=None,
                 pre_pool_out=False, loss='ce', average='global', probabilistic_ground_truth=False, with_mask=False,
                 ordinal_weights=None):

        super().__init__()

        self.average = average
        self.loss_type = loss
        self.n_classes = n_classes

        if self.multiclass > 1:
            self.class_plaus = nn.Parameter(torch.zeros(601, n_classes, self.multiclass))
            self.global_plaus = nn.Parameter(torch.zeros(n_classes, self.multiclass))
        else:
            self.class_plaus = nn.Parameter(torch.zeros(601, n_classes))
            self.global_plaus = nn.Parameter(torch.zeros(n_classes))

    def is_baseline(self):
        return True

    def forward(self, img, obj_cls):

        if self.multiclass > 1:
            out = torch.zeros(img.size(0), self.n_classes, self.multiclass)
        else:
            out = torch.zeros(img.size(0), self.n_classes)

        for i in range(img.size(0)):
            if self.average == 'per_class':
                out[i] = self.class_plaus[obj_cls[i]]
            else:
                out[i] = self.global_plaus

        return out,

    def set_dataset(self, dataset):

        mats_per_class = {i: [] for i in range(len(dataset.class_names))}
        for s in dataset.samples:
            mats_per_class[s[2]] += [s[4]]

        mask = np.ones(len(dataset.all_actions), dtype='bool')

        for i in mats_per_class.keys():
            if len(mats_per_class[i]) > 0:
                plaus = dataset.build_labels(np.array(mats_per_class[i]).sum(0), mask)

                self.class_plaus[i] = torch.tensor(plaus)

        self.global_plaus = nn.Parameter(self.class_plaus.mean(0))


class ImageClassifierConstBaseline(nn.Module):
    """ Baseline always predicting the same value """

    def __init__(self, multiclass=None, value=0, pretrained=False, n_classes=10, freeze_level=None,
                 pre_pool_out=False, loss='ce', average='global', probabilistic_ground_truth=False, with_mask=False,
                 ordinal_weights=None):

        super().__init__()

        self.average = average
        self.loss_type = loss
        self.n_classes = n_classes
        self.value = value

    def is_baseline(self):
        return True

    def forward(self, *x):
        img = x[0]

        if self.multiclass > 1:
            out = torch.ones(img.size(0), self.n_classes, self.multiclass) * self.value
        else:
            out = torch.ones(img.size(0), self.n_classes) * self.value

        return out,
