from torch import nn
from torch.nn import functional as nnf
from functools import partial
from .. import ModelBase


class MLP(ModelBase):
    """
    """

    def __init__(self, hidden=1024, n_classes=10, multiclass=False, vector_input=10,
                 n_frames=10, **kwargs):

        from ...metrics import AccuracyMulticlassMetric

        metrics = [partial(AccuracyMulticlassMetric, multiclass=multiclass, mistakes=(0, 1, 2))]
        super().__init__(('predicted_label',), metrics, data_types=(None,))

        self.multiclass = multiclass
        in_features = vector_input * n_frames

        n_outputs = n_classes * (1 if self.multiclass is None else self.multiclass)

        # print(kwargs)

        self.net = nn.Sequential(
            nn.Linear(in_features, hidden),
            nn.ReLU(),
            nn.Linear(hidden, hidden),
            nn.ReLU(),
            nn.Linear(hidden, hidden),
            nn.ReLU(),
            nn.Linear(hidden, n_outputs),
        )

    def forward(self, *inp):
        x = inp[0]
        x = x.view(x.size(0), -1)

        return self.net(x),

    def loss(self, y_pred, y_gt):

        pred = y_pred[0]
        gt = y_gt[0]

        if self.multiclass == 1:
            return nnf.binary_cross_entropy_with_logits(pred, gt)
        elif self.multiclass > 1:
            pred = pred.view(-1, self.multiclass)
            gt = gt.view(-1)

            return nnf.cross_entropy(pred, gt)
        else:
            raise NotImplementedError
