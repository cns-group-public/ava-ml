from torch import nn
import torch.nn.functional as nnf

# from ava.core.metrics import OrdinalCorrelationMetric
from ..feature_extraction import get_feature_extractor
from ...metrics import *


class ImageFEClassifier(nn.Module):
    """ Base class that turns feature extractors into classifiers.

    Args:
        - base: name of the feature extractor the classifier is based on
        for other arguments see ImageClassifierBase

    Returns (forward method):
        - either Vector of n_classes or Matrix (n_classes x multiclass)

    """

    def __init__(self, base, multiclass=None, pretrained=False, n_classes=10, freeze_level=None,
                 pre_pool_out=False, ignore_input=False, **kwargs):
        super().__init__()

        self.pre_pool_out = pre_pool_out
        self.ignore_input = ignore_input

        assert multiclass != 2, 'multiclass = 2 does not make sense. This is equivalent to binary. '

        self.net = get_feature_extractor(base, avgpool=False, flatten=False, pretrained=pretrained,
                                         freeze_level=freeze_level, **kwargs)

        self.classifier = nn.Linear(self.net.feature_size(), n_classes * multiclass if multiclass else n_classes)

    def get_features(self, img):
        features = self.net(img)
        return features

    def forward(self, img):

        if self.ignore_input:
            img = img * 0

        features = self.net(img)

        x = nnf.adaptive_avg_pool2d(features, (1, 1))
        x = x.squeeze(-1).squeeze(-1)

        out = self.classifier(x)

        if self.pre_pool_out:
            return out, features
        else:
            return out,

    def feature_size(self):
        return self.net.feature_size()


class Inception4Classifier(ImageFEClassifier):

    def __init__(self, **kwargs):
        super().__init__(base='inception4', **kwargs)


class Inception3Classifier(ImageFEClassifier):

    def __init__(self, **kwargs):
        super().__init__(base='inception3', **kwargs)


class XceptionClassifier(ImageFEClassifier):
    def __init__(self, **kwargs):
        super().__init__(base='xception', **kwargs)


class BNInceptionClassifier(ImageFEClassifier):
    def __init__(self, **kwargs):
        super().__init__(base='bninception', **kwargs)


class RN152Classifier(ImageFEClassifier):
    def __init__(self, **kwargs):
        super().__init__(base='resnet152', **kwargs)


class RN101Classifier(ImageFEClassifier):
    def __init__(self, **kwargs):
        super().__init__(base='resnet101', **kwargs)


class RN50Classifier(ImageFEClassifier):
    def __init__(self, **kwargs):
        super().__init__(base='resnet50', **kwargs)


class RN18Classifier(ImageFEClassifier):
    def __init__(self, **kwargs):
        super().__init__(base='resnet18', **kwargs)


class SqueezeNetClassifier(ImageFEClassifier):
    def __init__(self, **kwargs):
        super().__init__(base='squeezenet', **kwargs)


class MNISTNet(ImageFEClassifier):
    def __init__(self, k=3, **kwargs):
        super().__init__(base='mnist_net', k=k, **kwargs)


class DualImageClassifier(nn.Module):
    """
    Base class for classification problems that involve two input images.

    Args:
        - base: name of the feature extractor the classifier is based on
        for other arguments see ImageClassifierBase

     """

    def __init__(self, base, multiclass=None, pretrained=False, n_classes=10, freeze_level=None, 
                 pre_pool_out=False, share=False, dropout=None, merge='concat'):
        super().__init__()

        self.pre_pool_out = pre_pool_out
        self.merge = merge
        self.n_classes = n_classes

        self.net1 = get_feature_extractor(base, avgpool=False, flatten=False, pretrained=pretrained,
                                          freeze_level=freeze_level, intermediate=True)

        if not share:
            self.net2 = get_feature_extractor(base, avgpool=False, flatten=False, pretrained=pretrained,
                                              freeze_level=freeze_level, intermediate=True)
        else:
            self.net2 = self.net1

        self.dropout = nn.Dropout(dropout) if dropout is not None else None

        if self.merge == 'concat':
            n_features = self.net1.feature_size() + self.net2.feature_size()
        elif self.merge == 'add':
            n_features = self.net1.feature_size()
        else:
            raise NotImplementedError

        self.classifier = nn.Linear(n_features, n_classes * multiclass if multiclass else n_classes)

    def forward(self, img1, img2):
        features1, activations = self.net1(img1)  # self.classifier1(img1)
        features2, activations = self.net2(img2)  # self.classifier2(img2)

        x1 = nnf.adaptive_avg_pool2d(features1, (1, 1)).squeeze(-1).squeeze(-1)
        x2 = nnf.adaptive_avg_pool2d(features2, (1, 1)).squeeze(-1).squeeze(-1)

        if self.merge == 'concat':
            x = torch.cat([x1, x2], dim=1)

            # print(x.shape)
            x = self.dropout(x) if self.dropout is not None else x
            out = self.classifier(x)
        elif self.merge == 'add':
            x = x1 + x2
            x = self.dropout(x) if self.dropout is not None else x
            out = self.classifier(x)
        else:
            raise NotImplementedError

        if self.multiclass > 1:  # no need to transform this for binary case
            out = out.view(img1.size(0), self.n_classes, self.multiclass)

        if self.pre_pool_out:
            return out, features1, features2
        else:
            return out,

    def feature_size(self):
        return self.net.feature_size()


class DualResNet50Classifier(DualImageClassifier):

    def __init__(self, **kwargs):
        super().__init__(base='resnet50', **kwargs)


class DualResNet18Classifier(DualImageClassifier):

    def __init__(self, **kwargs):
        super().__init__(base='resnet18', **kwargs)
