import torch
from torch.nn import functional as nnf
from torch import nn


class DeepLabV3(nn.Module):
    """ Wrapper for the DeepLabV3 model on Torch Hub. """

    def __init__(self, in_channels=3, out_channels=10, pretrained=False):
        super().__init__()

        assert in_channels == 3

        self.net = torch.hub.load('pytorch/vision', 'deeplabv3_resnet101', pretrained=pretrained)

        if in_channels != 3:
            self.net.backbone.conv1 = nn.Conv2d(1, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)

        self.net.classifier[4] = nn.Conv2d(256, out_channels, kernel_size=1)

    def forward(self, x):
        input_size = x.size(2), x.size(3)
        x = self.net.backbone(x)
        x = self.net.classifier(x['out'])

        x = nnf.interpolate(x, input_size, mode='bilinear', align_corners=True)

        return x,
