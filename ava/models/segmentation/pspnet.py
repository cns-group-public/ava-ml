import torch
from torch import nn

from ..blocks.cnn import DecoderBlock
from torch.nn import functional as nnf, Parameter
from ..feature_extraction import get_feature_extractor
from ..blocks import DatasetNormalize, PyramidModule


class PSPNet(nn.Module):
    """
    Own implementation of PSPNet. Does not use auxiliary outputs for loss and synchronized batch norm.
    """

    def __init__(self, out_channels=10, decoder_shape='m', dropout=None, base='drn105', pretrained=False, with_skip=False):
        super().__init__()

        self.transfer_exclude_parameters = ['conv_end2.2.weight', 'conv_end2.2.bias', 'thresholds']
        self.pretrained = pretrained
        self.dropout = dropout
        self.with_skip = with_skip
        self.decoder_shape = decoder_shape
        self.base = base

        self.base_model = get_feature_extractor(base, avgpool=False, flatten=False, pretrained=pretrained, intermediate=True)
        dec_feats = {'m': 512, 's': 256, 'xs': 128}[decoder_shape]

        pyr_feats = (self.base_model.feature_size(), 256)
        self.psp = PyramidModule(pyr_feats[0], pyr_feats[1], dec_feats)
        # self.bn_end = nn.BatchNorm2d(1000)

        if with_skip:
            self.preskip_conv = nn.Conv2d(dec_feats, dec_feats, kernel_size=1)
            self.skip_conn = DecoderBlock(256 + dec_feats, dec_feats)

        seq = [
            nn.BatchNorm2d(dec_feats),  # momentum=.95?
            nn.ReLU(),
        ]

        if dropout is not None:
            seq += [nn.Dropout2d(p=dropout)]

        seq += [nn.Conv2d(dec_feats, out_channels, kernel_size=1)]
        self.conv_end2 = nn.Sequential(*seq)

        self.features_only = False
        self.normalize = DatasetNormalize([1], [0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

    def forward(self, x):
        x = self.normalize(x)

        original_size = x.size(2), x.size(3)
        x, activations = self.base_model(x)

        x = self.psp(x)

        if self.with_skip:
            skip_act = activations[-6]
            x = self.preskip_conv(x)
            x = self.skip_conn(x, skip_act)

        x = nnf.interpolate(x, original_size, mode='bilinear', align_corners=False)
        x = self.conv_end2(x)

        if self.binary:
            return torch.sigmoid(x),
        else:
            return nnf.log_softmax(x, dim=1),


