# Dense Models

Models, that provide pixel-wise predictions.
Metrics are implemented in _DenseBase in `base.py`.

## API

Each model expects at least these arguments:
* `out_channels`: Number of output channels. Most of the time this is the number of classes
* binary
* with_mask

The input must be normalized by the model, i.e. images are expected to have values in the range [0, 255].

For compatibility reasons, we expect every model to take an image as input and expect
 a ground truth label and a mask to evaluate the loss.

A model can output multiple predictions which are all considered to compute the loss (weighted by 
`loss_weights`).
For scoring only the first output is considered, though.


## Implemented Models
Currently, we provide implementations of these models:

| Model  | Pascal VOC 2012 mIoU | ADE150 mIoU |
| ------ | -------------------- | ----------- |
| SegNet [1] | n/a | n/a |
| UNet [2]   | n/a | n/a |
| FCN [3] |  n/a | n/a |

Implementations of modified backbone models (e.g. ResNet50) can be found in `backbone.py`.