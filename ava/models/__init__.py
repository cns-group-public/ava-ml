from os import listdir
from os.path import join

from .. import PATHS
from .image.image_classification import *

from .segmentation.dense_resnet import *
from .segmentation.deeplab_v3 import *
from .segmentation.pspnet import *

from .video.baseline import *
from .video.recurrent import *
from .video.frame_pooling import SingleFrameFeature, MultiFramePooling
from .video.space_time_conv import SpaceTime


import sys
sys.path.append('.')
sys.path.append(join(PATHS['CONTRIB_PATH'], 'models'))

# search in local folder first
for module in listdir('.'):
    if module.endswith('_model.py'):
        mod = __import__(module[:module.rindex('.py')], globals=globals())
        for attr in dir(mod):
            globals()[attr] = getattr(mod, attr)
