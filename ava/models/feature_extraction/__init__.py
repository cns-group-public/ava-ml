
FEATURE_EXTRACTORS = {
    'resnet18': ('.resnet', '_ResNetFeatures', dict(base='resnet18')),
    'resnet50': ('.resnet', '_ResNetFeatures', dict(base='resnet50')),
    'resnet101': ('.resnet', '_ResNetFeatures', dict(base='resnet101')),
    'resnet152': ('.resnet', '_ResNetFeatures', dict(base='resnet152')),
    'se_resnet50': ('.se_resnet', '_SEFeatures', dict(base='se_resnet50')),
    'se_resnet101': ('.se_resnet', '_SEFeatures', dict(base='se_resnet101')),
    'se_resnext50': ('.se_resnet', '_SEFeatures', dict(base='se_resnext50_32x4d')),
    'se_resnext101': ('.se_resnet', '_SEFeatures', dict(base='se_resnext101_32x4d')),
    'bninception': ('.inception', '_BNInceptionFeatures', dict()),
    'xception': ('.inception', '_XceptionFeatures', dict()),
    'inception4': ('.inception', '_IncV4Features', dict()),
    'inception3': ('.inception', '_IncV3Features', dict()),
    'squeezenet': ('.squeezenet', '_SqueezeNetFeatures', dict()),

    'drn38': ('.dilated_resnet', '_DilatedResNetFeatures', dict(base='drn_d_38')),
    'drn54': ('.dilated_resnet', '_DilatedResNetFeatures', dict(base='drn_d_54')),
    'drn105': ('.dilated_resnet', '_DilatedResNetFeatures', dict(base='drn_d_105')),

    # , drn_d_54, drn_d_105'

    'mnist_net': ('.lightweight', '_MNISTConvNet', dict()),
    'light_unet': ('.lightweight', '_LightUNet', dict()),
    'light_convnet': ('.lightweight', '_LightConvNet', dict()),

    'yolo': ('.yolo', '_YOLOFeatures', dict()),
    'yolo_openimages': ('.yolo', '_YOLOFeatures', dict(mode='openimages'))
}


def setup_feature_extractor(base, encoder_freeze, args=None):
    """ Convenience function for initializing a feature extractor """

    from ...core.common.serialization import class_config_str
    from ...core.model import initialize_model, load_pretrained_model
    from ...core.logging import logger
    from ...core.common.generic import find_file_in_folders
    from ... import PATHS

    from os.path import basename

    args = {} if args is None else args

    base_filename = find_file_in_folders(base, ['pretrained', PATHS['TRAINED_MODELS_PATH']])

    if type(base) in {tuple, list}:
        logger.warning_if(encoder_freeze is None, 'encoder freeze has no effect')
        logger.important('MODEL: init model: {}'.format(base))
        base_model = initialize_model(base[0], cli_args=base[1])
        base = class_config_str(base[0], base[1])
        features = base_model.get_features
    elif base in FEATURE_EXTRACTORS:
        logger.important('MODEL: init feature extractor: {}'.format(base))
        base_model = get_feature_extractor(base, avgpool=False, flatten=False, freeze_level=encoder_freeze, **args)
        features = base_model
    elif base_filename is not None:
        logger.important('MODEL: load pretrained model: {}'.format(base))
        base_model = load_pretrained_model(base_filename, cli_args={'freeze_level': encoder_freeze})
        base = basename(base)  # for the name, avoids slashes
        features = base_model.get_features
    else:
        raise ValueError('Invalid base: {}. This means no matching model, feature extractor or pretrained model has been'
                         'found.'.format(base))

    return base_model, features


def get_feature_extractor(name, **in_args):

    module, cls_name, args = FEATURE_EXTRACTORS[name]

    from importlib import import_module
    init = getattr(import_module(module, package=__package__), cls_name)

    init_args = dict(args)
    init_args.update(in_args)

    return init(**init_args)
