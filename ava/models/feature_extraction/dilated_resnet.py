from torch import nn
from math import ceil

from ...core.logging import logger
from ...core.model import IMAGENET_MEAN, IMAGENET_STD
from .base import _FeatureExtractor
from ..blocks import DatasetNormalize

class _DilatedResNetFeatures(_FeatureExtractor):
    """ Wrapper for ResNet feature extraction. """

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
    }

    def __init__(self, base='resnet50', pretrained=True, avgpool=True, flatten=True, freeze_level=None,
                 intermediate=False, layer=None, input_channels=3, feature_output_channels=None):

        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)
        self.base = base

        from ...third_party.drn.drn import drn_d_38, drn_d_54, drn_d_105

        if base == 'drn_d_38':
            self.net = drn_d_38(pretrained=pretrained)
            self._feature_size = 512
        elif base == 'drn_d_54':
            self.net = drn_d_54(pretrained=pretrained)
            self._feature_size = 512
        elif base == 'drn_d_105':
            self.net = drn_d_105(pretrained=pretrained)
            self._feature_size = 512
        else:
            raise ValueError(f'Invalid base: {base}')

        if input_channels != 3:
            self.net.layer0[0] = nn.Conv2d(input_channels, 16, kernel_size=(7, 7), stride=(1, 1), padding=(3, 3), bias=False)
            # self.input_std = self.input_mean = self.input_range = None
        else:
            self.normalize = DatasetNormalize(1, IMAGENET_MEAN, IMAGENET_STD, dims=4, color_dim=1)

        if feature_output_channels is not None:
            raise NotImplementedError

        self.layer_sequence = ['layer0', 'layer1', 'layer2', 'layer3', 'layer4', 'layer5', 'layer6', 'layer7', 'layer8']
        self.freeze(freeze_level)

        logger.important(f"FEATURE EXTRACTOR: init {base} (pretrained: {self.pretrained}, freeze: {freeze_level}"
                         f"intermediate: {intermediate}, layer: {layer}, flatten: {flatten})")

    def name(self):
        return self.base

    def feature_size(self):
        return self._feature_size

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2**5)) * ceil(input_size[1] / (2**5))


