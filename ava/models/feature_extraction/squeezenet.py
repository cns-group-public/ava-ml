from math import ceil

from ...core.logging import logger
from ...core.model import IMAGENET_MEAN, IMAGENET_STD
from .base import _FeatureExtractor
from ..blocks import DatasetNormalize

from torch import nn


class _SqueezeNetFeatures(_FeatureExtractor):
    """ Wrapper for SqueezeNet feature extraction. """

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
    }

    def __init__(self, pretrained=True, avgpool=True, flatten=True, freeze_level=None,
                 intermediate=False, layer=None, input_channels=3, feature_output_channels=None):
        print('intermediate:', intermediate)
        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)

        from torchvision.models import squeezenet1_1

        self.net = squeezenet1_1(pretrained)

        if input_channels != 3:
            self.net.features[0] = nn.Conv2d(input_channels, 64, kernel_size=(3, 3), stride=(2, 2))
        else:
            self.normalize = DatasetNormalize(1, IMAGENET_MEAN, IMAGENET_STD, dims=4, color_dim=1)

        if feature_output_channels is not None:
            self.net.features[12].expand1x1 = nn.Conv2d(64, feature_output_channels // 2, kernel_size=(3, 3), padding=(1, 1))
            self.net.features[12].expand3x3 = nn.Conv2d(64, feature_output_channels // 2, kernel_size=(3, 3), padding=(1, 1))

        self.freeze(freeze_level)

        self.layer_sequence = ['features']

        logger.important('FEATURE EXTRACTOR: init {} (pretrained: {}, freeze: {}, intermediate: {}, layer: {}, flatten: {})'.format(
            self.name(), self.pretrained, freeze_level, intermediate, layer, flatten))

    def feature_size(self):
        return 512 if self.feature_output_channels is None else self.feature_output_channels

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2**5)) * ceil(input_size[1] / (2**5))