from math import ceil

from .base import _FeatureExtractor
from ...models import DatasetNormalize


class _SEFeatures(_FeatureExtractor):

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
    }

    def __init__(self, base='se_resnet50', pretrained=True, avgpool=True, flatten=True, freeze_level=None,
                 intermediate=False, layer=None, input_channels=3, feature_output_channels=None):
        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)
        self.base = base

        from pretrainedmodels.models import senet

        if base == 'se_resnet50':
            self.net = senet.se_resnet50(pretrained='imagenet' if pretrained else None)
            self._feature_size = 2048
        elif base == 'se_resnet101':
            self.net = senet.se_resnet101(pretrained='imagenet' if pretrained else None)
            self._feature_size = 2048
        elif base == 'se_resnet152':
            self.net = senet.se_resnet152(pretrained='imagenet' if pretrained else None)
            self._feature_size = 2048
        elif base == 'se_resnext50_32x4d':
            self.net = senet.se_resnext50_32x4d(pretrained='imagenet' if pretrained else None)
            self._feature_size = 2048
        elif base == 'se_resnext101_32x4d':
            self.net = senet.se_resnext101_32x4d(pretrained='imagenet' if pretrained else None)
            self._feature_size = 2048
        else:
            raise ValueError('Invalid base: {}'.format(base))

        sett = senet.pretrained_settings[base]['imagenet']
        self.normalize = DatasetNormalize(1, sett['mean'], sett['std'], dims=4, color_dim=1)
        self.layer_sequence = ['layer0', 'layer1', 'layer2', 'layer3', 'layer4']
        self.freeze(freeze_level)

    def name(self):
        return self.base

    def feature_size(self):
        return self._feature_size

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2**5)) * ceil(input_size[1] / (2**5))