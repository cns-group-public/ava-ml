from os.path import join

import torch

from ... import PATHS
from .base import _FeatureExtractor


class _YOLOFeatures(_FeatureExtractor):
    """ An even smaller ConvNet """

    def __init__(self, pretrained=True, avgpool=True, flatten=True, freeze_level=None, mode='coco', output_type='occurrence',
                 intermediate=False, layer=None, input_channels=3, feature_output_channels=None, k=3):
        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)

        self.output_type = output_type
        self.k = k

        assert mode in {'openimages', 'coco'}
        config, names = ('-openimages', 'openimages.names') if mode == 'openimages' else ('', 'coco.names')

        from ...third_party.yolo.models import Darknet
        from ...third_party.yolo.utils.utils import non_max_suppression, load_classes
        self.non_max_suppression = non_max_suppression

        self.net = Darknet(join(PATHS['AVA_ROOT'], 'third_party/yolo/config/yolov3{}.cfg'.format(config)), img_size=608)
        self.names = load_classes(join(PATHS['AVA_ROOT'], 'third_party/yolo/data/' + names))

        for p in self.net.parameters():
            if hasattr(p, 'requires_grad'):
                p.requires_grad = False

        print(len(self.names))

        if pretrained:
            self.net.load_weights(join(PATHS['AVA_ROOT'], 'third_party/yolo/weights/yolov3{}.weights'.format(config)))

        # self.layer_sequence = ['all_layers']

    def forward(self, *x):

        img_batch = x[0]
        batch_size = img_batch.size(0)

        detections_batch = self.net(img_batch)
        detections_batch = self.non_max_suppression(detections_batch, len(self.names), 0.1, 0.05)

        if self.output_type == 'detections':
            return detections_batch
        elif self.output_type == 'occurrence':
            out = torch.zeros(batch_size, len(self.names), 1, 1)  # trailing 1 dimensions to match feature extraction API

            if next(self.parameters()).is_cuda:
                out = out.cuda()

            for i_batch, detections in enumerate(detections_batch):
                if detections is not None:
                    for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
                        out[i_batch, int(cls_pred), 0, 0] = cls_conf

            # print(out.shape, out.sum(3).sum(2).sum(1))

            return out
        else:
            pass


    def loss(self, y_pred, y_gt):
        raise NotImplementedError

    def name(self):
        return 'YOLOFeatures'

    def feature_size(self):
        return len(self.names)
        # return self._feature_size
