from torch import nn
from math import ceil

from ...models import DatasetNormalize
from ...core.logging import logger
from ...core.model import IMAGENET_MEAN, IMAGENET_STD
from .base import _FeatureExtractor


class _ResNetFeatures(_FeatureExtractor):
    """ Wrapper for ResNet feature extraction. """

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
        'except_fc': r'^(conv1|bn1|layer1|layer2|layer3|layer4.0|layer4.1)\.(.*)$',
        'l4.1': r'^(conv1|bn1|layer1|layer2|layer3|layer4.0)\.(.*)$',
        'l4': r'^(conv1|bn1|layer1|layer2|layer3)\.(.*)$',
        'l3+4': r'^(conv1|bn1|layer1|layer2)\.(.*)$',
    }

    def __init__(self, base='resnet50', pretrained=True, avgpool=True, flatten=True, freeze_level=None,
                 intermediate=False, layer=None, input_channels=3, feature_output_channels=None):

        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)
        self.base = base

        from torchvision.models import resnet50, resnet101, resnet152, resnet18

        if base == 'resnet50':
            self.net = resnet50(pretrained=pretrained)
            self._feature_size = 2048
        elif base == 'resnet18':
            self.net = resnet18(pretrained=pretrained)
            self._feature_size = 512
        elif base == 'resnet101':
            self.net = resnet101(pretrained=pretrained)
            self._feature_size = 2048
        elif base == 'resnet152':
            self.net = resnet152(pretrained=pretrained)
            self._feature_size = 2048

        if input_channels != 3:
            self.net.conv1 = nn.Conv2d(input_channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
            # self.input_std = self.input_mean = self.input_range = None
            # self.normalize = DatasetNormalize(
        else:
            self.normalize = DatasetNormalize(1, IMAGENET_MEAN, IMAGENET_STD, dims=4, color_dim=1)

        if feature_output_channels is not None:

            print(feature_output_channels)
            raise NotImplementedError
            # this is probably how it should work:
            # self.net.layer4[1].conv2 = nn.Conv2d(512, feature_output_channels, kernel_size=(3, 3), bias=False)
            # self.net.layer4[1].bn2 = nn.BatchNorm2d(feature_output_channels, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)

        self.layer_sequence = [['conv1', 'bn1', 'relu', 'maxpool'], 'layer1', 'layer2', 'layer3', 'layer4']
        self.freeze(freeze_level)

        logger.important('FEATURE EXTRACTOR: init {} (pretrained: {}, freeze: {}, intermediate: {}, layer: {}, flatten: {})'.format(
            self.name(), self.pretrained, freeze_level, intermediate, layer, flatten))

    def name(self):
        return self.base

    def feature_size(self):
        return self._feature_size

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2**5)) * ceil(input_size[1] / (2**5))


