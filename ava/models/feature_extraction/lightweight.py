from math import ceil

from ..blocks.cnn import EncoderBlock
from ...core.logging import logger
from .base import _FeatureExtractor

import torch
from torch import nn
from torch.nn import functional as nnf


class _LightConvNet(_FeatureExtractor):
    """ A very small ConvNet """

    def __init__(self, pretrained=True, avgpool=True, flatten=True, freeze_level=None,
                 intermediate=False, layer=None, input_channels=3, feature_output_channels=None):
        print('intermediate:', intermediate)
        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)

        self.net = nn.Module()
        self.net.all_layers = nn.Sequential(
            nn.Conv2d(input_channels, 16, 3),
            nn.ReLU(),
            nn.BatchNorm2d(16),
            nn.MaxPool2d(2),
            nn.Conv2d(16, 32, 3),
            nn.ReLU(),
            nn.BatchNorm2d(32),
            nn.MaxPool2d(2),
            nn.Conv2d(32, 64, 3),
            nn.ReLU(),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(2),
        )

        self.layer_sequence = ['all_layers']

    def name(self):
        return 'LightConvNet'

    def feature_size(self):
        return 64 if self.feature_output_channels is None else self.feature_output_channels


class _MNISTConvNet(_FeatureExtractor):
    """ An even smaller ConvNet """

    def __init__(self, pretrained=True, avgpool=True, flatten=True, freeze_level=None,
                 intermediate=False, layer=None, input_channels=3, feature_output_channels=None, k=3):
        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)

        self.k = k

        self.net = nn.Module()
        self.net.all_layers = nn.Sequential(
            EncoderBlock(int(input_channels), 4*k, 2, batch_norm=True),
            EncoderBlock(4*k, 8 * k, 2, batch_norm=True),
            EncoderBlock(8 * k, 4 * k, 2, batch_norm=True),
        )

        self.layer_sequence = ['all_layers']

    def name(self):
        return 'MNISTNet'

    def feature_size(self):
        return 4 * self.k


class _LightUNet(_FeatureExtractor):
    """ A Fully convolutional feature extractor """

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
    }

    def __init__(self, pretrained=True, avgpool=True, flatten=True, freeze_level=None,
                 intermediate=False, layer=None, input_channels=3, feature_output_channels=None):
        print('intermediate:', intermediate)
        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)

        self.encoder1 = nn.Sequential(
            nn.Conv2d(input_channels, 8, 3),
            nn.ReLU(),
            nn.BatchNorm2d(8),
            nn.Conv2d(8, 16, 3),
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Conv2d(16, 32, 3),
        )

        self.encoder2 = nn.Sequential(
            nn.Conv2d(32, 64, 3),
            nn.ReLU(),
            nn.MaxPool2d(2),
            nn.Conv2d(64, 32, 3),
            nn.ReLU(),
        )

        self.decoder1 = nn.Sequential(nn.Conv2d(32, 64, 3), nn.ReLU())
        self.decoder2 = nn.Sequential(nn.Conv2d(64 + 32, 64, 3), nn.ReLU())
        self.final = nn.Sequential(nn.Conv2d(64 + input_channels, 32, 3), nn.ReLU())

        self.freeze(freeze_level)

        self.layer_sequence = ['features']

        logger.important('FEATURE EXTRACTOR: init {} (pretrained: {}, freeze: {}, intermediate: {}, layer: {}, flatten: {})'.format(
            self.name(), self.pretrained, freeze_level, intermediate, layer, flatten))

    def name(self):
        return 'LightUNet'

    def forward(self, x):

        intermediate = self.encoder1(x)
        encoded = self.encoder2(intermediate)

        decoded1 = self.decoder1(encoded)
        decoded1 = nnf.interpolate(decoded1, intermediate.shape[2:])
        decoded1 = torch.cat([decoded1, intermediate], 1)

        decoded2 = self.decoder2(decoded1)
        decoded2 = nnf.interpolate(decoded2, x.shape[2:])
        decoded2 = torch.cat([decoded2, x], 1)

        decoded3 = self.final(decoded2)

        return decoded3

    def feature_size(self):
        return 32 if self.feature_output_channels is None else self.feature_output_channels

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2**5)) * ceil(input_size[1] / (2**5))