from math import ceil
from .base import _FeatureExtractor
from ..blocks import DatasetNormalize


class _IncV3Features(_FeatureExtractor):

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
        'm5': r'^(Conv2d_|Mixed_5)(.*)$',
        'm6': r'^(Conv2d_|Mixed_5|Mixed_6)(.*)$',
        'm7': r'^(Conv2d_|Mixed_5|Mixed_6|AuxLogits|Mixed_7)(.*)$',
    }

    def __init__(self, pretrained=True, avgpool=True, flatten=True, intermediate=False, layer=None, freeze_level=None):
        super().__init__(pretrained, avgpool, flatten, intermediate, layer)

        from torchvision.models import inception_v3

        # transform input triggers the normalization
        self.net = inception_v3(pretrained=pretrained, transform_input=True)  # transform input triggers normalization

        self.freeze(freeze_level)

        self.layer_sequence = ['Conv2d_1a_3x3', 'Conv2d_2a_3x3', 'Conv2d_2b_3x3', 'Conv2d_3b_1x1',
                               'Conv2d_4a_3x3',
                               'Mixed_5b', 'Mixed_5c', 'Mixed_5d', 'Mixed_6a', 'Mixed_6b', 'Mixed_6c', 'Mixed_6d',
                               'Mixed_6e', 'Mixed_7a', 'Mixed_7b', 'Mixed_7c']

    def feature_size(self):
        return 2048

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2 ** 5)) * ceil(input_size[1] / (2 ** 5))


class _IncV4Features(_FeatureExtractor):

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
    }

    def __init__(self, pretrained=True, avgpool=True, flatten=True, intermediate=False, layer=None, freeze_level=None,
                 input_channels=3, feature_output_channels=None):
        super().__init__(pretrained, avgpool, flatten, intermediate, layer, input_channels, feature_output_channels)

        from pretrainedmodels import inceptionv4

        self.net = inceptionv4(pretrained='imagenet' if pretrained else None)
        self.normalize = DatasetNormalize(1, [.5, .5, .5], [.5, .5, .5])

        if input_channels != 3:
            raise NotImplementedError

        self.freeze(freeze_level)

    def freeze(self, level):
        if level is not None:
            for n, p in self.net.features.named_parameters():
                if int(n[:n.index('.')]) <= level:
                    p.requires_grad = False

    def feature_size(self):
        return 1536

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2 ** 5)) * ceil(input_size[1] / (2 ** 5))

    def forward(self, x):
        x = self.normalize(x)
        x = self.net.features(x)
        x = self.postprocess(x)
        # if self.avgpool:
        #     x = nnf.avg_pool2d(x, (x.size(2), x.size(3)))
        #
        # if self.flatten:
        #     x = x.view(x.size(0), -1)

        return x


class _XceptionFeatures(_FeatureExtractor):

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
    }

    def __init__(self, pretrained=True, avgpool=True, flatten=True, intermediate=False, layer=None, freeze_level=None):
        super().__init__(pretrained, avgpool, flatten, intermediate, layer)

        from pretrainedmodels.models.xception import xception
        self.xception = xception(pretrained='imagenet' if pretrained else None)
        self.normalize = DatasetNormalize(1, [.5, .5, .5], [.5, .5, .5])

        self.freeze(freeze_level)

    def feature_size(self):
        return 2048

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2 ** 5)) * ceil(input_size[1] / (2 ** 5))

    def forward(self, x):
        x = self.normalize(x)
        x = self.xception.features(x)
        x = self.postprocess(x)

        return x


class _BNInceptionFeatures(_FeatureExtractor):

    FREEZE_LAYER_SELECTORS = {
        'complete': r'^(.*)$',
        'none': r'^$',
        'm5': r'^(Conv2d_|Mixed_5)(.*)$',
        'm6': r'^(Conv2d_|Mixed_5|Mixed_6)(.*)$',
        'm7': r'^(Conv2d_|Mixed_5|Mixed_6|AuxLogits|Mixed_7)(.*)$',
    }

    def __init__(self, pretrained=True, avgpool=True, flatten=True, freeze_level=None):
        super().__init__(pretrained, avgpool, flatten)

        from pretrainedmodels.models.bninception import bninception
        self.bn_inception = bninception(pretrained='imagenet' if pretrained else None)
        self.normalize = DatasetNormalize(255, [128, 117, 104], [1, 1, 1])

        self.freeze(freeze_level)

    def feature_size(self):
        return 1024

    def output_size(self, input_size):
        if self.avgpool:
            return self.feature_size()
        else:
            return self.feature_size() * ceil(input_size[0] / (2 ** 5)) * ceil(input_size[1] / (2 ** 5))

    def forward(self, x):
        x = self.normalize(x)
        x = self.bn_inception.features(x)
        x = self.postprocess(x)

        return x
