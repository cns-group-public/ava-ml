import re
from torch.nn import functional as nnf
from torch import nn
from ...core.logging import logger
from ...models import DatasetNormalize
#from ...core.model import ModelBase


class _FeatureExtractor(nn.Module):
    """ Base class for feature extraction """

    def __init__(self, pretrained=True, avgpool=True, flatten=True, intermediate=False, layer=None, input_channels=3,
                 feature_output_channels=None):
        super().__init__()

        self.flatten = flatten
        self.avgpool = avgpool
        self.pretrained = pretrained
        self.intermediate = intermediate
        self.layer = layer
        self.input_channels = input_channels
        self.feature_output_channels = feature_output_channels
        self.normalize = nn.Sequential()  # identity

        self.layer_sequence = None

    def loss(self, y_pred, y_gt):
        pass

    # def normalize(self, x):
    #     raise NotImplementedError

    def feature_size(self):
        raise NotImplementedError

    def forward_layers(self, x, net=None, layer_sequence=None):

        net = net if net is not None else self.net
        layer_sequence = layer_sequence if layer_sequence is not None else self.layer_sequence
        assert layer_sequence is not None, '{}: To use forward_layer a layer sequence needs to be defined'.format(self.__class__.__name__)

        activations = []
        for layer_names in layer_sequence:
            if type(layer_names) == str:
                layer_names = [layer_names]

            for layer_name in layer_names:
                try:
                    x = getattr(net, layer_name)(x)
                except RuntimeError as e:
                    raise RuntimeError(f'Failed to execute layer {layer_name} with input shape {x.shape}: {str(e)}')

                if self.layer == layer_name:
                    return (x, activations) if self.intermediate else x

            if self.intermediate:
                activations += [x]

        return x, activations

    def features(self, x):
        x = self.normalize(x)
        x, _ = self.forward_layers(x)
        return x

    def forward(self, x):

        x = self.normalize(x)
        x, activations = self.forward_layers(x) if self.intermediate else self.forward_layers(x)
        x = self.postprocess(x)
        return (x, activations) if self.intermediate else x

    def freeze(self, level):
        logger.info('FEATURE EXTRACTOR: Set freeze level to {}'.format(level))
        if level is not None:
            for n, p in self.net.named_parameters():
                if re.match(self.FREEZE_LAYER_SELECTORS[level], n):
                    p.requires_grad = False

    def postprocess(self, x):

        if self.avgpool:
            x = nnf.avg_pool2d(x, (x.size(2), x.size(3)))

        if self.flatten:
            x = x.view(x.size(0), -1)

        return x


