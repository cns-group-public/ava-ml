The feature extraction API provides access several state-of-the-art feature extractors.
For example, these can be combined with an image classifier 
(see `ImageClassifier` for details) or initialized in custom code using

```python
from ava.models.feature_extraction import get_feature_extractor
fe = get_feature_extractor('resnet50')
```