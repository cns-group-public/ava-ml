from pretrainedmodels.models.pnasnet import PNASNet5Large, pretrained_settings, model_zoo
from torch import nn


class _PNASFeatures(PNASNet5Large):
    """ Wraps the PNAS network """

    def __init__(self, pretrained=True, avgpool=True, flatten=True, mid_level_features=False):
        super().__init__()

        self.mid_level_features = mid_level_features
        self.flatten = flatten
        self.avgpool = avgpool

        if pretrained:
            settings = pretrained_settings['pnasnet5large']['imagenet']
            self.load_state_dict(model_zoo.load_url(settings['url']))

            new_last_linear = nn.Linear(self.last_linear.in_features, 1000)
            new_last_linear.weight.data = self.last_linear.weight.data[1:]
            new_last_linear.bias.data = self.last_linear.bias.data[1:]
            self.last_linear = new_last_linear

            self.input_space = settings['input_space']
            self.input_size = settings['input_size']
            self.input_range = settings['input_range']

            self.mean = settings['mean']
            self.std = settings['std']

    def features(self, x):
        mid_feats = []
        x_conv_0 = self.conv_0(x)
        x_stem_0 = self.cell_stem_0(x_conv_0)
        x_stem_1 = self.cell_stem_1(x_conv_0, x_stem_0)
        x_cell_0 = self.cell_0(x_stem_0, x_stem_1)
        mid_feats.append(x_cell_0)
        x_cell_1 = self.cell_1(x_stem_1, x_cell_0)
        x_cell_2 = self.cell_2(x_cell_0, x_cell_1)
        x_cell_3 = self.cell_3(x_cell_1, x_cell_2)
        mid_feats.append(x_cell_3)
        x_cell_4 = self.cell_4(x_cell_2, x_cell_3)
        x_cell_5 = self.cell_5(x_cell_3, x_cell_4)
        x_cell_6 = self.cell_6(x_cell_4, x_cell_5)
        mid_feats.append(x_cell_6)
        x_cell_7 = self.cell_7(x_cell_5, x_cell_6)
        x_cell_8 = self.cell_8(x_cell_6, x_cell_7)
        x_cell_9 = self.cell_9(x_cell_7, x_cell_8)
        mid_feats.append(x_cell_9)
        x_cell_10 = self.cell_10(x_cell_8, x_cell_9)
        x_cell_11 = self.cell_11(x_cell_9, x_cell_10)

        return x_cell_11, mid_feats

    def logits(self, features):
        x = self.relu(features)
        x = self.avg_pool(x)
        x = x.view(x.size(0), -1)
        x = self.dropout(x)
        x = self.last_linear(x)
        return x

    def forward(self, input):
        feat, mid_feats = self.features(input)

        if self.avg_pool:
            feat = self.logits(feat)

        if self.mid_level_features:
            return feat, mid_feats
        else:
            return feat
