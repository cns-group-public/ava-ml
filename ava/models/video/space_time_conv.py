from torch.nn import functional as nnf
from torch import nn


class SpaceTime(nn.Module):
    """ Following the LTC model by Varol et al. (slightly modified) """

    def __init__(self, n_classes, multiclass=None, input_channels=3, dense=1024, size='small', ignore_input=False,
                 **kwargs):
        super().__init__()

        from ...core.logging import logger
        logger.warning('Unused arguments: {}'.format(','.join(kwargs.keys())))

        self.ignore_input = ignore_input

        fs = {'small': [input_channels, 32, 64, 128, 256, 256],
              'original': [input_channels, 64, 128, 256, 256, 256],
              'faster': [input_channels, 64, 64, 128, 128, 256]}[size]

        kernels = {'small': [None, 3, 3, 3, 3, 2],
                   'original': [None, 3, 3, 3, 3, 3],
                   'faster': [None, 3, 3, 3, 3, 3]}[size]

        kernels_depth = {'small': [None, 1, 1, 3, 3, 2],
                         'original': [None, 3, 3, 3, 3, 3],
                         'faster': [None, 1, 3, 3, 3, 1]}[size]

        pool = {'small': [None, 1, 1, 2, 2, 2],
                'original': [None, 1, 2, 2, 2, 2],
                'faster': [None, 1, 1, 2, 1, 1]}[size]

        pad = {'small': [None, 1, 1, 1, 1, 0],
               'original': [None, 1, 1, 1, 1, 1],
               'faster': [None, 1, 1, 1, 1, 1]}[size]

        pad_depth = {'small': [None, 1, 1, 1, 1, 0],
                     'original': [None, 1, 1, 1, 1, 1],
                     'faster': [None, 0, 0, 0, 0, 0]}[size]

        def build_block(i):
            return nn.Sequential(
                nn.Conv3d(fs[i - 1], fs[i],
                          kernel_size=(kernels_depth[i], kernels[i], kernels[i]), padding=(pad_depth[i], pad[i], pad[i])),
                nn.ReLU(),
                nn.BatchNorm3d(fs[i]),
                nn.MaxPool3d((pool[i], 2, 2), stride=(pool[i], 2, 2)))

        n_outputs = n_classes * (multiclass if multiclass is not None else 1)

        self.convs = nn.ModuleList([build_block(i) for i in range(1, len(fs))])
        self.linear = nn.Sequential(
            nn.Linear(3 * 256, dense),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(dense, dense),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(dense, n_outputs))

    def forward(self, *x_vars):
        x = x_vars[0]

        if self.ignore_input:
            x = x * 0

        for conv in self.convs:
            x = conv(x)

        x = nnf.adaptive_avg_pool3d(x, (3, 1, 1))
        x = x.view(x.size(0), -1)
        x = self.linear(x)

        return x,




