import torch
from torch import nn
from torch.nn import Parameter


class VideoModeBaseline(nn.Module):

    def __init__(self, n_classes, n_frames, image_size=None, multiclass=False, auxiliary=None, with_lookahead=False,
                 lookahead=None):
        super().__init__()

        self.multiclass= multiclass
        self.n_classes = n_classes

        self.seen_labels = []
        param = [-1] if type(self.n_classes) == int else [-1 for _ in self.n_classes]
        self.mode = Parameter(torch.tensor(param, dtype=torch.int64), requires_grad=False)

    def is_baseline(self):
        return True

    def from_dataset(self, values):
        if type(self.n_classes) == int:
            self.mode[0] = int(values[0][0])
        else:
            for i in range(len(self.n_classes)):
                self.mode[i] = int(values[i][0])

    def feed(self, x, y):
        self.seen_labels += torch.max(y[0], dim=1)[1].cpu().numpy().tolist()
        a = torch.zeros(1)
        a.requires_grad = True
        return a,

    def forward(self, *x_vars):
        img = x_vars[0]
        batch_size = img.size(0) if img is not None else x_vars[2].size(0)

        # mode = stats.mode(self.seen_labels)

        if type(self.n_classes) == int:
            out = torch.zeros(batch_size, self.n_classes)
            out[:, self.mode] = 1
        else:
            out = torch.zeros(batch_size, sum(self.n_classes))
            offset = 0
            for i, c in enumerate(self.n_classes):
                out[:, offset+self.mode[i]] = 1
                assert offset == self.class_splits[i]
                # print(self.mode, i, int(self.mode[i]), offset + int(self.mode[i]))
                offset += c

        return out.cuda(),

