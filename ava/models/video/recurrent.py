import torch
from torch import nn
from torch.nn import functional as nnf
from ..blocks.temporal_convolution import _TemporalPyramidPooling
from ..feature_extraction import setup_feature_extractor
from ...core.logging import logger


class Recurrent(nn.Module):

    def __init__(self, n_classes,
                 base='resnet50', multiclass=None, encoder_freeze=None,
                 hop_sizes=(1,), resize_input=False, hidden_size=512, dropout=None,
                 tpp=None, lstm_layers=1, no_pretrain=False, ignore_input=False, input_channels=3,
                 feature_extraction_size=None, rnn_type='lstm', output_hidden=False, vector_input=None, **kwargs):
        super().__init__()

        logger.warning('Unused arguments: {}'.format(','.join(kwargs.keys())))

        self.multiclass = multiclass
        self.resize_input = resize_input
        self.hop_sizes = hop_sizes
        self.dropout = dropout
        self.lstm_layers = lstm_layers
        self.tpp = tpp
        self.ignore_input = ignore_input
        self.vector_input = vector_input
        self.input_channels = input_channels
        self.output_hidden = output_hidden

        self.encoder_freeze = encoder_freeze

        if self.vector_input is None:
            self.base_model, self.features = setup_feature_extractor(base, encoder_freeze, args=dict(
                pretrained=not no_pretrain, input_channels=input_channels, feature_output_channels=feature_extraction_size))

            feature_size = self.base_model.feature_size()
            self.do_normalization = False  # normalization is already done in the feature extractor
            self.base_name = self.base_model.name()
            self.bn_in = nn.BatchNorm2d(feature_size)
        else:
            feature_size = vector_input

        if rnn_type == 'lstm':
            self.lstm = nn.ModuleList([nn.LSTM(feature_size, hidden_size, bidirectional=True, num_layers=self.lstm_layers)
                                       for _ in range(len(self.hop_sizes))])
        elif rnn_type == 'rnn':
            self.lstm = nn.ModuleList([nn.RNN(feature_size, hidden_size, bidirectional=True, num_layers=self.lstm_layers)
                                       for _ in range(len(self.hop_sizes))])
        elif rnn_type == 'gru':
            self.lstm = nn.ModuleList([nn.GRU(feature_size, hidden_size, bidirectional=True, num_layers=self.lstm_layers)
                                       for _ in range(len(self.hop_sizes))])

        if tpp == 'parallel':
            self.tpp_module = _TemporalPyramidPooling(feature_size, feature_size, 'all')
            self.tpp_conv = nn.Sequential(
                nn.Conv1d(self.tpp_module.feature_size(), 2 * feature_size, kernel_size=1),
                nn.ReLU(),
                nn.Conv1d(2 * feature_size, n_classes, kernel_size=1))
        elif tpp == 'pre':
            self.tpp_module = _TemporalPyramidPooling(feature_size, feature_size, 'all')
            self.tpp_conv = nn.Conv1d(self.tpp_module.feature_size(), feature_size, kernel_size=1)
        else:
            self.tpp_module = None

        n_outputs =n_classes * (1 if multiclass is None else multiclass)

        self.post = nn.Sequential(
            nn.Conv1d(2 * hidden_size, 4*hidden_size, kernel_size=1, bias=False),
            nn.BatchNorm1d(4*hidden_size),
            nn.ReLU(),
            nn.Conv1d(4 * hidden_size, n_outputs, kernel_size=1)
        )

        if dropout is not None:
            self.dropout = nn.Dropout(dropout)

        if self.output_hidden:
            self.lstm_cell = []
            for m in self.lstm:
                cell_fwd = nn.LSTMCell(m.input_size, m.hidden_size, bias=m.bias)
                cell_fwd.weight_hh, cell_fwd.bias_hh = m.weight_hh_l0, m.bias_hh_l0
                cell_fwd.weight_ih, cell_fwd.bias_ih = m.weight_ih_l0, m.bias_ih_l0

                cell_rev = nn.LSTMCell(m.input_size, m.hidden_size, bias=m.bias)
                cell_rev.weight_hh, cell_rev.bias_hh = m.weight_hh_l0_reverse, m.bias_hh_l0_reverse
                cell_rev.weight_ih, cell_rev.bias_ih = m.weight_ih_l0_reverse, m.bias_ih_l0_reverse
                self.lstm_cell += [(cell_fwd, cell_rev)]
        else:
            self.lstm_cell = None

    def on_epoch_end(self, epoch):
        if type(self.encoder_freeze) == dict and epoch + 1 in self.encoder_freeze:
            self.base_model.freeze(self.encoder_freeze[epoch + 1])

    def forward(self, *x_vars):
        x = x_vars[0]

        if self.ignore_input:
            x = x * 0

        batch_size = x.size(0)
        n_frames = x.size(2)

        if self.vector_input is None:
            # re-arrange to combine frames with batch
            x_re = torch.transpose(x, 1, 2)
            x_re = x_re.contiguous().view(x_re.size(0)*x_re.size(1), self.input_channels, x_re.size(3), x_re.size(4))

            if self.resize_input:
                x_re = nnf.interpolate(x_re, size=(160, 240))

            feat = self.features(x_re)
            feat = self.bn_in(feat)
            feat = nnf.adaptive_avg_pool2d(feat, (1, 1))

            if self.dropout is not None:
                feat = self.dropout(feat)

            feature_dim, feat_h, feat_w = feat.shape[1:]
            feat = feat.view(batch_size, n_frames, feature_dim)

            if self.tpp == 'parallel':
                tpp_out = self.tpp_module(feat.permute(0, 2, 1))
                tpp_out = self.tpp_conv(tpp_out)
                tpp_pred = nnf.adaptive_avg_pool1d(tpp_out, 1).squeeze(2)
                # tpp_out.permute(0, 2, 1)  # compatible with RNN output
            elif self.tpp == 'pre':
                tpp_out = self.tpp_module(feat.permute(0, 2, 1))
                feat = self.tpp_conv(tpp_out)
                feat = feat.transpose(1, 2)

            # adapt feat size for rnn

        else:
            feat = x

        feat = torch.transpose(feat, 0, 1)
        rnn_outs = []

        if self.output_hidden:
            for i_hop, hop_size in enumerate(self.hop_sizes):

                state = None
                for x_step in feat[::hop_size]:  # forward
                    state = self.lstm_cell[i_hop][0](x_step, state)
                for x_step in feat[::hop_size][::-1]:  # rev
                    state = self.lstm_cell[i_hop][1](x_step, state)

        else:

            for i_hop, hop_size in enumerate(self.hop_sizes):
                rnn_out, hidden = self.lstm[i_hop](feat[::hop_size])
                rnn_out = torch.transpose(rnn_out, 0, 1)
                rnn_out = torch.transpose(rnn_out, 1, 2)
                rnn_outs += [rnn_out]

        rnn_out = torch.cat(rnn_outs, dim=2)

        rnn_pred = self.post(rnn_out)
        rnn_pred = nnf.adaptive_avg_pool1d(rnn_pred, 1).squeeze(2)

        pred = rnn_pred if self.tpp != 'pyramid' else rnn_pred + tpp_pred

        if self.output_hidden:
            return pred, rnn_out
        else:
            return pred,


class SNRecurrent(Recurrent):

    def __init__(self, n_classes, **kwargs):
        print(kwargs)
        super().__init__(n_classes, base='squeezenet', **kwargs)


class RN18Recurrent(Recurrent):

    def __init__(self, n_classes, **kwargs):
        super().__init__(n_classes, base='resnet18', **kwargs)


class RN50Recurrent(Recurrent):

    def __init__(self, n_classes, **kwargs):
        super().__init__(n_classes, base='resnet50', **kwargs)


class RN101Recurrent(Recurrent):

    def __init__(self, n_classes, **kwargs):
        super().__init__(n_classes, base='resnet101', **kwargs)


class Inc3Recurrent(Recurrent):
    def __init__(self, n_classes, **kwargs):
        super().__init__(n_classes, base='inception3', **kwargs)


class Inc4Recurrent(Recurrent):
    def __init__(self, n_classes, **kwargs):
        super().__init__(n_classes, base='inception4', **kwargs)








