from setuptools import setup, find_packages

setup(
    name='avaml',
    version='0.1.0',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'ava = ava.ava_cli:main'
        ]
    },
    install_requires=[
        'numpy>=1.17',
        'scipy>=1.2.1',
        'matplotlib>=3.0.3',
        'visdom',
        'torch>=1.1',
        'torchvision>=0.3',
        'line_profiler',
        'pretrainedmodels>=0.7',
        'scikit-image>=0.14.2',
        'fire',
        'pandas'
    ]
)
